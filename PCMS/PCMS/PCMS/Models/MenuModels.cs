﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace PCMS.Models
{

    public class MenuContext : DbContext
    {
        public MenuContext()
            : base("DefaultConnection")
        {
        }

        public virtual DbSet<menu_Groups> Menu_Groups { get; set; }
        public virtual DbSet<menu_Elements> Menu_Elements { get; set; }
        public virtual DbSet<facilities> Facilities { get; set; }
        public virtual DbSet<aspNetUsers> AspNetUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<aspNetUsers>()
                .HasMany(up => up.Facilities)
                .WithMany(course => course.AspNetUsers)
                .Map(mc =>
                {
                    mc.ToTable("UserFacilities");
                    mc.MapLeftKey("UserId");
                    mc.MapRightKey("FacilityId");
                }
            );
          
            base.OnModelCreating(modelBuilder);
        }

    }

    [Table("menu_Groups")]
    public partial class menu_Groups
    {
        public menu_Groups()
        {
            // this.Menu_Elements = new HashSet<menu_Elements>();
        }
        //[Key]
        //[DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [DisplayName("Bezeichnung")]
        public string Label { get; set; }
        [DisplayName("Reihenfolge")]
        public long SortOrder { get; set; }
        [DisplayName("Übergruppe")]
        public Nullable<long> ParentGroup { get; set; }
        [DisplayName("Icon")]
        public string Icon { get; set; }
        public bool Visible { get; set; }

        //public virtual ICollection<menu_Elements> Menu_Elements { get; set; }
    }
    [Table("menu_Elements")]
    public class menu_Elements
    {
        //[Key]
        //[DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [DisplayName("GruppenName")]
        public long MenuGroupId { get; set; }
        public string Name { get; set; }
        [DisplayName("Bezeichnung")]
        public string Label { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        [DisplayName("Berechtigung")]
        public string RequiredRole { get; set; }
        [DisplayName("Reihenfolge")]
        public long SortOrder { get; set; }
        public string Icon { get; set; }
        public bool Visible { get; set; }
        [ForeignKey("MenuGroupId")]
        public virtual menu_Groups Menu_AGroups { get; set; }
    }
    public class Menu
    {
        public Menu()
        {
            elem = new List<menu_Elements>();
        }
        public menu_Groups gr;
        public List<menu_Elements> elem;
    }
    
    [Table("Facilities")]
    public partial class facilities
    {
        public facilities()
        {
            AspNetUsers = new List<aspNetUsers>();
        }

    	public long Id{ get; set; }
    	public string Description{ get; set; }
    	public bool Active{ get; set; }
    	public bool UseMixingLines{ get; set; }
    	public int MixingLines{ get; set; }
    	public bool Concrete{ get; set; }
    	public bool Mortar{ get; set; }

        public virtual ICollection<aspNetUsers> AspNetUsers { get; set; }

    }
    [Table("AspNetUsers")]
    public partial class aspNetUsers
    {
        public aspNetUsers()
        {
            Facilities = new List<facilities>();
        }

        public string Id { get; set; }
        public string Email { get; set; }  
        public string UserName { get; set; }
        public long? Selectedfacility { get; set; }

        public virtual ICollection<facilities> Facilities { get; set; }
    }
    //[Table("UserFacilities")]
    //public partial class UserFacilities
    //{
        

    //    public long UserId { get; set; }
    //    public long FacilityId { get; set; }

        

    //}




}
