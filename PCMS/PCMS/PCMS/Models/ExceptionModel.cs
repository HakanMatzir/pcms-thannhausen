﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PCMS.Models
{
    public class ExceptionContext : DbContext
    {
        public ExceptionContext()
            : base("DefaultConnection")
        {
        }

        public virtual DbSet<ExceptionModel> ExceptionModel { get; set; }
    }
    [Table("Exception")]
    public partial class ExceptionModel
    {
        public long Id { get; set; }
        public string Message { get; set; }
        public string InnerExeptionMessage { get; set; }
        public string InnerExecptionTargetSiteName { get; set; }
        public string Source { get; set; }
        public string UserName { get; set; }
        public string StackTrace { get; set; }
        public string InnerExeptionStackTrace { get; set; }

        
        
        //public type unnown: datetime Timestamp { get; set; }
    }

  
}