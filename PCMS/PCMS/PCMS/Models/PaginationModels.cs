﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PCMS.Models
{
    public class PaginationModel
    {
        public PaginationModel()
        {
            this.pagFulFilter = new List<PaginationFulltextFilter>();
            this.topFilter = new List<TopFilter>();
        }
        public List<PaginationFulltextFilter> pagFulFilter { get; set; }
        public int? pageSize { get; set; }
        public int? page { get; set; }
        public string orderDir { get; set; }
        public string orderCol { get; set; }
        public string masterDataDir { get; set; }
        public string masterDataCol { get; set; }
        public bool report { get; set; }
        public List<TopFilter> topFilter { get; set; }

    }

    public class PaginationFulltextFilter
    {
        public string colName { get; set; }
        public string colVal { get; set; }
    }
    public class TopFilter
    {
        public string colName { get; set; }
        public string from { get; set; }
        public string to { get; set; }
    }
}
