﻿using Microsoft.Owin;
using Owin;
using System.Data.Entity;
using System.Threading;

[assembly: OwinStartupAttribute(typeof(PCMS.Startup))]
namespace PCMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {


            //check colum
            Helper.CheckColumHelper.startCheck();
                
            ConfigureAuth(app);
            if(Helper.Definitions.getFactoryGroup() == 0)
            {
               
                Thread deleiveryNoteThread = new Thread(Helper.DeliveryNotePrint.DeliveryNotePrintLoop);
                deleiveryNoteThread.Start();
                
            }
            
        }
    }
}
