﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;


//Treiber: https://download.epson-biz.com/modules/dot/index.php?page=single_soft&cid=70&pcat=1&pid=5

namespace PCMS.Helper
{
    public class DeliveryNotePrint
    {
        //private Areas.Order.Models.OrderContext db = new Areas.Order.Models.OrderContext();
        private static List<Areas.Order.Models.md_order_LoadingOrder> aktualOrders = new List<Areas.Order.Models.md_order_LoadingOrder>();

        private static int m_currentPageIndex;
        private static IList<Stream> m_streams;

        private static int checkStat()
        {
            //State 3 = in bearbeitung, 4=beendet, 5= gelöscht  
            Areas.Order.Models.OrderContext db = new Areas.Order.Models.OrderContext();
            var lo = db.Md_order_LoadingOrder.Where(a => a.State == 3 ).ToList();
            if (lo.Any())
            {
                foreach (var s in lo)
                {
                    if (!aktualOrders.Any(a => a.Id == s.Id))
                    {
                        aktualOrders.Add(s);
                        if (s.DeliveryNoteTypeId == 1 || s.DeliveryNoteTypeId == 2 || s.DeliveryNoteTypeId == 8) //Drucken beim laden
                        {
                            if(s.Printed != true)
                                printLot(s.OrderNumber);
                        }
                    }
                }
            }
            db.Dispose();
            return 0;
        }
        private static int checkList()
        {
            if (aktualOrders.Any())
            {
                foreach (var u in aktualOrders)
                {
                    Areas.Order.Models.OrderContext db = new Areas.Order.Models.OrderContext();
                    var o = db.Md_order_LoadingOrder.FirstOrDefault(a => a.Id == u.Id && (a.State == 4 || a.State == 5));
                    if (o != null)
                    {
                        if (o.State == 4)
                        {
                            aktualOrders.Remove(u);
                            MaterialMovingHelper.doubleEntries();
                            if (u.DeliveryNoteTypeId != 1 && u.DeliveryNoteTypeId != 2 && u.DeliveryNoteTypeId != 8)
                            { //Drucken wenn fertig
                                if (o.Printed != true)
                                    printLot(u.OrderNumber);
                            }
                            break;
                        }
                        else if (o.State == 5)
                        {
                            aktualOrders.Remove(u);
                            MaterialMovingHelper.doubleEntries();
                            break;
                        }
                    }
                    db.Dispose();
                }
            }
            return 0;
        }
        public static int printLot(string OrderNumber)
        {
            Areas.Material.Models.MaterialContext db2 = new Areas.Material.Models.MaterialContext();

            //OrderNumber = "12044";
            var model = db2.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber && !(a.LocationId == 42 || a.LocationId == 40)).ToList().OrderBy(a => int.Parse(a.LotNo)).ThenBy(a => a.ArticleNo);
            if (model == null)
            {
                return 1;
            }
            Areas.Order.Models.OrderContext orderContext = new Areas.Order.Models.OrderContext();
            Areas.Recipe.Models.RecipeContext recipeContext = new Areas.Recipe.Models.RecipeContext();
            Areas.Admin.Models.AdminContext adminContext = new Areas.Admin.Models.AdminContext();
            var loadingOrder = orderContext.Md_order_LoadingOrder.FirstOrDefault(a => a.OrderNumber == OrderNumber);
            int lotsAmount = Convert.ToInt32(loadingOrder.LotAmount);
            int lotsRemaining = Convert.ToInt32(loadingOrder.RemainingLotAmount);
            var material = orderContext.Md_order_Material.FirstOrDefault(a => a.MaterialNumber == loadingOrder.MaterialNumber && a.IsDeleted == false);
            var sortDetail = orderContext.Md_order_Sort.FirstOrDefault(a => a.Id == material.SortId);
            Areas.MasterData.Models.MasterDataContext mdContext = new Areas.MasterData.Models.MasterDataContext();
            var vehicle = mdContext.Md_masterData_Vehicle.FirstOrDefault(a => a.PlateNumber == loadingOrder.VehiclePlateNo && a.IsDeleted == false);
            var driver = mdContext.Md_masterData_Driver.FirstOrDefault(a => a.DriverNumber == loadingOrder.DriverNumber && a.IsDeleted == false);
            
            var strenghtClass = mdContext.Md_material_CompressionStrenghtGroup.FirstOrDefault(a => a.Id == sortDetail.CompressionId);
            var consistencyClass = mdContext.Md_material_ConsistencyGroup.FirstOrDefault(a => a.Id == sortDetail.ConsistencyId);
            var chlorideClass = mdContext.Md_material_ChlorideGroup.FirstOrDefault(a => a.Id == sortDetail.ChloridId);
            var customer = mdContext.Md_masterData_Customer.FirstOrDefault(a => a.CustomerId == loadingOrder.CustomerNumber && a.IsDeleted == false);
            Areas.MasterData.Models.md_masterData_ConstructionSite constructionSite = null;
            if (loadingOrder.ConstructionSiteNumber != null && customer != null)
                constructionSite = mdContext.Md_masterData_ConstructionSite.FirstOrDefault(a => a.ConstructionSiteId == loadingOrder.ConstructionSiteNumber && a.CustomerId == customer.Id && a.IsDeleted == false);
            var contiOrder = orderContext.Md_order_ContingentOrder.FirstOrDefault(a => a.ContingentNumber == loadingOrder.ContingentOrderNumber);
            var recipe = recipeContext.Md_recipe_Recipe.Include("RecipeMaterials").FirstOrDefault(a => a.Id == loadingOrder.RecipeId);
            var trader = mdContext.Md_masterData_Trader.FirstOrDefault(a => a.Id == loadingOrder.TraderId);

            List<ReportParameter> repParams = new List<ReportParameter>();

            repParams.Add(new ReportParameter("DeliveryNoteNo", loadingOrder.OrderNumber ?? ""));

            repParams.Add(new ReportParameter("FasilityName", "TBG Mittelschwaben"));
            repParams.Add(new ReportParameter("Date", (loadingOrder.RegistrationDate != null ? ((DateTime)loadingOrder.RegistrationDate).ToString("dd.MM.yyyy") : "")));
            repParams.Add(new ReportParameter("LoadingTime", (loadingOrder.RegistrationDate != null ? ((DateTime)loadingOrder.RegistrationDate).ToString("HH:mm") : "")));
            repParams.Add(new ReportParameter("VehicleNo", loadingOrder.VehicleNumber ?? ""));
            if(driver!=null && driver.Md_masterData_Contact != null)
            {
                repParams.Add(new ReportParameter("DriverName", (driver.Md_masterData_Contact.LastName ?? "") + " " + (driver.Md_masterData_Contact.FirstName ?? "")));
            }


            //if (vehicle != null && vehicle.md_masterData_Driver != null)
            //{
            //    repParams.Add(new ReportParameter("DriverName", vehicle.md_masterData_Driver.Md_masterData_Contact.LastName ?? "" + " " + vehicle.md_masterData_Driver.Md_masterData_Contact.FirstName ?? ""));
            //}
            string tempOut = loadingOrder.CementAdditionValue1 != null
                ? loadingOrder.CementAdditionValue1.ToString() + "°C"
                : "-";
            try
            {
                if (loadingOrder.CementAdditionValue1 == null &&
                    (loadingOrder.Printed == null || loadingOrder.Printed.Value == false))
                {
                    tempOut =
                    (Math.Round(db2.Md_Temperature.FirstOrDefault().TemperatureOutside, 2)
                        .ToString(CultureInfo.CurrentCulture)) ?? "-";
                    if (tempOut != "-")
                    {
                        db2.Database.ExecuteSqlCommand("UPDATE dbo.Md_order_LoadingOrder SET CementAdditionValue1 = " +
                                                       tempOut.Replace(',', '.') + " WHERE OrderNumber = '" +
                                                       OrderNumber + "'");
                        tempOut += "°C";
                    }

                }
            }
            catch
            {
                // ignored
            }


            repParams.Add(new ReportParameter("VehiclePlateNo", loadingOrder.VehiclePlateNo ?? ""));
            repParams.Add(new ReportParameter("TempOut",tempOut ));
            repParams.Add(new ReportParameter("ProcessabilityTime", loadingOrder.Workability != null ? loadingOrder.Workability.ToString() : ""));

            repParams.Add(new ReportParameter("OrderedQuantity", loadingOrder.OrderedQuantity != null ? loadingOrder.OrderedQuantity.ToString() : ""));
            repParams.Add(new ReportParameter("MaterialNo", loadingOrder.MaterialNumber ?? ""));
            repParams.Add(new ReportParameter("StrengthClass", strenghtClass != null ? strenghtClass.Class : ""));
            if (consistencyClass != null)
                repParams.Add(new ReportParameter("ConsistencyClass", consistencyClass.Class ?? ""));
            if (chlorideClass != null)
                repParams.Add(new ReportParameter("ChlorideClass", chlorideClass.Class ?? ""));
            repParams.Add(new ReportParameter("GradingGroup", sortDetail.Aggregate != null ? sortDetail.Aggregate : ""));
            repParams.Add(new ReportParameter("WZEQ", sortDetail.WZValue != null ? sortDetail.WZValue.ToString() : ""));
            repParams.Add(new ReportParameter("User", loadingOrder.UserName));

            if (sortDetail.ProgressId != null)
            {
                var proc = mdContext.Md_material_Progress.FirstOrDefault(a => a.Id == sortDetail.ProgressId);
                repParams.Add(new ReportParameter("StrengthDevelopment", proc.Description));
            }

            if (sortDetail.ExposureKombiId != null)
            {
                var exposure = mdContext.Md_material_ExposureKombi.FirstOrDefault(a => a.Id == sortDetail.ExposureKombiId); ;
                if (exposure != null)
                    repParams.Add(new ReportParameter("ExpositionClasses", exposure.ShortText));
            }
            if (sortDetail.NormId != null)
            {
                var norm = mdContext.Md_masterData_Material_Standard.FirstOrDefault(a => a.Id == sortDetail.NormId);

                if (norm != null)
                {

                    repParams.Add(new ReportParameter("DIN", norm.ShortName));
                }
            }
            if (material.MaterialTypeId != null)
            {
                var din = mdContext.Md_material_MaterialType.FirstOrDefault(a => a.Id == material.MaterialTypeId);
                if (din != null)
                {
                    repParams.Add(new ReportParameter("DINText", din.Description));
                }
            }
            string customerS = loadingOrder.CustomerNumber ?? "";
            if (loadingOrder.CustomerName != null)
            {
                customerS += "\n" + loadingOrder.CustomerName.Replace(", ", "\n");
                repParams.Add(new ReportParameter("CustomerName", loadingOrder.CustomerName.Replace(", ", "\n")));
            }
            if (customer != null)
            {
                customerS += "\n" + customer.Md_masterData_Address.Street + "\n" + customer.Md_masterData_Address.ZipCode + " " + customer.Md_masterData_Address.City;
                repParams.Add(new ReportParameter("CustomerStreet", customer.Md_masterData_Address.Street));
                repParams.Add(new ReportParameter("CustomerPlace", customer.Md_masterData_Address.ZipCode + " " + customer.Md_masterData_Address.City));
            }
            repParams.Add(new ReportParameter("CustomerNo", customerS));
            if (constructionSite != null)
            {
                repParams.Add(new ReportParameter("ConstructionSiteName", loadingOrder.ConstructionSiteDescription + "\n" + constructionSite.Md_masterData_Address.Street + ", " + constructionSite.Md_masterData_Address.ZipCode + " " + constructionSite.Md_masterData_Address.City));
                repParams.Add(new ReportParameter("ConstructionSiteNo", loadingOrder.ConstructionSiteNumber));
            }

            decimal work = loadingOrder.Workability ?? 0;
            var season = orderContext.Md_Config.Where(a => a.Id == 1).Select(a => a.Season).FirstOrDefault();
            var verz = orderContext.Md_order_RetardationTime.FirstOrDefault(a => a.RetardTime == work && a.Season == season);

            var orderComponets = orderContext.Md_Components.Where(a => a.LoadingOrderId == loadingOrder.Id);
            var cemenet = orderComponets.Where(a => a.Md_material_Material.MaterialGroupId == 2);
            string additiveOutOfSort = "";
            decimal cemSum = 0;
            if (verz != null && cemenet != null)
            {

                foreach (var ce in cemenet)
                {
                    cemSum += ce.LotSetPoint ?? 0;
                }
                additiveOutOfSort += "\n" + Math.Round(cemSum * ((verz.Value ?? 1) / 100.0m), 2) + "kg/m³ " + verz.Value + "% " + verz.Material.Name;
            }
            if (orderComponets.Any(a => a.Type == 1 && a.Weight != a.LotSetPoint))
            {
                foreach (var addition in orderComponets.Where(a => a.Weight != a.LotSetPoint && a.Type == 1))
                {
                    additiveOutOfSort += "\n" + (addition.LotSetPoint - addition.Weight) + "kg/m³ " + addition.Md_material_Material.Name;
                }
            }

            //repParams.Add(new ReportParameter("AdditiveOutOfSort", "0,02kg/m3 0,01% Luftporenbildner \n0,53kg/m3 0,015% Verzögerer"));
            repParams.Add(new ReportParameter("AdditiveOutOfSort", additiveOutOfSort));
            repParams.Add(new ReportParameter("AdditivePrice", loadingOrder.SpecialDescription1 + " " + loadingOrder.SpecialValue1 + "\n" + loadingOrder.SpecialDescription2 + " " + loadingOrder.SpecialValue2 + "\n" + loadingOrder.SpecialDescription3 + " " + loadingOrder.SpecialValue3 + "\n" + loadingOrder.SpecialDescription4 + " " + loadingOrder.SpecialValue4 + "\n"));
            //repParams.Add(new ReportParameter("Comment", loadingOrder.Comment1));
            var materialComment = "";
            materialComment += String.IsNullOrEmpty(material.Comment1) ? "" : material.Comment1;
            materialComment += String.IsNullOrEmpty(material.Comment2) ? "" : "\n" + material.Comment2;
            materialComment += String.IsNullOrEmpty(material.Comment3) ? "" : "\n" + material.Comment3;
            materialComment += String.IsNullOrEmpty(material.Comment4) ? "" : "\n" + material.Comment4;
            materialComment += String.IsNullOrEmpty(material.Comment5) ? "" : "\n" + material.Comment5;
            repParams.Add(new ReportParameter("Comment", materialComment));

            if (!(loadingOrder.DeliveryNoteTypeId == 3 || loadingOrder.DeliveryNoteTypeId == 4 || loadingOrder.DeliveryNoteTypeId == 5))
            {
                var loadingOrdComment = "";
                loadingOrdComment += String.IsNullOrEmpty(loadingOrder.Comment1) ? "" : loadingOrder.DNComment;
                //loadingOrdComment += String.IsNullOrEmpty(loadingOrder.Comment2) ? "" : "\n" + loadingOrder.Comment2;
                //loadingOrdComment += String.IsNullOrEmpty(loadingOrder.Comment3) ? "" : "\n" + loadingOrder.Comment3;
                repParams.Add(new ReportParameter("ConstructionSitePlace", loadingOrdComment));
            }
            //Selbstabholer
            PCMSDataSet ds3 = new PCMSDataSet();
            ds3.List.Clear();
            bool showCalc = false;
            
            //WerbeTexte
            if (loadingOrder.DeliveryNoteTypeId == 1)
            {
                repParams.Add(new ReportParameter("Advertising", loadingOrder.Advertising));
            }

            if (loadingOrder.DeliveryNoteTypeId == 8)//Barzahlung
            {
                showCalc = true;
                if (material.Price != null)
                {
                    ds3.List.AddListRow(loadingOrder.MaterialNumber, loadingOrder.MaterialDescription, loadingOrder.OrderedQuantity + "m³",( material.Price ?? 0).ToString(), (loadingOrder.OrderedQuantity * material.Price).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                }
                else
                {
                    ds3.List.AddListRow("", "Es wurder kein Preis für die Sorte gefunden", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                }
                if(loadingOrder.SpecialDescription1 != null)
                {
                    var ad = orderContext.Md_order_PriceMarkUp.FirstOrDefault(a => a.Description == loadingOrder.SpecialDescription1 && a.EDVCode == loadingOrder.SpecialNumber1);
                    if(ad.PriceType == 0)//Pauschal
                    {
                        ds3.List.AddListRow(ad.EDVCode, ad.Description, "1", (ad.Price ?? 0).ToString(), (ad.Price).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                    else//€/m³
                    {
                        ds3.List.AddListRow(ad.EDVCode, ad.Description, loadingOrder.OrderedQuantity + "m³", (ad.Price ?? 0).ToString(), (Math.Round(((loadingOrder.OrderedQuantity * ad.Price) ?? 0), 2) ).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                    
                }
                if (loadingOrder.SpecialDescription2 != null)
                {
                    var ad = orderContext.Md_order_PriceMarkUp.FirstOrDefault(a => a.Description == loadingOrder.SpecialDescription2 && a.EDVCode == loadingOrder.SpecialNumber2);
                    if (ad.PriceType == 0)
                    {
                        ds3.List.AddListRow(ad.EDVCode, ad.Description, "1", (ad.Price ?? 0).ToString(), (ad.Price).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                    else
                    {
                        ds3.List.AddListRow(ad.EDVCode, ad.Description, loadingOrder.OrderedQuantity + "m³", (ad.Price ?? 0).ToString(), (Math.Round(((loadingOrder.OrderedQuantity * ad.Price) ?? 0), 2)).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                }
                if (loadingOrder.SpecialDescription3 != null)
                {
                    var ad = orderContext.Md_order_PriceMarkUp.FirstOrDefault(a => a.Description == loadingOrder.SpecialDescription3 && a.EDVCode == loadingOrder.SpecialNumber3);
                    if (ad.PriceType == 0)
                    {
                        ds3.List.AddListRow(ad.EDVCode, ad.Description, "1", (ad.Price ?? 0).ToString(), (ad.Price).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                    else
                    {
                        ds3.List.AddListRow(ad.EDVCode, ad.Description, loadingOrder.OrderedQuantity + "m³", (ad.Price ?? 0).ToString(), (Math.Round(((loadingOrder.OrderedQuantity * ad.Price) ?? 0), 2)).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                }
                if (loadingOrder.SpecialDescription4 != null)
                {
                    var ad = orderContext.Md_order_PriceMarkUp.FirstOrDefault(a => a.Description == loadingOrder.SpecialDescription4 && a.EDVCode == loadingOrder.SpecialNumber4);
                    if (ad.PriceType == 0)
                    {
                        ds3.List.AddListRow(ad.EDVCode, ad.Description, "1", (ad.Price ?? 0).ToString(), (ad.Price).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                    else
                    {
                        ds3.List.AddListRow(ad.EDVCode, ad.Description, loadingOrder.OrderedQuantity + "m³", (ad.Price ?? 0).ToString(), (Math.Round(((loadingOrder.OrderedQuantity * ad.Price) ?? 0), 2)).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                }
                if (loadingOrder.SpecialDescription5 != null)
                {
                    var ad = orderContext.Md_order_PriceMarkUp.FirstOrDefault(a => a.Description == loadingOrder.SpecialDescription5 && a.EDVCode == loadingOrder.SpecialNumber5);
                    if (ad.PriceType == 0)
                    {
                        ds3.List.AddListRow(ad.EDVCode, ad.Description, "1", (ad.Price ?? 0).ToString(), (ad.Price).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                    else
                    {
                        ds3.List.AddListRow(ad.EDVCode, ad.Description, loadingOrder.OrderedQuantity + "m³", (ad.Price ?? 0).ToString(), (Math.Round(((loadingOrder.OrderedQuantity * ad.Price) ?? 0), 2) ).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                }
                if (customer.Discount != null)
                {
                    ds3.List.AddListRow("", "Rabatt", loadingOrder.OrderedQuantity + "m³", (customer.Discount ?? 0).ToString(), (Math.Round(((loadingOrder.OrderedQuantity * customer.Discount) ?? 0), 2) * -1).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                }

            }
            repParams.Add(new ReportParameter("ShowCalc", showCalc.ToString()));

            if (trader != null)
            {
                repParams.Add(new ReportParameter("Trader", trader.Name + "\n" + trader.Md_masterData_Address.Street + "\n" + trader.Md_masterData_Address.ZipCode + " " + trader.Md_masterData_Address.City));
            }

            if (!String.IsNullOrEmpty(loadingOrder.ContingentOrderNumber) && contiOrder != null)
            {
                repParams.Add(new ReportParameter("OrderedQunatityConti", contiOrder.OrderedQuantity != null ? contiOrder.OrderedQuantity.ToString() : ""));
                //MS 2017.01.17 change von contiOrder.DeliveredQuantity -> loadingOrder.DeliveredQuantity
                //repParams.Add(new ReportParameter("DeliveredQuantity", contiOrder.DeliveredQuantity != null ? contiOrder.DeliveredQuantity.ToString() : ""));
                repParams.Add(new ReportParameter("DeliveredQuantity", loadingOrder.DeliveredQuantity != null ? loadingOrder.DeliveredQuantity.ToString() : ""));
                // Änderung gemacht von Hakan Matzir am 23.02.2016
                //repParams.Add(new ReportParameter("BackAmount",( contiOrder.OrderedQuantity  ?? 0 - contiOrder.DeliveredQuantity ?? 0).ToString()));
                //MS 2017.01.17 change von contiOrder.DeliveredQuantity -> loadingOrder.DeliveredQuantity
                if (contiOrder.OrderedQuantity > loadingOrder.DeliveredQuantity)
                {

                    repParams.Add(new ReportParameter("BackAmount", (contiOrder.OrderedQuantity - loadingOrder.DeliveredQuantity ?? 0).ToString()));
                }
            }
            else
            {
                // Änderung gemacht von Hakan Matzir am 23.02.2016
                //repParams.Add(new ReportParameter("OrderedQunatityConti", loadingOrder.OrderedQuantity != null ? loadingOrder.OrderedQuantity.ToString() : ""));
                //repParams.Add(new ReportParameter("DeliveredQuantity", loadingOrder.OrderedQuantity != null ? loadingOrder.OrderedQuantity.ToString() : ""));
                //repParams.Add(new ReportParameter("BackAmount", ""));
            }

            //Neu 23.02.2016Manu
            List<long> numberList = new List<long>();
            int reCount = 1;
            string cement_Additive = "";
            string additive = "";

            var orderComponetns = orderContext.Md_Components.Where(a => a.LoadingOrderId == loadingOrder.Id && a.Type == 1).OrderBy(a=>a.Sort);
            repParams.Add(new ReportParameter("Header_00", "Menge"));
            if (loadingOrder.DeliveryNoteTypeId == 3 || loadingOrder.DeliveryNoteTypeId == 4 || loadingOrder.DeliveryNoteTypeId == 5)
                repParams.Add(new ReportParameter("Hide_00", "false"));
            else
                repParams.Add(new ReportParameter("Hide_00", "true"));
            if (orderComponetns != null)
            {
                foreach(var orderCo in orderComponetns)
                {
                    var mater = mdContext.Md_masterData_Material.FirstOrDefault(a => a.Id == orderCo.MaterialId);
                    if(mater.MaterialGroupId == 2)
                    {
                        cement_Additive += (cement_Additive.Length > 0 ? "\n" : "") + mater.Name;
                    }
                    if (mater.MaterialGroupId == 3)
                    {
                        additive += (cement_Additive.Length > 0 ? "\n" : "") + mater.Name;
                    }

                    if (reCount < 10)
                        repParams.Add(new ReportParameter("Header_0" + reCount, mater.ShortName));
                    else
                        repParams.Add(new ReportParameter("Header_" + reCount, mater.ShortName));
                    if (loadingOrder.DeliveryNoteTypeId == 3 || loadingOrder.DeliveryNoteTypeId == 4 || loadingOrder.DeliveryNoteTypeId == 5)
                    {
                        if (reCount < 10)
                            repParams.Add(new ReportParameter("Hide_0" + reCount, "false"));
                        else
                            repParams.Add(new ReportParameter("Hide_" + reCount, "false"));
                    }

                    else
                    {
                        if (reCount < 10)
                            repParams.Add(new ReportParameter("Hide_0" + reCount, "true"));
                        else
                            repParams.Add(new ReportParameter("Hide_" + reCount, "true"));
                    }

                    reCount++;
                    numberList.Add(mater.Id ?? 0);



                }
                for (int ll = reCount; ll < 11; ll++)
                {
                    if (reCount < 10)
                        repParams.Add(new ReportParameter("Hide_0" + reCount, "false"));
                    else
                        repParams.Add(new ReportParameter("Hide_" + reCount, "false"));
                }

            }
            repParams.Add(new ReportParameter("Cement_Additive", cement_Additive));
            repParams.Add(new ReportParameter("Additive", additive));
            

            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            PCMSDataSet ds2 = new PCMSDataSet();
            ds2.List.Clear();
            Dictionary<string, decimal> lotDSum = new Dictionary<string, decimal>();
            Dictionary<string, decimal> lotDSumSet = new Dictionary<string, decimal>();
            Dictionary<string, decimal> lotDSumWater = new Dictionary<string, decimal>();
            int[] round = new int[20];
            Array.Clear(round, 0, round.Length);
           // Dictionary<string, decimal> lotDSumWaterSpuel= new Dictionary<string, decimal>();
 
            if (loadingOrder.DeliveryNoteTypeId == 3 || loadingOrder.DeliveryNoteTypeId == 4 || loadingOrder.DeliveryNoteTypeId == 5)
            {
                for (int lo = 1; lo <= lotsAmount; lo++)
                {
                    var Lot = db2.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber && !(a.LocationId == 42 || a.LocationId == 40) && a.LotNo == lo.ToString() && (a.ArticleId != 33 || a.ArticleNo != "1000")).ToList().OrderBy(a => a.ArticleNo).Select(a => a.QuantityIs).ToArray();
                    Dictionary<string, string> lotD = new Dictionary<string, string>();
                    for (int o = 0; o <= 19; o++)
                    {
                        if (o == 0)
                            lotD.Add("val0", Convert.ToString(loadingOrder.LotSizeInProcent / 100));
                        else if (Lot.Length >= o)
                        {
                            var id = numberList[o - 1];
                            //var us = db2.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber && a.ArticleId == id && a.LotNo == lo.ToString()).ToList().Select(a => a.QuantityIs).FirstOrDefault();
                            //var usP = db2.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber && a.ArticleId == id && a.LotNo == lo.ToString()).ToList().Select(a => a.QuantitySetpoint).FirstOrDefault();

                            var mat = db2.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber && a.ArticleId == id && a.LotNo == lo.ToString()).FirstOrDefault();

                            // Änderungen von Hakan Matzir am 08.04.2016

                            var type = mdContext.Md_masterData_Material.Where(a => a.Id == mat.ArticleId).Select(a => a.MaterialGroupId).FirstOrDefault();
                           
                            if (type == 3)
                            {
                                lotD.Add("val" + o, Convert.ToString(Math.Abs(Math.Round(mat.QuantityIs ?? 0, 2))) + "kg");
                                round[o] = 2;
                            }
                                
                            else
                                lotD.Add("val" + o, Convert.ToString(Math.Abs(Math.Round(mat.QuantityIs ?? 0, 0))) + "kg");
                            if (lotDSum.ContainsKey("val" + o))
                            {
                                if (type == 3)
                                    lotDSum["val" + o] += (mat.QuantityIs ?? 0);
                                else
                                    lotDSum["val" + o] += (Math.Round(mat.QuantityIs ?? 0, 0));
                                lotDSumSet["val" + o] += (mat.QuantitySetpoint ?? 0);
                                if (type == 1)
                                {
                                    lotDSumWater["val" + o] += ((Math.Round(mat.QuantityIs ?? 0, 0) * ((mat.Wet ?? 0 )/ 100)));
                                }
                                else if (type == 3)
                                {
                                    lotDSumWater["val" + o] += (mat.QuantityIs ?? 0);
                                }
                                else if (type == 4)
                                {
                                    lotDSumWater["val" + o] += (mat.QuantityIs ?? 0);
    
                                }
                                
                            }
                            else
                            {
                                lotDSum.Add("val" + o, (mat.QuantityIs ?? 0));
                                lotDSumSet.Add("val" + o, (mat.QuantitySetpoint ?? 0));
                                if (type == 1)
                                {
                                    lotDSumWater.Add("val" + o, (((Math.Round( mat.QuantityIs ?? 0,0)) * ((mat.Wet ?? 0)/ 100))));
                                }
                                else if (type == 3)
                                {
                                    lotDSumWater.Add("val" + o, ((mat.QuantityIs ?? 0)));
                                }
                                else if (type == 4)
                                {
                                    lotDSumWater.Add("val" + o, ((mat.QuantityIs ?? 0)));
                                    //lotDSumWaterSpuel.Add("val" + o, ((mat.QuantitySetpoint ?? 0) - (mat.QuantityIs ?? 0)));
                                }
                                else
                                {
                                    lotDSumWater.Add("val" + o, (( 0)));
                                }

                            }

                        }

                        else {
                            lotD.Add("val" + o, "");
                            if (lotDSum.ContainsKey("val" + o))
                            {
                                lotDSum["val" + o] = 0;
                                lotDSumSet["val" + o] = 0;
                                lotDSumWater["val" + o] = 0;
                                //lotDSumWaterSpuel["val" + o] = 0;
                            }
                            else
                            {
                                lotDSum.Add("val" + o, 0);
                                lotDSumSet.Add("val" + o, 0);
                                lotDSumWater.Add("val" + o, 0);
                                //lotDSumWaterSpuel.Add("val" + o, 0);
                            }

                        }

                    }
                    ds.List.AddListRow(lotD["val0"], lotD["val1"], lotD["val2"], lotD["val3"], lotD["val4"], lotD["val5"], lotD["val6"], lotD["val7"], lotD["val8"], lotD["val9"], lotD["val10"], lotD["val11"], lotD["val12"], lotD["val13"], lotD["val14"], lotD["val15"], lotD["val16"], lotD["val17"], lotD["val18"], lotD["val19"]);
                }
                for (int lo = 1 + lotsAmount; lo <= lotsRemaining + lotsAmount; lo++)
                {
                    var Lot = db2.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber && !(a.LocationId == 42 || a.LocationId == 40) && a.LotNo == lo.ToString() && (a.ArticleId != 33 || a.ArticleNo != "1000"  ) ).ToList().OrderBy(a => a.ArticleNo).Select(a => a.QuantityIs).ToArray();
                    Dictionary<string, string> lotD = new Dictionary<string, string>();
                    for (int o = 0; o <= 19; o++)
                    {
                        if (o == 0)
                            //lotD.Add("val0", Convert.ToString((Math.Round(((loadingOrder.LotSizeInProcent / 100) * (loadingOrder.RemainingLot / 100)) ?? 0, 2))));
                            lotD.Add("val0", Convert.ToString((Math.Round(( (loadingOrder.RemainingLot / 100)) ?? 0, 2))));
                        else if (Lot.Length >= o)
                        {
                            var id = numberList[o - 1];
                            //var us = db2.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber && a.ArticleId == id && a.LotNo == lo.ToString()).ToList().Select(a => a.QuantityIs).FirstOrDefault();
                            //var usP = db2.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber && a.ArticleId == id && a.LotNo == lo.ToString()).ToList().Select(a => a.QuantitySetpoint).FirstOrDefault();
                            var mat = db2.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber && a.ArticleId == id && a.LotNo == lo.ToString()).FirstOrDefault();
                            var type = mdContext.Md_masterData_Material.Where(a => a.Id == mat.ArticleId).Select(a => a.MaterialGroupId).FirstOrDefault();
                            if (type == 3)
                            {
                                lotD.Add("val" + o, Convert.ToString(Math.Abs(Math.Round(mat.QuantityIs ?? 0, 2))) + "kg");
                                round[o] = 2;
                            }
                            else
                                lotD.Add("val" + o, Convert.ToString(Math.Abs(Math.Round(mat.QuantityIs ?? 0, 0))) + "kg");
                            if (lotDSum.ContainsKey("val" + o))
                            {
                                if (type == 3)
                                    lotDSum["val" + o] += (mat.QuantityIs ?? 0);
                                else
                                    lotDSum["val" + o] += (Math.Round( mat.QuantityIs ?? 0,0));
                                lotDSumSet["val" + o] += (mat.QuantitySetpoint ?? 0);
                                if (type == 1)
                                {
                                    lotDSumWater["val" + o] += ((Math.Round(mat.QuantityIs ?? 0, 0) * ((mat.Wet ?? 0 )/ 100)));
                                }
                                else if (type == 3)
                                {
                                    lotDSumWater["val" + o] += (mat.QuantityIs ?? 0);
                                }
                                else if (type == 4)
                                {
                                    lotDSumWater["val" + o] += (mat.QuantityIs ?? 0);
                                   // lotDSumWaterSpuel["val" + o] += (mat.QuantitySetpoint ?? 0) - (mat.QuantityIs ?? 0);
                                }
                            }
                            else
                            {
                                lotDSum.Add("val" + o, (mat.QuantityIs ?? 0));
                                lotDSumSet.Add("val" + o, (mat.QuantitySetpoint ?? 0));
                                if (type == 1)
                                {
                                    lotDSumWater.Add("val" + o, (((Math.Round(mat.QuantityIs ?? 0, 0)) * ((mat.Wet ?? 0) / 100))));
                                }
                                else if (type == 3)
                                {
                                    lotDSumWater.Add("val" + o, ((mat.QuantityIs ?? 0)));
                                }
                                else if (type == 4)
                                {
                                    lotDSumWater.Add("val" + o, ((mat.QuantityIs ?? 0)));
                                    //lotDSumWaterSpuel.Add("val" + o, ((mat.QuantitySetpoint ?? 0) - (mat.QuantityIs ?? 0)));
                                }
                                else
                                {
                                    lotDSumWater.Add("val" + o, ((0)));
                                }

                            }
                        }

                        else
                        {
                            lotD.Add("val" + o, "");
                            if (lotDSum.ContainsKey("val" + o))
                            {
                                lotDSum["val" + o] = 0;
                                lotDSumSet["val" + o] = 0;
                                lotDSumWater["val" + o] = 0;
                               // lotDSumWaterSpuel["val" + o] = 0;
                            }
                            else
                            {
                                lotDSum.Add("val" + o, 0);
                                lotDSumSet.Add("val" + o, 0);
                                lotDSumWater.Add("val" + o, 0);
                                //lotDSumWaterSpuel.Add("val" + o, 0);
                            }
                        }

                    }
                    ds.List.AddListRow(lotD["val0"], lotD["val1"], lotD["val2"], lotD["val3"], lotD["val4"], lotD["val5"], lotD["val6"], lotD["val7"], lotD["val8"], lotD["val9"], lotD["val10"], lotD["val11"], lotD["val12"], lotD["val13"], lotD["val14"], lotD["val15"], lotD["val16"], lotD["val17"], lotD["val18"], lotD["val19"]);
                }
                var spülwasser =  (Math.Round( db2.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber && (a.ArticleId == 33 || a.ArticleNo == "1000")).Select(a => a.QuantityIs).Sum() ?? 0));

                if (loadingOrder.DeliveryNoteTypeId == 5 || loadingOrder.DeliveryNoteTypeId == 4)
                    ds2.List.AddListRow("IS", Convert.ToString(Math.Abs(Math.Round(lotDSum["val1"], round[1]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val2"], round[2]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val3"], round[3]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val4"], round[4]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val5"], round[5]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val6"], round[6]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val7"], round[7]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val8"], round[8]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val9"], round[9]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val10"], round[10]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val11"], round[11]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val12"], round[12]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val13"], round[13]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val14"], round[14]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val15"], round[15]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val16"], round[16]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val17"], round[17]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val18"], round[18]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSum["val19"], round[19]))));
                if (loadingOrder.DeliveryNoteTypeId == 5 || loadingOrder.DeliveryNoteTypeId == 4 || loadingOrder.DeliveryNoteTypeId == 3)
                    ds2.List.AddListRow("SN", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val1"], round[1]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val2"], round[2]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val3"], round[3]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val4"], round[4]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val5"], round[5]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val6"], round[6]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val7"], round[7]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val8"], round[8]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val9"], round[9]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val10"], round[10]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val11"], round[11]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val12"], round[12]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val13"], round[13]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val14"], round[14]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val15"], round[15]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val16"], round[16]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val17"], round[17]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val18"], round[18]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumSet["val19"], round[19]))));
                if (loadingOrder.DeliveryNoteTypeId == 5 )
                    ds2.List.AddListRow("Wasser", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val1"], round[1]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val2"], round[2]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val3"], round[3]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val4"], round[4]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val5"], round[5]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val6"], round[6]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val7"], round[7]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val8"], round[8]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val9"], round[9]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val10"], round[10]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val11"], round[11]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val12"], round[12]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val13"], round[13]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val14"], round[14]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val15"], round[15]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val16"], round[16]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val17"], round[17]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val18"], round[18]))) + "kg", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val19"], round[19]))));
                if (loadingOrder.DeliveryNoteTypeId == 5)
                    ds2.List.AddListRow("Ges.Was.", Convert.ToString(Math.Abs(Math.Round(lotDSumWater["val1"] + lotDSumWater["val2"] + lotDSumWater["val3"]+ lotDSumWater["val4"]+ lotDSumWater["val5"]+ lotDSumWater["val6"]+ lotDSumWater["val7"]+ lotDSumWater["val8"]+ lotDSumWater["val9"]+ lotDSumWater["val10"] + (spülwasser * -1), 0))) + "kg", "Spülwasser", Math.Abs(spülwasser).ToString() +"kg", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            }

            db2.Dispose();
            orderContext.Dispose();
            recipeContext.Dispose();
            mdContext.Dispose();


            ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());
            ReportDataSource DSReport2 = new ReportDataSource("DataSet2", ds2.List.ToList());
            ReportDataSource DSReport3 = new ReportDataSource("DataSet3", ds3.List.ToList());





            ReportViewer reportViewer = new ReportViewer();
            
            reportViewer.ProcessingMode = ProcessingMode.Local;
            //reportViewer.LocalReport.ReportPath = "C:/Users/RoAdmin/Source/Repos/pcms/PCMS/PCMS/PCMS/Reports/Material/DelivaeryNote.rdlc";
            reportViewer.LocalReport.ReportPath = "C:\\inetpub\\wwwroot\\PCMS\\Reports\\Material\\DelivaeryNote.rdlc";

            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.DataSources.Add(DSReport2);
            reportViewer.LocalReport.DataSources.Add(DSReport3);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;

            Export(reportViewer.LocalReport);
            Print();
            //------------------
            //string mimeType = "";
            //string encoding = "";
            //string filenameExtension = "";
            //string[] streamids = null;
            //Warning[] warnings = null;

            //byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            //return File(file, mimeType);

            //ViewBag.ReportViewer = reportViewer;
            //return View();
            
            using (var db = new Areas.Order.Models.OrderContext())
            {
                db.Md_order_LoadingOrder.Attach(loadingOrder);
                loadingOrder.Printed = true;
                db.Entry(loadingOrder).Property(x => x.Printed).IsModified = true;
                db.SaveChanges();
            }

            return 0;
        }
        private static Stream CreateStream(string name,
      string fileNameExtension, Encoding encoding,
      string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }
        // Export the given report as an EMF (Enhanced Metafile) file.
        private static void Export(LocalReport report)
        {
            string deviceInfo =
              @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                <PageWidth>8.2in</PageWidth>
                <PageHeight>11.6in</PageHeight>
                <MarginTop>0.0in</MarginTop>
                <MarginLeft>0.0in</MarginLeft>
                <MarginRight>0.0in</MarginRight>
                <MarginBottom>0.0in</MarginBottom>
            </DeviceInfo>";

            //< PageWidth > 8.5in</ PageWidth >
            //< PageHeight > 11in</ PageHeight >
            //< MarginTop > 0.25in</ MarginTop >
            //< MarginLeft > 0.25in</ MarginLeft >
            //< MarginRight > 0.25in</ MarginRight >
            //< MarginBottom > 0.25in</ MarginBottom >
            Warning[] warnings;
            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream,
               out warnings);
            foreach (Stream stream in m_streams)
                stream.Position = 0;
        }
        // Handler for PrintPageEvents
        private static void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(m_streams[m_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            Rectangle adjustedRect = new Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        private static void Print()
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");
            PrintDocument printDoc = new PrintDocument();
            if (!String.IsNullOrWhiteSpace(Helper.Definitions.getPriner()))
                printDoc.PrinterSettings.PrinterName = Helper.Definitions.getPriner();
            if (!printDoc.PrinterSettings.IsValid)
            {
                throw new Exception("Error: cannot find the default printer.");
            }
            else
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                m_currentPageIndex = 0;
                printDoc.Print();
            }
        }
        public static void DeliveryNotePrintLoop()
        {
            int count = 0;
            while (true)
            {
                try
                {
                    checkStat();
                    checkList();
                    
                    if (count > 20)
                    {
                        count = 0;
                        Helper.LimsHelper.limsLoop();                        
                    }
                    count++;
                    System.Threading.Thread.Sleep( 6 * 1000);
                }
                catch (Exception e)
                {
                    System.Threading.Thread.Sleep(3000);
                    Helper.ExceptionHelper.LogException(e, "DruckThread");
                }

            }
        }
        public static Areas.Order.Models.md_order_LoadingOrder getAktualOder()
        {
            return aktualOrders.FirstOrDefault();
        }

    }
}