﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PCMS.Helper
{
    
    public class MaterialMovingHelper
    {
        private static Areas.Material.Models.MaterialContext db = new Areas.Material.Models.MaterialContext();
        public static void doubleEntries()
        {
            var model = db.Md_material_MaterialMoving.Where(u=>u.OrderNo != "0").GroupBy(x => new { x.ArticleId, x.OrderNo, x.QuantityIs, x.LotNo, x.BatchLine }).Select(y=>y.FirstOrDefault());
            var dupes = db.Md_material_MaterialMoving.Except(model).Where(o=>o.UserId=="prozess").ToList();
            int count = dupes.Count();
            //var us = count - 2;
            List < long > dIdList = dupes.Select(a => a.Id).ToList();
            //count = dIdList.Count;
            string ass = string.Join("\n", dIdList);
            string ua = ass;
            foreach (var dupe in dupes)
            {
                db.Md_material_MaterialMoving.Remove(dupe);
                PCMS.Models.ExceptionModel eM = new PCMS.Models.ExceptionModel();
                eM.UserName = "MaterialMoving";
                eM.Message = "Id=" + dupe.Id + ",OrderId=" + dupe.OrderId + ",OrderNo=" + dupe.OrderNo + ",ArticleId=" + dupe.ArticleId + ",LotNo=" + dupe.LotNo + ",BatchLine=" + dupe.BatchLine + ",QuantityIs=" + dupe.QuantityIs + ",MovingType=" + (dupe.MovingType != null ? (dupe.MovingType).ToString() : "-"  );
                Helper.ExceptionHelper.LogExceptionModel(eM);
            }
            db.SaveChanges("MaterialMoving");



        }
    }
}