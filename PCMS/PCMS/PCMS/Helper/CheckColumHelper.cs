﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PCMS.Helper
{
    public class CheckColumHelper
    {
        public static void startCheck()
        {
            //2016_03_23
            if (!isValidField("md_sieve_RuleGradingCurve", "A16"))
            {
                string query = "ALTER TABLE md_sieve_RuleGradingCurve ADD A16 decimal(16,4) NULL, B16 decimal(16,4) NULL,C16 decimal(16,4) NULL,U16 decimal(16,4) NULL";
                var context = new Areas.LIMS.Models.LimsContext();
                SqlConnection a = new SqlConnection(context.Database.Connection.ConnectionString);
                SqlCommand cmd = a.CreateCommand();
                cmd.CommandText = query;
                a.Open();
                cmd.ExecuteScalar();
                a.Close();
                a.Dispose();
            }
            //2016_04_13 Manuel Schall start            
            checkAdd("md_material_ConcreteFamilyGroup", "ReferenceSort", "bigint", "NULL");
            checkAdd("md_material_AggregateDetails", "RuleGradingCurveId", "bigint", "NULL");
            //2016_04_13 Manuel Schall end
            //2016_05_11 Manuel Schall start
            checkAdd("md_Customer", "Discount", "decimal(16,4)", "NULL");
            //2016_05_11 Manuel Schall end
            //2016_05_13 Manuel Schall start
            checkAdd("md_order_LoadingOrder", "UserName", "nvarchar(50)", "NULL");
            //2016_05_13 Manuel Schall end
            //2016_05_19 Manuel Schall start
            checkAdd("md_recipe_Recipe", "GradingCurvSieveRangeId", "bigint", "NULL");
            //2016_05_19 Manuel Schall end
            //2016_05_20 Manuel Schall start
            checkAdd("md_order_LoadingOrder", "Printed", "bit", "NULL");
            //2016_05_20 Manuel Schall end
            //2016_12_05 Manuel Schall start
            checkAdd("md_material_ConcreteFamilyGroup", "proceed", "int", "NULL");
            //2016_12_05 Manuel Schall end
            //2017_02_07 Manuel Schall start
            checkAdd("md_workDiary_WorkDiary", "Comment", "nvarchar(150)", "NULL");
            checkAdd("md_workDiary_WorkDiary", "UserName", "nvarchar(50)", "NULL");
            checkAdd("md_workDiary_WorkDiary", "CreationDate", "datetime", "NULL");
            //2017_02_07 Manuel Schall end
            //2017_02_07 Manuel Schall start
            checkAdd("md_workDiary_ScalTest", "StartWeight", "decimal(18,3)", "NULL");
            checkAdd("md_workDiary_ScalTest", "TotalWeight", "decimal(18,3)", "NULL");
            //2017_02_07 Manuel Schall end
            //2017_03_23 Manuel Schall start
            checkAdd("md_order_LoadingOrder", "Advertising", "nvarchar(200)", "NULL");
            if (!isValidField("md_order_Advertising", "Id"))
            {
                string query =
                "CREATE TABLE [dbo].[md_order_Advertising]([Id] [bigint] IDENTITY(1, 1) NOT NULL,[UId] [nvarchar](50) NULL,[ShortText] [nvarchar](50) NULL,[Text] [nvarchar](300) NULL,[Default] [bit] NULL,[IsActiv] [bit] NULL,[IsDeleted] [bit] NULL) ON[PRIMARY]";
                var context = new Areas.LIMS.Models.LimsContext();
                SqlConnection a = new SqlConnection(context.Database.Connection.ConnectionString);
                SqlCommand cmd = a.CreateCommand();
                cmd.CommandText = query;
                a.Open();
                cmd.ExecuteScalar();
                a.Close();
                a.Dispose();
            }
            //2017_03_23 Manuel Schall end
            //2017_07_10 Manuel Schall start
            checkAdd("md_sort_DirectoryDetails", "SortType", "nvarchar(200)", "NULL");
            //2017_07_10 Manuel Schall end

        }
        public static void checkAdd(string tableName, string columnName, string type, string _null)
        {
            if (!isValidField(tableName, columnName))
            {
                addColumn(tableName, columnName, type, _null);
            }
        }
        public static void addColumn(string tableName, string columnName,string type,string _null)
        {
            string query = "ALTER TABLE " +tableName+ " ADD "+ columnName +" "+ type +" "+_null;
            var context = new Areas.LIMS.Models.LimsContext();
            SqlConnection a = new SqlConnection(context.Database.Connection.ConnectionString);
            SqlCommand cmd = a.CreateCommand();
            cmd.CommandText = query;
            a.Open();
            cmd.ExecuteScalar();
            a.Close();
            a.Dispose();
        }
        public static bool isValidField(string tableName, string columnName)
        {
            var tblQuery = "SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS"
                           + " WHERE TABLE_NAME = @tableName AND"
                           + " COLUMN_NAME = @columnName";
            var context = new Areas.LIMS.Models.LimsContext();
            SqlConnection a = new SqlConnection(context.Database.Connection.ConnectionString);
            a.Open();
            SqlCommand cmd = a.CreateCommand();
            cmd.CommandText = tblQuery;
            var tblNameParam = new SqlParameter(
                "@tableName",
                System.Data.SqlDbType.NVarChar,
                128);

            tblNameParam.Value = tableName;
            cmd.Parameters.Add(tblNameParam);
            var colNameParam = new SqlParameter(
                "@columnName",
                System.Data.SqlDbType.NVarChar,
                128);

            colNameParam.Value = columnName;
            cmd.Parameters.Add(colNameParam);
            object objvalid = cmd.ExecuteScalar(); // will return 1 or null
            a.Close();
            a.Dispose();
            return objvalid != null;
        }
    }
    
}