﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PCMS.Helper
{
    //------!! Finger weg !!
    public static class Definitions
    {
       public static string[] allSieves = { "0,063", "0,125", "0,25", "0,5", "1", "1,4", "2", "2,8", "4", "5,6", "8", "11,2", "16", "22,4", "31,5", "45", "63" };
        public static int pageSize = 100;

        public static int getFactoryGroup()
        {
            Areas.LIMS.Models.LimsContext db = new Areas.LIMS.Models.LimsContext();
            var companytype = db.Md_masterData_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            return companytype;

        }
        public static string getPriner()
        {
            Areas.LIMS.Models.LimsContext db = new Areas.LIMS.Models.LimsContext();
            return db.Md_masterData_Config.Where(a => a.Id == 1).Select(a => a.PrintName).FirstOrDefault();
        }
        public static string setPriner(string printerName)
        {
            Areas.LIMS.Models.LimsContext db = new Areas.LIMS.Models.LimsContext();
            var conf = db.Md_masterData_Config.Where(a => a.Id == 1).FirstOrDefault();
            db.Entry(conf).State = System.Data.Entity.EntityState.Modified;
            conf.PrintName = printerName;
            db.SaveChanges();

            return printerName;

        }
    }
    
}