﻿using PCMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using PagedList;
using System.Net;

namespace PCMS.Controllers
{
    public class MenuController : Controller
    {
        //
        // GET: /Menu/
        private MenuContext dbMenu = new MenuContext();
        public ActionResult MenuPartial()
        {
            List<Menu> menu = new List<Menu>();
            ViewBag.Facility = Helper.MenuHelper.getAllFacilities(User.Identity.Name);
            var menuGroup = dbMenu.Menu_Groups.Where(a=>a.Visible == true).OrderBy(m => m.SortOrder).ToList(); // .Where(m => m.ParentGroup == null).ToList();
            foreach (menu_Groups g in menuGroup)
            {
                Menu tmp = new Menu();
                tmp.gr = g;
                
                foreach (var elem in dbMenu.Menu_Elements.Where(m => m.MenuGroupId == g.Id && m.Visible == true).OrderBy(m => m.SortOrder).ToList())
                {
                    if (Helper.MenuHelper.isAuthenticated(User.Identity.GetUserId(), elem.RequiredRole.Split(',')))
                    {
                        tmp.elem.Add(elem);
                    }
                }
                if(tmp.elem.Count()>0 || (tmp.gr.ParentGroup == null ))
                    menu.Add(tmp);
            }
            List<int> toDelete = new List<int>();
           bool hasChild = false;
               foreach(var meG in menu)
               {
                hasChild = false;
                if (meG.gr.ParentGroup == null)
                   {
                       foreach (var meG2 in menu)
                       {
                       if(meG2.gr.ParentGroup != null)
                       {
                           if (meG2.gr.ParentGroup == meG.gr.Id)
                           {
                               hasChild = true;
                               break;
                           }
                       }
                           
                       }
                       if (!hasChild && meG.elem.Count() == 0)
                       {
                           meG.gr.Visible = false; 
                       }
                  }
               }
            ViewBag.menu = buildMenu(menu);
            return View(menu);
        }
        public ActionResult facilityChange(long fId)
        {
            aspNetUsers user = dbMenu.AspNetUsers.Find(User.Identity.GetUserId());
            user.Selectedfacility = fId;
            if (ModelState.IsValid)
            {

                dbMenu.Entry(user).State = EntityState.Modified;
                dbMenu.SaveChanges();
               // return RedirectToAction("Index");
            }

            return Json("done");
        }
        public ActionResult Index(PaginationModel pg)
        {
            var model = dbMenu.Menu_Elements.Include(m => m.Menu_AGroups).Where(m=>m.Visible == true).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "MenuGroupId":
                                //model = model.Where(m => m.MenuGroupId != null && m.MenuGroupId.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.MenuGroupId = pgFF.colVal;
                                break;
                            case "Label":
                                model = model.Where(m => m.Label != null && m.Label.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Label = pgFF.colVal;
                                break;
                            case "RequiredRole":
                                model = model.Where(m => m.RequiredRole != null && m.RequiredRole.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.RequiredRole = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "MenuGroupId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MenuGroupId).ToList();
                    else
                        model = model.OrderByDescending(m => m.MenuGroupId).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "Label":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Label).ToList();
                    else
                        model = model.OrderByDescending(m => m.Label).ToList();
                    break;
                case "Area":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Area).ToList();
                    else
                        model = model.OrderByDescending(m => m.Area).ToList();
                    break;
                case "Controller":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Controller).ToList();
                    else
                        model = model.OrderByDescending(m => m.Controller).ToList();
                    break;
                case "Action":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Action).ToList();
                    else
                        model = model.OrderByDescending(m => m.Action).ToList();
                    break;
                case "RequiredRole":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RequiredRole).ToList();
                    else
                        model = model.OrderByDescending(m => m.RequiredRole).ToList();
                    break;
                case "SortOrder":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SortOrder).ToList();
                    else
                        model = model.OrderByDescending(m => m.SortOrder).ToList();
                    break;
                case "Icon":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Icon).ToList();
                    else
                        model = model.OrderByDescending(m => m.Icon).ToList();
                    break;
                case "Visible":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Visible).ToList();
                    else
                        model = model.OrderByDescending(m => m.Visible).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.menu_Elements menu_Elements = dbMenu.Menu_Elements.Find(id);
            if (menu_Elements == null)
            {
                return HttpNotFound();
            }
            ViewBag.MenuGroupId = new SelectList(dbMenu.Menu_Groups, "Id", "Label", menu_Elements.MenuGroupId);
            Areas.Admin.Models.AdminContext db2 = new Areas.Admin.Models.AdminContext(); 
            MultiSelectList rol = new MultiSelectList(db2.AspNetRoles.ToList(), "Name", "Name", menu_Elements.RequiredRole.Split(','));
            ViewBag.RequiredRole = rol;
            return View(menu_Elements);
        }

        // POST: Admin/aspNetUsers/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id")] Models.menu_Elements menu_Elements, string[] RequiredRole)
        {
            if (ModelState.IsValid)
            {
                Models.menu_Elements menu_ElementsN = dbMenu.Menu_Elements.Find(menu_Elements.Id);
                menu_ElementsN.RequiredRole = string.Join(",", RequiredRole);
                var item = dbMenu.Entry<Models.menu_Elements>(menu_ElementsN);
                item.State = EntityState.Modified;

                dbMenu.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.Selectedfacility = new SelectList(db.Facilities, "Id", "Description", aspNetUsers.Selectedfacility);
            return View(menu_Elements);
        }

        public string buildMenu(List<Menu> Model)
        {
            string menuString = "";
            //List<Menu> Model = new List<Menu>();
            foreach (var item in Model)
            {
                bool subGroup = false;
                bool mainGroup = false;
                if (item.gr.ParentGroup.ToString() == "")
                {
                    if (!item.gr.Visible)
                        continue;
                    menuString = menuString + "<li>";
                    menuString = menuString + "<a href = \"#\"><i class=\"fa fa-" + item.gr.Icon + " fa-fw\"></i>" + item.gr.Label + "</a>";
                    mainGroup = true;
                }
                foreach (var itemSub in Model)
                {
                    if (item.gr.Id == itemSub.gr.ParentGroup)
                    {
                        subGroup = true;
                    }
                }
                if ((item.elem.Count >= 1 || subGroup == true) && mainGroup == true)
                {
                    menuString = menuString + "<ul class=\"nav nav-second-level collapse\">";
                    foreach(var itemSub in Model)
                    {
                        if (item.gr.Id == itemSub.gr.ParentGroup)
                        {
                            menuString = menuString + "<li>";
                            menuString = menuString + "<a href = \"#\" ><i class=\"fa fa-" + itemSub.gr.Icon + " fa-fw\"></i>" + itemSub.gr.Label + "</a>";
                            if(itemSub.elem.Count > 0)
                            {
                                menuString = menuString + "<ul class=\"nav nav-third-level collapse\">";
                                foreach(var subItem in itemSub.elem)
                                {
                                    
                                        menuString = menuString + "<li>";
                                        menuString = menuString + "<a href = \"" + Url.Action(subItem.Action, subItem.Controller, new { area = subItem.Area }, null) + "\"><i class=\"fa fa-" + subItem.Icon + " fa-fw\"></i>" + subItem.Label + "</a>";
                                        menuString = menuString + "</li>";
                                    
                                }
                                menuString = menuString + "</ul>";
                             }
                            menuString = menuString + "</li>";
                        }
                  }
                  foreach(var subItem in item.elem)
                    {
                        
                     if (item.gr.Id == subItem.MenuGroupId)
                        {
                            menuString = menuString + "<li>";
                            //@*@Html.ActionLink("<i class=\"fa fa-" + subItem.Icon + "\"></i>" + subItem.Label, subItem.Action, subItem.Controller, new { area = subItem.Area }, null) *@
                            menuString = menuString + "<a href = \"" + Url.Action(subItem.Action, subItem.Controller, new { area = subItem.Area }, null) + "\"><i class=\"fa fa-" + subItem.Icon + " fa-fw\"></i>" + subItem.Label + "</a>";
                            menuString = menuString + "</li>";
                          }
                      }
                        
                    menuString = menuString + "</ul>";
                    }
                if (mainGroup == true)
                {
                    menuString = menuString + "</li>";
                }
                
            }
                return menuString;
        }
        public ActionResult getActualOrder()
        {
            var actualOrder = Helper.DeliveryNotePrint.getAktualOder();
            if (actualOrder != null)
            {
                return Json(new { OrderNumber = actualOrder.OrderNumber });
                }
            return Json(new { OrderNumber = "" });

        }
    }
}
