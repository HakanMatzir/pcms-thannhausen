﻿using PCMS.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PCMS.Controllers
{
    [Authorize]
    [LogActionFilter]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //return Redirect("/LIMS/lims_concrete_Test/NextTests");
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}