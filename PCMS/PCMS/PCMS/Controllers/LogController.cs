﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PCMS.Controllers
{
    public class LogContextA : DbContext
    {
        public LogContextA()
            : base("DefaultConnection")
        {
        }

        public virtual DbSet<Models.Log> Log { get; set; }
        //public virtual DbSet<LogWrite> LogWrite { get; set; }

    }
    public class LogController : Controller
    {
        LogContextA db = new LogContextA();
        public ActionResult Index(Models.PaginationModel pg)
        {
            DateTime Time1 = DateTime.Now;
            var model = db.Log.OrderByDescending(a => a.Timestamp).ToList();

           
            if (pg != null)
            {
                foreach (Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        //pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Controller":
                                model = model.Where(m => m.Controller != null && m.Controller == pgFF.colVal).ToList();
                                ViewBag.Controller = pgFF.colVal;
                                break;
                            case "Action":
                                model = model.Where(m => m.Action != null && m.Action.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Action = pgFF.colVal;
                                break;
                            case "UserAgent":
                                model = model.Where(m => m.UserAgent != null && m.UserAgent.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.UserAgent = pgFF.colVal;
                                break;
                            case "UserName":
                                model = model.Where(m => m.UserName != null && m.UserName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.UserName = pgFF.colVal;
                                break;
                            case "IP":
                                model = model.Where(m => m.IP != null && m.IP.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.IP = pgFF.colVal;
                                break;
                            case "Timestamp":
                               // model = model.Where(m => m.Timestamp != null && m.Timestamp.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Timestamp = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Controller":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Controller).ToList();
                    else
                        model = model.OrderByDescending(m => m.Controller).ToList();
                    break;
                case "Action":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Action).ToList();
                    else
                        model = model.OrderByDescending(m => m.Action).ToList();
                    break;
                case "UserAgent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserAgent).ToList();
                    else
                        model = model.OrderByDescending(m => m.UserAgent).ToList();
                    break;
                case "UserName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserName).ToList();
                    else
                        model = model.OrderByDescending(m => m.UserName).ToList();
                    break;
                case "IP":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.IP).ToList();
                    else
                        model = model.OrderByDescending(m => m.IP).ToList();
                    break;
                case "Timestamp":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Timestamp).ToList();
                    else
                        model = model.OrderByDescending(m => m.Timestamp).ToList();
                    break;
            }
            
            ViewBag.UserNames = new SelectList(db.Log.Select(a => a.UserName).Distinct().ToList(), ViewBag.UserName);
            //ViewBag.Action = new SelectList(db.Log.Select(a => a.Action).Distinct().ToList(), ViewBag.Action);
            ViewBag.Controller = new SelectList(db.Log.Select(a => a.Controller).Distinct().ToList(), ViewBag.Controller);
            
            
            var ma = model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize);
            DateTime Time2 = DateTime.Now;
            TimeSpan ts = Time2 - Time1;
            ViewBag.Milliseconds = ts.TotalSeconds + "s";
            return View(ma);
        }
        public ActionResult Index2(Models.PaginationModel pg)
        {
            DateTime Time1 = DateTime.Now;
            IQueryable<PCMS.Models.Log> model = db.Log.OrderByDescending(a => a.Timestamp);


            if (pg != null)
            {
                foreach (Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        //pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Controller":
                                model = model.Where(m => m.Controller != null && m.Controller == pgFF.colVal);
                                ViewBag.Controller = pgFF.colVal;
                                break;
                            case "Action":
                                model = model.Where(m => m.Action != null && m.Action.ToLower().Contains(pgFF.colVal));
                                ViewBag.Action = pgFF.colVal;
                                break;
                            case "UserAgent":
                                model = model.Where(m => m.UserAgent != null && m.UserAgent.ToLower().Contains(pgFF.colVal));
                                ViewBag.UserAgent = pgFF.colVal;
                                break;
                            case "UserName":
                                model = model.Where(m => m.UserName != null && m.UserName.ToLower().Contains(pgFF.colVal));
                                ViewBag.UserName = pgFF.colVal;
                                break;
                            case "IP":
                                model = model.Where(m => m.IP != null && m.IP.ToLower().Contains(pgFF.colVal));
                                ViewBag.IP = pgFF.colVal;
                                break;
                            case "Timestamp":
                                // model = model.Where(m => m.Timestamp != null && m.Timestamp.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Timestamp = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id);
                    else
                        model = model.OrderByDescending(m => m.Id);
                    break;
                case "Controller":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Controller);
                    else
                        model = model.OrderByDescending(m => m.Controller);
                    break;
                case "Action":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Action);
                    else
                        model = model.OrderByDescending(m => m.Action);
                    break;
                case "UserAgent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserAgent);
                    else
                        model = model.OrderByDescending(m => m.UserAgent);
                    break;
                case "UserName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserName);
                    else
                        model = model.OrderByDescending(m => m.UserName);
                    break;
                case "IP":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.IP);
                    else
                        model = model.OrderByDescending(m => m.IP);
                    break;
                case "Timestamp":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Timestamp);
                    else
                        model = model.OrderByDescending(m => m.Timestamp);
                    break;
            }
            
            ViewBag.UserNames = new SelectList(db.Log.Select(a => a.UserName).Distinct().ToList(), ViewBag.UserName);
            //ViewBag.Action = new SelectList(db.Log.Select(a => a.Action).Distinct().ToList(), ViewBag.Action);
            ViewBag.Controller = new SelectList(db.Log.Select(a => a.Controller).Distinct().ToList(), ViewBag.Controller);

            
            var ma = model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize);
            DateTime Time2 = DateTime.Now;
            TimeSpan ts = Time2 - Time1;
            ViewBag.Milliseconds = ts.TotalSeconds + "s";
            return View("Index",ma );
        }
        public ActionResult getAuditLogsById(long id)
        {
            var log = db.Log.Find(id);
            DateTime tTo = log.Timestamp.Value.AddMilliseconds(200);
            DateTime tFrom = log.Timestamp.Value.AddMilliseconds(-200);
            Areas.LIMS.Models.LimsContext db2 = new Areas.LIMS.Models.LimsContext();
            var auditLogs = db2.Auditlog.Where(a => a.eventdateutc < tTo && a.eventdateutc > tFrom && a.userid == log.UserName);
            return Json(auditLogs);

        }
        public ActionResult getAuditLogsByDateTableDate(string tableName, string id, string ticks)
        {
            DateTime tTo = DateTime.Now;
            if (!String.IsNullOrEmpty(ticks))
             tTo = new DateTime(Convert.ToInt64(ticks));
            Areas.LIMS.Models.LimsContext db2 = new Areas.LIMS.Models.LimsContext();
            var auditLogs = db2.Auditlog.Where(a => a.tablename == tableName && a.recordid == id && a.eventdateutc <= tTo);
            return Json(auditLogs);

        }
    }
}