﻿function refresh() {
    filter();
    $.ajax({
        //url: "/home/roles",
        url: window.location.href,
        type: "POST",
        dataType: "text",
        contentType: 'application/json',

        success: function (data) {
            $('.table-refresh').replaceWith(data);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
            alert(textStatus);
            alert(jqXHR);
        }
    });
}

function openModalSite(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            openConst("Übersicht", data, ["", ""]);
        }
    });
}

function openConst(titel, content, arg) {
    var modal = "";
    modal += '<div id="const" class="modal fade bs-example-modal-lg">';
    modal += '<div class="modal-dialog modal-lg">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}

function openModal(url, titel) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            chooseModal(titel, data, ["", ""]);
        }
    });
}

function chooseModal(titel, content, arg) {
    var modal = "";
    modal += '<div id="Modal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}


// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################       Auswahl LieferscheinTyp (Start)           ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openDeliveryNoteType(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            chooseDeliveryNote("Lieferscheintyp auswählen", data, ["", ""]);
        }
    });
}

function chooseDeliveryNote(titel, content, arg) {
    var modal = "";
    modal += '<div id="DeliveryModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseDNType(e) {
    $("#DeliveryNoteType_Description").val($(e).closest("tr").find("td[columnname='Description']").text().trim());
    $("#DeliveryNoteTypeId").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#DeliveryModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################        Auswahl LieferscheinTyp (Ende)           ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################         Auswahl Fahrzeugtyp (Start)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openVehicleType(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            chooseVehicleType("Fahrzeugtyp auswählen", data, ["", ""]);
        }
    });
}

function chooseVehicleType(titel, content, arg) {
    var modal = "";
    modal += '<div id="ProgressModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseVehiType(e) {
    $("#vehicleType_Description").val($(e).closest("tr").find("td[columnname='Description']").text().trim());
    $("#vehicle_VehicleTypeId").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#ProgressModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################          Auswahl Fahrzeugtyp (Ende)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################


// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################          Auswahl Material (Start)               ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openMaterial(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({
        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChooseMaterial("Sorte auswählen", data, ["", ""]);
        }
    });
}

function ChooseMaterial(titel, content, arg) {

    var modal = "";
    modal += '<div id="MaterialModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseMat(e) {

    $("#MaterialNumber").val($(e).closest("tr").find("td[columnname='ArticleNumber']").text().trim());
    $("#MaterialDescription").val($(e).closest("tr").find("td[columnname='Name']").text().trim());

    var Materialid = $(e).closest("tr").find("td[columnname='Id']").text().trim();
    $.post("/Order/md_order_Order/CheckRecipe", { id: Materialid }, function (data) {
        if (data == true)
            $.post("/Order/md_order_Order/ChooseRecipe", { id: Materialid }, function (data) { ChooseMaterial("Rezept auswählen", data, ["", ""]); });
        else {
            var test = $.post("/Order/md_order_Order/SelectRecipe", { id: Materialid }, function (data) {
                $("#RecipeNumber").val(data.RecNumber).text();
                $("#RecipeDescription").val(data.RecDescription).text();
                //$("#RecipeId").val($(e).closest("tr").find("td[columnname='RecipeNumber']").text().trim());
                //$("#RecipeDescription").val($(e).closest("tr").find("td[columnname='RecipeName']").text().trim());
                SelectSubstances(data.RecNumber);
            });
        }
    });

    $.post("/Order/md_order_Order/SelectNorm", { id: Materialid }, function (data) {
        $("#NormDescription").val(data.Norm).text();
    });

    $.post("/Order/md_order_Order/SelectConsistency", { id: Materialid }, function (data) {
        $("#ConsistencyDescription").val(data.ConsClass).text();
        $("#ConsistencyValue").val(data.ConsValue).text();
    });

    $("#MaterialModal").modal("hide");
}

function ChooseRec(e) {
    $("#RecipeNumber").val($(e).closest("tr").find("td[columnname='RecipeNumber']").text().trim());
    $("#RecipeDescription").val($(e).closest("tr").find("td[columnname='RecipeName']").text().trim());
    $("#MaterialModal").modal("hide");
    SelectSubstances($(e).closest("tr").find("td[columnname='Id']").text().trim());
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################            Auswahl Material (Ende)              ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################         Auswahl Entwicklung (Start)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openProgress(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChooseProgress("Entwicklung auswählen", data, ["", ""]);
        }
    });
}

function ChooseProgress(titel, content, arg) {
    var modal = "";
    modal += '<div id="ProgressModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseProg(e) {
    $("#progress_Description").val($(e).closest("tr").find("td[columnname='Description']").text().trim());
    $("#Progress input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#ProgressModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################          Auswahl Entwicklung (Ende)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################         Auswahl Fest. Zement (Start)            ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openCompressionCement(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChooseCompressionCement("Festigkeitsklasse auswählen", data, ["", ""]);
        }
    });
}

function ChooseCompressionCement(titel, content, arg) {
    var modal = "";
    modal += '<div id="CompCemModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseCompCem(e) {
    $("#compCement_Description").val($(e).closest("tr").find("td[columnname='Description']").text().trim());
    $("#CompCem input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#CompCemModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################          Auswahl Fest. Zement (Ende)            ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################


// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################             Auswahl Farbe (Start)               ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openColor(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChooseColor("Farbe auswählen", data, ["", ""]);
        }
    });
}

function ChooseColor(titel, content, arg) {
    var modal = "";
    modal += '<div id="ColorModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseCol(e) {
    $("#binderDetails_Color").val($(e).closest("tr").find("td[columnname='Description']").text().trim());
    $("#admixturesDetails_Color").val($(e).closest("tr").find("td[columnname='Description']").text().trim());
    $("#additivesDetails_Color").val($(e).closest("tr").find("td[columnname='Description']").text().trim());
    //$("#MatType input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#ColorModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################              Auswahl Farbe (Ende)               ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################          Auswahl Materialtyp (Start)            ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openMaterialType(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChooseMatType("Art auswählen", data, ["", ""]);
        }
    });
}

function ChooseMatType(titel, content, arg) {
    var modal = "";
    modal += '<div id="MatTypeModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseMatTyp(e) {
    $("#materialType_Description").val($(e).closest("tr").find("td[columnname='Description']").text().trim());
    $("#MatType input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#MatTypeModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################           Auswahl Materialtyp (Ende)            ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################          Auswahl Geologisch (Start)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openGeoSource(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChooseGeoSource("geolog. Herkunft auswählen", data, ["", ""]);
        }
    });
}

function ChooseGeoSource(titel, content, arg) {
    var modal = "";
    modal += '<div id="GeoModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseGeo(e) {
    $("#geo_Description").val($(e).closest("tr").find("td[columnname='Description']").text().trim());
    $("#Geo input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#GeoModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################           Auswahl Geologisch (Ende)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################          Auswahl Petrograph (Start)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openPetroType(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChoosePetroType("petrograph. Typ auswählen", data, ["", ""]);
        }
    });
}

function ChoosePetroType(titel, content, arg) {
    var modal = "";
    modal += '<div id="PetroModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function choosePetro(e) {
    $("#petrograph_Description").val($(e).closest("tr").find("td[columnname='Description']").text().trim());
    $("#Petro input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#PetroModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################           Auswahl Petrograph (Ende)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################


// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################             Auswahl Kornart (Start)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openGravelType(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChooseGravelType("Kornart auswählen", data, ["", ""]);
        }
    });
}

function ChooseGravelType(titel, content, arg) {
    var modal = "";
    modal += '<div id="GravelTypeModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseGrType(e) {
    $("#gravelType_Description").val($(e).closest("tr").find("td[columnname='Description']").text().trim());
    $("#GGType input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#GravelTypeModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################              Auswahl Kornart (Ende)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################


// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################           Auswahl Gesteinsart (Start)           ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openRock(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChooseRock("Gesteinsart auswählen", data, ["", ""]);
        }
    });
}

function ChooseRock(titel, content, arg) {
    var modal = "";
    modal += '<div id="RockModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseRoc(e) {
    $("#rock_Description").val($(e).closest("tr").find("td[columnname='Description']").text().trim());
    $("#Roc input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#RockModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################           Auswahl Gesteinsart (Ende)            ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################


// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################               Auswahl Alkali (Start)            ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openAlkali(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChooseAlkali("Alkaliklasse auswählen", data, ["", ""]);
        }
    });
}

function ChooseAlkali(titel, content, arg) {
    var modal = "";
    modal += '<div id="AlkaliModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseAlk(e) {
    $("#alkali_Class").val($(e).closest("tr").find("td[columnname='Class']").text().trim());
    $("#Alkali input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#AlkaliModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################              Auswahl Alkali (Ende)              ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################               Auswahl Mandant (Start)           ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openClient(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChooseClient("Mandant auswählen", data, ["", ""]);
        }
    });
}

function ChooseClient(titel, content, arg) {
    var modal = "";
    modal += '<div id="ClientModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseCli(e) {
    $("#client_ClientName").val($(e).closest("tr").find("td[columnname='ClientName']").text().trim());
    $("#client_ClientId").val($(e).closest("tr").find("td[columnname='ClientId']").text().trim());
    $("#Client input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#ClientModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################              Auswahl Mandant (Ende)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################


// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################             Auswahl Exposition (Start)          ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openExposure(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChooseExposure("Expositionsklasse auswählen", data, ["", ""]);
        }
    });
}

function ChooseExposure(titel, content, arg) {
    var modal = "";
    modal += '<div id="ExpoModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseExpo(e) {
    $("#exposure_ShortDescription").val($(e).closest("tr").find("td[columnname='Name']").text().trim());
    $("#ExpoId input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#ExpoModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################            Auswahl Exposition (Ende)            ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################             Auswahl Konsistenz (Start)          ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openConsistency(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            ChooseConsistency("Konsistenz auswählen", data, ["", ""]);
        }
    });
}

function ChooseConsistency(titel, content, arg) {
    var modal = "";
    modal += '<div id="ConsistModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseCons(e) {
    $("#consistency_Description").val($(e).closest("tr").find("td[columnname='Name']").text().trim());
    $("#ConsisId input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#ConsistModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################            Auswahl Konsistenz (Ende)            ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################             Auswahl Eignung (Start)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openSuitability(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            chooseSuitability("Eignung auswählen", data, ["", ""]);
        }
    });
}

function chooseSuitability(titel, content, arg) {
    var modal = "";
    modal += '<div id="SuitModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseSuit(e) {
    $("#suitability_Description").val($(e).closest("tr").find("td[columnname='Name']").text().trim());
    $("#SuitId input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#SuitModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################            Auswahl Eignung (Ende)               ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################


// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################             Auswahl Chloridkla (Start)          ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openChlorid(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            chooseChlorid("Chloridklasse auswählen", data, ["", ""]);
        }
    });
}

function chooseChlorid(titel, content, arg) {
    var modal = "";
    modal += '<div id="ChloridModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseChl(e) {
    $("#chlorid_Class").val($(e).closest("tr").find("td[columnname='Class']").text().trim());
    $("#ChlorId input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#ChloridModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################            Auswahl Chloridkla (Ende)            ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################             Auswahl Festigkeit (Start)          ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openCompression(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            chooseCompression("Festigkeitsklasse auswählen", data, ["", ""]);
        }
    });
}

function chooseCompression(titel, content, arg) {
    var modal = "";
    modal += '<div id="CompressionModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseComp(e) {
    $("#compression_Class").val($(e).closest("tr").find("td[columnname='Class']").text().trim());
    $("#Comp input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#CompressionModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################            Auswahl Festigkeit (Ende)            ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################             Auswahl Betonart (Start)            ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openConcrete(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            chooseConcrete("Betonart auswählen", data, ["", ""]);
        }
    });
}

function chooseConcrete(titel, content, arg) {
    var modal = "";
    modal += '<div id="ConcreteModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseConc(e) {
    $("#concrete_Description").val($(e).closest("tr").find("td[columnname='Name']").text().trim());
    $("#Conc input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#ConcreteModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                                 ##################################################
// #####################################################             Auswahl Betonart (Ende)             ##################################################
// #####################################################                                                 ##################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                             ######################################################
// #####################################################             Auswahl Norm (Start)            ######################################################
// #####################################################                                             ######################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openStandard(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            chooseStandard("Norm auswählen", data, ["", ""]);
        }
    });
}

function chooseStandard(titel, content, arg) {
    var modal = "";
    modal += '<div id="StandardModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseSt(e) {
    $("#standard_Name").val($(e).closest("tr").find("td[columnname='Name']").text().trim());
    $("#SId input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#standard_ShortName").val($(e).closest("tr").find("td[columnname='ShortName']").text().trim());
    $("#StandardModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                             ######################################################
// #####################################################             Auswahl Norm (Ende)             ######################################################
// #####################################################                                             ######################################################
// ########################################################################################################################################################
// ########################################################################################################################################################



// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                             ######################################################
// ##################################################### Zuweisung Fahrzeuge mit FahrzeugTyp (Start) ######################################################
// #####################################################                                             ######################################################
// ########################################################################################################################################################
// ########################################################################################################################################################

function openType(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            chooseType("Fahrzeugtyp zuweisen", data, ["", ""]);
        }
    });
}

function chooseType(titel, content, arg) {
    var modal = "";
    modal += '<div id="typeModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}
function chooseTy(e) {
    $("#customer_Name").val($(e).closest("tr").find("td[columnname='Name']").text().trim());
    $("#cId input").val($(e).closest("tr").find("td[columnname='Id']").text().trim());
    $("#typeModal").modal("hide");
}

// ########################################################################################################################################################
// ########################################################################################################################################################
// #####################################################                                             ######################################################
// ##################################################### Zuweisung Fahrzeuge mit FahrzeugTyp (Ende)  ######################################################
// #####################################################                                             ######################################################
// ########################################################################################################################################################
// ########################################################################################################################################################


// ##############################################################################################################################################################
// ##############################################################################################################################################################
// #####################################################                                                   ######################################################
// ##################################################### Zuweisung Baustelle & Fahrzeuge mit Kunden (Ende) ######################################################
// #####################################################                                                   ######################################################
// ##############################################################################################################################################################
// ##############################################################################################################################################################

function openCustomer(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            chooseCustomer("Kunden zuweisen", data, ["", ""]);
        }
    });
}

function chooseCustomer(titel, content, arg) {
    var modal = "";
    modal += '<div id="customerModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}

function chooseConstruction(titel, content, arg) {
    var modal = "";
    modal += '<div id="ConstructionModal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}



// ##############################################################################################################################################################
// ##############################################################################################################################################################
// #####################################################                                                    #####################################################
// ##################################################### Zuweisung Baustelle & Fahrzeuge mit Kunden (Ende)  #####################################################
// #####################################################                                                    #####################################################
// ##############################################################################################################################################################
// ##############################################################################################################################################################


function openVehicle(url) {
    var _url = "./MenuElemenetCreate";
    if (url) {
        _url = url;
    }
    $.ajax({
        url: _url,
        type: "GET",
        dataType: "text",
        success: function (data) {
            createModalVehi("Erstellen", data, ["", ""]);
        }
    });
}

function createModalVehi(titel, content, arg) {
    var modal = "";
    modal += '<div id="Modal" class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');
    $("#modalBtnSubmit").off("click");
    $(dModal).find("#modalBtnSubmit").on("click", function () {
        dModal.modal("hide");
    });
    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })


}

function openModalBig(url, titel) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            chooseModalBig(titel, data, ["", ""]);
        }
    });
}

function chooseModalBig(titel, content, arg) {
    var modal = "";
    modal += '<div id="Modal" class="modal fade">';
    modal += '<div class="modal-dialog" style="width:1200px">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');



    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}

//function chooseVehi(e) {
//    $("#VehicleNumber").val($(e).closest("tr").find("td[columnname='VehicleNumber']").text().trim());
//    $("#VehiclePlateNo").val($(e).closest("tr").find("td[columnname='PlateNumber']").text().trim());
//    $("#VehicleType").val($(e).closest("tr").find("td[columnname='Type']").text().trim());
//    $("#DriverNumber").val($(e).closest("tr").find("td[columnname='DriverNumber']").text().trim());
//    $("#DriverName").val($(e).closest("tr").find("td[columnname='DriverName']").text().trim());

//    $("#Modal").modal("hide");
//}

function openCreate(url) {

    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            createModal("Erstellen", data, "copyToLimsId", ["", ""]);
        }
    });
}
function openEdit(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            createModal("Bearbeiten", data, "copyToLimsId", ["", ""]);

        }
    });
}

function openStorno(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            createModal("Stornieren", data, "copyToLimsId", ["", ""]);
        }
    });
}

function openDone(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            createModal("LS ohne Produktion", data, "copyToLimsId", ["", ""]);
        }
    });
}

function openDelete(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            createModal("Löschen", data, "copyToLimsId", ["", ""]);
        }
    });
}
function openDetails(url) {
    var _url = "./MenuElementCreate";
    if (url) {
        _url = url;
    }
    $.ajax({

        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            createModal("Details", data, "copyToLimsId", ["", ""]);

        }
    });
}
function createModal(titel, content, fnc, arg) {
    var fnc2 = fnc;
    var modal = "";
    modal += '<div class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');
    $("#modalBtnSubmit").off("click");
    $(dModal).find("#modalBtnSubmit").on("click", function () {
        dModal.modal("hide");
    });
    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })


}
function createModalWButtons(titel, content, fnc, arg) {
    var fnc2 = fnc;
    var modal = "";
    modal += '<div class="modal fade">';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '<div class="modal-footer">';
    modal += '<button type="button" class="btn btn-default" data-dismiss="modal">abbrechen</button>';
    modal += '<button type="button" id="modalBtnSubmit" onclick="' + fnc + '" class="btn btn-primary">bestätigen</button>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');
    $("#modalBtnSubmit").off("click");
    $(dModal).find("#modalBtnSubmit").on("click", function () {
        //$("#modalBtnSubmit").on("click", function () {
        dModal.modal("hide");
        if (arg[2] != "")
            //hideRow(arg[2]);
            var fn = window[fnc2];
        fn(arg[0], arg[1], arg[2], arg[3]);
    });
    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })


}
function filterKeyDown(ev) {
    alert("neue FunktionsName");
    if (this.event.keyCode != 13) {
        return;
    }
    var filters = [];
    $(".head-filter").each(function (i, e) {
        var sFilter = {};
        sFilter["colName"] = $(e).attr("columname");
        sFilter["colVal"] = $(e).val();
        filters.push(sFilter);
    });
    $.ajax({
        //url: "/home/roles",
        url: window.location.href,
        type: "POST",
        data: JSON.stringify(filters),
        dataType: "text",
        contentType: 'application/json',

        success: function (data) {
            $('.table-refresh tbody').replaceWith(data);
            //$('.table-refresh tbody').html(data);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
            alert(textStatus);
            alert(jqXHR);
        }
    });
}
function headFilterChange(ev) {
    filter();

}
function pageCountFilterChange(e) {
    var pageCount = $(e).val();
    $(e).closest(".tableContent").find(".pagesize").each(function (i, s) {
        $(s).val(pageCount);
    })
    if ($(e).parents(".modal-dialog").length != 0)
        filterModal();
    else
        filter();
}
function headFilterKeyDown(ev) {

    if (this.event.keyCode != 13) {
        return;
    }
    filter();

}
function topDateFilterChange() {
    var from = $(".date-from").val();
    var to = $(".date-to").val();
    if (from != "" && to == "") {
        $(".date-to").val(from);
    }
    if (from == "" && to != "") {
        $(".date-from").val(to);
    }
    filter();
    //var now = new Date();
    //var day = ("0" + now.getDate()).slice(-2);
    //var month = ("0" + (now.getMonth() + 1)).slice(-2);
    //var today = now.getFullYear() + "-" + (month) + "-" + (day);
    //$('.date-to').val(today);
}

function reportDetails(para, repUrl, number, from, to) {
    var OrderNo = $("th[columnname='OrderNo'] input").val();
    var date = "";
    if ($("#OrderNumber").length > 0)
        OrderNo = $("#OrderNumber").val();
    if($("#date").length > 0)
        date = $("#date").val();
    if (para = "pdf") {
        var object = "<embed src=\"" + repUrl + "?Id=" + $("#Id").attr("value") + "&OrderNumber=" + OrderNo + "&from=" + from + "&to=" + to + "&date=" +date+ "&print=true\" type=\"application/pdf\" width=\"830\" height=\"800\" ></object>";
        pdfModal("Report", object, [""]);
    }
    else {


        $.ajax({
            //url: "/home/roles",
            url: repUrl,
            type: "POST",
            data: { Id: $("#Id").attr("value"), OrderNumber: OrderNo, number: number, from: from, to: to },
            dataType: "text",
            //contentType: 'application/json',

            success: function (data) {
                //$(tableName).parent("table").find("tbody").replaceWith(data);
                //$("form").replaceWith(data);

                openConst("Bericht", data, ["", ""]);

                //$("#reportContainer").html(data);
                // $("#reportContainer").show();
                // $("#getReportBtn").remove();
                // $("form").remove();


                //$('.table-refresh tbody').replaceWith(data);
                //$('.table-refresh tbody').html(data);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
                alert(textStatus);
                alert(jqXHR);
            }
        })
    }
}

function pdfModal(titel, content, arg) {
    var modal = "";
    modal += '<div id="aggre" class="modal fade bs-example-modal-lg">';
    modal += '<div class="modal-dialog modal-lg">';
    modal += '<div class="modal-content">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    modal += '<h4 class="modal-title">' + titel + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">';
    modal += '<p>' + content + '</p>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    modal += '</div>';
    var dModal = $(modal);
    dModal.modal('show');

    $(dModal).on('hidden.bs.modal', function (e) {
        $(this).remove();
    })
}

function filter(para, repUrl, repTable) {
    var object = {};
    var tableName = "";
    var filters = [];
    var topFilterArr = [];

    $(".head-filter").each(function (i, e) {
        var sFilter = {};
        sFilter["colName"] = $(e).attr("columnname");
        if ($(e).find("input").val())
            sFilter["colVal"] = $(e).find("input").val();
        if ($(e).find("select").val())
            sFilter["colVal"] = $(e).find("select").val();
        tableName = $(e).attr("tablename");
        if (sFilter["colVal"])
            filters.push(sFilter);
    });
    $(".topFilterContainer").each(function (i, e) {
        var topFilter = {};
        var colName = $(e).attr("columnname");
        var from = $(e).find(".date-from").val();
        var to = $(e).find(".date-to").val();
        topFilter["from"] = from;
        if (typeof to == "undefined")
            to = from;
        topFilter["to"] = to;
        topFilter["colName"] = colName;
        topFilterArr.push(topFilter);
    })



    var page = $(".tableContent[tablename='" + tableName + "'] .pagination .active a").attr("pn");
    var size = $(".tableContent[tablename='" + tableName + "'] .pagesize").val();
    var _url = window.location.href;
    var methode = "POST";
    if (para === "report") {
        //_url = '/Admin/Facilities/Report';
        _url = repUrl;
        if (repTable != "") {
            tableName = repTable;
        }
        object["report"] = true;
        methode = "GET";
        

    }
    object["page"] = page;
    object["pageSize"] = size;
    object["pagFulFilter"] = filters;
    object["topFilter"] = topFilterArr;
    object["orderCol"] = $(".tableContent[tablename='" + tableName + "'] .sort").attr("colname");
    object["orderDir"] = $(".tableContent[tablename='" + tableName + "'] .sort").attr("order");

    //object["pg"] = object;
    var pg = {};
    pg["pg"] = object;
    if (para === "report") {
        //$.param(pg)

        var object = "<embed src=\"" + repUrl + "?pgS=" + encodeURIComponent(JSON.stringify(object)) + "\" type=\"application/pdf\" width=\"830\" height=\"800\" ></object>";
        pdfModal("Report", object, [""]);
        return;
    }

    $.ajax({
        //url: "/home/roles",
        url: _url,
        type: methode,
        data: JSON.stringify(pg),
        dataType: "text",
        contentType: 'application/json',

        success: function (data) {
            if (para === "report") {
                openConst("Bericht", data, ["", ""]);
            }
            else {


                //$(tableName).parent("table").find("tbody").replaceWith(data);
                $(".tableContent[tablename='" + tableName + "']").replaceWith(data);
                init();
                if (object["orderCol"]) {
                    $(".tableContent[tablename='" + tableName + "'] th[colname='" + object["orderCol"] + "']").addClass("sort");
                    if (object["orderDir"] === "desc") {
                        $(".tableContent[tablename='" + tableName + "'] th[colname='" + object["orderCol"] + "']").attr("order", "desc");
                        $(".tableContent[tablename='" + tableName + "'] th[colname='" + object["orderCol"] + "']").append(' <i class="fa fa-sort-desc"></i>');
                    }
                    else {
                        $(".tableContent[tablename='" + tableName + "'] th[colname='" + object["orderCol"] + "']").attr("order", "asc");
                        $(".tableContent[tablename='" + tableName + "'] th[colname='" + object["orderCol"] + "']").append(' <i class="fa fa-sort-asc"></i>');
                    }


                }

            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
            alert(textStatus);
            alert(jqXHR);
        }
    });
}
function hideColumn(el) {
    var colname = $(el).attr("colname");
    $(el).hide();
    $("th[columnname='" + colname + "']").hide();
    $("td[columnname='" + colname + "']").hide();
}

$(document).on("keydown", "input:not(.notOnEnter):not(#Password)", function (e) {
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if (key == 13) {
        e.preventDefault();
        var inputs = $(this).closest('form').find(':input:visible');
        inputs.eq(inputs.index(this) + 1).focus().select();

    }
})
function init() {

    //$('input').keydown(function (e) {
    //    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    //    if (key == 13) {
    //        e.preventDefault();
    //        var inputs = $(this).closest('form').find(':input:visible');
    //        inputs.eq(inputs.index(this) + 1).focus();
    //    }
    //});


    $(".getReportBtn").on("click", function (e) {
        var url = $(this).attr("url");
        var para = $(this).attr("eType");
        //e.preventDefault();
        reportDetails('report', url);
    })
    
    //$(".pagination").on("click", "a", function () {
    //    $(".pagination .active").removeClass("active");
    //    $(this).parent().addClass("active");
    //    if ($(this).parents(".modal-content").length) {
    //        filterModal();
    //    }
    //    else {
    //        filter();
    //    }        
    //    return false;
    //})
    $(document).off("click", ".pagination a");
    $(document).on("click", ".pagination a", function () {
        $(".pagination .active").removeClass("active");
        $(this).parent().addClass("active");
        if ($(this).parents(".modal-content").length) {
            filterModal();
        }
        else {
            filter();
        }
        return false;
    })

    $(document).on("click", ".column-value4", function () {
        $(this).closest("table").find(".selected-row").removeClass("selected-row");
        $(this).closest("table").find(".function-row").remove();
        $(this).closest("tr").addClass("selected-row");
        var tr = $(this).closest("tr");
        var index = $($(tr).children("td[style!='display:none']")).index(this);
        console.log($(tr).children("td").length + " index: " + index);
        var btnDelete = '<button type="button" onclick="openDelete(\'' + $(this).closest("table").attr("deleteUrl") + "/" + $(this).closest("tr").attr("id") + '\')" class="btnDelete btn btn-xs btn-danger">löschen</button>';
        var btnMark = '<button type="button" onclick="openDelete(\'' + $(this).closest("table").attr("markUrl") + "/" + $(this).closest("tr").attr("id") + '\')" class="btnMark btn btn-xs btn-warning">markieren</button>';
        var emptyTd = (index === 0) ? "" : '<td colspan="' + (index) + '"></td>';

        $('<tr class="function-row" >' + emptyTd + '<td colspan="' + ($(tr).children("td").length - index) + '">' + btnDelete + btnMark + '</td></tr>').insertAfter(tr);
    })

    $(document).on("click", ".column-value", function () {
        $(this).closest("table").find(".selected-row").removeClass("selected-row");
        $(this).closest("table").find(".function-row").remove();
        $(this).closest("tr").addClass("selected-row");
        var tr = $(this).closest("tr");
        var index = $($(tr).children("td[style!='display:none']")).index(this);
        console.log($(tr).children("td").length + " index: " + index);
        var btnEdit = '<button type="button" onclick="location.href=\'' + $(this).closest("table").attr("editUrl") + "/" + $(this).closest("tr").attr("id") + '\'" class="btnEdit btn btn-xs btn-warning">bearbeiten</button>';
        var btnDelete = '<button type="button" onclick="openDelete(\'' + $(this).closest("table").attr("deleteUrl") + "/" + $(this).closest("tr").attr("id") + '\')" class="btnDelete btn btn-xs btn-danger">löschen</button>';
        var emptyTd = (index === 0) ? "" : '<td colspan="' + (index) + '"></td>';

        $('<tr class="function-row" >' + emptyTd + '<td colspan="' + ($(tr).children("td").length - index) + '">' + btnEdit + btnDelete + '</td></tr>').insertAfter(tr);
    })
    $(document).on("click", ".column-value2", function () {
        $(this).closest("table").find(".selected-row").removeClass("selected-row");
        $(this).closest("table").find(".function-row").remove();
        $(this).closest("tr").addClass("selected-row");
        var tr = $(this).closest("tr");
        var index = $($(tr).children("td[style!='display:none']")).index(this);
        console.log($(tr).children("td").length + " index: " + index);
        var btnEdit = '<button type="button" onclick="location.href=\'' + $(this).closest("table").attr("editUrl") + "/" + $(this).closest("tr").attr("id") + '\'" class="btnEdit btn btn-xs btn-warning">bearbeiten</button>';
        var btnDelete = '<button type="button" onclick="openStorno(\'' + $(this).closest("table").attr("deleteUrl") + "/" + $(this).closest("tr").attr("id") + '\')" class="btnDelete btn btn-xs btn-danger">stornieren</button>';
        var btnPrint = '<button type="button" onclick="openPrint(\'' + $(this).closest("table").attr("deleteUrl") + "/" + $(this).closest("tr").attr("id") + '\')" class="btnPrint btn btn-xs btn-success">drucken</button>';
        var btnDone = '<button type="button" onclick="openDone(\'' + $(this).closest("table").attr("doneUrl") + "/" + $(this).closest("tr").attr("id") + '\')" class="btnDone btn btn-xs btn-info">LS ohne Produktion</button>';
        var emptyTd = (index === 0) ? "" : '<td colspan="' + (index) + '"></td>';

        $('<tr class="function-row" >' + emptyTd + '<td colspan="' + ($(tr).children("td").length - index) + '">' + btnEdit + btnDelete + btnPrint + btnDone + '</td></tr>').insertAfter(tr);
    })

    $(document).on("click", ".column-value3", function () {
        $(this).closest("table").find(".selected-row").removeClass("selected-row");
        $(this).closest("table").find(".function-row").remove();
        $(this).closest("tr").addClass("selected-row");
        var tr = $(this).closest("tr");
        var index = $($(tr).children("td[style!='display:none']")).index(this);
        console.log($(tr).children("td").length + " index: " + index);
        var btnEdit = '<button type="button" onclick="location.href=\'' + $(this).closest("table").attr("editUrl") + "/" + $(this).closest("tr").attr("id") + '\'" class="btnEdit btn btn-xs btn-warning">bearbeiten</button>';
        var btnDelete = '<button type="button" onclick="openDelete(\'' + $(this).closest("table").attr("deleteUrl") + "/" + $(this).closest("tr").attr("id") + '\')" class="btnDelete btn btn-xs btn-danger">stornieren</button>';
        var emptyTd = (index === 0) ? "" : '<td colspan="' + (index) + '"></td>';

        $('<tr class="function-row" >' + emptyTd + '<td colspan="' + ($(tr).children("td").length - index) + '">' + btnEdit + btnDelete + '</td></tr>').insertAfter(tr);
    })

    //$(document).on("click", ".column-value3", function () {
    //    $(this).closest("table").find(".selected-row").removeClass("selected-row");
    //    $(this).closest("table").find(".function-row").remove();
    //    $(this).closest("tr").addClass("selected-row");
    //    var tr = $(this).closest("tr");
    //    var index = $($(tr).children("td[style!='display:none']")).index(this);
    //    console.log($(tr).children("td").length + " index: " + index);
    //    var btnEdit = '<button type="button" onclick="location.href=\'' + $(this).closest("table").attr("editUrl") + "/" + $(this).closest("tr").attr("id") + '\'" class="btnEdit btn btn-xs btn-warning">bearbeiten</button>';
    //    var btnDelete = '<button type="button" onclick="openDelete(\'' + $(this).closest("table").attr("deleteUrl") + "/" + $(this).closest("tr").attr("id") + '\')" class="btnDelete btn btn-xs btn-danger">löschen</button>';
    //    var btnPrint = '<button type="button" onclick="openPrint(\'' + $(this).closest("table").attr("deleteUrl") + "/" + $(this).closest("tr").attr("id") + '\')" class="btnPrint btn btn-xs btn-success">drucken</button>';
    //    var emptyTd = (index === 0) ? "" : '<td colspan="' + (index) + '"></td>';

    //    $('<tr class="function-row" >' + emptyTd + '<td colspan="' + ($(tr).children("td").length - index) + '">' + btnEdit + btnDelete + btnPrint + '</td></tr>').insertAfter(tr);
    //})
}
function sort(el) {
    if ($(el).hasClass("sort")) {
        if ($(el).attr("order") === "desc") {
            $(el).find("i").removeClass("fa-sort-desc");
            $(el).find("i").addClass("fa-sort-asc");
            $(el).attr("order", "asc");
        }
        else if ($(el).attr("order") === "asc") {
            $(el).find("i").removeClass("fa-sort-asc");
            $(el).find("i").addClass("fa-sort-desc");
            $(el).attr("order", "desc");
        }
    }
    else {
        $(el).parents("table").find(".sort i").remove();
        $(el).parents("table").find(".sort").removeClass("sort");
        $(el).addClass("sort");
        //$(el).find("i").removeClass("fa-sort-asc");
        //$(el).find("i").addClass("fa-sort-desc");
        $(el).attr("order", "desc");
        $(el).append(' <i class="fa fa-sort-desc"></i>');
    }
    if ($(el).parents(".modal-dialog").length != 0)
        filterModal();
    else
        filter();

}
$(document).ready(function () {
    init();
})

function deleteContactFromCompany(el) {
    createModalWButtons("Löschen", "sicher löschen?", "deleteContactFromCompanyAjax(" + $(el).attr("id") + ",'./md_order_company/deleteContactFromCompany')", [$(el).attr("id"), "/ul"]);
}
function deleteContactFromCompanyAjax(id, url) {
    var _url = url + "/" + id;
    $.ajax({
        url: _url,
        type: "GET",
        dataType: "text",
        //contentType: 'application/json',
        success: function (data) {
            alert("gelöscht");

        }
    });
}

//--------------------------------------- Modal Filter start
function headFilterChangeModal(ev) {
    filterModal();
}
function headFilterKeyDownModal(ev) {
    if (this.event.keyCode != 13) {
        return;
    }
    filterModal();
}
function filterModal(element, para, repUrl) {
    var object = {};
    var tableName = "";
    var filters = [];
    $(".modal-dialog .head-filter").each(function (i, e) {
        var sFilter = {};
        sFilter["colName"] = $(e).attr("columnname");
        if ($(e).find("input").val())
            sFilter["colVal"] = $(e).find("input").val();
        if ($(e).find("select").val())
            sFilter["colVal"] = $(e).find("select").val();
        tableName = $(e).attr("tablename");
        if (sFilter["colVal"])
            filters.push(sFilter);
    });
    var page = $(".tableContent[tablename='" + tableName + "'] .pagination .active a").attr("pn");
    var size = $(".tableContent[tablename='" + tableName + "'] .pagesize").val();
    var _url = window.location.href;
    if (para === "report") {
        //_url = '/Admin/Facilities/Report';
        _url = repUrl;
        object["report"] = true;
    }
    if ($(".modal-dialog table").attr("url"))
        _url = $(".modal-dialog table").attr("url");
    object["page"] = page;
    object["pageSize"] = size;
    object["pagFulFilter"] = filters;
    object["orderCol"] = $(".tableContent[tablename='" + tableName + "'] .sort").attr("colname");
    object["orderDir"] = $(".tableContent[tablename='" + tableName + "'] .sort").attr("order");

    //object["pg"] = object;
    var pg = {};
    pg["pg"] = object;
    $.ajax({
        //url: "/home/roles",
        url: _url,
        type: "POST",
        data: JSON.stringify(pg),
        dataType: "text",
        contentType: 'application/json',

        success: function (data) {
            //$(tableName).parent("table").find("tbody").replaceWith(data);
            $(".tableContent[tablename='" + tableName + "']").replaceWith(data);
            init();
            if (object["orderCol"]) {
                $(".modal-dialog .tableContent[tablename='" + tableName + "'] th[colname='" + object["orderCol"] + "']").addClass("sort");
                if (object["orderDir"] === "desc") {
                    $(".modal-dialog .tableContent[tablename='" + tableName + "'] th[colname='" + object["orderCol"] + "']").attr("order", "desc");
                    $(".modal-dialog .tableContent[tablename='" + tableName + "'] th[colname='" + object["orderCol"] + "']").append(' <i class="fa fa-sort-desc"></i>');
                }
                else {
                    $(".modal-dialog .tableContent[tablename='" + tableName + "'] th[colname='" + object["orderCol"] + "']").attr("order", "asc");
                    $(".modal-dialog .tableContent[tablename='" + tableName + "'] th[colname='" + object["orderCol"] + "']").append(' <i class="fa fa-sort-asc"></i>');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
            alert(textStatus);
            alert(jqXHR);
        }
    });
}
//--------------------------------------- Modal Filter ende
function getDateTimeLocal(u) {
    var now = new Date($.now())
      , year
      , month
      , date
      , hours
      , minutes
      , seconds
      , formattedDateTime
    ;

    year = now.getFullYear();
    //month = now.getMonth().toString().length === 1 ? '0' + (now.getMonth() + 1).toString() : now.getMonth() + 1;
    month = (now.getMonth() + 1).toString().length === 1 ? '0' + (now.getMonth() + 1).toString() : now.getMonth() + 1;
    date = now.getDate().toString().length === 1 ? '0' + (now.getDate()).toString() : now.getDate();
    hours = now.getHours().toString().length === 1 ? '0' + now.getHours().toString() : now.getHours();
    minutes = now.getMinutes().toString().length === 1 ? '0' + now.getMinutes().toString() : now.getMinutes();
    seconds = now.getSeconds().toString().length === 1 ? '0' + now.getSeconds().toString() : now.getSeconds();

    if (u == "date") {
        formattedDateTime = year + '-' + month + '-' + date;
    }
    else {
        //formattedDateTime = year + '-' + month + '-' + date + 'T' + hours + ':' + minutes + ':' + seconds;
        formattedDateTime = year + '-' + month + '-' + date + 'T' + hours + ':' + minutes;
    }



    //$(this).val(formattedDateTime);

    return formattedDateTime;
}
function openPrint(url) {

    var url = url.split('/');
    $.post("/Order/md_order_order/print/" + url[url.length - 1]);
}