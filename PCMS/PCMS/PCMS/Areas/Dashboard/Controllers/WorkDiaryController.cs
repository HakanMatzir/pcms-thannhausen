﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PCMS.Areas.Dashboard.Controllers
{
    public class WorkDiaryController : Controller
    {
        private Areas.Order.Models.OrderContext db_Order = new Order.Models.OrderContext();
        private Areas.WorkDiary.Models.WorkDiaryContext db_WorkDiary = new WorkDiary.Models.WorkDiaryContext();
        // GET: Dashboard/WorkDiary
        public ActionResult Index(DateTime? date)
        {

            DateTime now = date != null ? date.Value : DateTime.Now;
            //now = now.AddMonths(-1);
            DateTime start = new DateTime(now.Year, now.Month, 1);
            int dayInMonth = DateTime.DaysInMonth(now.Year, now.Month);
            DateTime end = new DateTime(now.Year, now.Month, dayInMonth);
            var order = db_Order.Md_order_LoadingOrder.Where(a => start <= a.RegistrationDate && a.RegistrationDate <= end && a.State == 4).ToList();
            //List<WorkDiary.Models.md_workDiary_workDiary> workDiaryList = new List<WorkDiary.Models.md_workDiary_workDiary>();
            string labels = "";
            string tmpMax = "";
            string tmpMin = "";
            string prodced = "";

            for (int i = 0; i < dayInMonth; i++)
            {
                var tmp = new WorkDiary.Models.md_workDiary_workDiary_View();
                DateTime nextDay = start.AddDays(1);
                var tmpOrder = order.Where(a => a.RegistrationDate.Value.Date == start.Date).ToList();
                labels += "'" + start.ToShortDateString() + "',";
                //tmp.Timestamp = start.Date;
                tmpMax += "'" + (tmpOrder.Max(a => a.CementAdditionValue1) != null ? tmpOrder.Max(a => a.CementAdditionValue1).ToString().Replace(',', '.') : "null,") + "',";
                //tmp.TempratureMax = tmpOrder.Max(a => a.CementAdditionValue1) ?? -111;
                tmpMin += "'" + (tmpOrder.Min(a => a.CementAdditionValue1) != null ? tmpOrder.Min(a => a.CementAdditionValue1).ToString().Replace(',', '.') : "null,") + "',";
                //tmp.TempratureMin = tmpOrder.Min(a => a.CementAdditionValue1) ?? -111;
                //tmp.DeliveryCount = tmpOrder.Count;
                prodced += "'" + (tmpOrder.Sum(a => a.OrderedQuantity) != null ? tmpOrder.Sum(a => a.OrderedQuantity).ToString().Replace(',', '.') : "null,") + "',";
                //tmp.DeliveryAmount = tmpOrder.Sum(a => a.OrderedQuantity) ?? -111;
                //tmp.ScaleTestCount = db_WorkDiary.Md_workDiary_ScalTest.Where(a => start <= a.Timestamp && a.Timestamp < nextDay).Count();
                //workDiaryList.Add(tmp);
                start = start.AddDays(1);
            }
            ViewBag.Month = WorkDiary.Models.DateTimeStd.month[now.Month - 1];
            ViewBag.Date = now;
            ViewBag.labels = labels.Substring(0,labels.Length -1);
            ViewBag.tmpMax = tmpMax.Substring(0, tmpMax.Length - 1);
            ViewBag.tmpMin = tmpMin.Substring(0, tmpMin.Length - 1);
            ViewBag.poduced = prodced.Substring(0, prodced.Length - 1);
            return View();

        }

        // GET: Dashboard/WorkDiary/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Dashboard/WorkDiary/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dashboard/WorkDiary/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Dashboard/WorkDiary/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Dashboard/WorkDiary/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Dashboard/WorkDiary/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Dashboard/WorkDiary/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
