﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Material.Models;

namespace PCMS.Areas.Dashboard.Controllers
{
    public class ProducedController : Controller
    {
        private readonly PCMS.Areas.Material.Models.MaterialContext _dbMaterialContext = new MaterialContext();
        // GET: Dashboard/Produced
        public ActionResult Index()
        {

            var materialMovin =
                _dbMaterialContext.Md_material_MaterialMoving.Where(a => a.RegistrationDate >= new DateTime(2016, 10, 1) && a.RegistrationDate <= new DateTime(2016, 10, 31))
                    .AsQueryable();
            var articelIds = materialMovin.Where(a => a.LotNo != "0")
                .Select(a => a.ArticleId)
                .Distinct();

            string labelString = "";
            string amountString = "";
            string colorString = "";
            var random = new Random();

            foreach (var articelId in articelIds)
            {

                var mdMaterialMaterialMoving = materialMovin.FirstOrDefault(a => a.ArticleId == articelId);
                labelString += "'" + (mdMaterialMaterialMoving != null ? mdMaterialMaterialMoving.ArticleNo + ", " + mdMaterialMaterialMoving.ArticleDescription : "--") + "',";
                amountString += Math.Abs(
                (materialMovin.Where(a => a.ArticleId == articelId)
                     .Select(a => a.QuantitySetpoint)
                     .Sum() ?? 0)).ToString(CultureInfo.InvariantCulture) + ",";
                colorString += $"\"#{random.Next(0x1000000):X6}\"" + ","; // = "#A197B9"
            }
            ViewBag.labels = labelString.Substring(0, labelString.Length - 1);
            ViewBag.amount = amountString.Substring(0, amountString.Length - 1);
            ViewBag.color = colorString.Substring(0, colorString.Length - 1);
            return View();
        }

        public ActionResult GetChartData(int direction = 1, string duration = "month")
        {

            var dateNow = DateTime.Now;
            var dateBefore = DateTime.Now.Date;
            switch (duration)
            {
                case "day":
                    dateBefore = dateBefore.AddDays(-1);
                    break;
                case "week":
                    dateBefore = dateBefore.AddDays(-1);
                    break;
                case "month":
                    dateBefore = dateBefore.AddMonths(-1);
                    break;
                case "year":
                    dateBefore = dateBefore.AddYears(-1);
                    break;
            }
            var materialMovin =
                _dbMaterialContext.Md_material_MaterialMoving.Where(a => a.RegistrationDate >= dateBefore && a.RegistrationDate <= dateNow)
                    .AsQueryable();
            IQueryable<long?> articelIds = null;
            switch (direction)
            {
                case 1:
                    articelIds = materialMovin.Where(a => a.LotNo != "0")
                        .Select(a => a.ArticleId)
                        .Distinct();
                    break;
                case 2:
                    articelIds = materialMovin.Where(a => a.LotNo == "0")
                        .Select(a => a.ArticleId)
                        .Distinct();
                    break;
            }
            var random = new Random();
            List<ChartData> chartDatas = new List<ChartData>();
            foreach (var articelId in articelIds)
            {

                var mdMaterialMaterialMoving = materialMovin.FirstOrDefault(a => a.ArticleId == articelId);

                chartDatas.Add(new ChartData()
                {
                    Color = $"#{random.Next(0x1000000):X6}",
                    Label = (mdMaterialMaterialMoving != null ? mdMaterialMaterialMoving.ArticleNo + ", " + mdMaterialMaterialMoving.ArticleDescription : "--"),
                    Value = Math.Abs(
                (materialMovin.Where(a => a.ArticleId == articelId)
                     .Select(a => a.QuantitySetpoint)
                     .Sum() ?? 0))
                });
            }

            return Json(new { label = chartDatas.Select(a => a.Label).ToList(), value = chartDatas.Select(a => a.Value).ToList(), color = chartDatas.Select(a => a.Color).ToList() });
        }
        // GET: Dashboard/Produced/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Dashboard/Produced/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dashboard/Produced/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Dashboard/Produced/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Dashboard/Produced/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Dashboard/Produced/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Dashboard/Produced/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

    }

    public class ChartData
    {
        public string Label { get; set; }
        public decimal Value { get; set; }
        public string Color { get; set; }


    }
}
