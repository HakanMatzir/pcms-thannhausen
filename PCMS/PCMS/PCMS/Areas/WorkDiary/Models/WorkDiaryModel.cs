﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PCMS.Areas.WorkDiary.Models
{
    public class WorkDiaryContext : DbContext
    {
        public WorkDiaryContext() : base("DefaultConnection")
        {
                
        }

        public DbSet<md_workDiary_ScalTest> Md_workDiary_ScalTest { get; set; }

        public System.Data.Entity.DbSet<PCMS.Areas.WorkDiary.Models.md_workDiary_workDiary> md_workDiary_workDiary { get; set; }
    }
    [Table("md_workDiary_ScalTest")]
    public partial class md_workDiary_ScalTest
    {
        public long Id { get; set; }
        [DisplayName("Waagen-Nr")]
        public int WeightNumber { get; set; }
        [DisplayName("Zeitstempel")]
        public DateTime Timestamp { get; set; }
        [DisplayName("Gewicht")]
        public decimal Weight { get; set; }
        [DisplayName("Gewicht gemessen")]
        public decimal WeightMeasured { get; set; }
        [DisplayName("Startgewicht")]
        public decimal? StartWeight { get; set; }
        [DisplayName("Gesammtgewicht")]
        public decimal? TotalWeight { get; set; }
        public bool IsDeleted { get; set; }

    }
    public class WorkDiaryModel
    {
    }
    public partial class md_workDiary_workDiary_View
    {
        [DisplayName("Datum")]
        public DateTime Timestamp { get; set; }
        [DisplayName("Temperatur Max")]
        public decimal TemperatureMax { get; set; }
        [DisplayName("Temperatur Min")]
        public decimal TemperatureMin { get; set; }
        [DisplayName("Lieferungen")]
        public int DeliveryCount { get; set; }
        [DisplayName("Liefermenge")]
        public decimal DeliveryAmount { get; set; }
        [DisplayName("Test Wiegungen")]
        public int ScaleTestCount { get; set; }
        public decimal? TemperatureMaxManual { get; set; }
        public decimal? TemperatureMinManual { get; set; }
        [DisplayName("Temperatur Beton")]
        public decimal? TemperatureConcrete { get; set; }
        [DisplayName("Resyclingwasser Dichte")]
        public decimal? DensityResyclingWater { get; set; }
        [DisplayName("Bemerkung"), MaxLength(150)]
        public string Comment { get; set; }

    }

    //USE [PCMS-ThannhausenB]
    //GO
    //
    ///****** Object:  Table [dbo].[md_workDiary_WorkDiary]    Script Date: 17.01.2017 16:14:42 ******/
    //SET ANSI_NULLS ON
    //GO
    //
    //SET QUOTED_IDENTIFIER ON
    //GO
    //
    //CREATE TABLE [dbo].[md_workDiary_WorkDiary](
    //	[Id] [bigint] IDENTITY(1,1) NOT NULL,
    //	[TimeStamp] [datetime] NOT NULL,
    //	[TemperatureMax] [decimal](18, 2) NULL,
    //	[TemperatureMin] [decimal](18, 2) NULL,
    //	[TemperatureConcrete] [decimal](18, 2) NULL,
    //	[DensityResyclingWater] [decimal](18, 2) NULL,
    //	[IsDeleted] [bit] NOT NULL
    //) ON [PRIMARY]
    //
    //GO
    public partial class md_workDiary_workDiary
    {
        [Key]
        public long Id { get; set; }
        [DisplayName("Datum")]
        public DateTime Timestamp { get; set; }
        [DisplayName("Temperatur Max")]
        public decimal TemperatureMax { get; set; }
        [DisplayName("Temperatur Min")]
        public decimal TemperatureMin { get; set; }
        [DisplayName("Temperatur Beton")]
        public decimal TemperatureConcrete { get; set; }
        [DisplayName("Resyclingwasser Dichte")]
        public decimal DensityResyclingWater { get; set; }
        public bool IsDeleted { get; set; }
        [DisplayName("Bemerkung"),MaxLength(150)]
        public string Comment { get; set; }
        public string UserName { get; set; }
        public DateTime? CreationDate { get; set; }    
    }
    public class DateTimeStd
    {
        public static string[] month = { "Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember" };
        public static string[] day = { "Montag", "Dienstag", "Mittwoch", "Donerstag", "Freitag", "Samstag", "Sonntag" };
    }
}