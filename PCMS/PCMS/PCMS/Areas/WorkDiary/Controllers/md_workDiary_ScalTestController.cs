﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using PCMS.Areas.WorkDiary.Models;
using PagedList;
using PCMS.Models;

namespace PCMS.Areas.WorkDiary.Controllers
{
    public class md_workDiary_ScalTestController : Controller
    {
        private WorkDiaryContext db = new WorkDiaryContext();

        // GET: WorkDiary/md_workDiary_ScalTest
        public ActionResult Index(PCMS.Models.PaginationModel pg)
        {
            var model = MdWorkDiaryScalTestFilter(pg);
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        private IQueryable<md_workDiary_ScalTest> MdWorkDiaryScalTestFilter(PaginationModel pg)
        {
            var model =
                db.Md_workDiary_ScalTest.Where(a => a.IsDeleted != true).OrderByDescending(a => a.Timestamp).AsQueryable();
            if (pg != null)
            {
                foreach (PCMS.Models.TopFilter top in pg.topFilter)
                {
                    switch (top.colName)
                    {
                        case "Timestamp":
                            if (!String.IsNullOrEmpty(top.@from) && !String.IsNullOrEmpty(top.to))
                            {
                                DateTime from = new DateTime(Convert.ToInt32(top.@from.Split('-')[0]),
                                    Convert.ToInt32(top.@from.Split('-')[1]), Convert.ToInt32(top.@from.Split('-')[2]));
                                DateTime to = new DateTime(Convert.ToInt32(top.to.Split('-')[0]),
                                    Convert.ToInt32(top.to.Split('-')[1]), Convert.ToInt32(top.to.Split('-')[2])).AddDays(1);

                                model = model.Where(m => m.Timestamp >= @from && m.Timestamp <= to).AsQueryable();
                                ViewBag.TimestampFrom = top.@from;
                                ViewBag.TimestampTo = top.to;
                            }
                            break;
                    }
                }

                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    switch (pgFF.colName)
                    {
                        case "WeightNumber":
                            int a = Convert.ToInt32(pgFF.colVal);
                            model = model.Where(m => m.WeightNumber != null && m.WeightNumber == a).AsQueryable();
                            ViewBag.WeightNumber = pgFF.colVal;
                            break;
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Id).AsQueryable();
                    break;
                case "WeightNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WeightNumber).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.WeightNumber).AsQueryable();
                    break;
                case "Timestamp":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Timestamp).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Timestamp).AsQueryable();
                    break;
                case "Weight":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Weight).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Weight).AsQueryable();
                    break;
                case "WeightMeasured":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WeightMeasured).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.WeightMeasured).AsQueryable();
                    break;
                case "IsDeleted":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.IsDeleted).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.IsDeleted).AsQueryable();
                    break;
            }
            return model;
        }


        // GET: WorkDiary/md_workDiary_ScalTest/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_workDiary_ScalTest md_workDiary_ScalTest = db.Md_workDiary_ScalTest.Find(id);
            if (md_workDiary_ScalTest == null)
            {
                return HttpNotFound();
            }
            return View(md_workDiary_ScalTest);
        }

        // GET: WorkDiary/md_workDiary_ScalTest/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WorkDiary/md_workDiary_ScalTest/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,WeightNumber,Timestamp,Weight,WeightMeasured,IsDeleted")] md_workDiary_ScalTest md_workDiary_ScalTest)
        {
            if (ModelState.IsValid)
            {
                db.Md_workDiary_ScalTest.Add(md_workDiary_ScalTest);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(md_workDiary_ScalTest);
        }

        // GET: WorkDiary/md_workDiary_ScalTest/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_workDiary_ScalTest md_workDiary_ScalTest = db.Md_workDiary_ScalTest.Find(id);
            if (md_workDiary_ScalTest == null)
            {
                return HttpNotFound();
            }
            return View(md_workDiary_ScalTest);
        }

        // POST: WorkDiary/md_workDiary_ScalTest/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,WeightNumber,Timestamp,Weight,WeightMeasured,IsDeleted")] md_workDiary_ScalTest md_workDiary_ScalTest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(md_workDiary_ScalTest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(md_workDiary_ScalTest);
        }
        public ActionResult Report(string pgS)
        {

            PCMS.Models.PaginationModel pg = JsonConvert.DeserializeObject<PCMS.Models.PaginationModel>(pgS);
            var model = MdWorkDiaryScalTestFilter(pg);
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();

            foreach (var dataset in model)
            {
                ds.List2.AddList2Row(dataset.Timestamp.ToString("dd.MM.yyyy HH:mm:ss"), dataset.WeightNumber.ToString(), dataset.StartWeight.ToString(), dataset.TotalWeight.ToString(), dataset.Weight.ToString(), dataset.WeightMeasured.ToString(), (dataset.WeightMeasured - dataset.Weight).ToString() , Math.Round(((dataset.WeightMeasured - dataset.Weight)/dataset.Weight) * 100,2).ToString(), "", "", "", "", "","", "", "","", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource dsReport = new ReportDataSource("ds", ds.List2.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("DateTime", DateTime.Now.ToShortDateString()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/WorkDiary/ScaleTest.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(dsReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 1200;
            reportViewer.Height = 700;

            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);
            //ViewBag.ReportViewer = reportViewer;
            //return View();
        }
        // GET: WorkDiary/md_workDiary_ScalTest/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_workDiary_ScalTest md_workDiary_ScalTest = db.Md_workDiary_ScalTest.Find(id);
            if (md_workDiary_ScalTest == null)
            {
                return HttpNotFound();
            }
            return View(md_workDiary_ScalTest);
        }

        // POST: WorkDiary/md_workDiary_ScalTest/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            md_workDiary_ScalTest md_workDiary_ScalTest = db.Md_workDiary_ScalTest.Find(id);
            db.Md_workDiary_ScalTest.Remove(md_workDiary_ScalTest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
