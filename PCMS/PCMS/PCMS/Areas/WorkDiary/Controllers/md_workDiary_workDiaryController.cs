﻿using PCMS.Areas.WorkDiary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;

namespace PCMS.Areas.WorkDiary.Controllers
{
    public class md_workDiary_workDiaryController : Controller
    {
        private Areas.Order.Models.OrderContext db_Order = new Order.Models.OrderContext();
        private Areas.WorkDiary.Models.WorkDiaryContext db_WorkDiary = new WorkDiaryContext();

        // GET: WorkDiary/md_workDiary_workDiary
        public ActionResult Index(DateTime? date)
        {
            DateTime now = date ?? DateTime.Now;
            var workDiaryList = MdWorkDiaryWorkDiaryViews(now);
            ViewBag.Month = DateTimeStd.month[now.Month - 1];
            ViewBag.Date = now;
            return View(workDiaryList);

        }

        private List<md_workDiary_workDiary_View> MdWorkDiaryWorkDiaryViews(DateTime now)
        {
            DateTime start = new DateTime(now.Year, now.Month, 1);
            int dayInMonth = DateTime.DaysInMonth(now.Year, now.Month);
            DateTime end = new DateTime(now.Year, now.Month, dayInMonth,23,59,59);
            var order =
                db_Order.Md_order_LoadingOrder.Where(
                    a => start <= a.RegistrationDate && a.RegistrationDate <= end && a.State == 4).ToList();
            var workDiary =
                db_WorkDiary.md_workDiary_workDiary.Where(
                    a => start <= a.Timestamp && a.Timestamp <= end && a.IsDeleted == false).ToList();
            List<md_workDiary_workDiary_View> workDiaryList = new List<md_workDiary_workDiary_View>();
            for (int i = 0; i < dayInMonth; i++)
            {
                var tmp = new md_workDiary_workDiary_View();
                DateTime nextDay = start.AddDays(1);
                var tmpWorkDiary = workDiary.FirstOrDefault(a => a.Timestamp == start.Date);
                var tmpOrder = order.Where(a => a.RegistrationDate != null && a.RegistrationDate.Value.Date == start.Date).ToList();
                tmp.Timestamp = start.Date;
                tmp.TemperatureMax = tmpOrder.Max(a => a.CementAdditionValue1) ?? -111;
                tmp.TemperatureMin = tmpOrder.Min(a => a.CementAdditionValue1) ?? -111;
                tmp.DeliveryCount = tmpOrder.Count;
                tmp.DeliveryAmount = tmpOrder.Sum(a => a.OrderedQuantity) ?? -111;
                tmp.ScaleTestCount = db_WorkDiary.Md_workDiary_ScalTest.Count(a => start <= a.Timestamp && a.Timestamp < nextDay);
                if (tmpWorkDiary != null)
                {
                    tmp.TemperatureMaxManual = tmpWorkDiary.TemperatureMax;
                    tmp.TemperatureMinManual = tmpWorkDiary.TemperatureMin;
                    tmp.TemperatureConcrete = tmpWorkDiary.TemperatureConcrete;
                    tmp.DensityResyclingWater = tmpWorkDiary.DensityResyclingWater;
                    tmp.Comment = tmpWorkDiary.Comment;
                }

                workDiaryList.Add(tmp);
                start = start.AddDays(1);
            }
            return workDiaryList;
        }

        // GET: WorkDiary/md_workDiary_workDiary/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: WorkDiary/md_workDiary_workDiary/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WorkDiary/md_workDiary_workDiary/Create
        [HttpPost]
        //public ActionResult Create(FormCollection collection)
        public ActionResult Create(md_workDiary_workDiary workDiary)
        {
            try
            {
                if(db_WorkDiary.md_workDiary_workDiary.Where(a=>a.Timestamp == workDiary.Timestamp).ToList().Count > 0)
                {
                    foreach(var wD in db_WorkDiary.md_workDiary_workDiary.Where(a => a.Timestamp == workDiary.Timestamp).ToList())
                    {
                        wD.IsDeleted = true;
                        db_WorkDiary.Entry(wD).State = System.Data.Entity.EntityState.Modified;
                        db_WorkDiary.SaveChanges();
                    }
                }

                // TODO: Add insert logic here
                workDiary.UserName = User.Identity.Name;
                workDiary.CreationDate = DateTime.Now;
                db_WorkDiary.md_workDiary_workDiary.Add(workDiary);
                db_WorkDiary.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View(workDiary);
            }
        }

        // GET: WorkDiary/md_workDiary_workDiary/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: WorkDiary/md_workDiary_workDiary/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: WorkDiary/md_workDiary_workDiary/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: WorkDiary/md_workDiary_workDiary/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ReportDetail(DateTime? date)
        {
            DateTime now = date ?? DateTime.Now;
            var workDiaryList = MdWorkDiaryWorkDiaryViews(now);
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();

            foreach (var dataset in workDiaryList)
            {
                ds.List.AddListRow(dataset.Timestamp.ToShortDateString(), dataset.TemperatureMaxManual.ToString() + (dataset.TemperatureMax != -111 ? "(" + dataset.TemperatureMax + ")" : "" ), dataset.TemperatureMinManual.ToString() + (dataset.TemperatureMin != -111 ? "(" + dataset.TemperatureMin + ")" : ""), dataset.TemperatureConcrete.ToString(), dataset.DensityResyclingWater.ToString(), dataset.DeliveryCount.ToString(), dataset.DeliveryAmount.ToString(), "","","", "","", "", "", "", "", "", "", "", "");
            }
            ReportDataSource dsReport = new ReportDataSource("DataSet1", ds.List.ToList());

            //ReportDataSource test = fillReport(pg);
            


            List<ReportParameter> repParams = new List<ReportParameter>
            {
                new ReportParameter("Date", DateTime.Now.ToString("HH:mm dd.MM.yyyy")),new ReportParameter("DateFrom", DateTimeStd.month[now.Month - 1] + " " + now.Year),
                //new ReportParameter("DateTo", to)
            };


            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/WorkDiary/WorkDiaryList.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(dsReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;

            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);

            return Json(true);
        }
    }
}
