﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PCMS.Areas.Order.Models;
using PCMS.Models;

namespace PCMS.Areas.Order.Controllers
{
    public class md_order_AdvertisingController : Controller
    {
        private OrderContext db = new OrderContext();

        // GET: Order/md_order_Advertising
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.md_order_Advertising.Where(a=>a.IsDeleted == false).OrderBy(a=>a.Id).AsQueryable();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                }
            }
            switch (pg.orderCol)
            {
                
                case "ShortText":
                    model = pg.orderDir.Equals("desc") ? model.OrderBy(m => m.ShortText).AsQueryable() : model.OrderByDescending(m => m.ShortText).AsQueryable();
                    break;
                case "Text":
                    model = pg.orderDir.Equals("desc") ? model.OrderBy(m => m.Text).AsQueryable() : model.OrderByDescending(m => m.Text).AsQueryable();
                    break;
                case "Default":
                    model = pg.orderDir.Equals("desc") ? model.OrderBy(m => m.Default).AsQueryable() : model.OrderByDescending(m => m.Default).AsQueryable();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }


        // GET: Order/md_order_Advertising/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_order_Advertising md_order_Advertising = db.md_order_Advertising.Find(id);
            if (md_order_Advertising == null)
            {
                return HttpNotFound();
            }
            return View(md_order_Advertising);
        }

        // GET: Order/md_order_Advertising/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Order/md_order_Advertising/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UId,ShortText,Text,Default,IsActiv,IsDeleted")] md_order_Advertising md_order_Advertising)
        {
            if (ModelState.IsValid)
            {
                md_order_Advertising.IsDeleted = false;
                md_order_Advertising.IsActiv = true;
                if(md_order_Advertising.Default == true)
                {
                    using (var dbTmp = new OrderContext())
                    {
                        dbTmp.md_order_Advertising.ToList().ForEach(a =>
                        {
                            a.Default = false;
                        });
                        dbTmp.SaveChanges();
                    }

                }
                db.md_order_Advertising.Add(md_order_Advertising);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(md_order_Advertising);
        }

        // GET: Order/md_order_Advertising/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_order_Advertising md_order_Advertising = db.md_order_Advertising.Find(id);
            if (md_order_Advertising == null)
            {
                return HttpNotFound();
            }
            return View(md_order_Advertising);
        }

        // POST: Order/md_order_Advertising/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UId,ShortText,Text,Default,IsActiv,IsDeleted")] md_order_Advertising md_order_Advertising)
        {
            if (ModelState.IsValid)
            {
                if (md_order_Advertising.Default == true)
                {
                    using (var dbTmp = new OrderContext())
                    {
                        dbTmp.md_order_Advertising.ToList().ForEach(a =>
                        {
                            a.Default = false;
                        });
                        dbTmp.SaveChanges();
                    }
                    
                }
                db.Entry(md_order_Advertising).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(md_order_Advertising);
        }

        // GET: Order/md_order_Advertising/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_order_Advertising md_order_Advertising = db.md_order_Advertising.Find(id);
            if (md_order_Advertising == null)
            {
                return HttpNotFound();
            }
            return View(md_order_Advertising);
        }

        // POST: Order/md_order_Advertising/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            md_order_Advertising md_order_Advertising = db.md_order_Advertising.Find(id);
            db.md_order_Advertising.Remove(md_order_Advertising);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
