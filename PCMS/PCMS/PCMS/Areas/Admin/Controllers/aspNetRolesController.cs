﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Admin.Models;
using PCMS.Models;
using PagedList;
using PCMS.Helper;

namespace PCMS.Areas.Admin.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class aspNetRolesController : Controller
    {
        private AdminContext db = new AdminContext();

        // GET: Admin/aspNetRoles
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.AspNetRoles.ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }


        // GET: Admin/aspNetRoles/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aspNetRoles aspNetRoles = db.AspNetRoles.Find(id);
            if (aspNetRoles == null)
            {
                return HttpNotFound();
            }
            return View(aspNetRoles);
        }

        // GET: Admin/aspNetRoles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/aspNetRoles/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] aspNetRoles aspNetRoles)
        {
            if (ModelState.IsValid)
            {
                aspNetRoles.Id = Guid.NewGuid().ToString();
                db.AspNetRoles.Add(aspNetRoles);
                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }

            return View(aspNetRoles);
        }

        // GET: Admin/aspNetRoles/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aspNetRoles aspNetRoles = db.AspNetRoles.Find(id);
            if (aspNetRoles == null)
            {
                return HttpNotFound();
            }
            return View(aspNetRoles);
        }

        // POST: Admin/aspNetRoles/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] aspNetRoles aspNetRoles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aspNetRoles).State = EntityState.Modified;
                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }
            return View(aspNetRoles);
        }

        // GET: Admin/aspNetRoles/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aspNetRoles aspNetRoles = db.AspNetRoles.Find(id);
            if (aspNetRoles == null)
            {
                return HttpNotFound();
            }
            return View(aspNetRoles);
        }

        // POST: Admin/aspNetRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            aspNetRoles aspNetRoles = db.AspNetRoles.Find(id);
            if(aspNetRoles.Name.Equals("developer") || aspNetRoles.Name.Equals("admin"))
                return RedirectToAction("Index");
            db.AspNetRoles.Remove(aspNetRoles);
            //db.SaveChanges();
            db.SaveChanges(User.Identity.Name);
            return RedirectToAction("Index");
        }


        [HttpPost]
        public ActionResult CreateRole(string roleName)
        {
            //System.Web.Security.
            //Roles.AddUserToRole(User.Identity.Name, "admin");

            if(db.AspNetRoles.Where(a=> a.Name.ToLower() == roleName.ToLower()).ToList().Count() > 0)
                return Json(false);
            if (!string.IsNullOrWhiteSpace(roleName))
            {
                aspNetRoles role = new aspNetRoles()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = roleName
                };
                db.AspNetRoles.Add(role);
                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);
                return Json(true);
            }
            else
            {
                return Json(false);
            }

        }
        [HttpPost]
        public ActionResult DeleteRole(string roleName)
        {
            var model = db.AspNetRoles.Where(a => a.Name.ToLower() == roleName.ToLower()).FirstOrDefault();
            if (model != null)
            {
                try
                {
                    db.AspNetRoles.Remove(model);
                    //db.SaveChanges();
                    db.SaveChanges(User.Identity.Name);
                    return Json(true);
                }
                catch(Exception e)
                {
                    return Json(e);
                }
            }
            else
            {
                return Json(false);
            }
        }
        


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        
}
}
