﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Admin.Models;
using PagedList;
using Microsoft.Reporting.WebForms;

namespace PCMS.Areas.Admin.Controllers
{
    public class facilitiesController : Controller
    {
        private AdminContext db = new AdminContext();

        // GET: Admin/facilities

        public ActionResult Index(PCMS.Models.PaginationModel pg)
        {
            var model = pageModel(pg);

            if (pg.report)
            {
                return View("Report", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        // GET: Admin/facilities/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            facilities facilities = db.Facilities.Find(id);
            if (facilities == null)
            {
                return HttpNotFound();
            }
            return View(facilities);
        }

        // GET: Admin/facilities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/facilities/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Description,Active,UseMixingLines,MixingLines,Concrete,Mortar")] facilities facilities)
        {
            if (ModelState.IsValid)
            {
                facilities.IsActive = true;
                facilities.IsDeleted = false;
                db.Facilities.Add(facilities);
                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }

            return View(facilities);
        }

        // GET: Admin/facilities/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            facilities facilities = db.Facilities.Find(id);
            if (facilities == null)
            {
                return HttpNotFound();
            }
            return View(facilities);
        }

        // POST: Admin/facilities/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description,Active,UseMixingLines,MixingLines,Concrete,Mortar")] facilities facilities)
        {
            if (ModelState.IsValid)
            {
                facilities.IsActive = true;
                facilities.IsDeleted = false;
                db.Entry(facilities).State = EntityState.Modified;
                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }
            return View(facilities);
        }

        // GET: Admin/facilities/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            facilities facilities = db.Facilities.Find(id);
            if (facilities == null)
            {
                return HttpNotFound();
            }
            return View(facilities);
        }

        // POST: Admin/facilities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            facilities facilities = db.Facilities.Find(id);
            facilities.IsActive = true;
            facilities.IsDeleted = true;
            db.Entry(facilities).State = EntityState.Modified;
            //db.Facilities.Remove(facilities);
            db.SaveChanges(User.Identity.Name);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Report(PCMS.Models.PaginationModel pg)
        {
            var model = pageModel(pg);
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            foreach (var dataset in model)
            {
                ds.List.AddListRow(dataset.Id.ToString(), dataset.Description, dataset.MixingLines.ToString(), dataset.Concrete.ToString(), dataset.Mortar.ToString(), dataset.UseMixingLines.ToString(), dataset.Active.ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Header_00", "ID"));
            repParams.Add(new ReportParameter("Hide_00", "false"));
            repParams.Add(new ReportParameter("Header_01", "Description"));
            repParams.Add(new ReportParameter("Hide_01", "false"));
            repParams.Add(new ReportParameter("Header_02", "Mischlinien"));
            repParams.Add(new ReportParameter("Hide_02", "false"));
            repParams.Add(new ReportParameter("Header_03", "Zement"));
            repParams.Add(new ReportParameter("Hide_03", "false"));
            repParams.Add(new ReportParameter("Header_04", "Putz"));
            repParams.Add(new ReportParameter("Hide_04", "false"));
            repParams.Add(new ReportParameter("Header_05", "mit Mischlinien"));
            repParams.Add(new ReportParameter("Hide_05", "false"));
            repParams.Add(new ReportParameter("Header_06", "Aktiv"));
            repParams.Add(new ReportParameter("Hide_06", "false"));
            repParams.Add(new ReportParameter("Header_07", ""));
            repParams.Add(new ReportParameter("Hide_07", "true"));
            repParams.Add(new ReportParameter("Header_08", ""));
            repParams.Add(new ReportParameter("Hide_08", "true"));
            repParams.Add(new ReportParameter("Header_09", ""));
            repParams.Add(new ReportParameter("Hide_09", "true"));

            repParams.Add(new ReportParameter("Title", "Facility List"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Liste der Werke"));
            repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count() + " Anzahl Mischlinien: " + model.Sum(c => c.MixingLines).ToString()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/ListReport.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 1000;
            reportViewer.Height = 700;



            ViewBag.ReportViewer = reportViewer;

            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // HELPERS
        public class DataHead
        {
            public string Title { get; set; }
            public string Date { get; set; }
            public string Footer { get; set; }
            public string Description { get; set; }
        }



        private List<facilities> pageModel(PCMS.Models.PaginationModel pg)
        {

            Areas.Admin.Models.AdminContext db = new Areas.Admin.Models.AdminContext();
            var model = db.Facilities.OrderBy(a => a.Id).Where(a => a.IsDeleted == false && a.IsActive== true).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Description":
                                model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                            case "Active":
                                model = model.Where(m => m.Active.ToString().ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Active = pgFF.colVal;
                                break;
                            case "Concrete":
                                model = model.Where(m => m.Concrete.ToString().ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Concrete = pgFF.colVal;
                                break;
                            case "Mortar":
                                model = model.Where(m => m.Mortar.ToString().ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Mortar = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
                case "Active":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Active).ToList();
                    else
                        model = model.OrderByDescending(m => m.Active).ToList();
                    break;
                case "UseMixingLines":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UseMixingLines).ToList();
                    else
                        model = model.OrderByDescending(m => m.UseMixingLines).ToList();
                    break;
                case "MixingLines":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MixingLines).ToList();
                    else
                        model = model.OrderByDescending(m => m.MixingLines).ToList();
                    break;
                case "Concrete":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Concrete).ToList();
                    else
                        model = model.OrderByDescending(m => m.Concrete).ToList();
                    break;
                case "Mortar":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Mortar).ToList();
                    else
                        model = model.OrderByDescending(m => m.Mortar).ToList();
                    break;
            }


            return model;
        }


    }
}
