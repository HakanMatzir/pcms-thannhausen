﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Admin.Models;
using PagedList;
using PCMS.Models;

namespace PCMS.Areas.Admin.Controllers
{
    public class aspNetUsersController : Controller
    {
        private AdminContext db = new AdminContext();

        // GET: Admin/aspNetUsers
       
            public ActionResult Index(PaginationModel pg)
        {
            var model = db.AspNetUsers.ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Email":
                                model = model.Where(m => m.Email != null && m.Email.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Email = pgFF.colVal;
                                break;
                            case "UserName":
                                model = model.Where(m => m.UserName != null && m.UserName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.UserName = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Email":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Email).ToList();
                    else
                        model = model.OrderByDescending(m => m.Email).ToList();
                    break;               
                case "UserName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserName).ToList();
                    else
                        model = model.OrderByDescending(m => m.UserName).ToList();
                    break;
                case "Selectedfacility":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Selectedfacility).ToList();
                    else
                        model = model.OrderByDescending(m => m.Selectedfacility).ToList();
                    break;
            }
                       
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

    

    // GET: Admin/aspNetUsers/Details/5
    public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.aspNetUsers aspNetUsers = db.AspNetUsers.Find(id);
            if (aspNetUsers == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUsers);
        }

        // GET: Admin/aspNetUsers/Create
        public ActionResult Create()
        {
            ViewBag.Selectedfacility = new SelectList(db.Facilities, "Id", "Description");
            return View();
        }

        // POST: Admin/aspNetUsers/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,UserName,Selectedfacility")] Models.aspNetUsers aspNetUsers)
        {
            if (ModelState.IsValid)
            {
                db.AspNetUsers.Add(aspNetUsers);
                //db.SaveChanges();
                //db.SaveChanges(aspNetUsers.UserName);
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }

            ViewBag.Selectedfacility = new SelectList(db.Facilities, "Id", "Description", aspNetUsers.Selectedfacility);
            return View(aspNetUsers);
        }

        // GET: Admin/aspNetUsers/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.aspNetUsers aspNetUsers = db.AspNetUsers.Find(id);
            if (aspNetUsers == null)
            {
                return HttpNotFound();
            }
            ViewBag.Selectedfacility = new SelectList(db.Facilities, "Id", "Description", aspNetUsers.Selectedfacility).ToList();

            List<string> rolsId = new List<string>();

            foreach(var si in aspNetUsers.UsersRoles)
            {               
                    rolsId.Add(si.AspNetRoles.Id);  
            }
            MultiSelectList rol = new MultiSelectList(db.AspNetRoles.ToList(), "Id", "Name",rolsId);
            ViewBag.roles = rol;

            return View(aspNetUsers);
        }

        // POST: Admin/aspNetUsers/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,UserName,Selectedfacility")] Models.aspNetUsers aspNetUsers,string[] Facilities,string[] userRoles)
        {
            if (ModelState.IsValid)
            {
                if (Facilities != null) { 
                if (!Facilities.Contains(Convert.ToString(aspNetUsers.Selectedfacility)))
                {
                    aspNetUsers.Selectedfacility = null;
                }
                }
                var item = db.Entry<Models.aspNetUsers>(aspNetUsers);
                item.State = EntityState.Modified;
                //facility
                item.Collection(i => i.Facilities).Load();
                aspNetUsers.Facilities.Clear();
                //db.Entry(aspNetUsers).State = EntityState.Modified;
                //aspNetUsers.Facilities.Clear();
                if (Facilities != null)
                {
                    foreach (string s in Facilities)
                    {
                        aspNetUsers.Facilities.Add(db.Facilities.Find(Convert.ToInt64(s)));
                    }
                }
                //Roles
                item.Collection(i => i.UsersRoles).Load();
                aspNetUsers.UsersRoles.Clear();
                if (userRoles != null)
                {
                    foreach (string s in userRoles)
                    {
                        aspNetUsers.UsersRoles.Add(new aspNetUserRoles {UserId= aspNetUsers.Id, RoleId = s });
                    }
                }


                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }
            ViewBag.Selectedfacility = new SelectList(db.Facilities, "Id", "Description", aspNetUsers.Selectedfacility);
            return View(aspNetUsers);
        }

        // GET: Admin/aspNetUsers/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.aspNetUsers aspNetUsers = db.AspNetUsers.Find(id);
            if (aspNetUsers == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUsers);
        }

        // POST: Admin/aspNetUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Models.aspNetUsers aspNetUsers = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(aspNetUsers);
           // db.SaveChanges();
            db.SaveChanges(User.Identity.Name);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
