﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Material.Models;
using PCMS.Areas.Order.Models;
using PCMS.Models;
using PagedList;

namespace PCMS.Areas.Material.Controllers
{
    public class md_material_LocationGroupController : Controller
    {
        private MaterialContext db = new MaterialContext();

        // GET: Material/md_material_LocationGroup
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_material_LocationGroup.Where(m => m.IsDeleted == false).OrderBy(a => a.Description).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Description":
                                model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }



        // GET: Material/md_material_LocationGroup/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_material_LocationGroup md_material_LocationGroup = db.Md_material_LocationGroup.Find(id);
            if (md_material_LocationGroup == null)
            {
                return HttpNotFound();
            }
            return View(md_material_LocationGroup);
        }

        // GET: Material/md_material_LocationGroup/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Material/md_material_LocationGroup/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Description")] md_material_LocationGroup md_material_LocationGroup)
        {
            if (ModelState.IsValid)
            {
                md_material_LocationGroup.IsActive = true;
                md_material_LocationGroup.IsDeleted = false;
                db.Md_material_LocationGroup.Add(md_material_LocationGroup);
                db.SaveChanges(User.Identity.Name);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(md_material_LocationGroup);
        }

        // GET: Material/md_material_LocationGroup/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_material_LocationGroup md_material_LocationGroup = db.Md_material_LocationGroup.Find(id);
            if (md_material_LocationGroup == null)
            {
                return HttpNotFound();
            }
            return View(md_material_LocationGroup);
        }

        // POST: Material/md_material_LocationGroup/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description")] md_material_LocationGroup md_material_LocationGroup)
        {
            if (ModelState.IsValid)
            {
                md_material_LocationGroup.IsActive = true;
                md_material_LocationGroup.IsDeleted = false;
                db.Entry(md_material_LocationGroup).State = EntityState.Modified;
                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }
            return View(md_material_LocationGroup);
        }

        // GET: Material/md_material_LocationGroup/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_material_LocationGroup md_material_LocationGroup = db.Md_material_LocationGroup.Find(id);
            if (md_material_LocationGroup == null)
            {
                return HttpNotFound();
            }
            List<md_material_Location> mL = db.Md_material_Location.Where(a => a.Md_material_LocationGroup.Id == id).ToList();
            ViewBag.MaterialLocation = mL;
            return View(md_material_LocationGroup);
        }

        // POST: Material/md_material_LocationGroup/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            md_material_LocationGroup md_material_LocationGroup = db.Md_material_LocationGroup.Find(id);
            md_material_LocationGroup.IsActive = true;
            md_material_LocationGroup.IsDeleted = true;
            db.Entry(md_material_LocationGroup).State = EntityState.Modified;
            //db.Md_material_LocationGroup.Remove(md_material_LocationGroup);
            db.SaveChanges(User.Identity.Name);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
