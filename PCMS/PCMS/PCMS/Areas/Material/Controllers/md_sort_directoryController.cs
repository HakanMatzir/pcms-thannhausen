﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using PCMS.Areas.Material.Models;
using Microsoft.Reporting.WebForms;
using System.Net;

namespace PCMS.Areas.Material.Controllers
{

    //    USE[PCMS]
    //GO

    ///****** Object:  Table [dbo].[md_sort_Directory]    Script Date: 16.03.2016 16:56:23 ******/
    //SET ANSI_NULLS ON
    //GO

    //SET QUOTED_IDENTIFIER ON
    //GO

    //CREATE TABLE[dbo].[md_sort_Directory](
    //	[Id]
    //    [bigint] IDENTITY(1,1) NOT NULL,

    //[Identity] [nvarchar](128) NULL,
    //	[Description]
    //    [nvarchar](50) NULL,
    //	[Number]
    //    [nvarchar](50) NULL,
    //	[DescriptionHeaderLeft]
    //    [nvarchar](100) NULL,
    //	[DescriptionHeaderRight]
    //    [nvarchar](100) NULL,
    //	[DescriptionLeft1]
    //    [nvarchar](100) NULL,
    //	[DescriptionLeft2]
    //    [nvarchar](100) NULL,
    //	[DescriptionRight1]
    //    [nvarchar](100) NULL,
    //	[DescriptionRight2]
    //    [nvarchar](100) NULL,
    //	[Place]
    //    [nvarchar](50) NULL,
    //	[Name]
    //    [nvarchar](50) NULL,
    //	[FacoryId]
    //    [bigint]
    //    NULL,
    //	[Rev]
    //    [bigint]
    //    NULL,
    //	[IsActive]
    //    [bit]
    //    NULL,
    //	[IsDeleted]
    //    [bit]
    //    NULL,
    // CONSTRAINT[PK_md_sort_Directory] PRIMARY KEY CLUSTERED
    //(
    //   [Id] ASC
    //)WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]
    //) ON[PRIMARY]

    //GO
    //USE[PCMS]
    //GO

/****** Object:  Table [dbo].[md_sort_DirectoryDetails]    Script Date: 16.03.2016 16:56:50 ******/
//SET ANSI_NULLS ON
//GO

//SET QUOTED_IDENTIFIER ON
//GO

//CREATE TABLE[dbo].[md_sort_DirectoryDetails](
//	[SortDirectoryId]
//    [bigint]
//    NULL,
//	[Type]
//    [int]
//    NULL,
//	[GroupName]
//    [nvarchar](100) NULL,
//	[SortNo]
//    [nvarchar](50) NULL,
//	[Compression]
//    [nvarchar](50) NULL,
//	[Exposure]
//    [nvarchar](50) NULL,
//	[Consistency]
//    [nvarchar](50) NULL,
//	[Progress]
//    [nvarchar](50) NULL,
//	[Cement]
//    [nvarchar](50) NULL,
//	[WZValue]
//    [nvarchar](50) NULL,
//	[Aggreagete]
//    [nvarchar](50) NULL,
//	[Additive]
//    [nvarchar](50) NULL,
//	[AdditiveWeight]
//    [nvarchar](50) NULL,
//	[Admixture]
//    [nvarchar](50) NULL,
//	[AdditionalInfo]
//    [nvarchar](100) NULL,
//	[Sort]
//    [int]
//    NULL
//) ON[PRIMARY]

//GO


    public class md_sort_directoryController : Controller
    {
        private MaterialContext db = new MaterialContext();
        // GET: Material/md_sort_directory
        public ActionResult Index(PCMS.Models.PaginationModel pg)
        {
            var model = db.Md_sort_Directory.Where(a=>a.IsDeleted == false).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Description":
                                model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                            case "Number":
                                model = model.Where(m => m.Number != null && m.Number.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Number = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    model = pg.orderDir.Equals("desc") ? model.OrderBy(m => m.Id).ToList() : model.OrderByDescending(m => m.Id).ToList();
                    break;               
                case "Description":
                    model = pg.orderDir.Equals("desc") ? model.OrderBy(m => m.Description).ToList() : model.OrderByDescending(m => m.Description).ToList();
                    break;
                case "Number":
                    model = pg.orderDir.Equals("desc") ? model.OrderBy(m => m.Number).ToList() : model.OrderByDescending(m => m.Number).ToList();
                    break;
                
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(md_sort_Directory md_sort_Directory)
        {
            
            if (ModelState.IsValid)
            {
                md_sort_Directory.IsActive = true;
                md_sort_Directory.IsDeleted = false;
                db.Md_sort_Directory.Add(md_sort_Directory);
                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }

            //ViewBag.GroupId = new SelectList(db.Md_material_LocationGroup.Where(m => m.IsDeleted == false), "Id", "Description", md_material_Location.MaterialId);
            return View(md_sort_Directory);
        }
        public ActionResult Edit(long? id)
        {
            md_sort_Directory md_sort_Directory = db.Md_sort_Directory.Find(id);
            md_sort_Directory.Md_sort_DirectoryDetails = md_sort_Directory.Md_sort_DirectoryDetails.OrderBy(a => a.Sort).ToList();
            return View(md_sort_Directory);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(md_sort_Directory md_sort_Directory, List<md_sort_DirectoryDetails> md_sort_DirectoryDetails)
        {
            if (ModelState.IsValid)
            {
                if (md_sort_DirectoryDetails != null)
                {
                    foreach (var u in md_sort_DirectoryDetails)
                    {
                        u.SortDirectoryId = md_sort_Directory.Id;
                    }
                }
                md_sort_Directory.Md_sort_DirectoryDetails.Clear();
                db.Entry(md_sort_Directory).State = EntityState.Modified;
                db.Entry(md_sort_Directory).Collection(st => st.Md_sort_DirectoryDetails).Load();
                md_sort_Directory.Md_sort_DirectoryDetails.Clear();
                
                foreach (var u in md_sort_DirectoryDetails)
                {
                    md_sort_Directory.Md_sort_DirectoryDetails.Add(u);
                }
                md_sort_Directory.IsDeleted = false;
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
                //return View(md_sort_Directory);
            }
              
            return View(md_sort_Directory);
        }
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_sort_Directory md_sort_Directory = db.Md_sort_Directory.Find(id);
            
            if (md_sort_Directory == null)
            {
                return HttpNotFound();
            }
            return View(md_sort_Directory);
        }

        // POST: Material/md_material_Location/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            md_sort_Directory md_sort_Directory = db.Md_sort_Directory.Find(id);
            md_sort_Directory.IsActive = true;
            md_sort_Directory.IsDeleted = true;
            db.Entry(md_sort_Directory).State = EntityState.Modified;
            //db.Md_material_Location.Remove(md_material_Location);
            db.SaveChanges(User.Identity.Name);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Copy(long IdCopy, string DescriptionCopy, string NumberCopy)
        {
            var model = db.Md_sort_Directory.Include(a => a.Md_sort_DirectoryDetails).AsNoTracking().FirstOrDefault(a => a.Id == IdCopy);
            model.Id = 0;
            model.Number = NumberCopy;
            model.Description = DescriptionCopy;
            db.Md_sort_Directory.Add(model);
            db.SaveChanges(User.Identity.Name);
            return RedirectToAction("Edit", new { id = model.Id });

        }
        public ActionResult choosConcrete(PCMS.Models.PaginationModel pg)
        {
            var  db = new  Areas.RecipeM.Models.RecipeMContext();
            var model = new List< Areas.RecipeM.Models.md_masterData_Material>();
            if (Helper.Definitions.getFactoryGroup() == 1)
                model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 8).OrderBy(a => a.Id).ToList();
            else if (Helper.Definitions.getFactoryGroup() == 0)
                model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 5).OrderBy(a => a.Id).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Name = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => (m.ShortName ?? "").ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ShortName = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ArticleNumber = pgFF.colVal;
                                break;
                            case "MaterialGroupId":
                                model = model.Where(m => m.MaterialGroupId == Convert.ToInt64(pgFF.colVal)).ToList();
                                ViewBag.MaterialGroupId = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    model = pg.orderDir.Equals("desc") ? model.OrderBy(m => m.Id).ToList() : model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Name":
                    model = pg.orderDir.Equals("desc") ? model.OrderBy(m => m.Name).ToList() : model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    model = pg.orderDir.Equals("desc") ? model.OrderBy(m => m.ShortName).ToList() : model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    model = pg.orderDir.Equals("desc") ? model.OrderBy(m => m.ArticleNumber).ToList() : model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }
            ViewBag.MaterialGroup = new SelectList(db.Md_material_MaterialGroup.Where(a => a.Id != 1 && a.Id != 8), "Id", "Description", ViewBag.MaterialGroupId);
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
        public ActionResult getConcrete(long Id)
        {
            var db2 = new Recipe.Models.RecipeContext();
            var db = new Areas.MasterData.Models.MasterDataContext();
            var con = db.Md_masterData_Material.FirstOrDefault(a => a.Id == Id);
            var conDet = db.Md_material_SortDetails.FirstOrDefault(a => a.Id == con.SortId);
            var compression = db.Md_material_CompressionStrenghtGroup.Where(a => a.Id == conDet.CompressionId).Select(a=>a.Class).FirstOrDefault();
            var exposure = db.Md_material_ExposureKombi.Where(a => a.Id == conDet.ExposureKombiId).Select(a=>a.ShortText).FirstOrDefault();
            var consistency = db.Md_material_ConsistencyGroup.Where(a => a.Id == conDet.ConsistencyId).Select(a=>a.Class).FirstOrDefault();
            var progress = db.Md_material_Progress.Where(a => a.Id == conDet.ProgressId).Select(a => a.Description).FirstOrDefault();
            var sortType = "";
            if (con.MaterialTypeId != null)
            {
                sortType = db.Md_material_MaterialType.Find(con.MaterialTypeId).Description;
            }
            
            var cement = db2.Md_recipe_Recipe.Include(m => m.RecipeMaterials).FirstOrDefault(a => a.RecipeConcrete.Any(i => i.MaterialId == Id));
            var cmentStr = "";
            var additive = "";
            var additeveWeight = "";
            var admixture = "";
            if (cement != null)
            {
                cmentStr = cement.RecipeMaterials.Where(a => a.Type == 1 && a.Md_material_Material.MaterialGroupId == 2 && a.Md_material_Material.IsDeleted == false ).Select(a => a.Md_material_Material.ShortName).FirstOrDefault();
                additive = string.Join(",", cement.RecipeMaterials.Where(a => a.Type == 1 && a.Md_material_Material.MaterialGroupId == 6 && a.Md_material_Material.IsDeleted == false).Select(a => a.Md_material_Material.ShortName).ToList());
                additeveWeight = string.Join(",", cement.RecipeMaterials.Where(a => a.Type == 1 && a.Md_material_Material.MaterialGroupId == 6 && a.Md_material_Material.IsDeleted == false).Select(a => a.Value).ToList());
                admixture = string.Join(", ", cement.RecipeMaterials.Where(a => a.Type == 1 && a.Md_material_Material.MaterialGroupId == 3 && a.Md_material_Material.IsDeleted == false).Select(a => a.Md_material_Material.ShortName).ToList());

            }
            
            return Json(new { con= con, compression = compression ?? "", exposure = exposure ?? "", consistency = consistency ?? "", progress = progress ?? "", wz = conDet.WZValue != null ? conDet.WZValue.ToString() : "" , aggregate = conDet.Aggregate ?? "", cement = cmentStr ?? "", sortType =sortType??"", additive = additive , additeveWeight = additeveWeight, admixture= admixture });
        }
        public ActionResult ReportDetail(long? Id)
        {
            try
            {
                //var model = db.Md_recipe_Recipe.Find(Id);
                var model = db.Md_sort_Directory.Include(m => m.Md_sort_DirectoryDetails).FirstOrDefault(m => m.Id == Id);
                //Areas.RecipeM.Models.RecipeMContext dbM = new RecipeM.Models.RecipeMContext();
                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();

                int i = 1;
               
                foreach (var dataset in model.Md_sort_DirectoryDetails.OrderBy(a=>a.Sort))
                {

                    if (dataset.Type == 0)
                    {
                        

                        //ds.List.AddListRow(i.ToString(), m.MaterialNumber, m.Name, dataset.Unit == 0 ? "kg" : "%", dataset.Value.ToString(), m.Density.ToString() ?? "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                    else if (dataset.Type == 1)
                    {

                        ds.List.AddListRow(dataset.GroupName, dataset.SortNo, dataset.Compression, dataset.Exposure, dataset.Consistency, dataset.Progress, dataset.Cement, dataset.WZValue, dataset.Aggreagete, dataset.Additive, dataset.AdditiveWeight, dataset.Admixture, dataset.AdditionalInfo, dataset.SortType, "", "", "", "", "", dataset.Sort.ToString());
                    }
                    


                    i++;
                }
                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

                //ReportDataSource DSReport = new ReportDataSource("ds", ds.RuleGradingCurve.ToList());

               // var db2 = new MasterData.Models.MasterDataContext();
               // long sortId = model.RecipeConcrete.FirstOrDefault().Md_material_Material.SortId ?? 0;
               // var wz = db2.Md_material_SortDetails.Where(a => a.Id == sortId).Select(a => a.WZValue).FirstOrDefault();

                List<ReportParameter> repParams = new List<ReportParameter>
                {
                    new ReportParameter("DescriptionHeaderLeft", model.DescriptionHeaderLeft),
                    new ReportParameter("DescriptionHeaderRight", model.DescriptionHeaderright),
                    new ReportParameter("DescriptionLeft",
                        model.DescriptionLeft1 +
                        (string.IsNullOrEmpty(model.DescriptionLeft2) ? "" : ("\n" + model.DescriptionLeft2))),
                    new ReportParameter("DescriptionRight",
                        model.DescriptionRight1 +
                        (string.IsNullOrEmpty(model.DescriptionRight2) ? "" : ("\n" + model.DescriptionRight2))),
                    new ReportParameter("Place", model.Place),
                    new ReportParameter("Name", model.Name),
                    new ReportParameter("Date", DateTime.Now.ToShortDateString())
                };





                ReportViewer reportViewer = new ReportViewer {ProcessingMode = ProcessingMode.Local};
                reportViewer.LocalReport.ReportPath = "Reports/Material/Sort_Directory.rdlc";
                

                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");
            }
            catch (Exception e)
            {
                PCMS.Helper.ExceptionHelper.LogException(e, User.Identity.Name);
                return View("~/Views/Shared/Exception", e);
            }
        }
    }
    
}