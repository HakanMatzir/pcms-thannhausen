﻿using Microsoft.Reporting.WebForms;
using PagedList;
using PCMS.Areas.Material.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace PCMS.Areas.Material.Controllers
{
    public class ReportsController : Controller
    {
        private MaterialContext db = new MaterialContext();
        private Order.Models.OrderContext db2 = new Order.Models.OrderContext();
        // GET: Material/Reports
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult getMaterialMoving(string oderNumber)
        {


            var mm = db.Md_material_MaterialMoving.Where(a => a.OrderNo == oderNumber && a.LotNo != "0").ToList();
            var lo = db2.Md_order_LoadingOrder.Where(a => a.OrderNumber == oderNumber).FirstOrDefault();
            foreach (var item in mm)
            {
                item.LotNoExtern = Convert.ToString(db2.Md_order_Material.Where(a => a.Id == item.ArticleId).FirstOrDefault().MaterialGroupId);
                if (item.LotNoExtern == "6")
                {
                    MasterData.Models.MasterDataContext dbMasterData = new MasterData.Models.MasterDataContext();
                    item.Wet = dbMasterData.Md_material_AdditivesDetail.Where(a => a.Id == dbMasterData.Md_masterData_Material.Where(u => u.Id == item.ArticleId).FirstOrDefault().AdditivesId).FirstOrDefault().KValue;
                }
            }
            return Json(new { materialMoving = mm, loadinOrder = lo });
        }

        public ActionResult choosLoadingOrder(int? selected, string dateFrom, string dateTo)
        {


            selected = selected ?? 4;
            int day = DateTime.Now.Day;
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            //var modelLoading = db2.Md_order_LoadingOrder.Where(a => a.RegistrationDate.Value.Day == day && a.RegistrationDate.Value.Month == month && a.RegistrationDate.Value.Year == year && a.State == selected).OrderBy(a => a.Sort).ToList();

            var modelLoading = db2.Md_order_LoadingOrder.OrderBy(a => a.RegistrationDate).ToList();
            if (selected != null)
                modelLoading = modelLoading.Where(a => a.State == selected).ToList();

            if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                modelLoading = modelLoading.Where(m => m.RegistrationDate != null && m.RegistrationDate.Value.Date >= new DateTime(Convert.ToInt32(dateFrom.Split('-')[0]), Convert.ToInt32(dateFrom.Split('-')[1]), Convert.ToInt32(dateFrom.Split('-')[2])).Date && m.RegistrationDate.Value.Date <= new DateTime(Convert.ToInt32(dateTo.Split('-')[0]), Convert.ToInt32(dateTo.Split('-')[1]), Convert.ToInt32(dateTo.Split('-')[2])).Date).ToList();

            }


            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();

            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Alle", Value = "0" });
            listItems.Add(new SelectListItem { Text = "Gesperrt", Value = "1" });
            listItems.Add(new SelectListItem { Text = "Freigegeben", Value = "2", Selected = true });
            listItems.Add(new SelectListItem { Text = "in Bearbeitung", Value = "3" });
            listItems.Add(new SelectListItem { Text = "beendet", Value = "4" });
            listItems.Add(new SelectListItem { Text = "storniert", Value = "5" });

            ViewBag.SelectState = new SelectList(listItems, "Value", "Text", selected);
            ViewBag.DateFrom = dateFrom;
            ViewBag.DateTo = dateTo;
            return View((modelLoading));
        }
        public ActionResult chooseDriver(PCMS.Models.PaginationModel pg)
        {
            var mdContext = new Areas.MasterData.Models.MasterDataContext();
            // var model = mdContext.Md_masterData_Driver.Where(m => m.IsActive == true && m.IsDeleted == false).Include(m => m.Md_masterData_Address).Include(m => m.Md_masterData_Contact).OrderBy(a => a.Id).ToList();
            var model = mdContext.Md_masterData_Driver.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "DriverId":
                                model = model.Where(m => m.DriverNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.DriverNumber = pgFF.colVal;
                                break;
                            case "LastName":
                                model = model.Where(m => m.Md_masterData_Contact != null && m.Md_masterData_Contact.LastName != null && m.Md_masterData_Contact.LastName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.LastName = pgFF.colVal;
                                break;
                            case "FirstName":
                                model = model.Where(m => m.Md_masterData_Contact != null && m.Md_masterData_Contact.FirstName != null && m.Md_masterData_Contact.FirstName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FirstName = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address != null && m.Md_masterData_Address.Street != null && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Street = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address != null && m.Md_masterData_Address.ZipCode != null && m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ZipCode = pgFF.colVal;
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address != null && m.Md_masterData_Address.City != null && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.City = pgFF.colVal;
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "DriverId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.DriverNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.DriverNumber).ToList();
                    break;
                case "LastName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Contact.LastName).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Contact.LastName).ToList();
                    break;
                case "FirstName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Contact.FirstName).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Contact.FirstName).ToList();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).ToList();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).ToList();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
        public ActionResult ChooseCustomer(PCMS.Models.PaginationModel pg)
        {
            var model = db2.Md_order_Customer.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "CustomerId":
                                model = model.Where(m => m.CustomerId != null && m.CustomerId.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address.Street != null && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode != null && m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address.City != null && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).ToList();
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "CustomerId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CustomerId).ToList();
                    else
                        model = model.OrderByDescending(m => m.CustomerId).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).ToList();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).ToList();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }



        public ActionResult CheckConstructionSite(long id)
        {
            if (db2.Md_order_ConstructionSite.Where(a => a.CustomerId == id).Count() > 1)
            {
                return Json(true);
            }
            else
            {
                return Json(false);
            }
        }

        public ActionResult ChooseConstructionSite(PCMS.Models.PaginationModel pg, long? id, string number)
        {
            //if (id != null)
            //{
            var cusId = db2.Md_order_Customer.Where(a => a.CustomerId == id.ToString()).Select(a => a.Id).FirstOrDefault();
            if (cusId == null)
                cusId = 0;
            id = cusId;
            var model = db2.Md_order_ConstructionSite.Where(m => m.IsActive == true && m.IsDeleted == false && m.CustomerId == cusId).OrderBy(a => a.ConstructionSiteId).ToList();

            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "ConstructionSiteId":
                                model = model.Where(m => m.ConstructionSiteId != null && m.CustomerId == id && m.ConstructionSiteId.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name != null && m.CustomerId == id && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address.Street != null && m.CustomerId == id && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode != null && m.CustomerId == id && m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address.City != null && m.CustomerId == id && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "ConstructionSiteId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ConstructionSiteId).ToList();
                    else
                        model = model.OrderByDescending(m => m.ConstructionSiteId).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).ToList();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).ToList();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).ToList();
                    break;
            }

            ViewBag.CustomerId = id;
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));

            //var model = db.Md_order_ConstructionSite.Where(a => a.CustomerId == id).ToList();
            //return View(model);
            //}


        }


        public ActionResult reportMaterial(string number, string from, string to)
        {
            if (String.IsNullOrWhiteSpace(number))
            {



                //var model = db.Md_material_MaterialMoving.Where(m => m.ArticleNo != "" &&  !(m.LocationId == 40 || m.LocationId == 42)).ToList();
                var model = db.Md_material_MaterialMoving.Where(m => m.ArticleNo != "" && m.LotNo != "0").ToList();

                model = model.Where(m => m.RegistrationDate.Value.Date >= new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).Date && m.RegistrationDate.Value.Date <= new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).Date).ToList();

                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();



                var MaterialList = model.Select(a => a.ArticleNo).Distinct().ToList();

                decimal sumPlus = 0;
                decimal sumMinus = 0;
                foreach (var u in MaterialList)
                {
                    var mo = model.Where(a => a.ArticleNo == u).FirstOrDefault();
                    sumPlus = model.Where(a => a.ArticleNo == u && a.MovingType == 0).Select(a => a.QuantityIs).Sum() ?? 0;
                    sumMinus = model.Where(a => a.ArticleNo == u && a.MovingType == 1).Select(a => a.QuantityIs).Sum() ?? 0;
                    decimal diff = sumPlus + sumMinus;
                    ds.List.AddListRow(mo.ArticleNo, mo.ArticleDescription, Math.Round(sumPlus, 2).ToString(), Math.Round(sumMinus, 2).ToString(), Math.Round(diff, 2).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                }

                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

                //ReportDataSource test = fillReport(pg);

                List<ReportParameter> repParams = new List<ReportParameter>();
                repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
                repParams.Add(new ReportParameter("DateFrom", new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).ToShortDateString()));
                repParams.Add(new ReportParameter("DateTo", new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).ToShortDateString()));
                // repParams.Add(new ReportParameter("OrderNo", OrderNumber));
                //repParams.Add(new ReportParameter("RecipeDescription", recipeDescription));
                //repParams.Add(new ReportParameter("IsValue", model.Sum(a => a.QuantityIs).ToString()));
                //repParams.Add(new ReportParameter("SetValue", model.Sum(a => a.QuantitySetpoint).ToString()));



                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/Material/MaterialList.rdlc";
                //reportViewer.ShowPrintButton = false;
                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;


                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);
                //ViewBag.ReportViewer = reportViewer;

                //return View("Report");

            }
            else
            {
                var model = db.Md_material_MaterialMoving.Where(m => m.ArticleNo == number).ToList();
                model = model.Where(m => m.RegistrationDate.Value.Date >= new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).Date && m.RegistrationDate.Value.Date <= new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).Date).ToList();
                if (model == null)
                {
                    return HttpNotFound();
                }
                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();
                string recipeDescription = "";
                foreach (var dataset in model)
                {
                    string abw = "--";
                    if (dataset.QuantityIs != null && dataset.QuantitySetpoint != null)
                    {
                        abw = Math.Round((dataset.QuantityIs - dataset.QuantitySetpoint ?? 0), 2).ToString();
                    }
                    recipeDescription = dataset.RecipeDescription;
                    ds.List.AddListRow(dataset.LotNo, dataset.ArticleNo, dataset.ArticleDescription, dataset.QuantitySetpoint.HasValue ? Math.Round(dataset.QuantitySetpoint.Value, 2).ToString() : "", dataset.QuantityIs.HasValue ? dataset.QuantityIs.Value.ToString() : "", abw, dataset.ApprovedTolPlus.HasValue ? Math.Round(dataset.ApprovedTolPlus.Value, 2).ToString() : "", dataset.ApprovedTolMinus.HasValue ? Math.Round(dataset.ApprovedTolMinus.Value, 2).ToString() : "", dataset.Trailing.HasValue ? Math.Round(dataset.Trailing.Value, 2).ToString() : "", dataset.ShiftFine.HasValue ? Math.Round(dataset.ShiftFine.Value, 2).ToString() : "", dataset.LineNo.ToString(), (dataset.RegistrationDate ?? new DateTime()).ToString("dd.MM.yyyy HH:mm:ss"), "", "", "", "", "", "", "", "");
                }
                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

                //ReportDataSource test = fillReport(pg);

                List<ReportParameter> repParams = new List<ReportParameter>();
                repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
                repParams.Add(new ReportParameter("DateFrom", new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).ToShortDateString()));
                repParams.Add(new ReportParameter("DateTo", new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).ToShortDateString()));
                // repParams.Add(new ReportParameter("OrderNo", OrderNumber));
                repParams.Add(new ReportParameter("RecipeDescription", recipeDescription));
                repParams.Add(new ReportParameter("IsValue", model.Sum(a => a.QuantityIs).ToString()));
                repParams.Add(new ReportParameter("SetValue", model.Sum(a => a.QuantitySetpoint).ToString()));



                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/Material/MaterialMoving.rdlc";
                //reportViewer.ShowPrintButton = false;
                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;


                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);



                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");
            }
        }

        public ActionResult reportFabrikat(string number, string from, string to)
        {
            if (String.IsNullOrWhiteSpace(number))
            {


                //var model = new List<Areas.Material.Models.md_material_MaterialMoving>();
                //if (Helper.Definitions.getFactoryGroup() == 1)
                //     model = db.Md_material_MaterialMoving.Where(m => m.RecipeDescription != "" && ( m.LocationId == 40 || m.LocationId == 42)).ToList();
                //else if (Helper.Definitions.getFactoryGroup() == 0)
                //    model = db.Md_material_MaterialMoving.Where(m => m.LotNo == "0").ToList();
                //model = model.Where(m => m.RegistrationDate.Value.Date >= new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).Date && m.RegistrationDate.Value.Date <= new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).Date).ToList();
                DateTime fromD = new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).Date;
                DateTime toD = new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).Date;
                var model = db2.Md_order_LoadingOrder.Where(m => EntityFunctions.TruncateTime(m.RegistrationDate.Value) >= fromD && EntityFunctions.TruncateTime(m.RegistrationDate.Value) <= toD && m.State == 4);


                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();



                var MaterialList = model.Select(a => a.MaterialNumber).Distinct().ToList();

                decimal sumPlus = 0;
                decimal sumMinus = 0;
                foreach (var u in MaterialList)
                {
                    if (String.IsNullOrEmpty(u))
                        continue;
                    var mo = model.Where(a => a.MaterialNumber == u).FirstOrDefault();
                    sumPlus = model.Where(a => a.MaterialNumber == u).Select(a => a.OrderedQuantity).Sum() ?? 0;
                    sumMinus = model.Where(a => a.MaterialNumber == u).Select(a => a.BackAmount).Sum() ?? 0;
                    decimal diff = sumPlus + sumMinus;
                    ds.List.AddListRow(mo.MaterialNumber, mo.MaterialDescription, Math.Round(sumPlus, 2).ToString(), Math.Round(diff, 2).ToString(), Math.Round(sumMinus, 2).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                }

                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

                //ReportDataSource test = fillReport(pg);

                List<ReportParameter> repParams = new List<ReportParameter>();
                repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
                repParams.Add(new ReportParameter("DateFrom", new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).ToShortDateString()));
                repParams.Add(new ReportParameter("DateTo", new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).ToShortDateString()));
                // repParams.Add(new ReportParameter("OrderNo", OrderNumber));
                //repParams.Add(new ReportParameter("RecipeDescription", recipeDescription));
                //repParams.Add(new ReportParameter("IsValue", model.Sum(a => a.QuantityIs).ToString()));
                //repParams.Add(new ReportParameter("SetValue", model.Sum(a => a.QuantitySetpoint).ToString()));



                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/Material/MaterialListF.rdlc";
                //reportViewer.ShowPrintButton = false;
                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");

            }
            else
            {
                var model = db.Md_material_MaterialMoving.Where(m => m.ArticleNo == number).ToList();
                model = model.Where(m => m.RegistrationDate.Value.Date >= new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).Date && m.RegistrationDate.Value.Date <= new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).Date).ToList();
                if (model == null)
                {
                    return HttpNotFound();
                }
                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();
                string recipeDescription = "";
                foreach (var dataset in model)
                {
                    string abw = "--";
                    if (dataset.QuantityIs != null && dataset.QuantitySetpoint != null)
                    {
                        abw = Math.Round((dataset.QuantityIs - dataset.QuantitySetpoint) ?? 0, 2).ToString();
                    }
                    recipeDescription = dataset.RecipeDescription;
                    ds.List.AddListRow(dataset.LotNo, dataset.ArticleNo, dataset.ArticleDescription, dataset.QuantitySetpoint.HasValue ? Math.Round(dataset.QuantitySetpoint.Value, 2).ToString() : "", dataset.QuantityIs.HasValue ? Math.Round(dataset.QuantityIs.Value, 2).ToString() : "", abw, dataset.ApprovedTolPlus.HasValue ? Math.Round(dataset.ApprovedTolPlus.Value, 2).ToString() : "", dataset.ApprovedTolMinus.HasValue ? Math.Round(dataset.ApprovedTolMinus.Value, 2).ToString() : "", dataset.Trailing.HasValue ? dataset.Trailing.Value.ToString() : "", dataset.ShiftFine.HasValue ? dataset.ShiftFine.Value.ToString() : "", dataset.LineNo.ToString(), (dataset.RegistrationDate ?? new DateTime()).ToString("dd.MM.yyyy HH:mm:ss"), "", "", "", "", "", "", "", "");
                }
                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

                //ReportDataSource test = fillReport(pg);

                List<ReportParameter> repParams = new List<ReportParameter>();
                repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
                repParams.Add(new ReportParameter("DateFrom", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")));
                repParams.Add(new ReportParameter("DateTo", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")));
                // repParams.Add(new ReportParameter("OrderNo", OrderNumber));
                repParams.Add(new ReportParameter("RecipeDescription", recipeDescription));
                repParams.Add(new ReportParameter("IsValue", model.Sum(a => a.QuantityIs).ToString()));
                repParams.Add(new ReportParameter("SetValue", model.Sum(a => a.QuantitySetpoint).ToString()));



                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                //reportViewer.LocalReport.ReportPath = "Reports/Material/MaterialLot.rdlc";
                reportViewer.LocalReport.ReportPath = "Reports/Material/MaterialMoving.rdlc";
                //reportViewer.ShowPrintButton = false;
                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");
            }
        }
        public ActionResult reportDriver(string number, string from, string to)
        {
            if (String.IsNullOrWhiteSpace(number))
            {


                //var model = new List<Areas.Material.Models.md_material_MaterialMoving>();
                //if (Helper.Definitions.getFactoryGroup() == 1)
                //    model = db.Md_material_MaterialMoving.Where(m => m.RecipeDescription != "" && (m.LocationId == 40 || m.LocationId == 42)).ToList();
                //else if (Helper.Definitions.getFactoryGroup() == 0)
                //    model = db.Md_material_MaterialMoving.Where(m => m.LotNo == "0").ToList();
                DateTime toD = new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).Date;
                DateTime fromD = new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).Date;
                var model = db2.Md_order_LoadingOrder.Where(m => m.RegistrationDate > fromD && m.State == 4).ToList();
                model = model.Where(m => m.RegistrationDate.Value.Date >= fromD && m.RegistrationDate.Value.Date <= toD).ToList();


                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();



                var driverList = model.Select(a => a.DriverNumber).Distinct().ToList();

                decimal order = 0;
                decimal backorder = 0;
                foreach (var u in driverList.OrderBy(a => a, new SemiNumericComparer()))
                {
                    decimal sumPlus = 0;
                    decimal sumMinus = 0;
                    int count = 0;
                    var driver = db2.Md_order_Driver.FirstOrDefault(a => a.DriverNumber == u && a.IsDeleted == false);
                    if (String.IsNullOrEmpty(u))
                    {
                        sumPlus = model.Where(a => a.DriverNumber == null).Select(a => a.OrderedQuantity).Sum() ?? 0;
                        sumMinus = model.Where(a => a.DriverNumber == null).Select(a => a.BackAmount).Sum() ?? 0;
                        count = model.Where(a => a.DriverNumber == null).Select(a => a.BackAmount).Count();
                    }
                    else
                    {
                        sumPlus = model.Where(a => a.DriverNumber == u).Select(a => a.OrderedQuantity).Sum() ?? 0;
                        sumMinus = model.Where(a => a.DriverNumber == u).Select(a => a.BackAmount).Sum() ?? 0;
                        count = model.Where(a => a.DriverNumber == u).Select(a => a.BackAmount).Count();
                    }

                    backorder += sumMinus;
                    order += sumPlus;
                    decimal diff = sumPlus + sumMinus;
                    var driverName = "Selbstabholer";
                    if (driver != null && driver.Md_masterData_Contact != null)
                        driverName = driver.Md_masterData_Contact.LastName + " " + driver.Md_masterData_Contact.FirstName;

                    ds.List.AddListRow(u ?? "--", driverName,count.ToString(), Math.Round(sumPlus, 2).ToString(), Math.Round(sumMinus, 2).ToString(), Math.Round(diff, 2).ToString(),  "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                }

                ReportDataSource dsReport = new ReportDataSource("DataSet1", ds.List.ToList());

                //ReportDataSource test = fillReport(pg);

                List<ReportParameter> repParams = new List<ReportParameter>();
                repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy HH:mm")));
                repParams.Add(new ReportParameter("DateFrom", new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).ToShortDateString()));
                repParams.Add(new ReportParameter("DateTo", new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).ToShortDateString()));
                // repParams.Add(new ReportParameter("OrderNo", OrderNumber));
                //repParams.Add(new ReportParameter("RecipeDescription", recipeDescription));
                repParams.Add(new ReportParameter("IsValue", order.ToString() + "m²"));
                repParams.Add(new ReportParameter("SetValue", backorder.ToString() + "m²"));
                repParams.Add(new ReportParameter("Total", (order - backorder).ToString() + "m²"));



                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/MasterData/DriverKumuList.rdlc";
                //reportViewer.ShowPrintButton = false;
                reportViewer.LocalReport.DataSources.Add(dsReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");

            }
            else
            {
                var driver = db2.Md_order_Driver.Where(a => a.DriverNumber == number && a.IsActive == true && a.IsDeleted == false).FirstOrDefault();
                var model = db2.Md_order_LoadingOrder.Where(m => m.DriverNumber == number && m.State == 4).ToList();
                model = model.Where(m => m.RegistrationDate.Value.Date >= new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).Date && m.RegistrationDate.Value.Date <= new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).Date).ToList();
                if (model == null)
                {
                    return HttpNotFound();
                }
                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();
                string recipeDescription = "";
                decimal order = 0;
                decimal backorder = 0;


                foreach (var dataset in model)
                {
                    string abw = "--";
                    if (dataset.OrderedQuantity != null && dataset.BackAmount != null)
                    {
                        abw = Math.Round((dataset.OrderedQuantity - dataset.BackAmount) ?? 0, 2).ToString();
                    }
                    order += dataset.OrderedQuantity ?? 0;
                    backorder += dataset.BackAmount ?? 0;
                    recipeDescription = dataset.RecipeDescription;
                    ds.List.AddListRow(dataset.OrderNumber, dataset.CustomerName, dataset.ConstructionSiteDescription, dataset.OrderedQuantity.HasValue ? Math.Round(dataset.OrderedQuantity.Value, 2).ToString() : "", dataset.BackAmount.HasValue ? Math.Round(dataset.BackAmount.Value, 2).ToString() : "", abw, (dataset.RegistrationDate ?? new DateTime()).ToString("dd.MM.yyyy HH:mm:ss"), "", "", "", "", "", "", "", "", "", "", "", "", "");
                }
                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

                //ReportDataSource test = fillReport(pg);

                List<ReportParameter> repParams = new List<ReportParameter>();
                repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
                repParams.Add(new ReportParameter("DateFrom", new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).ToShortDateString()));
                repParams.Add(new ReportParameter("DateTo", new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).ToShortDateString()));
                if (driver != null && driver.Md_masterData_Contact != null)
                    repParams.Add(new ReportParameter("Driver", driver.DriverNumber + ", " + driver.Md_masterData_Contact.LastName + " " + driver.Md_masterData_Contact.FirstName));
                //repParams.Add(new ReportParameter("DateFrom", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")));
                //repParams.Add(new ReportParameter("DateTo", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")));
                // repParams.Add(new ReportParameter("OrderNo", OrderNumber));
                repParams.Add(new ReportParameter("RecipeDescription", recipeDescription));
                repParams.Add(new ReportParameter("IsValue", order.ToString() + "m²"));
                repParams.Add(new ReportParameter("SetValue", backorder.ToString() + "m²"));
                repParams.Add(new ReportParameter("Total", (order - backorder).ToString() + "m²"));
                repParams.Add(new ReportParameter("Count", (model.Count).ToString()));



                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;

                //reportViewer.LocalReport.ReportPath = "Reports/Material/MaterialLot.rdlc";
                reportViewer.LocalReport.ReportPath = "Reports/MasterData/DriverMovingList.rdlc";
                //reportViewer.ShowPrintButton = false;
                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");
            }
        }
        public ActionResult reportCustomer(string number, string from, string to, string conNumber)
        {
            if (String.IsNullOrWhiteSpace(number) && String.IsNullOrWhiteSpace(conNumber))
            {
                //var model = new List<Areas.Material.Models.md_material_MaterialMoving>();
                //if (Helper.Definitions.getFactoryGroup() == 1)
                //    model = db.Md_material_MaterialMoving.Where(m => m.RecipeDescription != "" && (m.LocationId == 40 || m.LocationId == 42)).ToList();
                //else if (Helper.Definitions.getFactoryGroup() == 0)
                //    model = db.Md_material_MaterialMoving.Where(m => m.LotNo == "0").ToList();
                DateTime toD = new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).Date;
                DateTime fromD = new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).Date;
                var model = db2.Md_order_LoadingOrder.Where(m => m.RegistrationDate > fromD && m.State != 5).ToList();
                model = model.Where(m => m.RegistrationDate.Value.Date >= fromD && m.RegistrationDate.Value.Date <= toD).ToList();


                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();



                var DriverList = model.Select(a => a.CustomerNumber).Distinct().ToList();

                decimal order = 0;
                decimal backorder = 0;
                foreach (var u in DriverList.OrderBy(a => a, new SemiNumericComparer()))
                {
                    decimal sumPlus = 0;
                    decimal sumMinus = 0;
                    var customer = db2.Md_order_Customer.Where(a => a.CustomerId == u && a.IsDeleted == false).FirstOrDefault();
                    if (String.IsNullOrEmpty(u))
                    {
                        sumPlus = model.Where(a => a.CustomerNumber == null).Select(a => a.OrderedQuantity).Sum() ?? 0;
                        sumMinus = model.Where(a => a.CustomerNumber == null).Select(a => a.BackAmount).Sum() ?? 0;
                    }
                    else
                    {
                        sumPlus = model.Where(a => a.CustomerNumber == u).Select(a => a.OrderedQuantity).Sum() ?? 0;
                        sumMinus = model.Where(a => a.CustomerNumber == u).Select(a => a.BackAmount).Sum() ?? 0;
                    }

                    backorder += sumMinus;
                    order += sumPlus;
                    decimal diff = sumPlus - sumMinus;
                    var customerName = "Nicht eingetragen";
                    if (customer != null)
                        customerName = customer.Name;

                    ds.List.AddListRow(u ?? "--", customerName, Math.Round(sumPlus, 2).ToString(), Math.Round(sumMinus, 2).ToString(), Math.Round(diff, 2).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                }

                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

                //ReportDataSource test = fillReport(pg);

                List<ReportParameter> repParams = new List<ReportParameter>();
                repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
                repParams.Add(new ReportParameter("DateFrom", new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).ToShortDateString()));
                repParams.Add(new ReportParameter("DateTo", new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).ToShortDateString()));
                // repParams.Add(new ReportParameter("OrderNo", OrderNumber));
                //repParams.Add(new ReportParameter("RecipeDescription", recipeDescription));
                repParams.Add(new ReportParameter("IsValue", order.ToString() + "m²"));
                repParams.Add(new ReportParameter("SetValue", backorder.ToString() + "m²"));
                repParams.Add(new ReportParameter("Total", (order - backorder).ToString() + "m²"));



                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/MasterData/CustomerKumuList.rdlc";
                //reportViewer.ShowPrintButton = false;
                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");

            }
            else if (String.IsNullOrWhiteSpace(conNumber))
            {
                //var model = new List<Areas.Material.Models.md_material_MaterialMoving>();
                //if (Helper.Definitions.getFactoryGroup() == 1)
                //    model = db.Md_material_MaterialMoving.Where(m => m.RecipeDescription != "" && (m.LocationId == 40 || m.LocationId == 42)).ToList();
                //else if (Helper.Definitions.getFactoryGroup() == 0)
                //    model = db.Md_material_MaterialMoving.Where(m => m.LotNo == "0").ToList();
                DateTime toD = new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).Date;
                DateTime fromD = new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).Date;
                var model = db2.Md_order_LoadingOrder.Where(m => m.RegistrationDate > fromD && m.State != 5 && m.CustomerNumber == number).ToList();
                model = model.Where(m => m.RegistrationDate.Value.Date >= fromD && m.RegistrationDate.Value.Date <= toD).ToList();


                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();



                var constructionNumbers = model.Select(a => a.ConstructionSiteDescription).Distinct().ToList();

                decimal order = 0;
                decimal backorder = 0;
                foreach (var u in constructionNumbers.OrderBy(a => a, new SemiNumericComparer()))
                {
                    decimal sumPlus = 0;
                    decimal sumMinus = 0;

                    if (String.IsNullOrEmpty(u))
                    {
                        sumPlus = model.Where(a => a.ConstructionSiteDescription == null).Select(a => a.OrderedQuantity).Sum() ?? 0;
                        sumMinus = model.Where(a => a.ConstructionSiteDescription == null).Select(a => a.BackAmount).Sum() ?? 0;
                    }
                    else
                    {
                        sumPlus = model.Where(a => a.ConstructionSiteDescription == u).Select(a => a.OrderedQuantity).Sum() ?? 0;
                        sumMinus = model.Where(a => a.ConstructionSiteDescription == u).Select(a => a.BackAmount).Sum() ?? 0;
                    }

                    backorder += sumMinus;
                    order += sumPlus;
                    decimal diff = sumPlus - sumMinus;


                    ds.List.AddListRow(model.Where(a => a.ConstructionSiteDescription == u).Select(a => a.ConstructionSiteNumber).FirstOrDefault() ?? "--", u ?? "--", Math.Round(sumPlus, 2).ToString(), Math.Round(sumMinus, 2).ToString(), Math.Round(diff, 2).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                }
                var customer = db2.Md_order_Customer.Where(a => a.CustomerId == number && a.IsDeleted == false).FirstOrDefault();
                var customerName = "Nicht eingetragen";
                if (customer != null)
                    customerName = customer.CustomerId + ", " + customer.Name;

                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

                //ReportDataSource test = fillReport(pg);

                List<ReportParameter> repParams = new List<ReportParameter>();
                repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
                repParams.Add(new ReportParameter("DateFrom", new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).ToShortDateString()));
                repParams.Add(new ReportParameter("DateTo", new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).ToShortDateString()));
                //repParams.Add(new ReportParameter("OrderNo", OrderNumber));
                //repParams.Add(new ReportParameter("RecipeDescription", recipeDescription));
                repParams.Add(new ReportParameter("IsValue", order.ToString() + "m²"));
                repParams.Add(new ReportParameter("SetValue", backorder.ToString() + "m²"));
                repParams.Add(new ReportParameter("Total", (order - backorder).ToString() + "m²"));
                repParams.Add(new ReportParameter("Customer", customerName));


                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/MasterData/ConstructionsiteList.rdlc";
                //reportViewer.ShowPrintButton = false;
                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");

            }
            else
            {
                var driver = db2.Md_order_Driver.Where(a => a.DriverNumber == number && a.IsActive == true && a.IsDeleted == false).FirstOrDefault();
                DateTime toD = new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).Date;
                DateTime fromD = new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).Date;
                var model = db2.Md_order_LoadingOrder.Where(m => m.RegistrationDate > fromD && m.State != 5 && m.CustomerNumber == number && m.ConstructionSiteNumber == conNumber).ToList();
                model = model.Where(m => m.RegistrationDate.Value.Date >= new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).Date && m.RegistrationDate.Value.Date <= new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).Date).ToList();
                if (model == null)
                {
                    return HttpNotFound();
                }
                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();
                string recipeDescription = "";
                decimal order = 0;
                decimal backorder = 0;


                foreach (var dataset in model)
                {
                    string abw = "--";
                    if (dataset.OrderedQuantity != null && dataset.BackAmount != null)
                    {
                        abw = Math.Round((dataset.OrderedQuantity - dataset.BackAmount) ?? 0, 2).ToString();
                    }
                    order += dataset.OrderedQuantity ?? 0;
                    backorder += dataset.BackAmount ?? 0;
                    recipeDescription = dataset.RecipeDescription;
                    ds.List.AddListRow(dataset.OrderNumber, dataset.CustomerName, dataset.ConstructionSiteDescription, dataset.MaterialNumber, dataset.OrderedQuantity.HasValue ? Math.Round(dataset.OrderedQuantity.Value, 2).ToString() : "", dataset.BackAmount.HasValue ? Math.Round(dataset.BackAmount.Value, 2).ToString() : "", abw, (dataset.RegistrationDate ?? new DateTime()).ToString("dd.MM.yyyy HH:mm:ss"), "", "", "", "", "", "", "", "", "", "", "", "");
                }
                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

                //ReportDataSource test = fillReport(pg);

                List<ReportParameter> repParams = new List<ReportParameter>();
                repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
                repParams.Add(new ReportParameter("DateFrom", new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).ToShortDateString()));
                repParams.Add(new ReportParameter("DateTo", new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).ToShortDateString()));
                if (driver != null && driver.Md_masterData_Contact != null)
                    repParams.Add(new ReportParameter("Driver", driver.DriverNumber + ", " + driver.Md_masterData_Contact.LastName + " " + driver.Md_masterData_Contact.FirstName));
                //repParams.Add(new ReportParameter("DateFrom", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")));
                //repParams.Add(new ReportParameter("DateTo", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")));
                // repParams.Add(new ReportParameter("OrderNo", OrderNumber));
                repParams.Add(new ReportParameter("RecipeDescription", recipeDescription));
                repParams.Add(new ReportParameter("IsValue", order.ToString() + "m²"));
                repParams.Add(new ReportParameter("SetValue", backorder.ToString() + "m²"));
                repParams.Add(new ReportParameter("Total", (order - backorder).ToString() + "m²"));



                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;

                //reportViewer.LocalReport.ReportPath = "Reports/Material/MaterialLot.rdlc";
                reportViewer.LocalReport.ReportPath = "Reports/MasterData/ConstructionsiteDetails.rdlc";
                //reportViewer.ShowPrintButton = false;
                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");
            }
        }
        public ActionResult reportAggregate(string number, string from, string to)
        {


            var matNo = db2.Md_order_Material.Where(a => a.MaterialGroupId == 1 && a.IsDeleted != null).Select(a => a.MaterialNumber).ToList();

            //var model = db.Md_material_MaterialMoving.Where(m => m.ArticleNo != "" &&  !(m.LocationId == 40 || m.LocationId == 42)).ToList();
            var model = db.Md_material_MaterialMoving.Where(m => m.ArticleNo != "" && m.MovingType == 0 && matNo.Contains(m.ArticleNo) ).ToList();

            model = model.Where(m => m.RegistrationDate.Value.Date >= new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).Date && m.RegistrationDate.Value.Date <= new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).Date).ToList();

            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();



            var MaterialList = model.OrderBy(m => m.ArticleNo).Select(a => a.ArticleNo).Distinct().ToList();

            decimal sumPlus = 0;
            decimal sumMinus = 0;
            //foreach (var u in MaterialList)
            //{
            //    var mo = model.Where(a => a.ArticleNo == u).FirstOrDefault();
            //    sumPlus = model.Where(a => a.ArticleNo == u && a.MovingType == 0).Select(a => a.QuantityIs).Sum() ?? 0;
            //    sumMinus = model.Where(a => a.ArticleNo == u && a.MovingType == 1).Select(a => a.QuantityIs).Sum() ?? 0;
            //    decimal diff = sumPlus + sumMinus;
            //    ds.List.AddListRow(mo.ArticleNo, mo.ArticleDescription, Math.Round(sumPlus, 2).ToString(), Math.Round(sumMinus, 2).ToString(), Math.Round(diff, 2).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            //}
            foreach (var item in model)
            {
                string exLot = item.LotNoExtern.Trim();
                ds.List.AddListRow(item.ArticleNo, item.ArticleDescription, item.RegistrationDate.Value.ToShortDateString(), item.LotNoExtern, item.LocationDescription, (item.QuantityIs ?? 0).ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            }

            ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

            //ReportDataSource test = fillReport(pg);

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy") + "\n" +DateTime.Now.ToShortTimeString() ));
            repParams.Add(new ReportParameter("DateFrom", new DateTime(Convert.ToInt32(from.Split('-')[0]), Convert.ToInt32(from.Split('-')[1]), Convert.ToInt32(from.Split('-')[2])).ToShortDateString()));
            repParams.Add(new ReportParameter("DateTo", new DateTime(Convert.ToInt32(to.Split('-')[0]), Convert.ToInt32(to.Split('-')[1]), Convert.ToInt32(to.Split('-')[2])).ToShortDateString()));
            // repParams.Add(new ReportParameter("OrderNo", OrderNumber));
            //repParams.Add(new ReportParameter("RecipeDescription", recipeDescription));
            //repParams.Add(new ReportParameter("IsValue", model.Sum(a => a.QuantityIs).ToString()));
            //repParams.Add(new ReportParameter("SetValue", model.Sum(a => a.QuantitySetpoint).ToString()));



            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/Material/MaterialAggregateList.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;


            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);
            //ViewBag.ReportViewer = reportViewer;

            //return View("Report");


        }
        public class SemiNumericComparer : IComparer<string>
        {
            public int Compare(string s1, string s2)
            {
                if (string.IsNullOrEmpty(s1) && !string.IsNullOrEmpty(s2))
                    return 1;
                if (!string.IsNullOrEmpty(s1) && string.IsNullOrEmpty(s2))
                    return -1;
                if (string.IsNullOrEmpty(s1) && string.IsNullOrEmpty(s2))
                    return 0;

                if (IsNumeric(s1) && IsNumeric(s2))
                {
                    if (Convert.ToInt32(s1) > Convert.ToInt32(s2)) return 1;
                    if (Convert.ToInt32(s1) < Convert.ToInt32(s2)) return -1;
                    if (Convert.ToInt32(s1) == Convert.ToInt32(s2)) return 0;
                }



                if (IsNumeric(s1) && !IsNumeric(s2))
                    return -1;

                if (!IsNumeric(s1) && IsNumeric(s2))
                    return 1;

                return string.Compare(s1, s2, true);
            }

            public static bool IsNumeric(object value)
            {
                try
                {
                    int i = Convert.ToInt32(value.ToString());
                    return true;
                }
                catch (FormatException)
                {
                    return false;
                }
            }
        }

    }
}