﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PCMS.Areas.Material.Models
{
    public class MaterialContext : DbContext
    {
        public MaterialContext()
            : base("DefaultConnection")
        {
        }

        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<md_material_MaterialMoving>().Property(e => e.ApprovedTolPlus).HasColumnName("ApprovedTol+");
            modelBuilder.Entity<md_material_MaterialMoving>().Property(e => e.ApprovedTolMinus).HasColumnName("ApprovedTol-");
            modelBuilder.Entity<md_sort_Directory>()
               .HasMany(up => up.Md_sort_DirectoryDetails)
               //.WithRequired(s => s.Lims_aggregate_TestD)
               .WithRequired()
               .HasForeignKey(s => s.SortDirectoryId);

            base.OnModelCreating(modelBuilder);
        }
        public virtual DbSet<md_material_Location> Md_material_Location { get; set; }
        public virtual DbSet<md_material_LocationGroup> Md_material_LocationGroup { get; set; }
        public virtual DbSet<md_material_MaterialMoving> Md_material_MaterialMoving { get; set; }
        public virtual DbSet<md_sort_Directory> Md_sort_Directory { get; set; }
        public virtual DbSet<LIMS.Models.AuditLog> Auditlog { get; set; }
        public virtual DbSet<md_Temperature> Md_Temperature { get; set; }

        public int SaveChanges(string userId)
        {
            try
            {
                foreach (var ent in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
                {
                    // For each changed record, get the audit record entries and add them
                    foreach (LIMS.Models.AuditLog x in Helper.AuditLogH.GetAuditRecordsForChange(ent, userId))
                    {
                        this.Auditlog.Add(x);
                    }
                }
                int u = base.SaveChanges();
                return u;
            }
            // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
            catch (Exception e)
            {
                Helper.ExceptionHelper.LogException(e, userId);
                return base.SaveChanges();
            }

            // Call the original SaveChanges(), which will save both the changes made and the audit records

        }

        
    }
    [Table("md_material_MaterialMoving")]
    public partial class md_material_MaterialMoving
    {
        public long Id { get; set; }
        [DisplayName("Benutzer")]
        public string UserId { get; set; }
        [DisplayName("Bewegungsart")]
        public int? MovingType { get; set; }
        [DisplayName("Zeilennummer")]
        public int? LineNo { get; set; }
        [DisplayName("Lagerortnummer")]
        public string LocationNo { get; set; }
        [DisplayName("Lagerort Min")]
        public decimal? LocationMin { get; set; }
        [DisplayName("Lagerort Max")]
        public decimal? LocationMax { get; set; }
        [DisplayName("Lagerortbezeichnung")]
        public string LocationDescription { get; set; }
        [DisplayName("Lagerort Id")]
        public long? LocationId { get; set; }
        [DisplayName("Lagerortart")]
        public long? LocationType { get; set; }
        [DisplayName("Artikel Id")]
        public long? ArticleId { get; set; }
        [DisplayName("Artikelbezeichnung")]
        public string ArticleDescription { get; set; }
        [DisplayName("Artikelnummer")]
        public string ArticleNo { get; set; }
        [DisplayName("Rezeptbezeichnung")]
        public string RecipeDescription { get; set; }
        [DisplayName("Rezept Id")]
        public long? RecipeId { get; set; }
        [DisplayName("Rezept Variante")]
        public long? RecipeVariant { get; set; }
        [DisplayName("Waagennummer")]
        public string ScaleNo { get; set; }
        [DisplayName("Menge Soll")]
        public decimal? QuantitySetpoint { get; set; }
        [DisplayName("Menge Ist")]
        public decimal? QuantityIs { get; set; }
        [DisplayName("Einheit")]
        public string Unit { get; set; }
        [DisplayName("Auftrags Id")]
        public long? OrderId { get; set; }
        [DisplayName("Auftragsnummer")]
        public string OrderNo { get; set; }
        [DisplayName("Auftragszeilennummer")]
        public int? OrderLineNo { get; set; }
        [DisplayName("Chargennummer")]
        public string LotNo { get; set; }
        [DisplayName("Extern")]
        public string LotNoExtern { get; set; }
        public DateTime? WarrantyDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        [DisplayName("Datum")]
        public DateTime? RegistrationDate { get; set; }
        [DisplayName("Toleranz +")]
        public decimal? ApprovedTolPlus { get; set; }
        [DisplayName("Toleranz -")]
        public decimal? ApprovedTolMinus { get; set; }
        [DisplayName("Nachlauf")]
        public decimal? Trailing { get; set; }
        [DisplayName("Feindosierung")]
        public decimal? ShiftFine { get; set; }
        [DisplayName("Startzeit")]
        public DateTime? StartDate { get; set; }
        [DisplayName("Stopzeit")]
        public DateTime? StopDate { get; set; }
        [DisplayName("Werks Id")]
        public long? FacilitieId { get; set; }
        [DisplayName("Werk")]
        public string FacilitieDescription { get; set; }
        [DisplayName("Exportiert")]
        public bool? Exported { get; set; }
        public decimal? Wet { get; set; }
        public int? BatchLine { get; set; }
    }


    [Table("md_material_Location")]
    public partial class md_material_Location
    {
        public long Id { get; set; }
        [DisplayName("Lagergruppe")]
        public long GroupId { get; set; }
        [DisplayName("Material")]
        public long? MaterialId { get; set; }
        [DisplayName("Werk")]
        public long FacilityId { get; set; }
        [DisplayName("Lagernummer")]
        public string Number { get; set; }
        [DisplayName("Beschreibung")]
        public string Description { get; set; }
        [DisplayName("Max")]
        public decimal Max { get; set; }
        [DisplayName("Min")]
        public decimal Min { get; set; }
        [DisplayName("Bestand")]
        public decimal Value { get; set; }
        [DisplayName("Kapazität")]
        public decimal Capacity { get; set; }
        [DisplayName("Aktiv Entnahme")]
        public bool Active1 { get; set; }
        [DisplayName("Aktiv Befüllung")]
        public bool Active2 { get; set; }
        [DisplayName("Aktiv3")]
        public bool Active3 { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        [ForeignKey("GroupId")]
        public virtual md_material_LocationGroup Md_material_LocationGroup { get; set; }
        [ForeignKey("MaterialId")]
        public virtual Areas.MasterData.Models.md_masterData_Material Md_material_Material { get; set; }

        public decimal getValue()
        {
            MaterialContext db = new MaterialContext();
            return  db.Md_material_MaterialMoving.Where(a => a.LocationId == this.Id).Select(a => a.QuantityIs).Sum() ?? 0;
        }
    }

    [Table("md_material_LocationGroup")]
    public partial class md_material_LocationGroup
    {
        public long Id { get; set; }
        [DisplayName("Lagerart")]
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
    //[Table("md_material_Material")]
    //public partial class md_material_Material
    //{
    //    public long Id { get; set; }
    //    public string ArticleNumber { get; set; }
    //    public long? MaterialGroupId { get; set; }
    //    public string Name { get; set; }
    //    public string ShortName { get; set; }
    //    public decimal? Stock { get; set; }
    //    public decimal? Density { get; set; }
    //    public long? PriceGroupId { get; set; }
    //    public decimal? Humidity { get; set; }
    //    public long? AggregateId { get; set; }
    //    public decimal? GradingGroup { get; set; }
    //    public decimal? MaxAggregateSize { get; set; }
    //    public decimal? BulkDensity { get; set; }
    //    public decimal? KValue { get; set; }
    //    public long? SieveSetId { get; set; }
    //    public long? NormId { get; set; }
    //    public long? ConcreteFamilyId { get; set; }
    //    public long? SuitabilityGroupId { get; set; }
    //    public long? RecipeId { get; set; }
    //    public long? ExposureGroupId { get; set; }
    //    public long? ConsistencyGroupId { get; set; }
    //    public long? CompressionStrenghtGroupId { get; set; }
    //    public long? MaxAggregateSizeGroupId { get; set; }
    //    public long? ChlorideGroupId { get; set; }
    //    public decimal? MaxWCValue { get; set; }
    //    public decimal? MaxAirContent { get; set; }
    //    public decimal? MinAirContent { get; set; }
    //    public decimal? MaxConcreteContent { get; set; }
    //    public decimal? MinConcreteContent { get; set; }
    //    public decimal? MaxFineAggregateContent { get; set; }
    //    public decimal? MaxFlowSpread { get; set; }
    //    public decimal? MinFlowSpread { get; set; }
    //    public decimal? MaxBulkDensity { get; set; }
    //    public decimal? MinBulkDensity { get; set; }
    //    public decimal? MaxFlyAshContent { get; set; }
    //    public decimal? MaxSilicaContent { get; set; }
    //    public string Comment1 { get; set; }
    //    public string Comment2 { get; set; }
    //    public string Comment3 { get; set; }
    //    public string Comment4 { get; set; }
    //    public string Comment5 { get; set; }
    //    public bool? ExternalMonitoring { get; set; }
    //    public int? TestingAge { get; set; }
    //    public decimal? CuringPeriod1 { get; set; }
    //    public decimal? CuringPeriod2 { get; set; }
    //    public decimal? CuringPeriod3 { get; set; }
    //    public decimal? CuringPeriod4 { get; set; }
    //    public string RFID { get; set; }

    //}
    [Table("md_sort_Directory")]
    public partial class md_sort_Directory
    {
        public md_sort_Directory()
        {
            Md_sort_DirectoryDetails = new List<md_sort_DirectoryDetails>();
        }
        public long Id { get; set; }
        public string Identity { get; set; }
        [DisplayName("Verzeichnisname")]
        public string Description { get; set; }
        [DisplayName("Verzeichnisnummer")]
        public string Number { get; set; }
        [DisplayName("Haubtüberschrift links")]
        public string DescriptionHeaderLeft { get; set; }
        [DisplayName("Haubtüberschrift rechts")]
        public string DescriptionHeaderright { get; set; }
        [DisplayName("Teilüberschrift1 links")]
        public string DescriptionLeft1 { get; set; }
        [DisplayName("Teilüberschrift2 links")]
        public string DescriptionLeft2 { get; set; }
        [DisplayName("Teilüberschrift1 rechts")]
        public string DescriptionRight1 { get; set; }
        [DisplayName("Teilüberschrift2 rechts")]
        public string DescriptionRight2 { get; set; }
        [DisplayName("Ort")]
        public string Place { get; set; }
        [DisplayName("Unterzeichner")]
        public string Name { get; set; }
        public long FacoryId { get; set; }
        public long Rev { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public virtual ICollection<md_sort_DirectoryDetails> Md_sort_DirectoryDetails { get; set; }
    }
    [Table("md_sort_DirectoryDetails")]
    public partial class md_sort_DirectoryDetails
    {
        [Key, Column(Order = 0)]
        public long SortDirectoryId { get; set; }
        public int Type { get; set; }
        public string GroupName { get; set; }
        public string SortNo { get; set; }
        public string Compression { get; set; }
        public string Exposure { get; set; }
        public string Consistency { get; set; }
        public string Progress { get; set; }
        public string Cement { get; set; }
        public string WZValue { get; set; }
        public string Aggreagete { get; set; }
        public string Additive { get; set; }
        public string AdditiveWeight { get; set; }
        public string Admixture { get; set; }
        public string AdditionalInfo { get; set; }
        public string SortType { get; set; }
        [Key, Column(Order = 1)]
        public int Sort { get; set; }
    }
    [Table("md_Temperature")]
    public partial class md_Temperature
    {
        public int Id { get; set; }
        public decimal TemperatureOutside { get; set; }
        public DateTime LastUpdate { get; set; }
    }


}