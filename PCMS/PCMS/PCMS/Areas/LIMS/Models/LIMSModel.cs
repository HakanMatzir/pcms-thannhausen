﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace PCMS.Areas.LIMS.Models
{
    public class LimsContext : DbContext
    {
        public LimsContext()
            : base("DefaultConnection")
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<lims_gradingCurv_GradingCurv>()
            //    .HasMany(up => up.Materials)
            //    .WithMany(course => course.GradingCurvs)
            //    .Map(mc =>
            //    {
            //        mc.ToTable("lims_gradingCurvMaterial");
            //        mc.MapLeftKey("MaterialId");
            //        mc.MapRightKey("GradingCurvId");
            //    }
            //);

            //base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Sieve.Models.md_sieve_SieveSet>()
                .HasMany(up => up.Sieves)
                .WithMany(course => course.SieveSet)
                .Map(mc =>
                {
                    mc.ToTable("md_sieve_SieveSet_Sieves");
                    mc.MapLeftKey("SieveSetId");
                    mc.MapRightKey("SievesId");
                }
            );
            modelBuilder.Entity<lims_concrete_Test>()
                .HasMany(up => up.Lims_concrete_ProbWets)
                //.WithRequired(s => s.Lims_aggregate_TestD)
                .WithRequired()
                .HasForeignKey(s => s.TestId);

            modelBuilder.Entity<lims_concrete_ProbWet>()
               .HasMany(up => up.Lims_concrete_ProbWet_Consistency)
               //.WithRequired(s => s.Lims_aggregate_TestD)
               .WithRequired()
               .HasForeignKey(s => s.ProbWetId);
            modelBuilder.Entity<lims_concrete_ProbWet>().Property(a => a.Dry).HasPrecision(18, 3);
            modelBuilder.Entity<lims_concrete_ProbWet>().Property(a => a.Volume).HasPrecision(18, 3);
            modelBuilder.Entity<lims_concrete_ProbWet>().Property(a => a.Tara).HasPrecision(18, 3);
            modelBuilder.Entity<lims_concrete_ProbWet>().Property(a => a.Wet).HasPrecision(18, 3);

            modelBuilder.Entity<lims_concrete_Test>()
               .HasMany(up => up.Lims_concrete_ProbDrys)
               //.WithRequired(s => s.Lims_aggregate_TestD)
               .WithRequired()
               .HasForeignKey(s => s.TestId);
            modelBuilder.Entity<lims_concrete_ProbDry>().Property(a => a.Volumen).HasPrecision(18, 3);
            modelBuilder.Entity<lims_concrete_ProbDry>().Property(a => a.Measures).HasPrecision(18, 3);


            base.OnModelCreating(modelBuilder);

        }

        public virtual DbSet<lims_aggregate_Test> Lims_aggregate_Test { get; set; }
        public virtual DbSet<lims_aggregate_TestType> Lims_aggregate_TestType { get; set; }
        public virtual DbSet<lims_aggregate_TestStateGroup> Lims_aggregate_TestStateGroup { get; set; }
        public virtual DbSet<lims_aggregate_ProbType> Lims_aggregate_ProbType { get; set; }
        public virtual DbSet<lims_aggregate_ProbPlaceDetailsGroup> Lims_aggregate_ProbPlaceDetailsGroup { get; set; }
        public virtual DbSet<lims_aggregate_TestNorm> Lims_aggregate_TestNorm { get; set; }
        public virtual DbSet<Sieve.Models.md_sieve_RuleGradingCurve> Md_sieve_RuleGradingCurve { get; set; }
        public virtual DbSet<lims_gradingCurvMaterial> Lims_gradingCurvMaterial { get; set; }
        public virtual DbSet<md_masterData_Config> Md_masterData_Config { get; set; }
        public virtual DbSet<lims_concrete_SelectMenu> Lims_concrete_SelectMenu { get; set; }
        public virtual DbSet<lims_morat_SelectMenu> Lims_morat_SelectMenu { get; set; }
        public virtual DbSet<lims_concrete_ProbWet_Consistency> Lims_concrete_ProbWet_Consistency { get; set; }

        public virtual DbSet<AuditLog> Auditlog { get; set; }


        public System.Data.Entity.DbSet<PCMS.Areas.Sieve.Models.md_sieve_SieveSet> Md_sieve_SieveSet { get; set; }
        public System.Data.Entity.DbSet<PCMS.Areas.LIMS.Models.lims_aggregate_ProbPlaceGroup> lims_aggregate_ProbPlaceGroup { get; set; }

        public System.Data.Entity.DbSet<md_masterData_Material> md_material_Material { get; set; }

        public System.Data.Entity.DbSet<PCMS.Areas.LIMS.Models.lims_gradingCurv_GradingCurv> lims_gradingCurv_GradingCurv { get; set; }

        public System.Data.Entity.DbSet<PCMS.Areas.LIMS.Models.lims_concrete_Test> Lims_concrete_Test { get; set; }
        public System.Data.Entity.DbSet<PCMS.Areas.LIMS.Models.lims_concrete_ProbWet> Lims_concrete_ProbWet { get; set; }
        public System.Data.Entity.DbSet<PCMS.Areas.LIMS.Models.lims_concrete_ProbDry> Lims_concrete_ProbDry { get; set; }



        public int SaveChanges(string userId)
        {
            // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
            foreach (var ent in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
            {
                // For each changed record, get the audit record entries and add them
                foreach (AuditLog x in Helper.AuditLogH.GetAuditRecordsForChange(ent, userId))
                {
                    this.Auditlog.Add(x);
                }
            }

            // Call the original SaveChanges(), which will save both the changes made and the audit records
            return base.SaveChanges();
        }

        public System.Data.Entity.DbSet<PCMS.Areas.LIMS.Models.lims_morat_Test> lims_morat_Test { get; set; }

        
    }

    [Table("lims_aggregate_Test")]
    public partial class lims_aggregate_Test
    {
        [Key]
        public long Id { get; set; }
        [DisplayName("Prüfungs-Nr")]
        public string TestNumber { get; set; }
        [DisplayName("Erfassung am")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? RegistraionDate { get; set; }
        [DisplayName("Prüfung am")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? TestDate { get; set; }
        [DisplayName("Art der Prüfung")]
        public long? TestTypeId { get; set; }
        [DisplayName("Auftrag")]
        public string OrderNumber { get; set; }
        [DisplayName("Auftraggeber")]
        public long? OrderClient { get; set; }
        [DisplayName("Material")]
        public long MaterialId { get; set; }
        [DisplayName("Status")]
        public long? StateId { get; set; }
        [DisplayName("Ausreißer")]
        public bool? Outliner { get; set; }
        [DisplayName("Beurteilung")]
        public string Rating { get; set; }
        [DisplayName("Liefermenge")]
        public decimal? DeliverQuantity { get; set; }
        [DisplayName("Lieferschein-Nr")]
        public string DeliveryNoteNo { get; set; }
        [DisplayName("Lieferant")]
        public long? SupplierId { get; set; }
        [DisplayName("Menge")]
        public decimal? Quantity { get; set; }
        [DisplayName("Probenahme")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTH:mm:ss}", ApplyFormatInEditMode = true)]
        public  DateTime? ProbTakingDatetime { get; set; }
        [DisplayName("Probeneingang")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTH:mm:ss}", ApplyFormatInEditMode = true)]
        public  DateTime? ProbEnteringDatetime { get; set; }
        [DisplayName("Art der Probe")]
        public long? ProbTypeId { get; set; }
        [DisplayName("Entnahmeort")]
        public long? ProbPlaceId { get; set; }
        [DisplayName("Stelle")]
        public long? ProbPlaceDetail { get; set; }
        [DisplayName("Masse feucht")]
        public decimal? MaterialWet { get; set; }
        [DisplayName("Masse trocken")]
        public decimal? MaterialDry { get; set; }
        [DisplayName("Wassergehalt")]
        public decimal? WaterContent { get; set; }
        [DisplayName("Eigenfeuchte")]
        public decimal? IntrinsicMoisture { get; set; }
        [DisplayName("Masse feucht")]
        public decimal? CoreMaterialWet { get; set; }
        [DisplayName("Masse trocken")]
        public decimal? CoreMaterialDry { get; set; }
        [DisplayName("Kernfeuchte")]
        public decimal? CoreMoisture { get; set; }
        [DisplayName("Art der Siebung")]
        public string KindOfSieving { get; set; }
        [DisplayName("Nach Norm")]
        public long NormId { get; set; }
        [DisplayName("Siebsatz")]
        public long? SievSetId { get; set; }
        [DisplayName("Erfassungsart")]
        public long InputType { get; set; }
        [DisplayName("Versuche")]
        public string TryCount { get; set; }
        public decimal M1 { get; set; }
        public decimal S0_063 { get; set; }
        public decimal S0_125 { get; set; }
        public decimal S0_25 { get; set; }
        public decimal S0_5 { get; set; }
        public decimal S1 { get; set; }
        public decimal S1_4 { get; set; }
        public decimal S2 { get; set; }
        public decimal S2_8 { get; set; }
        public decimal S4 { get; set; }
        public decimal S5_6 { get; set; }
        public decimal S8 { get; set; }
        public decimal S11_2 { get; set; }
        public decimal S16 { get; set; }
        public decimal S22_4 { get; set; }
        public decimal S31_5 { get; set; }
        public decimal S45 { get; set; }
        public decimal S63 { get; set; }
        public decimal KValue { get; set; }
        public decimal DValue { get; set; }
        public long? FactoryId { get; set; }
        [DisplayName("Plattigkeit FI")]
        public decimal? Flakiness { get; set; }
        public bool? FlakinessCheck { get; set; }
        [DisplayName("Konform SI")]
        public decimal? Conform { get; set; }
        public bool? ConformCheck { get; set; }
        [DisplayName("Wasserauf.")]
        public decimal? Water { get; set; }
        public bool? WaterCheck { get; set; }
        [DisplayName("Feinanteile.")]
        public decimal? FineShare { get; set; }
        public bool? FineShareCheck { get; set; }
        [DisplayName("Beurt.v.Feinant.(MB/SE")]
        public decimal? RatingFineShare { get; set; }
        public bool? RatingFineShareCheck { get; set; }
        [DisplayName("Korndichte")]
        public decimal? GraingrossDensity { get; set; }
        [DisplayName("Prüfer")]
        public long? ProbUserId { get; set; }
        [DisplayName("Prüfer")]
        public string ProbUserName { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }


        [NotMapped]
        public decimal helper { get; set; }

        [DisplayName("Probenehmer")]
        public long? UserId { get; set; }
        [ForeignKey("TestTypeId")]
        [DisplayName("Art der Prüfung")]
        public virtual lims_aggregate_TestType Lims_aggregate_TestType { get; set; }
        [ForeignKey("MaterialId")]
        //public virtual PCMS.Areas.Material.Models.md_material_Material Md_material_Material { get; set; }
        public virtual md_masterData_Material Md_material_Material { get; set; }
        [ForeignKey("StateId")]
        public virtual lims_aggregate_TestStateGroup Lims_aggregate_TestStateGroup { get; set; }
        [ForeignKey("ProbPlaceId")]
        public virtual lims_aggregate_ProbPlaceGroup Lims_aggregate_ProbPlaceGroup { get; set; }
        [ForeignKey("ProbTypeId")]
        public virtual lims_aggregate_ProbType Lims_aggregate_ProbType { get; set; }
        [ForeignKey("ProbPlaceDetail")]
        [DisplayName("Stelle")]
        public virtual lims_aggregate_ProbPlaceDetailsGroup Lims_aggregate_ProbPlaceDetailsGroup { get; set; }
        [ForeignKey("NormId")]
        [DisplayName("Nach Norm")]
        public virtual lims_aggregate_TestNorm Lims_aggregate_TestNorm { get; set; }

        

        public List<KeyValuePair<string,decimal>>  kumulativ {
            get {
                return getKumulativList().ToList();
                }
            
        }

        public Dictionary<string,decimal> getKumulativList()
        {

            helper = 0;
            Dictionary<string, decimal> returnList = new Dictionary<string, decimal>()
            {
                
                { "0,063", calcKumu(this.S0_063) },
                { "0,125", calcKumu(this.S0_125) },
                { "0,25", calcKumu(this.S0_25) },
                { "0,5", calcKumu(this.S0_5) },
                { "1", calcKumu(this.S1) },
                { "1,4", calcKumu(this.S1_4) },
                { "2", calcKumu(this.S2) },
                { "2,8", calcKumu(this.S2_8) },
                { "4", calcKumu(this.S4) },
                { "5,6", calcKumu(this.S5_6) },
                { "8", calcKumu(this.S8) },
                { "11,2", calcKumu(this.S11_2) },
                { "16", calcKumu(this.S16) },
                { "22,4", calcKumu(this.S22_4) },
                { "31,5", calcKumu(this.S31_5) },
                { "45", calcKumu(this.S45) },
                { "63", calcKumu(this.S63) },

            };

            
            return returnList; 
        }
        public decimal calcKumu(decimal va)
        {
            if(this.InputType == 0)
            {
                return 100 - ( va / (this.M1 / 100));
            }            
            else if(this.InputType == 1)
            {
                helper += va;
                return 100 - ((this.M1 - va) / (this.M1 / 100));
            }
            else if (this.InputType == 2)
            {
                return va;
            }
            else 
            {
                return 100 - va;
            }
        }
    
    }
    [Table("lims_aggregate_TestNorm")]
    public partial class lims_aggregate_TestNorm
    {
        [Key]
        public long Id { get; set; }
        [DisplayName("nach Norm")]
        public string Description { get; set; }
    }

    [Table("lims_aggregate_TestType")]
    public partial class lims_aggregate_TestType
    {
        [Key]
        public long Id { get; set; }
        [DisplayName("Art der Prüfung")]
        public string Description { get; set; }
    }

    [Table("lims_aggregate_TestStateGroup")]
    public partial class lims_aggregate_TestStateGroup
    {
        [Key]
        public long Id { get; set; }
        [DisplayName("Status")]
        public string Description { get; set; }
    }

    [Table("lims_aggregate_ProbType")]
    public partial class lims_aggregate_ProbType
    {
        [Key]
        public long Id { get; set; }
        [DisplayName("Art der Probe")]
        public string Description { get; set; }
    }

    [Table("lims_aggregate_ProbPlaceGroup")]
    public partial class lims_aggregate_ProbPlaceGroup
    {
        [Key]
        public long Id { get; set; }
        [DisplayName("Entnahmeort")]
        public string Description { get; set; }
    }

    [Table("lims_aggregate_ProbPlaceDetailsGroup")]
    public partial class lims_aggregate_ProbPlaceDetailsGroup
    {
        [Key]
        public long Id { get; set; }
        [DisplayName("Stelle")]
        public string Description { get; set; }
    }
    [Table("lims_gradingCurv_GradingCurv")]
    public partial class lims_gradingCurv_GradingCurv
    {
        public lims_gradingCurv_GradingCurv() {
            Lims_gradingCurvMaterial = new List<lims_gradingCurvMaterial>();
        }
        [Key]
        public long Id { get; set; }
        [DisplayName("Rezeptgruppe")]
        public long ResciveGroupId { get; set; }
        [DisplayName("Sieblinie")]
        public string SieveLineNumber { get; set; }
        [DisplayName("Bezeichnung")]
        public string Description { get; set; }
        [DisplayName("Regelsieblinie")]
        public long SieveRangeId { get; set; }
        [DisplayName("Preis")]
        public decimal Price { get; set; }
        public decimal S0_063 { get; set; }
        public decimal S0_125 { get; set; }
        public decimal S0_25 { get; set; }
        public decimal S0_5 { get; set; }
        public decimal S1 { get; set; }
        public decimal S1_4 { get; set; }
        public decimal S2 { get; set; }
        public decimal S2_8 { get; set; }
        public decimal S4 { get; set; }
        public decimal S5_6 { get; set; }
        public decimal S8 { get; set; }
        public decimal S11_2 { get; set; }
        public decimal S16 { get; set; }
        public decimal S22_4 { get; set; }
        public decimal S31_5 { get; set; }
        public decimal S45 { get; set; }
        public decimal S63 { get; set; }
        public decimal KValue { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }


        [ForeignKey("SieveRangeId")]
        [DisplayName("Nach Norm")]
        public virtual Sieve.Models.md_sieve_RuleGradingCurve Md_sieve_RuleGradingCurve { get; set; }
        //[InverseProperty("lims_gradingCurv_GradingCurv")]
        //public virtual ICollection<lims_gradingCurvMaterial> CradingMaterial { get; set; }
        [ScriptIgnore]
        public virtual ICollection<lims_gradingCurvMaterial> Lims_gradingCurvMaterial { get; set; }
    }
    [Table("md_material_Material")]
    public partial class md_masterData_Material
    {
        public md_masterData_Material()
        {
            Lims_gradingCurvMaterial = new List<lims_gradingCurvMaterial>();
        }
        [Key]
        public long? Id { get; set; }
        public long? MaterialGroupId { get; set; }
        public long? MaterialTypeId { get; set; }
        public decimal? Price { get; set; }
        public long? DelivererId { get; set; }
        public long? FacilitiesId { get; set; }

        [DisplayName("Artikelnummer")]
        public string ArticleNumber { get; set; }
        [DisplayName("Materialnummer")]
        public string MaterialNumber { get; set; }
        [DisplayName("Bezeichnung")]
        public string Name { get; set; }
        [DisplayName("Kurz-Bezeichnung")]
        public string ShortName { get; set; }
        [DisplayName("Dichte")]
        public decimal? Density { get; set; }
        [DisplayName("Einheit")]
        public string Unit { get; set; }
        [DisplayName("Bemerkung 1")]
        public string Comment1 { get; set; }
        [DisplayName("Bemerkung 2")]
        public string Comment2 { get; set; }
        [DisplayName("Bemerkung 3")]
        public string Comment3 { get; set; }
        [DisplayName("Bemerkung 4")]
        public string Comment4 { get; set; }
        [DisplayName("Bemerkung 5")]
        public string Comment5 { get; set; }
        [DisplayName("Chipnummer")]
        public string RFID { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        // Detail FK_Tabellen
        public long? AdditivesId { get; set; }
        public long? AdmixturesId { get; set; }
        public long? AggregateId { get; set; }
        public long? BinderId { get; set; }
        public long? SortId { get; set; }
        [ForeignKey("DelivererId")]
        public virtual md_masterData_Deliverer Md_masterData_Deliverer { get; set; }
        [ForeignKey("AdditivesId")]
        public virtual MasterData.Models.md_material_AdditivesDetail Md_material_AdditivesDetail { get; set; }
        //[InverseProperty("md_masterData_Material")]
        //public virtual ICollection<lims_gradingCurvMaterial> CradingMaterial { get; set; }
        [ScriptIgnore]
        public ICollection<lims_gradingCurvMaterial> Lims_gradingCurvMaterial { get; set; }


    }
    
    [Table("md_Deliverer")]
    public partial class md_masterData_Deliverer
    {
        [Key]
        public long? Id { get; set; }
        public long? AddressId { get; set; }
        public long? ContactId { get; set; }
        [DisplayName("Lieferant")]
        public string Name { get; set; }
        [DisplayName("Lieferantennummer")]
        public string DelivererId { get; set; }
        [DisplayName("Lieferantennummer EDV")]
        public string DelivererIdEDV { get; set; }
        [DisplayName("Aktiv")]
        public bool IsActive { get; set; }
        [DisplayName("Gelöscht")]
        public bool IsDeleted { get; set; }

        [ForeignKey("AddressId")]
        public virtual MasterData.Models.md_masterData_Address Md_masterData_Address { get; set; }

        //[ForeignKey("ContactId")]
        //public virtual MasterData.Models.md_masterData_Contact Md_masterData_Contact { get; set; }
    }
    [Table("lims_gradingCurvMaterial")]
    public partial class lims_gradingCurvMaterial
    {
        [Key, Column(Order = 0) ]
        public long GradingCurvId { get; set; }
        [Key, Column(Order = 1)]
        public long MaterialId { get; set; }
        public decimal Percent { get; set; }
        public int Sort { get; set; }
        [ForeignKey("GradingCurvId")]
        // [InverseProperty("CradingMaterial")]
        [ScriptIgnore]
        public virtual lims_gradingCurv_GradingCurv Lims_gradingCurv_GradingCurv { get; set; }
        [ForeignKey("MaterialId")]
        // [InverseProperty("CradingMaterial")]
        [ScriptIgnore]
        public virtual md_masterData_Material Md_masterData_Material { get; set; }
    }
    [Table("md_Config")]
    public partial class md_masterData_Config
    {
        [Key]
        public int Id { get; set; }
        public int CompanyTypeId { get; set; }
        public string PrintName { get; set; }
    }
    [Table("auditlog")]
    public partial class AuditLog
    {
        public Guid auditlogid { get; set; }
        public string userid { get; set; }
        public DateTime eventdateutc { get; set; }
        public string eventtype { get; set; }
        public string tablename { get; set; }
        public string recordid { get; set; }
        public string columnname { get; set; }
        public string originalvalue { get; set; }
        public string newvalue { get; set; }
        //[ForeignKey("userid")]
        //public virtual users Users { get; set; }
    }

    [Table("lims_concrete_Test")]
    public partial class lims_concrete_Test
    {
        public lims_concrete_Test()
        {
            Lims_concrete_ProbWets = new List<lims_concrete_ProbWet>();
            Lims_concrete_ProbDrys = new List<lims_concrete_ProbDry>();
            //Lims_concrete_ProbWets.Add(new lims_concrete_ProbWet(){
            //    DateOfManufacture = DateTime.Now
            //});

        }
        [Key]
        public long Id { get; set; }
        public string Identity { get; set; }
        [DisplayName("Betonprüfung-Nr")]
        public string TestNumber { get; set; }
        [DisplayName("Erfassung am")]
        public DateTime TestDate { get; set; }
        [DisplayName("Prüfungsart")]
        public long TestType { get; set; }
        [DisplayName("Auftrag-Nr")]
        public string OrderNumber { get; set; }
        [DisplayName("Auftraggeber")]
        public long OrderClient { get; set; }        
        public long SortId { get; set; }
        [DisplayName("Sorte")]
        public string SortName { get; set; }        
        public long RecipeId { get; set; }
        [DisplayName("Rezept")]
        public string RecipeName { get; set; }
        [DisplayName("Status")]
        public long Stauts { get; set; }
        [DisplayName("Prüfprofil")]
        public long Probprofil { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public long Rev { get; set; }
        public long FactoryId { get; set; }

        public virtual ICollection<lims_concrete_ProbWet> Lims_concrete_ProbWets {get; set; }
        public virtual ICollection<lims_concrete_ProbDry> Lims_concrete_ProbDrys { get; set; }
    }
    

    [Table("lims_concrete_ProbWet")]
    public partial class lims_concrete_ProbWet
    {
        public lims_concrete_ProbWet()
        {
            Lims_concrete_ProbWet_Consistency = new List<lims_concrete_ProbWet_Consistency>();
        }
        [Key]
        public long Id { get; set; }
        public string Identity { get; set; }
        //[Required]
        public long TestId { get; set; }
        [Required]
        [DisplayName("Probe-Nr")]
        public string ProbNumber { get; set; }
        [DisplayName("Lieferschein-Nr")]
        public string DeliveryNoteNo { get; set; }
        [DisplayName("Entnahmeort")]
        public long? ProbPlaceId { get; set; }
        [DisplayName("Probenehmer")]
        public string ProbUserName { get; set; }
        public string ProbUserId { get; set; }
        [DisplayName("Herstelldatum")]
        public DateTime DateOfManufacture { get; set; }
        [DisplayName("Luftgehalt")]
        public long? AirContentId { get; set; }
        [DisplayName("Frischbetondichte")]
        public long? WetConcreteDensity { get; set; }
        [DisplayName("Konsistenz")]
        public long? ConsistencyId { get; set; }
        [DisplayName("Wassergehalt/ w/z-Wert")]
        public long? WaterContentId { get; set; }
        [DisplayName("Zementgeh./Ergibigkeit")]
        public long? ContentOfCement { get; set; }
        [DisplayName("Auswaschversuch")]
        public long? WashOutTry { get; set; }
        [DisplayName("Kunde")]
        public string CustomerName { get; set; }
        [DisplayName("KundenNr")]
        public long? CustomerId { get; set; }
        [DisplayName("Baustelle")]
        public string ConstructionSiteName { get; set; }
        [DisplayName("BaustelleNr.")]
        public long? ConstructionSiteId { get; set; }
        [DisplayName("Bauteil")]
        public long? ComponentId { get; set; }
        [DisplayName("Lufttemperatur[°C]")]
        public decimal AirTemp { get; set; }
        [DisplayName("Betontemperatur[°C]")]
        public decimal ConcreteTemp { get; set; }
        [DisplayName("Luftgehalt[%]")]
        public decimal AirContent { get; set; }
        [DisplayName("Verdichtungsart")]
        public long? CompressionModeId { get; set; }
        [DisplayName("Einbringung")]
        public long? ContributionId { get; set; }
        [DisplayName("Prüfer")]
        public string ProbUserId2 { get; set; }
        [DisplayName("Witterung")]
        public long? Weather { get; set; }
        [DisplayName("Beton+Form[kg]")]
        public decimal ConcreteShape { get; set; }
        [DisplayName("Formgewicht[kg]")]
        public decimal ShapeWeight { get; set; }
        [DisplayName("Betongewicht[kg]")]
        public decimal ConcreteWeight { get; set; }
        [DisplayName("Volumen[dm³]")]
        [DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal Volume { get; set; }
        [DisplayName("Rohdichte[kg/m³]")]
        public int Density { get; set; }
        [DisplayName("Tara[kg]")]
        [DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal Tara { get; set; }
        [DisplayName("feucht[kg]")]
        [DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal Wet { get; set; }
        [DisplayName("trocken[kg]")]
        [DataType("decimal(18 ,3)")]
        [DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal Dry { get; set; }
        [DisplayName("Kernfeuchte[kg/m³]")]
        public int CoreMoisture { get; set; }
        [DisplayName("wirk. Wasser-Gehalt[kg/m³]")]
        public int RealWaterContent { get; set; }
        [DisplayName("Zement[kg/m³]")]
        public int Cement { get; set; }
        [DisplayName("Anr. Bindemittel")]
        public int OtherBinders { get; set; }
        [DisplayName("w/z-Wert")]
        public decimal WZValue { get; set; }
        [DisplayName("w/z(eq)")]
        public decimal WZEQValue { get; set; }
        [DisplayName("Größkorn")]
        public long? MaximumGrainSizeId { get; set; }
        [DisplayName("Gesteinskörnungsklasse")]
        public long? AggregateClass { get; set; }
        [DisplayName("Anz. Körnung")]
        public long? GritsNumber { get; set; }
        [DisplayName("SL bis 4mm i W oh AB")]
        public bool? SLTo4mm { get; set; }
        [DisplayName("Lieferschein-Nr")]
        public string IsDeliveryNoteNo { get; set; }
        [DisplayName("Herstelldatum")]
        public DateTime IsProducionDate { get; set; }
        [DisplayName("Liefermenge")]
        public decimal IsDeliveryQuantity { get; set; }
        [DisplayName("Mischungsgewicht")]
        public int IsMixtureWeight { get; set; }
        [DisplayName("Zement")]
        public int IsCementKg { get; set; }
        public int IsCementKgM { get; set; }
        [DisplayName("Zusatzstoff")]
        public int IsAdditiveKg { get; set; }
        public int IsAdditveKgM { get; set; }
        [DisplayName("Anr. Bindemittel")]
        public int IsOtherBindersKg { get; set; }
        public int IsOtherBindersKgM { get; set; }
        [DisplayName("Zusatzmittel 1")]
        public decimal IsAdmixture1Kg { get; set; }
        public decimal IsAdmixture1KgM { get; set; }
        [DisplayName("Zusatzmittel 2")]
        public decimal IsAdmixture2Kg { get; set; }
        public decimal IsAdmixture2KgM { get; set; }
        [DisplayName("Gesteinsk. tr. ges.")]
        public int IsAggreagateDryKg { get; set; }
        public int IsAggreagateDryKgM { get; set; }
        [DisplayName("Gesteinsk. Wasser")]
        public int IsAggreagateWaterKg { get; set; }
        public int IsAggreagateWaterKgM { get; set; }
        [DisplayName("Gesteinsk. feucht")]
        public int IsAggreagateWetKg { get; set; }
        public int IsAggreagateWetKgM { get; set; }
        [DisplayName("Zugabewasser")]
        public int IsAdditionWaterKg { get; set; }
        public int IsAdditionWaterKgM { get; set; }
        [DisplayName("Wassergehalt Chargenp.")]
        public int IsWaterChargeKg { get; set; }
        public int IsWaterChargeKgM { get; set; }
        [DisplayName("Mischzeit")]
        public int IsMixingTime { get; set; }
        [DisplayName("Ergibigkeit")]
        public decimal IsProductiveness1 { get; set; }
        public decimal IsProductiveness2 { get; set; }
        [DisplayName("Rohdichte")]
        public int IsRawDensity { get; set; }
        [DisplayName("Zusatzstoff tr. 1")]
        public int IsAdditive1Kg { get; set; }
        public int IsAdditive1KgM { get; set; }
        public decimal IsAdditive1K { get; set; }
        public decimal IsAdditive1Max { get; set; }
        [DisplayName("Zusatzstoff tr. 2")]
        public int IsAdditive2Kg { get; set; }
        public int IsAdditive2KgM { get; set; }
        public decimal IsAdditive2K { get; set; }
        public decimal IsAdditive2Max { get; set; }
        [DisplayName("Zusatzstoff tr. 3")]
        public int IsAdditive3Kg { get; set; }
        public int IsAdditive3KgM { get; set; }
        public decimal IsAdditive3K { get; set; }
        public decimal IsAdditive3Max { get; set; }
        [DisplayName("Zusatzstoff Wasser")]
        public int IsAdditiveWaterKg { get; set; }
        public int IsAdditiveWaterKgM { get; set; }
        [DisplayName("Gesteinskörnung 1")]
        public int IsAggregat1Kg { get; set; }
        public int IsAggregat1KgM { get; set; }
        public decimal IsAggregat1Wet { get; set; }
        [DisplayName("Gesteinskörnung 2")]
        public int IsAggregat2Kg { get; set; }
        public int IsAggregat2KgM { get; set; }
        public decimal IsAggregat2Wet { get; set; }
        [DisplayName("Gesteinskörnung 3")]
        public int IsAggregat3Kg { get; set; }
        public int IsAggregat3KgM { get; set; }
        public decimal IsAggregat3Wet { get; set; }
        [DisplayName("Gesteinskörnung 4")]
        public int IsAggregat4Kg { get; set; }
        public int IsAggregat4KgM { get; set; }
        public decimal IsAggregat4Wet { get; set; }
        [DisplayName("Gesteinskörnung 5")]
        public int IsAggregat5Kg { get; set; }
        public int IsAggregat5KgM { get; set; }
        public decimal IsAggregat5Wet { get; set; }
        //[ForeignKey("TestId")]
        //public virtual lims_aggregate_Test Lims_aggregate_TestD  { get; set;}

        public virtual ICollection<lims_concrete_ProbWet_Consistency> Lims_concrete_ProbWet_Consistency { get; set; }

    }
    [Table("lims_concrete_ProbWet_Consistency ")]
    public partial class lims_concrete_ProbWet_Consistency
    {
        [Key]
        public long Id { get; set; }
        public string Identity { get; set; }
        //[Required]
        public long ProbWetId { get; set; }
        public decimal? ConsistencyValue { get; set; }
        public string Measure { get; set; }
        public string ConsistencyType { get; set; }
        public int? Time { get; set; }
        public bool Fm { get; set; }
        public decimal Lp { get; set; }
    }
    [Table("lims_concrete_ProbDry")]
    public partial class lims_concrete_ProbDry
    {
        public long Id { get; set; }
        public string Identity { get; set; }
        public long TestId { get; set; }
        [DisplayName("Probekörper-Nr")]
        public string TestSpecimenNo { get; set; }
        [DisplayName("Probe-Nr")]
        public string ProbNumber { get; set; }
        [DisplayName("Prüfungsart")]
        public string ProbType { get; set; }
        [DisplayName("Form")]
        public string Sharp { get; set; }
        [DisplayName("Prüfalter")]
        public string TestAgeValue { get; set; }
        public string TestAgeType { get; set; }
        [DisplayName("Prüfdatum")]
        public DateTime TestDateTime { get; set; }
        [DisplayName("Bruchlast [kN]")]
        public decimal? BreakingLoad { get; set; }
        [DisplayName("Umr.-faktor")]
        public decimal? ConversionFactor { get; set; }
        [DisplayName("Druckfläche [mm²]")]
        public int? PressureSurface { get; set; }
        [DisplayName("Bruchtyp")]
        public string BreakingType { get; set; }
        [DisplayName("Festigkeit [N/mm²]")]
        public decimal? Strength { get; set; }
        [DisplayName("Festigkeit* [N/mm²]")]
        public decimal? Strength2 { get; set; }
        [DisplayName("Rückprallh.")]
        public decimal? Scleroscope { get; set; }
        [DisplayName("Prüfnorm")]
        public string TestStandard { get; set; }
        [DisplayName("Lagerung")]
        public string Storage { get; set; }
        [DisplayName("ungewöhnlich")]
        public bool Unusual { get; set; }
        [DisplayName("Stützweite [mm]")]
        public decimal? Span { get; set; }
        [DisplayName("Anz. Lasten")]
        public int? Charges { get; set; }
        [DisplayName("Bruch (Lage)")]
        public string BreakPosition { get; set; }
        [DisplayName("Bruchstellenoberfläche")]
        public string BreakTop { get; set; }
        [DisplayName("Dauer")]
        public int? Duration { get; set; }
        [DisplayName("MW")]
        public decimal? MW { get; set; }
        [DisplayName("Max")]
        public decimal? Max { get; set; }
        [DisplayName("Wasserinwirkung")]
        public string WaterEffect { get; set; }
        [DisplayName("zur Einfüllung")]
        public string FillingDirection { get; set; }
        public int? WaterMp0 { get; set; }
        public int? WaterMp1 { get; set; }
        public int? WaterMp2 { get; set; }
        public int? WaterMp3 { get; set; }
        public int? WaterMp4 { get; set; }
        public int? WaterMp5 { get; set; }
        public int? WaterMp6 { get; set; }
        public int? WaterMp7 { get; set; }
        public int? WaterMp8 { get; set; }
        public int? WaterMp9 { get; set; }
        public int? WaterMp10 { get; set; }
        public int? WaterMp11 { get; set; }
        public int? WaterMp12 { get; set; }
        public int? WaterMp13 { get; set; }
        public int? WaterMp14 { get; set; }
        public int? WaterMp15 { get; set; }
        public int? WaterMp16 { get; set; }
        public int? WaterMp17 { get; set; }
        public int? WaterMp18 { get; set; }
        public int? WaterMp19 { get; set; }
        public int? WaterMp20 { get; set; }
        [DisplayName("Prüfung")]
        public string Testname { get; set; }
        [DisplayName("Wert")]
        public string TestValue { get; set; }
        [DisplayName("ME")]
        public string Unit { get; set; }
        [DisplayName("Prüfungstext")]
        public string TestText { get; set; }
        [DisplayName("Durchmesser")]
        public decimal? Diameter { get; set; }
        [DisplayName("Länge [mm]")]
        public decimal? Length { get; set; }
        [DisplayName("Breite [mm]")]
        public decimal? Width { get; set; }
        [DisplayName("Höhe [mm]")]
        public decimal? Height { get; set; }
        [DisplayName("Volumen [dm³]")]
        [DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal? Volumen { get; set; }
        [DisplayName("Masse [kg]")]
        [DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal? Measures { get; set; }
        [DisplayName("Rohdichte[kg/m³]")]
        public int? GrossDensity { get; set; }
        [DisplayName("Vol.-Bestimmung über")]
        public string VolDetemining { get; set; }
        [DisplayName("Prüfer")]
        public string ProbUserId { get; set; }
        [DisplayName("Probekörperbez.")]
        public string TestSpecimenDescription { get; set; }
        [DisplayName("Zust. bei Anlieferung")]
        public string Status { get; set; }
        [DisplayName("Feuchtezustand")]
        public string WetStatus { get; set; }
        [DisplayName("Vorbereitung")]
        public string Comparison { get; set; }
        [DisplayName("Abgleich")]
        public string Preperation { get; set; }
        [DisplayName("Konformität")]
        public bool Conformity { get; set; }
        [DisplayName("Produktionskontrolle")]
        public bool ProductionControl { get; set; }
        [DisplayName("Begründung")]
        public string Reason { get; set; }
        [DisplayName("Abweichung")]
        public string Deviation { get; set; }
    }


    [Table("lims_concrete_SelectMenu")]
    public partial class lims_concrete_SelectMenu
    {
        public long Id { get; set; }
        public long MenuId { get; set; }
        public string Description { get; set; }
    }

    [Table("lims_morat_Test")]
    public partial class lims_morat_Test
    {
        public long Id { get; set; }
        public string Identity { get; set; }
        [DisplayName("Produkt-Nr./Sorte")]
        public string MaterilaNumber { get; set; }
        [DisplayName("Festigkeitsklasse")]
        public string StrengthClass { get; set; }
        [DisplayName("Entnahmestelle")]
        public string ProbPlace { get; set; }
        [DisplayName("Empfänger/Kunde")]
        public string Customer { get; set; }
        [DisplayName("Baustelle")]
        public string ConstruktionSite { get; set; }
        [DisplayName("Lieferdatum")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DeliveryDate { get; set; }
        [DisplayName("Lieferschein-Nr.")]
        public string OrderNumber { get; set; }
        [DisplayName("0,125")]
        public decimal? Sieve0_125 { get; set; }
        [DisplayName("0,25")]
        public decimal? Sieve0_25 { get; set; }
        [DisplayName("0,5")]
        public decimal? Sieve0_5 { get; set; }
        [DisplayName("0,71")]
        public decimal? Sieve0_71 { get; set; }
        [DisplayName("1,0")]
        public decimal? Sieve1_0 { get; set; }
        [DisplayName("1,25")]
        public decimal? Sieve1_25 { get; set; }
        [DisplayName("1,6")]
        public decimal? Sieve1_6 { get; set; }
        [DisplayName("2,0")]
        public decimal? Sieve2_0 { get; set; }
        [DisplayName("4,0")]
        public decimal? Sieve4_0 { get; set; }
        [DisplayName("8,0")]
        public decimal? Sieve8_0 { get; set; }
        [DisplayName("Prüfungsdatum")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FreshProbDate { get; set; }
        [DisplayName("Wassergehalt [ltr./to]")]
        public decimal? FreshWatherContent { get; set; }
        [DisplayName("Ausbreitmaß [cm]")]
        public decimal? FreshSlump { get; set; }
        [DisplayName("Fließmaß [cm]")]
        public decimal? FreshSlumpFlow { get; set; }
        [DisplayName("Frischmörtelrohdichte [kg/ltr]")]
        public decimal? FreshMoratDensity { get; set; }
        [DisplayName("Luftgehalt [Vol-%]")]
        public decimal? FreshAirContent { get; set; }
        [DisplayName("Volumenzunahme 250ml Becher [mm]")]
        public decimal? FreshCup250 { get; set; }
        [DisplayName("Bemerkung")]
        public string FreshComment { get; set; }
        [DisplayName("Prüfungsdatum")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FixedPropDate { get; set; }
        [DisplayName("Trockenrohdichte Luft [kg/ltr]")]
        public decimal? FixedMoratDensity0 { get; set; }
        public decimal? FixedMoratDensity1 { get; set; }
        public decimal? FixedMoratDensity2 { get; set; }
        [DisplayName("Trockenrohdichte [kg/ltr]")]
        public decimal? FixedMoratDensityM { get; set; }
        [DisplayName("Trockenrohdichte Ofen [kg/ltr]")]
        public decimal? FixedMoratDensity24h105C { get; set; }
        [DisplayName("Biegezugfestigkeit [N/mm²]")]
        public decimal? FixedFlexuralStrength0 { get; set; }
        public decimal? FixedFlexuralStrength1 { get; set; }
        public decimal? FixedFlexuralStrength2 { get; set; }
        [DisplayName("Biegezugfestigkeit [N/mm²]")]
        public decimal? FixedFlexuralStrengthM { get; set; }
        [DisplayName("Druckfestigkeit [N/mm²]")]
        public decimal? FixedCompressiveStrength0 { get; set; }
        public decimal? FixedCompressiveStrength1 { get; set; }
        public decimal? FixedCompressiveStrength2 { get; set; }
        [DisplayName("Druckfestigkeit [N/mm²]")]
        public decimal? FixedCompressiveStrengthM { get; set; }
        [DisplayName("Ausgeführt von")]
        public string TesterName { get; set; }
        [DisplayName("Ausgeführt von")]
        public string TesterName2 { get; set; }
        [DisplayName("Ausgeführt von")]
        public string TesterName3 { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }
    [Table("lims_morat_SelectMenu")]
    public partial class lims_morat_SelectMenu
    {
        public long Id { get; set; }
        public long MenuId { get; set; }
        public string Description { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
    }

    public class LIMSModel
    {
    }
}

/* 2016_01_12
lims_morat_SelectMenu Add IsDeleted  IsActive */

//GO
//IF COL_LENGTH('lims_morat_SelectMenu', 'IsActive') IS NULL
//BEGIN
//    ALTER Table lims_morat_SelectMenu
//    ADD IsActive bit;
//	UPDATE lims_morat_SelectMenu SET IsActive = 1;
//END

//GO
//IF COL_LENGTH('lims_morat_SelectMenu', 'IsDeleted') IS NULL
//BEGIN
//    ALTER Table lims_morat_SelectMenu
//    ADD IsDeleted bit;
//	UPDATE lims_morat_SelectMenu SET IsDeleted = 0;
//END
