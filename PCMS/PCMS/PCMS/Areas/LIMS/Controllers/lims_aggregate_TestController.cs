﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.LIMS.Models;
using Microsoft.Reporting.WebForms;
using PagedList;
using PCMS.Helper;
using Newtonsoft.Json;

namespace PCMS.Areas.LIMS.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class lims_aggregate_TestController : Controller
    {
        private LimsContext db = new LimsContext();
        private MasterData.Models.MasterDataContext dbMaster = new MasterData.Models.MasterDataContext();

        // GET: LIMS/lims_aggregate_Test
        //public ActionResult Index()
        //{
        //    var lims_aggregate_Test = db.Lims_aggregate_Test.Include(l => l.Lims_aggregate_ProbPlaceDetailsGroup).Include(l => l.Lims_aggregate_ProbPlaceGroup).Include(l => l.Lims_aggregate_ProbType).Include(l => l.Lims_aggregate_TestStateGroup).Include(l => l.Lims_aggregate_TestType).Include(l => l.Md_material_Material);
        //    return View(lims_aggregate_Test.ToList());
        //}

        public ActionResult Index(PCMS.Models.PaginationModel pg)
        {

            var companytype = db.Md_masterData_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            //var lims_ag db.Lims_aggregate_Test.Include(l => l.Lims_aggregate_ProbPlaceDetailsGroup).Include(l => l.Lims_aggregate_ProbPlaceGroup).Include(l => l.Lims_aggregate_ProbType).Include(l => l.Lims_aggregate_TestStateGroup).Include(l => l.Lims_aggregate_TestType).Include(l => l.Md_material_Material);
            var model = pageModel(pg);
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        // GET: LIMS/lims_aggregate_Test/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lims_aggregate_Test lims_aggregate_Test = db.Lims_aggregate_Test.Find(id);
            if (lims_aggregate_Test == null)
            {
                return HttpNotFound();
            }
            return View(lims_aggregate_Test);
        }

        // GET: LIMS/lims_aggregate_Test/Create
        public ActionResult Create()
        {
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            ViewBag.ProbPlaceDetail = new SelectList(db.Lims_aggregate_ProbPlaceDetailsGroup, "Id", "Description");
            ViewBag.ProbPlaceId = new SelectList(db.lims_aggregate_ProbPlaceGroup, "Id", "Description");
            ViewBag.ProbTypeId = new SelectList(db.Lims_aggregate_ProbType, "Id", "Description");
            ViewBag.StateId = new SelectList(db.Lims_aggregate_TestStateGroup, "Id", "Description");
            ViewBag.TestTypeId = new SelectList(db.Lims_aggregate_TestType, "Id", "Description");
            if(ViewBag.CompanyType == 0)
                ViewBag.MaterialId = new SelectList(db.md_material_Material.OrderBy(m => m.Name).Where(m => m.IsDeleted != true && (m.MaterialGroupId == 1 )) , "Id", "Name");
            else
                ViewBag.MaterialId = new SelectList(db.md_material_Material.OrderBy(m => m.Name).Where(m => m.IsDeleted != true), "Id", "Name");
            ViewBag.NormId = new SelectList(db.Lims_aggregate_TestNorm, "Id","Description");
            ViewBag.SievSetId = new SelectList(db.Md_sieve_SieveSet, "Id", "Description");
            var KindOfSieving = new List<SelectListItem>();
            KindOfSieving.Add(new SelectListItem { Text = "trocken", Value = "0" });
            KindOfSieving.Add(new SelectListItem { Text = "nass", Value = "1" });
            ViewBag.KindOfSieving = KindOfSieving;
            //var tryCpount = new List<SelectListItem>();
            //tryCpount.Add(new SelectListItem {Text="ein",Value="0",Selected=true });
            //tryCpount.Add(new SelectListItem { Text = "zwei", Value = "1" });
            //ViewBag.TryCount = tryCpount;
            var InputType = new List<SelectListItem>();
            InputType.Add(new SelectListItem { Text = "kumulativ", Value = "0" });
            InputType.Add(new SelectListItem { Text = "tariert", Value = "1" });
            InputType.Add(new SelectListItem { Text = "Durchgangsprozent", Value = "2" });
            InputType.Add(new SelectListItem { Text = "Rückstandsprozent", Value = "3" });
            ViewBag.InputType = InputType;
            ViewBag.ProbUserId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Id", "Description");
            ViewBag.UserId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Id", "Description");            
            return View();
        }

        // POST: LIMS/lims_aggregate_Test/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id, TestNumber, RegistraionDate, TestDate, TestTypeId, OrderNumber, OrderClient, MaterialId, StateId, Outliner, Rating, DeliverQuantity, DeliveryNoteNo, SupplierId, Quantity, ProbTakingDatetime, ProbEnteringDatetime, ProbTypeId, ProbPlaceId, ProbPlaceDetail, MaterialWet, MaterialDry, WaterContent, IntrinsicMoisture, CoreMaterialWet, CoreMaterialDry, CoreMoisture, KindOfSieving, NormId, SievSetId, InputType, TryCount, M1, S0_063, S0_125, S0_25, S0_5, S1, S1_4, S2, S2_8, S4, S5_6, S8, S11_2, S16, S22_4, S31_5, S45, S63, KValue, DValue,Flakiness,FlakinessCheck,Conform,ConformCheck,Water,WaterCheck,FineShare,FineShareCheck,RatingFineShare,RatingFineShareCheck,GraingrossDensity,ProbUserId, UserId")] lims_aggregate_Test lims_aggregate_Test)
        {
            if (ModelState.IsValid)
            {
                lims_aggregate_Test.IsDeleted = false;
                lims_aggregate_Test.IsActive = true;
                db.Lims_aggregate_Test.Add(lims_aggregate_Test);
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }

            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();

            ViewBag.ProbPlaceDetail = new SelectList(db.Lims_aggregate_ProbPlaceDetailsGroup, "Id", "Description", lims_aggregate_Test.ProbPlaceDetail);
            ViewBag.ProbPlaceId = new SelectList(db.lims_aggregate_ProbPlaceGroup, "Id", "Description", lims_aggregate_Test.ProbPlaceId);
            ViewBag.ProbTypeId = new SelectList(db.Lims_aggregate_ProbType, "Id", "Description", lims_aggregate_Test.ProbTypeId);
            ViewBag.StateId = new SelectList(db.Lims_aggregate_TestStateGroup, "Id", "Description", lims_aggregate_Test.StateId);
            ViewBag.TestTypeId = new SelectList(db.Lims_aggregate_TestType, "Id", "Description", lims_aggregate_Test.TestTypeId);
            if (ViewBag.CompanyType == 0)
                ViewBag.MaterialId = new SelectList(db.md_material_Material.OrderBy(m => m.Name).Where(m => m.IsDeleted != true && (m.MaterialGroupId == 1 )), "Id", "Name", lims_aggregate_Test.MaterialId);
            else
                ViewBag.MaterialId = new SelectList(db.md_material_Material.OrderBy(m => m.Name).Where(m => m.IsDeleted != true), "Id", "Name", lims_aggregate_Test.MaterialId);
            ViewBag.NormId = new SelectList(db.Lims_aggregate_TestNorm, "Id", "Description",lims_aggregate_Test.NormId);
            ViewBag.SievSetId = new SelectList(db.Md_sieve_SieveSet, "Id", "Description",lims_aggregate_Test.SievSetId);
            var KindOfSieving = new List<SelectListItem>();
            KindOfSieving.Add(new SelectListItem { Text = "trocken", Value = "0" });
            KindOfSieving.Add(new SelectListItem { Text = "nass", Value = "1" });
            KindOfSieving.Find(a => a.Value == lims_aggregate_Test.KindOfSieving).Selected = true;
            ViewBag.KindOfSieving = KindOfSieving;
            //var tryCpount = new List<SelectListItem>();
            //tryCpount.Add(new SelectListItem { Text = "ein", Value = "0", Selected = true });
            //tryCpount.Add(new SelectListItem { Text = "zwei", Value = "1" });
            //tryCpount.Find(a => a.Value == lims_aggregate_Test.TryCount).Selected = true;
            //ViewBag.TryCount = tryCpount;
            var InputType = new List<SelectListItem>();
            InputType.Add(new SelectListItem { Text = "kumulativ", Value = "0" });
            InputType.Add(new SelectListItem { Text = "tariert", Value = "1" });
            InputType.Add(new SelectListItem { Text = "Durchgangsprozent", Value = "2" });
            InputType.Add(new SelectListItem { Text = "Rückstandsprozent", Value = "3" });
            InputType.Find(a => a.Value == lims_aggregate_Test.InputType.ToString()).Selected = true;
            ViewBag.InputType = InputType;
            ViewBag.ProbUserId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Id", "Description", lims_aggregate_Test.ProbUserId);
            ViewBag.UserId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Id", "Description", lims_aggregate_Test.UserId);
            return View(lims_aggregate_Test);
        }

        // GET: LIMS/lims_aggregate_Test/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lims_aggregate_Test lims_aggregate_Test = db.Lims_aggregate_Test.Find(id);
            if (lims_aggregate_Test == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            ViewBag.ProbPlaceDetail = new SelectList(db.Lims_aggregate_ProbPlaceDetailsGroup, "Id", "Description", lims_aggregate_Test.ProbPlaceDetail);
            ViewBag.ProbPlaceId = new SelectList(db.lims_aggregate_ProbPlaceGroup, "Id", "Description", lims_aggregate_Test.ProbPlaceId);
            ViewBag.ProbTypeId = new SelectList(db.Lims_aggregate_ProbType, "Id", "Description", lims_aggregate_Test.ProbTypeId);
            ViewBag.StateId = new SelectList(db.Lims_aggregate_TestStateGroup, "Id", "Description", lims_aggregate_Test.StateId);
            ViewBag.TestTypeId = new SelectList(db.Lims_aggregate_TestType, "Id", "Description", lims_aggregate_Test.TestTypeId);
            if (ViewBag.CompanyType == 0)
                ViewBag.MaterialId = new SelectList(db.md_material_Material.OrderBy(m => m.Name).Where(m => m.IsDeleted != true && (m.MaterialGroupId == 1 )), "Id", "Name", lims_aggregate_Test.MaterialId);
            else
                ViewBag.MaterialId = new SelectList(db.md_material_Material.OrderBy(m => m.Name).Where(m => m.IsDeleted != true), "Id", "Name", lims_aggregate_Test.MaterialId);
            ViewBag.NormId = new SelectList(db.Lims_aggregate_TestNorm, "Id", "Description", lims_aggregate_Test.NormId);
            ViewBag.SievSetId = new SelectList(db.Md_sieve_SieveSet, "Id", "Description", lims_aggregate_Test.SievSetId);
            var KindOfSieving = new List<SelectListItem>();
            KindOfSieving.Add(new SelectListItem { Text = "trocken", Value = "0" });
            KindOfSieving.Add(new SelectListItem { Text = "nass", Value = "1" });
            KindOfSieving.Find(a => a.Value == lims_aggregate_Test.KindOfSieving).Selected = true;
            ViewBag.KindOfSieving = KindOfSieving;
            //var tryCpount = new List<SelectListItem>();
            //tryCpount.Add(new SelectListItem { Text = "ein", Value = "0", Selected = true });
            //tryCpount.Add(new SelectListItem { Text = "zwei", Value = "1" });
            //tryCpount.Find(a => a.Value == lims_aggregate_Test.TryCount).Selected = true;
            //ViewBag.TryCount = tryCpount;
            var InputType = new List<SelectListItem>();
            InputType.Add(new SelectListItem { Text = "kumulativ", Value = "0" });
            InputType.Add(new SelectListItem { Text = "tariert", Value = "1" });
            InputType.Add(new SelectListItem { Text = "Durchgangsprozent", Value = "2" });
            InputType.Add(new SelectListItem { Text = "Rückstandsprozent", Value = "3" });
            InputType.Find(a => a.Value == lims_aggregate_Test.InputType.ToString()).Selected = true;
            ViewBag.InputType = InputType;
            ViewBag.ProbUserId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Id", "Description", lims_aggregate_Test.ProbUserId);
            ViewBag.UserId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Id", "Description", lims_aggregate_Test.UserId);
            var aggrId = db.md_material_Material.Where(a => a.Id == lims_aggregate_Test.MaterialId).FirstOrDefault().AggregateId;
            if(aggrId!=null)
                ViewBag.GradingCurvSieveRangeId = dbMaster.Md_material_AggregateDetails.Where(a => a.Id == aggrId).FirstOrDefault().RuleGradingCurveId ;
            return View(lims_aggregate_Test);
        }
        public ActionResult EditH(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lims_aggregate_Test lims_aggregate_Test = db.Lims_aggregate_Test.Find(id);
            if (lims_aggregate_Test == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            ViewBag.ProbPlaceDetail = new SelectList(db.Lims_aggregate_ProbPlaceDetailsGroup, "Id", "Description", lims_aggregate_Test.ProbPlaceDetail);
            ViewBag.ProbPlaceId = new SelectList(db.lims_aggregate_ProbPlaceGroup, "Id", "Description", lims_aggregate_Test.ProbPlaceId);
            ViewBag.ProbTypeId = new SelectList(db.Lims_aggregate_ProbType, "Id", "Description", lims_aggregate_Test.ProbTypeId);
            ViewBag.StateId = new SelectList(db.Lims_aggregate_TestStateGroup, "Id", "Description", lims_aggregate_Test.StateId);
            ViewBag.TestTypeId = new SelectList(db.Lims_aggregate_TestType, "Id", "Description", lims_aggregate_Test.TestTypeId);
            if (ViewBag.CompanyType == 0)
                ViewBag.MaterialId = new SelectList(db.md_material_Material.OrderBy(m => m.Name).Where(m => m.IsDeleted != true && (m.MaterialGroupId == 1 )), "Id", "Name", lims_aggregate_Test.MaterialId);
            else
                ViewBag.MaterialId = new SelectList(db.md_material_Material.OrderBy(m => m.Name).Where(m => m.IsDeleted != true), "Id", "Name", lims_aggregate_Test.MaterialId);
            ViewBag.NormId = new SelectList(db.Lims_aggregate_TestNorm, "Id", "Description", lims_aggregate_Test.NormId);
            ViewBag.SievSetId = new SelectList(db.Md_sieve_SieveSet, "Id", "Description", lims_aggregate_Test.SievSetId);
            var KindOfSieving = new List<SelectListItem>();
            KindOfSieving.Add(new SelectListItem { Text = "trocken", Value = "0" });
            KindOfSieving.Add(new SelectListItem { Text = "nass", Value = "1" });
            KindOfSieving.Find(a => a.Value == lims_aggregate_Test.KindOfSieving).Selected = true;
            ViewBag.KindOfSieving = KindOfSieving;
            //var tryCpount = new List<SelectListItem>();
            //tryCpount.Add(new SelectListItem { Text = "ein", Value = "0", Selected = true });
            //tryCpount.Add(new SelectListItem { Text = "zwei", Value = "1" });
            //tryCpount.Find(a => a.Value == lims_aggregate_Test.TryCount).Selected = true;
            //ViewBag.TryCount = tryCpount;
            var InputType = new List<SelectListItem>();
            InputType.Add(new SelectListItem { Text = "kumulativ", Value = "0" });
            InputType.Add(new SelectListItem { Text = "tariert", Value = "1" });
            InputType.Add(new SelectListItem { Text = "Durchgangsprozent", Value = "2" });
            InputType.Add(new SelectListItem { Text = "Rückstandsprozent", Value = "3" });
            InputType.Find(a => a.Value == lims_aggregate_Test.InputType.ToString()).Selected = true;
            ViewBag.InputType = InputType;
            var history = db.Auditlog.Where(a => a.recordid == lims_aggregate_Test.Id.ToString() && a.tablename == "lims_aggregate_Test" ).OrderBy(a => a.eventdateutc).GroupBy(a => a.eventdateutc);

            var historyList = new List<SelectListItem>();
            
            foreach (var u in history)
            {
                historyList.Add(new SelectListItem { Text = u.Key.ToLongDateString() +" "+u.Key.ToLongTimeString() + " " + u.Key.Millisecond, Value = u.Key.Ticks.ToString() });
            }

            ViewBag.History = historyList;
            return View(lims_aggregate_Test);
        }

        // POST: LIMS/lims_aggregate_Test/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, TestNumber, RegistraionDate, TestDate, TestTypeId, OrderNumber, OrderClient, MaterialId, StateId, Outliner, Rating, DeliverQuantity, DeliveryNoteNo, SupplierId, Quantity, ProbTakingDatetime, ProbEnteringDatetime, ProbTypeId, ProbPlaceId, ProbPlaceDetail, MaterialWet, MaterialDry, WaterContent, IntrinsicMoisture, CoreMaterialWet, CoreMaterialDry, CoreMoisture, KindOfSieving, NormId, SievSetId, InputType, TryCount, M1, S0_063, S0_125, S0_25, S0_5, S1, S1_4, S2, S2_8, S4, S5_6, S8, S11_2, S16, S22_4, S31_5, S45, S63, KValue, DValue,Flakiness,FlakinessCheck,Conform,ConformCheck,Water,WaterCheck,FineShare,FineShareCheck,RatingFineShare,RatingFineShareCheck,GraingrossDensity,ProbUserId, UserId")] lims_aggregate_Test lims_aggregate_Test)
        {
            if (ModelState.IsValid)
            {
                lims_aggregate_Test.IsDeleted = false;
                lims_aggregate_Test.IsActive = true;
                db.Entry(lims_aggregate_Test).State = EntityState.Modified;
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();

            ViewBag.ProbPlaceDetail = new SelectList(db.Lims_aggregate_ProbPlaceDetailsGroup, "Id", "Description", lims_aggregate_Test.ProbPlaceDetail);
            ViewBag.ProbPlaceId = new SelectList(db.lims_aggregate_ProbPlaceGroup, "Id", "Description", lims_aggregate_Test.ProbPlaceId);
            ViewBag.ProbTypeId = new SelectList(db.Lims_aggregate_ProbType, "Id", "Description", lims_aggregate_Test.ProbTypeId);
            ViewBag.StateId = new SelectList(db.Lims_aggregate_TestStateGroup, "Id", "Description", lims_aggregate_Test.StateId);
            ViewBag.TestTypeId = new SelectList(db.Lims_aggregate_TestType, "Id", "Description", lims_aggregate_Test.TestTypeId);
            if (ViewBag.CompanyType == 0)
                ViewBag.MaterialId = new SelectList(db.md_material_Material.OrderBy(m => m.Name).Where(m => m.IsDeleted != true && (m.MaterialGroupId == 1 )), "Id", "Name", lims_aggregate_Test.MaterialId);
            else
                ViewBag.MaterialId = new SelectList(db.md_material_Material.OrderBy(m => m.Name).Where(m => m.IsDeleted != true), "Id", "Name",lims_aggregate_Test.MaterialId);
            ViewBag.NormId = new SelectList(db.Lims_aggregate_TestNorm, "Id", "Description", lims_aggregate_Test.NormId);
            ViewBag.SievSetId = new SelectList(db.Md_sieve_SieveSet, "Id", "Description", lims_aggregate_Test.SievSetId);
            var KindOfSieving = new List<SelectListItem>();
            KindOfSieving.Add(new SelectListItem { Text = "trocken", Value = "0" });
            KindOfSieving.Add(new SelectListItem { Text = "nass", Value = "1" });
            KindOfSieving.Find(a => a.Value == lims_aggregate_Test.KindOfSieving).Selected = true;
            ViewBag.KindOfSieving = KindOfSieving;
            //var tryCpount = new List<SelectListItem>();
            //tryCpount.Add(new SelectListItem { Text = "ein", Value = "0", Selected = true });
            //tryCpount.Add(new SelectListItem { Text = "zwei", Value = "1" });
            //tryCpount.Find(a => a.Value == lims_aggregate_Test.TryCount).Selected = true;
            //ViewBag.TryCount = tryCpount;
            var InputType = new List<SelectListItem>();
            InputType.Add(new SelectListItem { Text = "kumulativ", Value = "0" });
            InputType.Add(new SelectListItem { Text = "tariert", Value = "1" });
            InputType.Add(new SelectListItem { Text = "Durchgangsprozent", Value = "2" });
            InputType.Add(new SelectListItem { Text = "Rückstandsprozent", Value = "3" });
            InputType.Find(a => a.Value == lims_aggregate_Test.InputType.ToString()).Selected = true;
            ViewBag.InputType = InputType;
            ViewBag.ProbUserId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Id", "Description", lims_aggregate_Test.ProbUserId);
            ViewBag.UserId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Id", "Description", lims_aggregate_Test.UserId);
            var aggrId = db.md_material_Material.Where(a => a.Id == lims_aggregate_Test.MaterialId).FirstOrDefault().AggregateId;
            if (aggrId != null)
                ViewBag.GradingCurvSieveRangeId = dbMaster.Md_material_AggregateDetails.Where(a => a.Id == aggrId).FirstOrDefault().RuleGradingCurveId;
            return View(lims_aggregate_Test);
        }

        // GET: LIMS/lims_aggregate_Test/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lims_aggregate_Test lims_aggregate_Test = db.Lims_aggregate_Test.Find(id);
            if (lims_aggregate_Test == null)
            {
                return HttpNotFound();
            }
            return View(lims_aggregate_Test);
        }

        // POST: LIMS/lims_aggregate_Test/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            lims_aggregate_Test lims_aggregate_Test = db.Lims_aggregate_Test.Find(id);
            lims_aggregate_Test.IsActive = true;
            lims_aggregate_Test.IsDeleted = true;
            db.Entry(lims_aggregate_Test).State = EntityState.Modified;
            //db.Lims_aggregate_Test.Remove(lims_aggregate_Test);
            db.SaveChanges(User.Identity.Name);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Copy(long IdCopy, string NumberCopy)
        {
            //var model = db.Md_masterData_Material.AsNoTracking().FirstOrDefault(a => a.Id == IdCopy);
            var model = db.Lims_aggregate_Test.AsNoTracking().FirstOrDefault(a => a.Id == IdCopy);
            model.Id = 0;
            model.TestNumber = NumberCopy;

            db.Lims_aggregate_Test.Add(model);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = model.Id });
        }

        //**********************************************************************Reporting
        public ReportDataSource fillReport(PCMS.Models.PaginationModel pg)
        {
            var model = pageModel(pg);
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();

            foreach (var dataset in model)
            {
                ds.List.AddListRow(dataset.Id.ToString(), (dataset.TestNumber ?? ""), dataset.Md_material_Material.Name.ToString(), dataset.Lims_aggregate_TestType.Description.ToString(), dataset.Lims_aggregate_TestNorm.Description.ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            }

         

            ReportDataSource DSReport;

            return DSReport = new ReportDataSource("dsList", ds.List.ToList());
        }


        //public ActionResult Report(PCMS.Models.PaginationModel pg)
        public ActionResult Report(string pgS)
        {
            PCMS.Models.PaginationModel pg = JsonConvert.DeserializeObject<PCMS.Models.PaginationModel>(pgS);
            var model = pageModel(pg);
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();

            foreach (var dataset in model)
            {
                //ds.List.AddListRow(dataset.Id.ToString(), (dataset.TestNumber ?? ""), dataset.Md_material_Material.Name.ToString(), dataset.Lims_aggregate_TestType.Description.ToString(), dataset.Lims_aggregate_TestNorm.Description.ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "","");
                var listKomu = dataset.getKumulativList();
                ds.List2.AddList2Row(dataset.Id.ToString(), (dataset.TestNumber ?? ""), dataset.Md_material_Material.Name.ToString(), dataset.Lims_aggregate_TestType.Description.ToString(), dataset.DeliveryNoteNo, dataset.ProbTakingDatetime.HasValue ? dataset.ProbTakingDatetime.Value.ToShortDateString() : "",  decToString(listKomu["0,063"]), decToString(listKomu["0,125"]), decToString(listKomu["0,25"]), decToString(listKomu["0,5"]), decToString(listKomu["1"]), decToString(listKomu["1,4"]), decToString(listKomu["2"]), decToString(listKomu["2,8"]), decToString(listKomu["4"]), decToString(listKomu["5,6"]), decToString(listKomu["8"]), decToString(listKomu["11,2"]), decToString(listKomu["16"]), decToString(listKomu["22,4"]), decToString(listKomu["31,5"]), decToString(listKomu["45"]), decToString(listKomu["63"]), "", "", "", "", "","","","");
                
            }
            ReportDataSource DSReport = new ReportDataSource("ds", ds.List2.ToList());

            ReportDataSource test = fillReport(pg);

            List<ReportParameter> repParams = new List<ReportParameter>();
            //repParams.Add(new ReportParameter("Header_00", "ID"));
            //repParams.Add(new ReportParameter("Hide_00", "false"));
            //repParams.Add(new ReportParameter("Header_01", "PrüfungsNr."));
            //repParams.Add(new ReportParameter("Hide_01", "false"));
            //repParams.Add(new ReportParameter("Header_02", "Material"));
            //repParams.Add(new ReportParameter("Hide_02", "false"));
            //repParams.Add(new ReportParameter("Header_03", "Art der Prüfung"));
            //repParams.Add(new ReportParameter("Hide_03", "false"));
            //repParams.Add(new ReportParameter("Header_04", "Nach Norm"));
            //repParams.Add(new ReportParameter("Hide_04", "false"));
            //repParams.Add(new ReportParameter("Header_05", ""));
            //repParams.Add(new ReportParameter("Hide_05", "true"));
            //repParams.Add(new ReportParameter("Header_06", ""));
            //repParams.Add(new ReportParameter("Hide_06", "true"));
            //repParams.Add(new ReportParameter("Header_07", ""));
            //repParams.Add(new ReportParameter("Hide_07", "true"));
            //repParams.Add(new ReportParameter("Header_08", ""));
            //repParams.Add(new ReportParameter("Hide_08", "true"));
            //repParams.Add(new ReportParameter("Header_09", ""));
            //repParams.Add(new ReportParameter("Hide_09", "true"));
            //
            //repParams.Add(new ReportParameter("Title", "Gesteinskörnungsprüfung"));
            repParams.Add(new ReportParameter("DateTime",DateTime.Now.ToShortDateString()));
            //repParams.Add(new ReportParameter("Description", "Liste der Gesteinskörnungsprüfungen"));
            //repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/LIMS/AggregateList.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 1200;
            reportViewer.Height = 700;

            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);
            //ViewBag.ReportViewer = reportViewer;
            //return View();
        }

        public ActionResult ReportDetail(long? Id)
        {
            try
            {
                var model = db.Lims_aggregate_Test.Find(Id);
                List<string> Sieves = Definitions.allSieves.ToList();

                var aggrId = db.md_material_Material.Where(a => a.Id == model.MaterialId).FirstOrDefault().AggregateId;
                //MasterData.Models.md_material_AggregateDetails agMateria = null;
                long rulId = 0;
                if (aggrId != null)
                    rulId = dbMaster.Md_material_AggregateDetails.Where(a => a.Id == aggrId).FirstOrDefault().RuleGradingCurveId ?? 0;
                var rule = db.Md_sieve_RuleGradingCurve.Where(a => a.Id == rulId).FirstOrDefault();
               



                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();

                Dictionary<string, decimal> durchgang = model.getKumulativList();
                var fields = durchgang.Values.ToList();
                
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[0], fields[0], rule.A0 ?? -1, rule.B0 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[1], fields[1], rule.A1 ?? -1, rule.B1 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[2], fields[2], rule.A2 ?? -1, rule.B0 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[3], fields[3], rule.A3 ?? -1, rule.B3 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[4], fields[4], rule.A4 ?? -1, rule.B4 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[5], fields[5], rule.A5 ?? -1, rule.B5 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[6], fields[6], rule.A6 ?? -1, rule.B6 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[7], fields[7], rule.A7 ?? -1, rule.B7 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[8], fields[8], rule.A8 ?? -1, rule.B8 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[9], fields[9], rule.A9 ?? -1, rule.B9 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[10], fields[10], rule.A10 ?? -1, rule.B10 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[11], fields[11], rule.A11 ?? -1, rule.B11 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[12], fields[12], rule.A12 ?? -1, rule.B12 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[13], fields[13], rule.A13 ?? -1, rule.B13 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[14], fields[14], rule.A14 ?? -1, rule.B14 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[15], fields[15], rule.A15 ?? -1, rule.B15 ?? -1, 0);
                ds.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[16], fields[16], rule.A16 ?? -1, rule.B16 ?? -1, 0);

                

                //int counter = 0;
                //foreach (string s in Sieves)
                //{
                //    ds.RuleGradingCurve.AddRuleGradingCurveRow(s, A[counter], B[counter], C[counter], U[counter]);
                //    counter++;
                //}

                int count = 0;
                decimal lastValue=0;
                for (int i = 0; i < ds.RuleGradingCurve.Count; i++)
                {
                    if (ds.RuleGradingCurve[i].B == -1)
                    {
                
                        if (lastValue == 100)
                        {
                            ds.RuleGradingCurve[i].B = 100;
                        }
                        else
                        {
                            decimal top = -2;
                            count = 0;
                            for (int r = i + 1; r < ds.RuleGradingCurve.Count; r++)
                            {
                                count++;
                                if (ds.RuleGradingCurve[r].B != -1)
                                {
                                    top = ds.RuleGradingCurve[r].B;
                                    break;
                                }
                
                            }
                            if(Convert.ToDecimal( ds.RuleGradingCurve[i].Sieve) <=1)
                            {

                                ds.RuleGradingCurve[i].B = (lastValue + ((top - lastValue) / ((count + 1))));
                                //Anpasung an den logarytmus unter 1 da doppet so großer abstand
                                ds.RuleGradingCurve[i].B += ((top - lastValue) / ((count + 1))) / 2.0m;
                               
                            }
                            else{
                                ds.RuleGradingCurve[i].B = (lastValue + ((top - lastValue) / ((count + 1)))) ;
                            }
                            
                            ds.RuleGradingCurve[i].U = 1;
                            lastValue = ds.RuleGradingCurve[i].B;
                        }
                    }
                    else
                    {
                        lastValue = ds.RuleGradingCurve[i].B;
                    }
                }
                count = 0;
                lastValue = 0;
                for (int i = 0; i < ds.RuleGradingCurve.Count; i++)
                {
                    if (ds.RuleGradingCurve[i].C == -1)
                    {
                        if (lastValue == 100)
                        {
                            ds.RuleGradingCurve[i].C = 100;
                        }
                        else {
                            decimal top = -2;
                            count = 0;
                            for (int r = i + 1; r < ds.RuleGradingCurve.Count; r++)
                            {
                                count++;
                                if (ds.RuleGradingCurve[r].C != -1)
                                {
                                    top = ds.RuleGradingCurve[r].C;
                                    break;
                                }

                            }
                            ds.RuleGradingCurve[i].C = lastValue + ((top - lastValue) / (count + 1));
                            ds.RuleGradingCurve[i].U = 2;
                            lastValue = ds.RuleGradingCurve[i].C;
                        }
                    }
                    else
                    {
                        lastValue = ds.RuleGradingCurve[i].C;
                    }
                }

                ReportDataSource DSReport = new ReportDataSource("ds", ds.RuleGradingCurve.ToList());

                List<ReportParameter> repParams = new List<ReportParameter>();

                repParams.Add(new ReportParameter("orderClientName", model.OrderClient.ToString()));
                repParams.Add(new ReportParameter("orderClientStreet", ""));
                repParams.Add(new ReportParameter("orderClientZIP", ""));
                repParams.Add(new ReportParameter("orderClientCity", ""));
                repParams.Add(new ReportParameter("testNumber", model.TestNumber));
                repParams.Add(new ReportParameter("testDate", model.TestDate.Value.ToShortDateString()));
                repParams.Add(new ReportParameter("testNumber", model.TestNumber));
                repParams.Add(new ReportParameter("materialNumber", model.Md_material_Material.MaterialNumber));
                repParams.Add(new ReportParameter("materialDescription", model.Md_material_Material.Name));
                repParams.Add(new ReportParameter("materialCategory", model.Md_material_Material.MaterialTypeId.ToString()));
                repParams.Add(new ReportParameter("producerName", ""));
                repParams.Add(new ReportParameter("producerStreet", ""));
                repParams.Add(new ReportParameter("producerZIP", ""));
                repParams.Add(new ReportParameter("producerCity", ""));
                repParams.Add(new ReportParameter("deliveryNoteNo", model.DeliveryNoteNo));
                repParams.Add(new ReportParameter("delivererName", ""));
                repParams.Add(new ReportParameter("delivererStreet", ""));
                repParams.Add(new ReportParameter("delivererZIP", ""));
                repParams.Add(new ReportParameter("delivererCity", ""));
                repParams.Add(new ReportParameter("probeTakingDate", model.ProbTakingDatetime.Value.ToShortDateString()));
                repParams.Add(new ReportParameter("probePlace", model.Lims_aggregate_ProbPlaceGroup.Description ?? "-"));
                repParams.Add(new ReportParameter("probePlaceDetail", model.Lims_aggregate_ProbPlaceDetailsGroup.Description ?? "-"));
                repParams.Add(new ReportParameter("probeTaker", db.Lims_concrete_SelectMenu.Where(a=>a.Id == model.ProbUserId).FirstOrDefault().Description));
                repParams.Add(new ReportParameter("probeType", model.Lims_aggregate_ProbType.Description ?? "-"));
                repParams.Add(new ReportParameter("probeQuantity", model.Quantity.ToString()));
                repParams.Add(new ReportParameter("probeEnteringDate", model.ProbEnteringDatetime.Value.ToShortDateString()));
                repParams.Add(new ReportParameter("sieveSet", model.SievSetId.ToString()));
                repParams.Add(new ReportParameter("kValue", model.KValue.ToString()));
                repParams.Add(new ReportParameter("dValue", model.DValue.ToString()));

                

                string SievesList = "";
                //for (int i = 0; i < Sieves.Count; i++)
                //{
                //    SievesList += Sieves[i].ToString() + ";";
                //}
                //for (int i = 0; i < (17 - Sieves.Count); i++)
                //{
                //    SievesList += "-;";
                //}

                //repParams.Add(new ReportParameter("SievesList", SievesList));

                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/LIMS/AggregateTestDetail.rdlc";
                //reportViewer.ShowPrintButton = false;
                //reportViewer.LocalReport.DataSources.Add(DSReportSieves);
                //reportViewer.LocalReport.DataSources.Add(DSReportA);
                //reportViewer.LocalReport.DataSources.Add(DSReportB);
                //reportViewer.LocalReport.DataSources.Add(DSReportC);
                //reportViewer.LocalReport.DataSources.Add(DSReportU);

                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;
                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);



            }
            catch (Exception e)
            {
                PCMS.Helper.ExceptionHelper.LogException(e, User.Identity.Name);
                return View("~/Views/Shared/Exception", e);
            }
        }

        public ActionResult GetSieveId(long id)
        {
            var aggrId = db.md_material_Material.Where(a => a.Id == id).FirstOrDefault().AggregateId;
            if (aggrId != null)
                return  Json( dbMaster.Md_material_AggregateDetails.Where(a => a.Id == aggrId).FirstOrDefault().RuleGradingCurveId);
            return null;
        }

        private List<lims_aggregate_Test> pageModel(PCMS.Models.PaginationModel pg)
        {
            var model = db.Lims_aggregate_Test.Where(m=>m.IsDeleted == false).Include(m => m.Lims_aggregate_TestType).Include(m => m.Md_material_Material).Include(m => m.Lims_aggregate_TestStateGroup).Include(m => m.Lims_aggregate_ProbPlaceGroup).Include(m => m.Lims_aggregate_TestNorm).OrderByDescending(a=>a.ProbEnteringDatetime).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.TopFilter top in pg.topFilter)
                {
                    switch (top.colName)
                    {
                        case "ProbTakingDatetime":
                            if (!String.IsNullOrEmpty(top.from) && !String.IsNullOrEmpty(top.to))
                            {
                                model = model.Where(m => m.ProbTakingDatetime.Value.Date >= new DateTime(Convert.ToInt32(top.from.Split('-')[0]), Convert.ToInt32(top.from.Split('-')[1]), Convert.ToInt32(top.from.Split('-')[2])).Date && m.ProbTakingDatetime.Value.Date <= new DateTime(Convert.ToInt32(top.to.Split('-')[0]), Convert.ToInt32(top.to.Split('-')[1]), Convert.ToInt32(top.to.Split('-')[2])).Date).ToList();
                                ViewBag.ProbTakingDatetimeFrom = top.from;
                                ViewBag.ProbTakingDatetimeTo = top.to;
                            }
                            break;
                    }
                }

                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "TestNumber":
                                model = model.Where(m => m.TestNumber != null && m.TestNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.TestNumber = pgFF.colVal;
                                break;
                            case "TestTypeId":
                                model = model.Where(m => m.TestTypeId != null && m.TestTypeId.Equals(pgFF.colVal)).ToList();
                                ViewBag.TestTypeId = pgFF.colVal;
                                break;
                            case "MaterialId":
                                //model = model.Where(m => m.MaterialId.Equals(pgFF.colVal)).ToList();
                                model = model.Where(m => m.Md_material_Material.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.MaterialId = pgFF.colVal;
                                break;
                            case "SupplierId":
                                model = model.Where(m => m.SupplierId != null && m.SupplierId.Equals(pgFF.colVal)).ToList();
                                ViewBag.SupplierId = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "TestNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.TestNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.TestNumber).ToList();
                    break;
                case "RegistraionDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RegistraionDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.RegistraionDate).ToList();
                    break;
                case "TestDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.TestDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.TestDate).ToList();
                    break;
                case "TestTypeId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.TestTypeId).ToList();
                    else
                        model = model.OrderByDescending(m => m.TestTypeId).ToList();
                    break;
                case "OrderNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.OrderNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.OrderNumber).ToList();
                    break;
                case "OrderClient":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.OrderClient).ToList();
                    else
                        model = model.OrderByDescending(m => m.OrderClient).ToList();
                    break;
                case "MaterialId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialId).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialId).ToList();
                    break;
                case "StateId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.StateId).ToList();
                    else
                        model = model.OrderByDescending(m => m.StateId).ToList();
                    break;
                case "Outliner":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Outliner).ToList();
                    else
                        model = model.OrderByDescending(m => m.Outliner).ToList();
                    break;
                case "Rating":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Rating).ToList();
                    else
                        model = model.OrderByDescending(m => m.Rating).ToList();
                    break;
                case "DeliverQuantity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.DeliverQuantity).ToList();
                    else
                        model = model.OrderByDescending(m => m.DeliverQuantity).ToList();
                    break;
                case "DeliveryNoteNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.DeliveryNoteNo).ToList();
                    else
                        model = model.OrderByDescending(m => m.DeliveryNoteNo).ToList();
                    break;
                case "SupplierId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SupplierId).ToList();
                    else
                        model = model.OrderByDescending(m => m.SupplierId).ToList();
                    break;
                case "Quantity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Quantity).ToList();
                    else
                        model = model.OrderByDescending(m => m.Quantity).ToList();
                    break;
                case "ProbTakingDatetime":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ProbTakingDatetime).ToList();
                    else
                        model = model.OrderByDescending(m => m.ProbTakingDatetime).ToList();
                    break;
                case "ProbEnteringDatetime":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ProbEnteringDatetime).ToList();
                    else
                        model = model.OrderByDescending(m => m.ProbEnteringDatetime).ToList();
                    break;
                case "ProbTypeId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ProbTypeId).ToList();
                    else
                        model = model.OrderByDescending(m => m.ProbTypeId).ToList();
                    break;
                case "ProbPlaceId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ProbPlaceId).ToList();
                    else
                        model = model.OrderByDescending(m => m.ProbPlaceId).ToList();
                    break;
                case "ProbPlaceDetail":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ProbPlaceDetail).ToList();
                    else
                        model = model.OrderByDescending(m => m.ProbPlaceDetail).ToList();
                    break;
                case "MaterialWet":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialWet).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialWet).ToList();
                    break;
                case "MaterialDry":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialDry).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialDry).ToList();
                    break;
                case "WaterContent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WaterContent).ToList();
                    else
                        model = model.OrderByDescending(m => m.WaterContent).ToList();
                    break;
                case "IntrinsicMoisture":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.IntrinsicMoisture).ToList();
                    else
                        model = model.OrderByDescending(m => m.IntrinsicMoisture).ToList();
                    break;
                case "CoreMaterialWet":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CoreMaterialWet).ToList();
                    else
                        model = model.OrderByDescending(m => m.CoreMaterialWet).ToList();
                    break;
                case "CoreMaterialDry":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CoreMaterialDry).ToList();
                    else
                        model = model.OrderByDescending(m => m.CoreMaterialDry).ToList();
                    break;
                case "CoreMoisture":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CoreMoisture).ToList();
                    else
                        model = model.OrderByDescending(m => m.CoreMoisture).ToList();
                    break;
                case "KindOfSieving":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.KindOfSieving).ToList();
                    else
                        model = model.OrderByDescending(m => m.KindOfSieving).ToList();
                    break;
                case "NormId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.NormId).ToList();
                    else
                        model = model.OrderByDescending(m => m.NormId).ToList();
                    break;
                case "SievSetId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SievSetId).ToList();
                    else
                        model = model.OrderByDescending(m => m.SievSetId).ToList();
                    break;
                case "InputType":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.InputType).ToList();
                    else
                        model = model.OrderByDescending(m => m.InputType).ToList();
                    break;
                case "TryCount":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.TryCount).ToList();
                    else
                        model = model.OrderByDescending(m => m.TryCount).ToList();
                    break;
                case "M1":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.M1).ToList();
                    else
                        model = model.OrderByDescending(m => m.M1).ToList();
                    break;
                case "S0_063":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S0_063).ToList();
                    else
                        model = model.OrderByDescending(m => m.S0_063).ToList();
                    break;
                case "S0_125":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S0_125).ToList();
                    else
                        model = model.OrderByDescending(m => m.S0_125).ToList();
                    break;
                case "S0_25":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S0_25).ToList();
                    else
                        model = model.OrderByDescending(m => m.S0_25).ToList();
                    break;
                case "S0_5":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S0_5).ToList();
                    else
                        model = model.OrderByDescending(m => m.S0_5).ToList();
                    break;
                case "S1":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S1).ToList();
                    else
                        model = model.OrderByDescending(m => m.S1).ToList();
                    break;
                case "S1_4":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S1_4).ToList();
                    else
                        model = model.OrderByDescending(m => m.S1_4).ToList();
                    break;
                case "S2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S2).ToList();
                    else
                        model = model.OrderByDescending(m => m.S2).ToList();
                    break;
                case "S2_8":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S2_8).ToList();
                    else
                        model = model.OrderByDescending(m => m.S2_8).ToList();
                    break;
                case "S4":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S4).ToList();
                    else
                        model = model.OrderByDescending(m => m.S4).ToList();
                    break;
                case "S5_6":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S5_6).ToList();
                    else
                        model = model.OrderByDescending(m => m.S5_6).ToList();
                    break;
                case "S8":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S8).ToList();
                    else
                        model = model.OrderByDescending(m => m.S8).ToList();
                    break;
                case "S11_2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S11_2).ToList();
                    else
                        model = model.OrderByDescending(m => m.S11_2).ToList();
                    break;
                case "S16":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S16).ToList();
                    else
                        model = model.OrderByDescending(m => m.S16).ToList();
                    break;
                case "S22_4":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S22_4).ToList();
                    else
                        model = model.OrderByDescending(m => m.S22_4).ToList();
                    break;
                case "S31_5":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S31_5).ToList();
                    else
                        model = model.OrderByDescending(m => m.S31_5).ToList();
                    break;
                case "S45":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S45).ToList();
                    else
                        model = model.OrderByDescending(m => m.S45).ToList();
                    break;
                case "S63":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.S63).ToList();
                    else
                        model = model.OrderByDescending(m => m.S63).ToList();
                    break;
                case "KValue":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.KValue).ToList();
                    else
                        model = model.OrderByDescending(m => m.KValue).ToList();
                    break;
                case "FactoryId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FactoryId).ToList();
                    else
                        model = model.OrderByDescending(m => m.FactoryId).ToList();
                    break;
                case "DValue":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.DValue).ToList();
                    else
                        model = model.OrderByDescending(m => m.DValue).ToList();
                    break;
                case "UserId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserId).ToList();
                    else
                        model = model.OrderByDescending(m => m.UserId).ToList();
                    break;
                case "Flakiness":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Flakiness).ToList();
                    else
                        model = model.OrderByDescending(m => m.Flakiness).ToList();
                    break;
                case "FlakinessCheck":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FlakinessCheck).ToList();
                    else
                        model = model.OrderByDescending(m => m.FlakinessCheck).ToList();
                    break;
                case "Conform":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Conform).ToList();
                    else
                        model = model.OrderByDescending(m => m.Conform).ToList();
                    break;
                case "ConformCheck":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ConformCheck).ToList();
                    else
                        model = model.OrderByDescending(m => m.ConformCheck).ToList();
                    break;
                case "Water":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Water).ToList();
                    else
                        model = model.OrderByDescending(m => m.Water).ToList();
                    break;
                case "WaterCheck":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WaterCheck).ToList();
                    else
                        model = model.OrderByDescending(m => m.WaterCheck).ToList();
                    break;
                case "FineShare":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FineShare).ToList();
                    else
                        model = model.OrderByDescending(m => m.FineShare).ToList();
                    break;
                case "FineShareCheck":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FineShareCheck).ToList();
                    else
                        model = model.OrderByDescending(m => m.FineShareCheck).ToList();
                    break;
                case "RatingFineShare":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RatingFineShare).ToList();
                    else
                        model = model.OrderByDescending(m => m.RatingFineShare).ToList();
                    break;
                case "RatingFineChareCheck":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RatingFineShareCheck).ToList();
                    else
                        model = model.OrderByDescending(m => m.RatingFineShareCheck).ToList();
                    break;
                case "GraingrossDensity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.GraingrossDensity).ToList();
                    else
                        model = model.OrderByDescending(m => m.GraingrossDensity).ToList();
                    break;
            }
            return model.ToList();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public static string decToString(Decimal? value)
        {
            if (value == null)
                return "";
            else
                return Math.Round(value??0,1).ToString();
        }
    }
}
