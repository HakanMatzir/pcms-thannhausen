﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.LIMS.Models;
using PCMS.Helper;
using System.Web.Security;
using PCMS.Models;
using PagedList;
using Microsoft.Reporting.WebForms;


//USE[PCMS]
//GO
//INSERT INTO[dbo].[lims_concrete_SelectMenu]([MenuId],[Description],[IsActive],[IsDeleted])VALUES(32,'nicht begonnen',1,0)
//GO
//INSERT INTO[dbo].[lims_concrete_SelectMenu]([MenuId],[Description],[IsActive],[IsDeleted])VALUES(32,'zurückgestellt',1,0)
//GO
//INSERT INTO[dbo].[lims_concrete_SelectMenu]([MenuId],[Description],[IsActive],[IsDeleted])VALUES(32,'in Bearbeitung',1,0)
//GO
//INSERT INTO[dbo].[lims_concrete_SelectMenu]([MenuId],[Description],[IsActive],[IsDeleted])VALUES(32,'erledigt',1,0)
//GO
//INSERT INTO[dbo].[lims_concrete_SelectMenu]([MenuId],[Description],[IsActive],[IsDeleted])VALUES(33,'Erstprüfung(Eignungsprüfung)',1,0)
//GO
//INSERT INTO[dbo].[lims_concrete_SelectMenu]([MenuId],[Description],[IsActive],[IsDeleted])VALUES(33,'Güteprüfung',1,0)
//GO
//INSERT INTO[dbo].[lims_concrete_SelectMenu]([MenuId],[Description],[IsActive],[IsDeleted])VALUES(33,'Erhärtungsprüfung',1,0)
//GO
//INSERT INTO[dbo].[lims_concrete_SelectMenu]([MenuId],[Description],[IsActive],[IsDeleted])VALUES(33,'Vergleichsprüfung',1,0)
//GO
//INSERT INTO[dbo].[lims_concrete_SelectMenu]([MenuId],[Description],[IsActive],[IsDeleted])VALUES(33,'Konformitätsprüfung',1,0)
//GO
//INSERT INTO[dbo].[lims_concrete_SelectMenu]([MenuId],[Description],[IsActive],[IsDeleted])VALUES(33,'Identitätsprüfung',1,0)
//GO
//INSERT INTO[dbo].[lims_concrete_SelectMenu]([MenuId],[Description],[IsActive],[IsDeleted])VALUES(33,'Konformitätsprüfung',1,0)
//GO



namespace PCMS.Areas.LIMS.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class lims_concrete_TestController : Controller
    {
        private LimsContext db = new LimsContext();

        // GET: LIMS/lims_concrete_Test
        public ActionResult Index(PaginationModel pg)
        {
            //var model = db.Lims_concrete_Test.OrderBy(a => a.TestNumber).ToList();  // geändert von Hakan Matzir am 25.02.2016
            var model = db.Lims_concrete_Test.Where(a => a.IsDeleted == false).OrderByDescending(a => a.TestNumber).ToList(); // geändert von Hakan Matzir am 25.02.2016
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "TestNumber":
                                model = model.Where(m => m.TestNumber != null && m.TestNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.TestNumber = pgFF.colVal;
                                break;
                            case "TestDate":
                               // model = model.Where(m => m.TestDate != null && m.TestDate.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.TestDate = pgFF.colVal;
                                break;
                            case "TestType":
                                //model = model.Where(m => m.TestType != null && m.TestType.ToLower().Contains(pgFF.colVal)).ToList();
                                model = model.Where(m => m.TestType == Convert.ToInt64(pgFF.colVal)).ToList();
                                ViewBag.TestType = pgFF.colVal;
                                break;
                            case "OrderNumber":
                                model = model.Where(m => m.OrderNumber != null && m.OrderNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.OrderNumber = pgFF.colVal;
                                break;
                            case "SortName":
                                model = model.Where(m => m.SortName != null && m.SortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.SortName = pgFF.colVal;
                                break;
                            case "Stauts":
                                model = model.Where(m => m.Stauts == Convert.ToInt64( pgFF.colVal)).ToList();
                                ViewBag.Stauts = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Identity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Identity).ToList();
                    else
                        model = model.OrderByDescending(m => m.Identity).ToList();
                    break;
                case "TestNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.TestNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.TestNumber).ToList();
                    break;
                case "TestDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.TestDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.TestDate).ToList();
                    break;
                case "TestType":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.TestType).ToList();
                    else
                        model = model.OrderByDescending(m => m.TestType).ToList();
                    break;
                case "OrderNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.OrderNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.OrderNumber).ToList();
                    break;
                case "OrderClient":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.OrderClient).ToList();
                    else
                        model = model.OrderByDescending(m => m.OrderClient).ToList();
                    break;
                case "SortId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SortId).ToList();
                    else
                        model = model.OrderByDescending(m => m.SortId).ToList();
                    break;
                case "RecipeId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RecipeId).ToList();
                    else
                        model = model.OrderByDescending(m => m.RecipeId).ToList();
                    break;
                case "Stauts":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Stauts).ToList();
                    else
                        model = model.OrderByDescending(m => m.Stauts).ToList();
                    break;
                case "Probprofil":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Probprofil).ToList();
                    else
                        model = model.OrderByDescending(m => m.Probprofil).ToList();
                    break;
                case "IsDeleted":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.IsDeleted).ToList();
                    else
                        model = model.OrderByDescending(m => m.IsDeleted).ToList();
                    break;
                case "IsActive":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.IsActive).ToList();
                    else
                        model = model.OrderByDescending(m => m.IsActive).ToList();
                    break;
                case "Rev":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Rev).ToList();
                    else
                        model = model.OrderByDescending(m => m.Rev).ToList();
                    break;
                case "FactoryId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FactoryId).ToList();
                    else
                        model = model.OrderByDescending(m => m.FactoryId).ToList();
                    break;
            }
            var Stauts = new List<SelectListItem>();
            Stauts.Add(new SelectListItem { Text = "nicht begonnen", Value = "0", Selected = (ViewBag.Stauts == "0" ? true : false) });
            Stauts.Add(new SelectListItem { Text = "zurückgestellt", Value = "1", Selected = (ViewBag.Stauts == "1" ? true : false) });
            Stauts.Add(new SelectListItem { Text = "in Bearbeitung", Value = "2", Selected = (ViewBag.Stauts == "2" ? true : false) });
            Stauts.Add(new SelectListItem { Text = "erledigt", Value = "3", Selected = (ViewBag.Stauts == "3" ? true : false) });
            ViewBag.Stauts = Stauts;
            var TestType = new List<SelectListItem>();
            TestType.Add(new SelectListItem { Text = "Erstprüfung(Eignungsprüfung)", Value = "0",Selected = (ViewBag.TestType == "0" ? true : false) });
            TestType.Add(new SelectListItem { Text = "Güteprüfung", Value = "1", Selected = (ViewBag.TestType == "1" ? true : false) });
            TestType.Add(new SelectListItem { Text = "Erhärtungsprüfung", Value = "2", Selected = (ViewBag.TestType == "2" ? true : false) });
            TestType.Add(new SelectListItem { Text = "Vergleichsprüfung", Value = "3", Selected = (ViewBag.TestType == "3" ? true : false) });
            TestType.Add(new SelectListItem { Text = "Konformitätsprüfung", Value = "4", Selected = (ViewBag.TestType == "4" ? true : false) });
            TestType.Add(new SelectListItem { Text = "Identitätsprüfung", Value = "5", Selected = (ViewBag.TestType == "5" ? true : false) });
            TestType.Add(new SelectListItem { Text = "Konformitätsprüfung", Value = "6", Selected = (ViewBag.TestType == "6" ? true : false) });
            ViewBag.TestType = TestType;
            //return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));  // geändert von Hakan Matzir am 25.02.2016
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));  // geändert von Hakan Matzir am 25.02.2016
        }


        // GET: LIMS/lims_concrete_Test/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lims_concrete_Test lims_concrete_Test = db.Lims_concrete_Test.Find(id);
            if (lims_concrete_Test == null)
            {
                return HttpNotFound();
            }
            return View(lims_concrete_Test);
        }

        // GET: LIMS/lims_concrete_Test/Create
        public ActionResult Create()
        {
            lims_concrete_Test te = new lims_concrete_Test();
            var limsWet = new lims_concrete_ProbWet();
            limsWet.Lims_concrete_ProbWet_Consistency = new List<lims_concrete_ProbWet_Consistency>();
            limsWet.Lims_concrete_ProbWet_Consistency.Add(new lims_concrete_ProbWet_Consistency());
            limsWet.Lims_concrete_ProbWet_Consistency.Add(new lims_concrete_ProbWet_Consistency());
            limsWet.Lims_concrete_ProbWet_Consistency.Add(new lims_concrete_ProbWet_Consistency());
            limsWet.Lims_concrete_ProbWet_Consistency.Add(new lims_concrete_ProbWet_Consistency());
            te.Lims_concrete_ProbWets.Add(limsWet);
            

            //ProbNorm
            ViewBag.ProbPlaceId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 1), "Id", "Description");
            ViewBag.ProbUserName = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Description", "Description");
            ViewBag.ProbUserId2 = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Description", "Description");
            ViewBag.AirContentId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 2), "Id", "Description");
            ViewBag.WetConcreteDensity = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 3), "Id", "Description");
            ViewBag.ConsistencyId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 4), "Id", "Description");
            ViewBag.WaterContentId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 5), "Id", "Description");
            ViewBag.ContentOfCement = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 6), "Id", "Description");
            ViewBag.WashOutTry = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 7), "Id", "Description");
            ViewBag.ComponentId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 8), "Id", "Description");
            //ProbResults
            ViewBag.CompressionModeId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 9), "Id", "Description");
            ViewBag.ContributionId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 10), "Id", "Description");
            ViewBag.Weather = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 11), "Id", "Description");
            //Parameters
            ViewBag.MaximumGrainSizeId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 12), "Id", "Description");
            ViewBag.AggregateClass = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 13), "Id", "Description");
            ViewBag.GritsNumber = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 14), "Id", "Description");

            //-------------Trocken
            var ProbType = new List<SelectListItem>();
            ProbType.Add(new SelectListItem { Text = "Druckfestigkeit", Value = "Druckfestigkeit" });
            ProbType.Add(new SelectListItem { Text = "Biegezugfestigkeit", Value = "Biegezugfestigkeit" });
            ProbType.Add(new SelectListItem { Text = "Spaltzugfestigkeit", Value = "Spaltzugfestigkeit" });
            ProbType.Add(new SelectListItem { Text = "Wassereindringtiefe", Value = "Wassereindringtiefe" });
            ProbType.Add(new SelectListItem { Text = "Sonstiges", Value = "Sonstiges" });
            ViewBag.ProbType = ProbType;
            ViewBag.TestAgeValue = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 15), "Description", "Description");
            ViewBag.TestAgeType = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 16), "Description", "Description");
            ViewBag.Sharp = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 17), "Description", "Description");
            ViewBag.TestStandard = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 18), "Description", "Description");
            ViewBag.Storage = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 19), "Description", "Description");
            ViewBag.Charges = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 20), "Description", "Description");
            ViewBag.WaterEffect = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 21), "Description", "Description");
            ViewBag.FillingDirection = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 22), "Description", "Description");
            ViewBag.Testname = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 23), "Description", "Description");
            ViewBag.Unit = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 24), "Description", "Description");
            ViewBag.VolDetemining = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 25), "Description", "Description");
            //PCMS.Areas.Admin.Models.AdminContext adminContext = new Admin.Models.AdminContext();
            //ViewBag.ProbUserId = new SelectList(adminContext.AspNetUsers, "Id", "UserName");
            ViewBag.ProbUserId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Description", "Description");
            ViewBag.Status = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 27), "Description", "Description");
            ViewBag.WetStatus = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 28), "Description", "Description");
            ViewBag.Comparison = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 29), "Description", "Description");
            ViewBag.Preperation = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 30), "Description", "Description");
            ViewBag.Reason = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 31), "Description", "Description");
            var Volume = new List<SelectListItem>();
            Volume.Add(new SelectListItem { Text = "3,375", Value = "3,375" });
            Volume.Add(new SelectListItem { Text = "6,750", Value = "6,750" });
            Volume.Add(new SelectListItem { Text = "8", Value = "8" });
            Volume.Add(new SelectListItem { Text = "10,125", Value = "10,125" });
            Volume.Add(new SelectListItem { Text = "13,500", Value = "13,500" });
            Volume.Add(new SelectListItem { Text = "16,875", Value = "16,875" });
            Volume.Add(new SelectListItem { Text = "20,250", Value = "20,250" });
            ViewBag.Volume = Volume;
            //Betontest

            var Stauts = new List<SelectListItem>();
            Stauts.Add(new SelectListItem { Text = "nicht begonnen", Value = "0" });
            Stauts.Add(new SelectListItem { Text = "zurückgestellt", Value = "1" });
            Stauts.Add(new SelectListItem { Text = "in Bearbeitung", Value = "2" });
            Stauts.Add(new SelectListItem { Text = "erledigt", Value = "3" });
            ViewBag.Stauts = Stauts;
            var TestType = new List<SelectListItem>();
            TestType.Add(new SelectListItem { Text = "Erstprüfung(Eignungsprüfung)", Value = "0" });
            TestType.Add(new SelectListItem { Text = "Güteprüfung", Value = "1" });
            TestType.Add(new SelectListItem { Text = "Erhärtungsprüfung", Value = "2" });
            TestType.Add(new SelectListItem { Text = "Vergleichsprüfung", Value = "3" });
            TestType.Add(new SelectListItem { Text = "Konformitätsprüfung", Value = "4" });
            TestType.Add(new SelectListItem { Text = "Identitätsprüfung", Value = "5" });
            TestType.Add(new SelectListItem { Text = "Konformitätsprüfung", Value = "6" });
            ViewBag.TestType = TestType;
            //ViewBag.Stauts = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 32), "Id", "Description");
            //ViewBag.TestType = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 33), "Id", "Description");
            

            return View(te);
        }

        // POST: LIMS/lims_concrete_Test/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,Identity,TestNumber,TestDate,TestType,OrderNumber,OrderClient,SortId,RecipeId,Stauts,Probprofil,IsDeleted,IsActive,Rev,FactoryId")] lims_concrete_Test lims_concrete_Test)
        public ActionResult Create(lims_concrete_Test lims_concrete_Test,List<lims_concrete_ProbWet> Lims_concrete_ProbWet, List<lims_concrete_ProbDry> Lims_concrete_ProbDry)

        {

            
            var f= 0;
            foreach(var u in ModelState.Values)
            {
                if (u.Errors.Count() > 0)
                {
                    f =2;
                }
            }
            //lims_concrete_Test.Lims_concrete_ProbWets.Clear();

            if (ModelState.IsValid)
            {
                lims_concrete_Test.Identity = Guid.NewGuid().ToString();

                foreach (var i in Lims_concrete_ProbWet)
                {
                    // i.Identity = Guid.NewGuid().ToString();
                    i.Identity = Guid.NewGuid().ToString();
                    lims_concrete_Test.Lims_concrete_ProbWets.Add(i);
                }
                foreach (var i in Lims_concrete_ProbDry)
                {
                    // i.Identity = Guid.NewGuid().ToString();
                    i.Identity = Guid.NewGuid().ToString();
                    lims_concrete_Test.Lims_concrete_ProbDrys.Add(i);
                }
                db.Lims_concrete_Test.Add(lims_concrete_Test);
                db.SaveChanges();
                //foreach (var i in Lims_concrete_ProbWet)
                //{
                //    i.Identity = Guid.NewGuid().ToString();
                //    i.TestId = lims_concrete_Test.Id;
                //    db.lims_concrete_ProbWet.Add(i);
                //}
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            lims_concrete_Test.Lims_concrete_ProbWets = Lims_concrete_ProbWet;
            lims_concrete_Test.Lims_concrete_ProbDrys = Lims_concrete_ProbDry;
            //ProbNorm
            ViewBag.ProbPlaceId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 1), "Id", "Description");
            ViewBag.ProbUserName = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Description", "Description");
            ViewBag.ProbUserId2 = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Description", "Description");
            ViewBag.AirContentId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 2), "Id", "Description");
            ViewBag.WetConcreteDensity = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 3), "Id", "Description");
            ViewBag.ConsistencyId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 4), "Id", "Description");
            ViewBag.WaterContentId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 5), "Id", "Description");
            ViewBag.ContentOfCement = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 6), "Id", "Description");
            ViewBag.WashOutTry = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 7), "Id", "Description");
            ViewBag.ComponentId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 8), "Id", "Description");
            //ProbResults
            ViewBag.CompressionModeId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 9), "Id", "Description");
            ViewBag.ContributionId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 10), "Id", "Description");
            ViewBag.Weather = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 11), "Id", "Description");
            //Parameters
            ViewBag.MaximumGrainSizeId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 12), "Id", "Description");
            ViewBag.AggregateClass = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 13), "Id", "Description");
            ViewBag.GritsNumber = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 14), "Id", "Description");

            //-------------Trocken
            var ProbType = new List<SelectListItem>();
            ProbType.Add(new SelectListItem { Text = "Druckfestigkeit", Value = "Druckfestigkeit" });
            ProbType.Add(new SelectListItem { Text = "Biegezugfestigkeit", Value = "Biegezugfestigkeit" });
            ProbType.Add(new SelectListItem { Text = "Spaltzugfestigkeit", Value = "Spaltzugfestigkeit" });
            ProbType.Add(new SelectListItem { Text = "Wassereindringtiefe", Value = "Wassereindringtiefe" });
            ProbType.Add(new SelectListItem { Text = "Sonstiges", Value = "Sonstiges" });
            ViewBag.ProbType = ProbType;
            ViewBag.TestAgeValue = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 15), "Description", "Description");
            ViewBag.TestAgeType = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 16), "Description", "Description");
            ViewBag.Sharp = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 17), "Description", "Description");
            ViewBag.TestStandard = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 18), "Description", "Description");
            ViewBag.Storage = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 19), "Description", "Description");
            ViewBag.Charges = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 20), "Description", "Description");
            ViewBag.WaterEffect = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 21), "Description", "Description");
            ViewBag.FillingDirection = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 22), "Description", "Description");
            ViewBag.Testname = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 23), "Description", "Description");
            ViewBag.Unit = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 24), "Description", "Description");
            ViewBag.VolDetemining = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 25), "Description", "Description");
            //PCMS.Areas.Admin.Models.AdminContext adminContext = new Admin.Models.AdminContext();
            //ViewBag.ProbUserId = new SelectList(adminContext.AspNetUsers, "Id", "UserName");
            ViewBag.ProbUserId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Description", "Description");
            ViewBag.Status = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 27), "Description", "Description");
            ViewBag.WetStatus = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 28), "Description", "Description");
            ViewBag.Comparison = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 29), "Description", "Description");
            ViewBag.Preperation = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 30), "Description", "Description");
            ViewBag.Reason = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 31), "Description", "Description");
            var Volume = new List<SelectListItem>();
            Volume.Add(new SelectListItem { Text = "3,375", Value = "3,375" });
            Volume.Add(new SelectListItem { Text = "6,750", Value = "6,750" });
            Volume.Add(new SelectListItem { Text = "8", Value = "8" });
            Volume.Add(new SelectListItem { Text = "10,125", Value = "10,125" });
            Volume.Add(new SelectListItem { Text = "13,500", Value = "13,500" });
            Volume.Add(new SelectListItem { Text = "16,875", Value = "16,875" });
            Volume.Add(new SelectListItem { Text = "20,250", Value = "20,250" });
            ViewBag.Volume = Volume;
            //Betontest
            var Stauts = new List<SelectListItem>();
            Stauts.Add(new SelectListItem { Text = "nicht begonnen", Value = "0" });
            Stauts.Add(new SelectListItem { Text = "zurückgestellt", Value = "1" });
            Stauts.Add(new SelectListItem { Text = "in Bearbeitung", Value = "2" });
            Stauts.Add(new SelectListItem { Text = "erledigt", Value = "3" });
            ViewBag.Stauts = Stauts;
            var TestType = new List<SelectListItem>();
            TestType.Add(new SelectListItem { Text = "Erstprüfung(Eignungsprüfung)", Value = "0" });
            TestType.Add(new SelectListItem { Text = "Güteprüfung", Value = "1" });
            TestType.Add(new SelectListItem { Text = "Erhärtungsprüfung", Value = "2" });
            TestType.Add(new SelectListItem { Text = "Vergleichsprüfung", Value = "3" });
            TestType.Add(new SelectListItem { Text = "Konformitätsprüfung", Value = "4" });
            TestType.Add(new SelectListItem { Text = "Identitätsprüfung", Value = "5" });
            TestType.Add(new SelectListItem { Text = "Konformitätsprüfung", Value = "6" });
            ViewBag.TestType = TestType;
            //ViewBag.Stauts = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 32), "Id", "Description");
            //ViewBag.TestType = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 33), "Id", "Description");

            return View(lims_concrete_Test);
        }

        // GET: LIMS/lims_concrete_Test/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lims_concrete_Test lims_concrete_Test = db.Lims_concrete_Test.Find(id);

            //lims_concrete_Test.Lims_concrete_ProbDrys.Add(new lims_concrete_ProbDry());
            if (lims_concrete_Test == null)
            {
                return HttpNotFound();
            }
            //var model = db.Lims_concrete_ProbWet.Where(a => a.TestId == id);
            ////lims_concrete_Test.Lims_concrete_ProbWets.Clear();
            //foreach (var usa in model)
            //{
            //    lims_concrete_Test.Lims_concrete_ProbWets.Add(usa);
            //}
            ViewBag.ProbPlaceId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 1), "Id", "Description");

            ViewBag.ProbUserName = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Description", "Description");
            ViewBag.ProbUserId2 = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Description", "Description");
            //ViewBag.ProbPlaceId = db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 1).ToList();
            ViewBag.AirContentId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 2), "Id", "Description");
            ViewBag.WetConcreteDensity = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 3), "Id", "Description");
            ViewBag.ConsistencyId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 4), "Id", "Description");
            ViewBag.WaterContentId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 5), "Id", "Description");
            ViewBag.ContentOfCement = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 6), "Id", "Description");
            ViewBag.WashOutTry = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 7), "Id", "Description");
            ViewBag.ComponentId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 8), "Id", "Description");
            //ProbResults
            ViewBag.CompressionModeId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 9), "Id", "Description");
            ViewBag.ContributionId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 10), "Id", "Description");
            ViewBag.Weather = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 11), "Id", "Description");
            //Parameters
            ViewBag.MaximumGrainSizeId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 12), "Id", "Description");
            ViewBag.AggregateClass = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 13), "Id", "Description");
            ViewBag.GritsNumber = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 14), "Id", "Description");

            //-------------Trocken
            var ProbType = new List<SelectListItem>();
            ProbType.Add(new SelectListItem { Text = "Druckfestigkeit", Value = "Druckfestigkeit" });
            ProbType.Add(new SelectListItem { Text = "Biegezugfestigkeit", Value = "Biegezugfestigkeit" });
            ProbType.Add(new SelectListItem { Text = "Spaltzugfestigkeit", Value = "Spaltzugfestigkeit" });
            ProbType.Add(new SelectListItem { Text = "Wassereindringtiefe", Value = "Wassereindringtiefe" });
            ProbType.Add(new SelectListItem { Text = "Sonstiges", Value = "Sonstiges" });
            ViewBag.ProbType = ProbType;
            ViewBag.TestAgeValue = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 15), "Description", "Description");
            ViewBag.TestAgeType = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 16), "Description", "Description");
            ViewBag.Sharp = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 17), "Description", "Description");
            ViewBag.TestStandard = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 18), "Description", "Description");
            ViewBag.Storage = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 19), "Description", "Description");
            ViewBag.Charges = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 20), "Description", "Description");
            ViewBag.WaterEffect = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 21), "Description", "Description");
            ViewBag.FillingDirection = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 22), "Description", "Description");
            ViewBag.Testname = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 23), "Description", "Description");
            ViewBag.Unit = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 24), "Description", "Description");
            ViewBag.VolDetemining = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 25), "Description", "Description");
            //PCMS.Areas.Admin.Models.AdminContext adminContext = new Admin.Models.AdminContext();
            //ViewBag.ProbUserId = new SelectList(adminContext.AspNetUsers, "Id", "UserName");
            ViewBag.ProbUserId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Description", "Description");
            ViewBag.Status = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 27), "Description", "Description");
            ViewBag.WetStatus = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 28), "Description", "Description");
            ViewBag.Comparison = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 29), "Description", "Description");
            ViewBag.Preperation = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 30), "Description", "Description");
            ViewBag.Reason = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 31), "Description", "Description");

            //ViewBag.Volume = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 32), "Description", "Description");
            var Volume = new List<SelectListItem>();
            Volume.Add(new SelectListItem { Text = "3,375", Value = "3,375" });
            Volume.Add(new SelectListItem { Text = "6,750", Value = "6,750" });
            Volume.Add(new SelectListItem { Text = "8", Value = "8" });
            Volume.Add(new SelectListItem { Text = "10,125", Value = "10,125" });
            Volume.Add(new SelectListItem { Text = "13,500", Value = "13,500" });
            Volume.Add(new SelectListItem { Text = "16,875", Value = "16,875" });
            Volume.Add(new SelectListItem { Text = "20,250", Value = "20,250" });
            ViewBag.Volume = Volume;
            //Betontest
            var Stauts = new List<SelectListItem>();
            Stauts.Add(new SelectListItem { Text = "nicht begonnen", Value = "0" });
            Stauts.Add(new SelectListItem { Text = "zurückgestellt", Value = "1" });
            Stauts.Add(new SelectListItem { Text = "in Bearbeitung", Value = "2" });
            Stauts.Add(new SelectListItem { Text = "erledigt", Value = "3" });
            ViewBag.Stauts = Stauts;
            var TestType = new List<SelectListItem>();
            TestType.Add(new SelectListItem { Text = "Erstprüfung(Eignungsprüfung)", Value = "0" });
            TestType.Add(new SelectListItem { Text = "Güteprüfung", Value = "1" });
            TestType.Add(new SelectListItem { Text = "Erhärtungsprüfung", Value = "2" });
            TestType.Add(new SelectListItem { Text = "Vergleichsprüfung", Value = "3" });
            TestType.Add(new SelectListItem { Text = "Konformitätsprüfung", Value = "4" });
            TestType.Add(new SelectListItem { Text = "Identitätsprüfung", Value = "5" });
            TestType.Add(new SelectListItem { Text = "Konformitätsprüfung", Value = "6" });
            ViewBag.TestType = TestType;

            //ViewBag.Stauts = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 32), "Id", "Description");
            //ViewBag.TestType = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 33), "Id", "Description");



            return View(lims_concrete_Test);
        }

        // POST: LIMS/lims_concrete_Test/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Identity,TestNumber,TestDate,TestType,OrderNumber,OrderClient,SortId,RecipeId,Stauts,Probprofil,IsDeleted,IsActive,Rev,FactoryId,SortName,RecipeName")] lims_concrete_Test lims_concrete_Test, List<lims_concrete_ProbWet> Lims_concrete_ProbWet, List<lims_concrete_ProbDry> Lims_concrete_ProbDry)
        {
         
            //db.Entry(lims_concrete_Test).Collection(st => st.RecipeConcrete).Load();
            //lims_concrete_Test.Lims_concrete_ProbWets.Clear();
            if (ModelState.IsValid)
            {
                db.Entry(lims_concrete_Test).State = EntityState.Modified;
                db.Entry(lims_concrete_Test).Collection(st => st.Lims_concrete_ProbWets).Load();
                db.Entry(lims_concrete_Test).Collection(st => st.Lims_concrete_ProbDrys).Load();
                //---------------Wet Start
                foreach (var i in lims_concrete_Test.Lims_concrete_ProbWets.ToList())
                {
                    if (!Lims_concrete_ProbWet.Any(a => a.Id == i.Id))
                        db.Lims_concrete_ProbWet.Remove(i);
                }
              
                foreach(var u in Lims_concrete_ProbWet)
                {
                    var wetTest = lims_concrete_Test.Lims_concrete_ProbWets.Where(a => a.Id == u.Id).FirstOrDefault();
                    if (wetTest != null)
                    {
                        u.TestId = lims_concrete_Test.Id;
                        db.Entry(wetTest).CurrentValues.SetValues(u);
                        for(int ba=0; ba< wetTest.Lims_concrete_ProbWet_Consistency.Count; ba++)
                        {
                            db.Entry(wetTest.Lims_concrete_ProbWet_Consistency.ElementAt(ba)).CurrentValues.SetValues(u.Lims_concrete_ProbWet_Consistency.ElementAt(ba));
                        }
                    }
                    else
                    {
                        u.TestId = lims_concrete_Test.Id;
                        u.Id = 0;
                        u.Identity = Guid.NewGuid().ToString();
                        lims_concrete_Test.Lims_concrete_ProbWets.Add(u);
                    }
                }
                //-----------------Dry Start
                foreach (var i in lims_concrete_Test.Lims_concrete_ProbDrys.ToList())
                {
                    if (!Lims_concrete_ProbDry.Any(a => a.Id == i.Id))
                        db.Lims_concrete_ProbDry.Remove(i);
                }

                foreach (var u in Lims_concrete_ProbDry)
                {
                    var dryTest = lims_concrete_Test.Lims_concrete_ProbDrys.Where(a => a.Id == u.Id).FirstOrDefault();
                    if (dryTest != null)
                    {
                        u.TestId = lims_concrete_Test.Id;
                        db.Entry(dryTest).CurrentValues.SetValues(u);                        
                    }
                    else
                    {
                        u.TestId = lims_concrete_Test.Id;
                        u.Id = 0;
                        u.Identity = Guid.NewGuid().ToString();
                        lims_concrete_Test.Lims_concrete_ProbDrys.Add(u);
                    }
                }




                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }
            lims_concrete_Test.Lims_concrete_ProbWets = Lims_concrete_ProbWet;
            lims_concrete_Test.Lims_concrete_ProbDrys = Lims_concrete_ProbDry;
            ViewBag.ProbPlaceId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 1), "Id", "Description");
            ViewBag.ProbUserName = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Description", "Description");
            ViewBag.ProbUserId2 = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Description", "Description");
            //ViewBag.ProbPlaceId = db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 1).ToList();
            ViewBag.AirContentId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 2), "Id", "Description");
            ViewBag.WetConcreteDensity = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 3), "Id", "Description");
            ViewBag.ConsistencyId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 4), "Id", "Description");
            ViewBag.WaterContentId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 5), "Id", "Description");
            ViewBag.ContentOfCement = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 6), "Id", "Description");
            ViewBag.WashOutTry = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 7), "Id", "Description");
            ViewBag.ComponentId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 8), "Id", "Description");
            //ProbResults
            ViewBag.CompressionModeId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 9), "Id", "Description");
            ViewBag.ContributionId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 10), "Id", "Description");
            ViewBag.Weather = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 11), "Id", "Description");
            //Parameters
            ViewBag.MaximumGrainSizeId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 12), "Id", "Description");
            ViewBag.AggregateClass = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 13), "Id", "Description");
            ViewBag.GritsNumber = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 14), "Id", "Description");

            //-------------Trocken
            var ProbType = new List<SelectListItem>();
            ProbType.Add(new SelectListItem { Text = "Druckfestigkeit", Value = "Druckfestigkeit" });
            ProbType.Add(new SelectListItem { Text = "Biegezugfestigkeit", Value = "Biegezugfestigkeit" });
            ProbType.Add(new SelectListItem { Text = "Spaltzugfestigkeit", Value = "Spaltzugfestigkeit" });
            ProbType.Add(new SelectListItem { Text = "Wassereindringtiefe", Value = "Wassereindringtiefe" });
            ProbType.Add(new SelectListItem { Text = "Sonstiges", Value = "Sonstiges" });
            ViewBag.ProbType = ProbType;
            ViewBag.TestAgeValue = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 15), "Description", "Description");
            ViewBag.TestAgeType = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 16), "Description", "Description");
            ViewBag.Sharp = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 17), "Description", "Description");
            ViewBag.TestStandard = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 18), "Description", "Description");
            ViewBag.Storage = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 19), "Description", "Description");
            ViewBag.Charges = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 20), "Description", "Description");
            ViewBag.WaterEffect = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 21), "Description", "Description");
            ViewBag.FillingDirection = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 22), "Description", "Description");
            ViewBag.Testname = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 23), "Description", "Description");
            ViewBag.Unit = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 24), "Description", "Description");
            ViewBag.VolDetemining = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 25), "Description", "Description");
            //PCMS.Areas.Admin.Models.AdminContext adminContext = new Admin.Models.AdminContext();
            //ViewBag.ProbUserId = new SelectList(adminContext.AspNetUsers, "Id", "UserName");
            ViewBag.ProbUserId = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 26), "Description", "Description");
            ViewBag.Status = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 27), "Description", "Description");
            ViewBag.WetStatus = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 28), "Description", "Description");
            ViewBag.Comparison = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 29), "Description", "Description");
            ViewBag.Preperation = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 30), "Description", "Description");
            ViewBag.Reason = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 31), "Description", "Description");

            //ViewBag.Volume = new SelectList(db.Lims_concrete_SelectMenu.Where(m => m.MenuId == 32), "Description", "Description");
            var Volume = new List<SelectListItem>();
            Volume.Add(new SelectListItem { Text = "3,375", Value = "3,375" });
            Volume.Add(new SelectListItem { Text = "6,750", Value = "6,750" });
            Volume.Add(new SelectListItem { Text = "8", Value = "8" });
            Volume.Add(new SelectListItem { Text = "10,125", Value = "10,125" });
            Volume.Add(new SelectListItem { Text = "13,500", Value = "13,500" });
            Volume.Add(new SelectListItem { Text = "16,875", Value = "16,875" });
            Volume.Add(new SelectListItem { Text = "20,250", Value = "20,250" });
            ViewBag.Volume = Volume;
            //Betontest
            var Stauts = new List<SelectListItem>();
            Stauts.Add(new SelectListItem { Text = "nicht begonnen", Value = "0" });
            Stauts.Add(new SelectListItem { Text = "zurückgestellt", Value = "1" });
            Stauts.Add(new SelectListItem { Text = "in Bearbeitung", Value = "2" });
            Stauts.Add(new SelectListItem { Text = "erledigt", Value = "3" });
            ViewBag.Stauts = Stauts;
            var TestType = new List<SelectListItem>();
            TestType.Add(new SelectListItem { Text = "Erstprüfung(Eignungsprüfung)", Value = "0" });
            TestType.Add(new SelectListItem { Text = "Güteprüfung", Value = "1" });
            TestType.Add(new SelectListItem { Text = "Erhärtungsprüfung", Value = "2" });
            TestType.Add(new SelectListItem { Text = "Vergleichsprüfung", Value = "3" });
            TestType.Add(new SelectListItem { Text = "Konformitätsprüfung", Value = "4" });
            TestType.Add(new SelectListItem { Text = "Identitätsprüfung", Value = "5" });
            TestType.Add(new SelectListItem { Text = "Konformitätsprüfung", Value = "6" });
            ViewBag.TestType = TestType;


            return View(lims_concrete_Test);
        }

        // GET: LIMS/lims_concrete_Test/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lims_concrete_Test lims_concrete_Test = db.Lims_concrete_Test.Find(id);
            if (lims_concrete_Test == null)
            {
                return HttpNotFound();
            }
            return View(lims_concrete_Test);
        }

        // POST: LIMS/lims_concrete_Test/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            lims_concrete_Test lims_concrete_Test = db.Lims_concrete_Test.Find(id);
            lims_concrete_Test.IsActive = true;
            lims_concrete_Test.IsDeleted = true;
            db.Entry(lims_concrete_Test).State = EntityState.Modified;
            //db.SaveChanges();
            //db.SaveChanges(User.Identity.Name);
            //db.Lims_concrete_Test.Remove(lims_concrete_Test);
            db.SaveChanges(User.Identity.Name);
            return RedirectToAction("Index");
        }
        public ActionResult createTag(string tagName, int menuId)
        {
            lims_concrete_SelectMenu menu = new lims_concrete_SelectMenu()
            {
                MenuId = menuId,
                Description = tagName
            };
            db.Lims_concrete_SelectMenu.Add(menu);
            db.SaveChanges();
            return Json(new { success = true, value = menu.Id });
            
        }
        
        public ActionResult Copy(long IdCopy, string DescriptionCopy, string NumberCopy)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var model = db.Lims_concrete_Test.Include(a => a.Lims_concrete_ProbDrys).Include(a => a.Lims_concrete_ProbWets).Include(a => a.Lims_concrete_ProbWets.Select(u=>u.Lims_concrete_ProbWet_Consistency)).AsNoTracking().FirstOrDefault(a => a.Id == IdCopy);
            db.Configuration.ProxyCreationEnabled = true;
            model.Id = 0;
            foreach(var dry in model.Lims_concrete_ProbDrys)
            {
                dry.Id = 0;
                dry.TestId = 0;
            }
            foreach (var wet in model.Lims_concrete_ProbWets)
            {
                wet.Id = 0;
                wet.TestId = 0;
                
                foreach(var cons in wet.Lims_concrete_ProbWet_Consistency)
                {
                    cons.Id = 0;
                    cons.ProbWetId = 0;
                }
            }
            model.TestNumber = model.TestNumber + "C";
            model.OrderNumber = model.OrderNumber + "C";
            Random rnd = new Random();
            
            model.TestDate = model.TestDate.AddDays(rnd.Next(1, 22));
            db.Lims_concrete_Test.Add(model);
            db.SaveChanges(User.Identity.Name);
            return RedirectToAction("Edit", new { id = model.Id });

        }
        public ActionResult choosLoadingOrder(int? selected, string dateFrom, string dateTo)
        {


            selected = selected ?? 4;
            int day = DateTime.Now.Day;
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            //var modelLoading = db2.Md_order_LoadingOrder.Where(a => a.RegistrationDate.Value.Day == day && a.RegistrationDate.Value.Month == month && a.RegistrationDate.Value.Year == year && a.State == selected).OrderBy(a => a.Sort).ToList();
            var db2 = new Order.Models.OrderContext();
            var modelLoading =   db2.Md_order_LoadingOrder.OrderBy(a => a.RegistrationDate).ToList();
            

            if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                modelLoading = modelLoading.Where(m => m.RegistrationDate != null && m.RegistrationDate.Value.Date >= new DateTime(Convert.ToInt32(dateFrom.Split('-')[0]), Convert.ToInt32(dateFrom.Split('-')[1]), Convert.ToInt32(dateFrom.Split('-')[2])).Date && m.RegistrationDate.Value.Date <= new DateTime(Convert.ToInt32(dateTo.Split('-')[0]), Convert.ToInt32(dateTo.Split('-')[1]), Convert.ToInt32(dateTo.Split('-')[2])).Date).ToList();

            }
            else
            {
                modelLoading = modelLoading.Where(m => m.RegistrationDate != null && m.RegistrationDate.Value.Date == new DateTime(year, month, day).Date).ToList();
            }

            if (selected != null && selected != 0)
                modelLoading = modelLoading.Where(a => a.State == selected).ToList();


            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();

            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Alle", Value = "0" });
            listItems.Add(new SelectListItem { Text = "Gesperrt", Value = "1" });
            listItems.Add(new SelectListItem { Text = "Freigegeben", Value = "2", Selected = true });
            listItems.Add(new SelectListItem { Text = "in Bearbeitung", Value = "3" });
            listItems.Add(new SelectListItem { Text = "beendet", Value = "4" });
            listItems.Add(new SelectListItem { Text = "storniert", Value = "5" });

            ViewBag.SelectState = new SelectList(listItems, "Value", "Text", selected);
            ViewBag.DateFrom = dateFrom ??DateTime.Now.ToString("yyyy-MM-dd");
            ViewBag.DateTo = dateTo;
            return View((modelLoading));
        }
        public ActionResult ReportDetails(int id)
        {
            var model = db.Lims_concrete_Test.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            foreach (var dataset in model.Lims_concrete_ProbWets)
            {           
                ds.List.AddListRow(dataset.ProbNumber, dataset.CustomerName, dataset.ConstructionSiteName, getSelectList( dataset.ComponentId),"" ,"",  "",  "",  "", "", "",  "", "",  "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

            PCMSDataSet ds2 = new PCMSDataSet();
            ds2.List.Clear();
            foreach (var dataset in model.Lims_concrete_ProbWets)
            {
                
                ds2.List.AddListRow(dataset.ProbNumber, dataset.IsDeliveryNoteNo, dataset.DateOfManufacture.ToShortDateString(), dataset.DateOfManufacture.ToShortTimeString(), getSelectList(dataset.ProbPlaceId), dataset.ProbUserName, dataset.ConcreteTemp.ToString(), dataset.AirTemp.ToString(), "", "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport2 = new ReportDataSource("DataSet2", ds2.List.ToList());

            PCMSDataSet ds3 = new PCMSDataSet();
            ds3.List.Clear();
            foreach (var dataset in model.Lims_concrete_ProbWets)
            {
                string[] konsi = new string[4];
                for (int i = 0; i < 4; i++)
                {
                    konsi[i] = dataset.Lims_concrete_ProbWet_Consistency.ElementAt(i).ConsistencyValue != null ? "F" + dataset.Lims_concrete_ProbWet_Consistency.ElementAt(i).Time + "+FM " + Math.Round( dataset.Lims_concrete_ProbWet_Consistency.ElementAt(i).ConsistencyValue ?? 0,0) : "";
                }
                ds3.List.AddListRow(dataset.ProbNumber, dataset.DateOfManufacture.ToShortDateString(),  konsi[0], konsi[1], konsi[2], konsi[3], (dataset.Lims_concrete_ProbWet_Consistency.ElementAt(0).Lp ).ToString() , dataset.Density.ToString(), dataset.WZEQValue.ToString(), "", "", "","" ,"" , "", "", "", "", "", "");
            }
            ReportDataSource DSReport3 = new ReportDataSource("DataSet3", ds3.List.ToList());

            PCMSDataSet ds4 = new PCMSDataSet();
            ds4.List.Clear();
            string testStandard = "";
            string sorage = "";
            string testerName = "";
            foreach (var dataset in model.Lims_concrete_ProbDrys)
            {
                if (!string.IsNullOrEmpty(dataset.TestStandard))
                    testStandard = dataset.TestStandard;
                if (!string.IsNullOrEmpty(dataset.Storage))
                    sorage = dataset.Storage;
                if (!string.IsNullOrEmpty(dataset.ProbUserId))
                    testerName = dataset.ProbUserId;
                ds4.List.AddListRow(dataset.ProbNumber, dataset.TestSpecimenNo, dataset.TestDateTime.ToShortDateString(),  Math.Round(dataset.Length ?? 0,0).ToString() , Math.Round(dataset.Width ?? 0, 0).ToString(), Math.Round(dataset.Height ?? 0, 0).ToString(), Math.Round(dataset.Measures ?? 0, 3).ToString(), dataset.GrossDensity != null ? dataset.GrossDensity.ToString() : "" , Math.Round( dataset.BreakingLoad ?? 0,0).ToString(), dataset.Strength.ToString(), dataset.Strength2.ToString(), "", "", "", "", "", "", "", "", dataset.Sharp + " Prüfalter " + dataset.TestAgeValue + " " + dataset.TestAgeType);
            }
            ReportDataSource DSReport4 = new ReportDataSource("DataSet4", ds4.List.ToList());

            var db2 = new Recipe.Models.RecipeContext();
            var db3 = new Areas.MasterData.Models.MasterDataContext();
            var con = db3.Md_masterData_Material.Where(a => a.Id == model.SortId).FirstOrDefault();
            var conDet = db3.Md_material_SortDetails.Where(a => a.Id == con.SortId).FirstOrDefault();
            var compression = db3.Md_material_CompressionStrenghtGroup.Where(a => a.Id == conDet.CompressionId).Select(a => a.Class).FirstOrDefault();
            var exposure = db3.Md_material_ExposureKombi.Where(a => a.Id == conDet.ExposureKombiId).Select(a => a.ShortText).FirstOrDefault();
            var consistency = db3.Md_material_ConsistencyGroup.Where(a => a.Id == conDet.ConsistencyId).Select(a => a.Class).FirstOrDefault();
            var progress = db3.Md_material_Progress.Where(a => a.Id == conDet.ProgressId).Select(a => a.Description).FirstOrDefault();

            


            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
            repParams.Add(new ReportParameter("MaterialNumber", con.ArticleNumber));
            repParams.Add(new ReportParameter("MaterialName", con.Name));
            repParams.Add(new ReportParameter("Exposition", exposure));
            repParams.Add(new ReportParameter("StrengthClass", compression));
            repParams.Add(new ReportParameter("Consistence", consistency));
            repParams.Add(new ReportParameter("process", progress));
            repParams.Add(new ReportParameter("ProbNorm", testStandard + "\n" + sorage));
            repParams.Add(new ReportParameter("ProbNr", model.TestNumber));
            repParams.Add(new ReportParameter("TesterName", testerName));


            string testType = "";
            if (model.TestType == 0)
                testType = "Erstprüfung(Eignungsprüfung)";
            else if (model.TestType == 1)
                testType = "Güteprüfung";
            else if (model.TestType == 2)
                testType = "Erhärtungsprüfung";
            else if (model.TestType == 3)
                testType = "Vergleichsprüfung";
            else if (model.TestType == 4)
                testType = "Konformitätsprüfung";
            else if (model.TestType == 5)
                testType = "Identitätsprüfung";
            else if (model.TestType == 6)
                testType = "Konformitätsprüfung";


            repParams.Add(new ReportParameter("ProbType", testType));
            //repParams.Add(new ReportParameter("RecipeNumber", loadingOrder.RecipeNumber));
            //repParams.Add(new ReportParameter("LotAmount", loadingOrder.OrderedQuantity != null ? loadingOrder.OrderedQuantity.ToString() : "-"));
            //repParams.Add(new ReportParameter("WaterContent", Convert.ToString(waterSum)));
            //repParams.Add(new ReportParameter("BackAmount", Convert.ToString(loadingOrder.BackAmount ?? 0)));
            //repParams.Add(new ReportParameter("Customer", loadingOrder.CustomerNumber + ", " + loadingOrder.CustomerName));
            //repParams.Add(new ReportParameter("ConstructionSite", loadingOrder.ConstructionSiteNumber + ", " + loadingOrder.ConstructionSiteDescription));
            //repParams.Add(new ReportParameter("aggregateSum", Convert.ToString(aggreSum)));
            //if (waterSum != 0 && binderSum != 0)
            //    repParams.Add(new ReportParameter("wz", Math.Round((waterSum / binderSum), 2).ToString()));



            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/LIMS/ConcreteTestDetail.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.DataSources.Add(DSReport2);
            reportViewer.LocalReport.DataSources.Add(DSReport3);
            reportViewer.LocalReport.DataSources.Add(DSReport4);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;

            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);

            //ViewBag.ReportViewer = reportViewer;
            //return View();
        }
        public ActionResult NextTests()
        {
            //var test = db.Lims_concrete_Test.Where(a => a.Lims_concrete_ProbDrys.Any(b => b.BreakingLoad != null) && a.IsDeleted == false).ToList();
            var dry = db.Lims_concrete_ProbDry.Where(a => a.BreakingLoad == null ).OrderBy(u => u.TestDateTime).ToList();
            for (int i = dry.Count - 1; i >= 0; i--)
            {
                if (db.Lims_concrete_Test.Find(dry.ElementAt(i).TestId).IsDeleted == true)
                {
                    dry.RemoveAt(i);
                }
            }
            return View(dry);
        }
        public string getSelectList(long? id)
        {
            if (id == null)
                return "";
            return db.Lims_concrete_SelectMenu.Find(id).Description;


        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
