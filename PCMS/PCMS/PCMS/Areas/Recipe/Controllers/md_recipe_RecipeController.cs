﻿using PCMS.Areas.LIMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using PagedList;
using PCMS.Areas.Recipe.Models;
using PCMS.Models;
using PCMS.Helper;
using System.Net;
using Microsoft.Reporting.WebForms;

namespace PCMS.Areas.Recipe.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_recipe_RecipeController : Controller
    {
        private Models.RecipeContext db = new Models.RecipeContext();
        //private Sieve.Models.SieveContext db2 = new Sieve.Models.SieveContext();
        private LIMS.Models.LimsContext db3 = new LIMS.Models.LimsContext();
        // GET: Recipe/md_recipe_SieveLine

        public ActionResult Index(PaginationModel pg)
        {
            //var model = db.Md_recipe_Recipe.Where(m => m.IsDeleted == false ).Include(m => m.Lims_gradingCurv_GradingCurv).OrderBy(a => a.Number).ToList();
            var model = db.Md_recipe_Recipe.Where(m => m.IsDeleted == false && m.RecipeTypeId == 1).Include(m => m.Lims_gradingCurv_GradingCurv).OrderBy(a => a.Number).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    pgFF.colVal = pgFF.colVal.ToLower();
                    switch (pgFF.colName)
                    {
                        case "Description":
                            model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                            ViewBag.Description = pgFF.colVal;
                            break;
                        case "Number":
                            model = model.Where(m => m.Number != null && m.Number.ToLower().Contains(pgFF.colVal)).ToList();
                            ViewBag.Number = pgFF.colVal;
                            break;
                        case "GradingCurvId":
                            model = model.Where(m => m.GradingCurvId == Convert.ToInt64(pgFF.colVal)).ToList();
                            ViewBag.GradingCurvId = pgFF.colVal;
                            break;
                        case "RecipeTypeId":
                            model = model.Where(m => m.RecipeTypeId == Convert.ToInt64(pgFF.colVal)).ToList();
                            ViewBag.RecipeTypeId = pgFF.colVal;
                            break;
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
                case "Number":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Number).ToList();
                    else
                        model = model.OrderByDescending(m => m.Number).ToList();
                    break;
                case "NumberEDV":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.NumberEDV).ToList();
                    else
                        model = model.OrderByDescending(m => m.NumberEDV).ToList();
                    break;
                case "GradingCurvId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.GradingCurvId).ToList();
                    else
                        model = model.OrderByDescending(m => m.GradingCurvId).ToList();
                    break;
                case "MaterialId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialId).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialId).ToList();
                    break;
                case "RecipeTypeId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RecipeTypeId).ToList();
                    else
                        model = model.OrderByDescending(m => m.RecipeTypeId).ToList();
                    break;
                case "AirContent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.AirContent).ToList();
                    else
                        model = model.OrderByDescending(m => m.AirContent).ToList();
                    break;
                case "ConsistencySetPoint":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ConsistencySetPoint).ToList();
                    else
                        model = model.OrderByDescending(m => m.ConsistencySetPoint).ToList();
                    break;
                case "MaxWCValue":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaxWCValue).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaxWCValue).ToList();
                    break;
                case "HotWater":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.HotWater).ToList();
                    else
                        model = model.OrderByDescending(m => m.HotWater).ToList();
                    break;
                case "ActualValueTolerance":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ActualValueTolerance).ToList();
                    else
                        model = model.OrderByDescending(m => m.ActualValueTolerance).ToList();
                    break;
                case "SetPointDeviation":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SetPointDeviation).ToList();
                    else
                        model = model.OrderByDescending(m => m.SetPointDeviation).ToList();
                    break;
                case "MixerFilling":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MixerFilling).ToList();
                    else
                        model = model.OrderByDescending(m => m.MixerFilling).ToList();
                    break;
                case "WasteWaterFactor":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WasteWaterFactor).ToList();
                    else
                        model = model.OrderByDescending(m => m.WasteWaterFactor).ToList();
                    break;
                case "BatchProgram":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.BatchProgram).ToList();
                    else
                        model = model.OrderByDescending(m => m.BatchProgram).ToList();
                    break;
                case "FillMonitoringCement":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FillMonitoringCement).ToList();
                    else
                        model = model.OrderByDescending(m => m.FillMonitoringCement).ToList();
                    break;
                case "FillRating":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FillRating).ToList();
                    else
                        model = model.OrderByDescending(m => m.FillRating).ToList();
                    break;
                case "CementEmptyingDelay":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CementEmptyingDelay).ToList();
                    else
                        model = model.OrderByDescending(m => m.CementEmptyingDelay).ToList();
                    break;
                case "WaterEmptyingDelay":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WaterEmptyingDelay).ToList();
                    else
                        model = model.OrderByDescending(m => m.WaterEmptyingDelay).ToList();
                    break;
                case "AdditiveEmptyingDelay":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.AdditiveEmptyingDelay).ToList();
                    else
                        model = model.OrderByDescending(m => m.AdditiveEmptyingDelay).ToList();
                    break;
                case "FoamAdditionTime":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FoamAdditionTime).ToList();
                    else
                        model = model.OrderByDescending(m => m.FoamAdditionTime).ToList();
                    break;
                case "MixingTime":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MixingTime).ToList();
                    else
                        model = model.OrderByDescending(m => m.MixingTime).ToList();
                    break;
                case "EmptyingPartlyOpen1":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.EmptyingPartlyOpen1).ToList();
                    else
                        model = model.OrderByDescending(m => m.EmptyingPartlyOpen1).ToList();
                    break;
                case "EmptyingPartlyOpen2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.EmptyingPartlyOpen2).ToList();
                    else
                        model = model.OrderByDescending(m => m.EmptyingPartlyOpen2).ToList();
                    break;
                case "EmptyingOpen":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.EmptyingOpen).ToList();
                    else
                        model = model.OrderByDescending(m => m.EmptyingOpen).ToList();
                    break;
                case "SuitabilityGroupId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SuitabilityGroupId).ToList();
                    else
                        model = model.OrderByDescending(m => m.SuitabilityGroupId).ToList();
                    break;
                case "ConcreteFamilyId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ConcreteFamilyId).ToList();
                    else
                        model = model.OrderByDescending(m => m.ConcreteFamilyId).ToList();
                    break;
                case "ExposureGroupId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ExposureGroupId).ToList();
                    else
                        model = model.OrderByDescending(m => m.ExposureGroupId).ToList();
                    break;
                case "MaterialGroupId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialGroupId).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialGroupId).ToList();
                    break;
                case "MasterUnit":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MasterUnit).ToList();
                    else
                        model = model.OrderByDescending(m => m.MasterUnit).ToList();
                    break;
                case "AirVoidContent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.AirVoidContent).ToList();
                    else
                        model = model.OrderByDescending(m => m.AirVoidContent).ToList();
                    break;
                case "WaterContent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WaterContent).ToList();
                    else
                        model = model.OrderByDescending(m => m.WaterContent).ToList();
                    break;
                case "ContentOfCement":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ContentOfCement).ToList();
                    else
                        model = model.OrderByDescending(m => m.ContentOfCement).ToList();
                    break;
                case "Additive":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Additive).ToList();
                    else
                        model = model.OrderByDescending(m => m.Additive).ToList();
                    break;
                case "SolidMaterialrest":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SolidMaterialrest).ToList();
                    else
                        model = model.OrderByDescending(m => m.SolidMaterialrest).ToList();
                    break;
                case "Aggregate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Aggregate).ToList();
                    else
                        model = model.OrderByDescending(m => m.Aggregate).ToList();
                    break;
                case "AggregateFixed":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.AggregateFixed).ToList();
                    else
                        model = model.OrderByDescending(m => m.AggregateFixed).ToList();
                    break;
                case "SurfaceDamp":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SurfaceDamp).ToList();
                    else
                        model = model.OrderByDescending(m => m.SurfaceDamp).ToList();
                    break;
                case "AdditionWater":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.AdditionWater).ToList();
                    else
                        model = model.OrderByDescending(m => m.AdditionWater).ToList();
                    break;
                case "WetConcreteDensity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WetConcreteDensity).ToList();
                    else
                        model = model.OrderByDescending(m => m.WetConcreteDensity).ToList();
                    break;
                case "MealGrain":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MealGrain).ToList();
                    else
                        model = model.OrderByDescending(m => m.MealGrain).ToList();
                    break;
                case "MealFineGrain":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MealFineGrain).ToList();
                    else
                        model = model.OrderByDescending(m => m.MealFineGrain).ToList();
                    break;
                case "MortarContent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MortarContent).ToList();
                    else
                        model = model.OrderByDescending(m => m.MortarContent).ToList();
                    break;
                case "ClementPaste":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ClementPaste).ToList();
                    else
                        model = model.OrderByDescending(m => m.ClementPaste).ToList();
                    break;
                case "WZValue":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WZValue).ToList();
                    else
                        model = model.OrderByDescending(m => m.WZValue).ToList();
                    break;
                case "WZValueEq":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WZValueEq).ToList();
                    else
                        model = model.OrderByDescending(m => m.WZValueEq).ToList();
                    break;
                case "Price":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Price).ToList();
                    else
                        model = model.OrderByDescending(m => m.Price).ToList();
                    break;
            }
            ViewBag.GradingCurvId = new SelectList(db3.lims_gradingCurv_GradingCurv, "Id", "Description", ViewBag.GradingCurvId);

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Definitions.pageSize));
        }
        public ActionResult Create()
        {
            ViewBag.GradingCurvId = new SelectList(db3.lims_gradingCurv_GradingCurv.Where(a => a.IsDeleted == false), "Id", "Description");
            //ViewBag.GradingCurvId = new SelectList(db3.lims_gradingCurv_GradingCurv.Where(a=> a.Id == 0), "Id", "Description");
            ViewBag.MaterialId = new SelectList(db.Md_masterData_Material.Where(a => a.MaterialGroupId == 5), "Id", "Name");
            ViewBag.GradingCurvSieveRangeId = new SelectList(db3.Md_sieve_RuleGradingCurve, "Id", "Description");
            ViewBag.RecipeTypeId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Description, Number, GradingCurvId, MasterUnit, AirVoidContent, WaterContent, ContentOfCement, Additive, SolidMaterialrest, Aggregate, AggregateFixed, SurfaceDamp, AdditionWater, WetConcreteDensity, MealGrain, MealFineGrain, MortarContent, ClementPaste, Price, MixingTime, InitialProduction_0_Count, InitialProduction_0_Val, InitialProduction_1_Count, InitialProduction_1_Val, SteadyProduction_0_Count, SteadyProduction_0_Val, SteadyProduction_1_Count, SteadyProduction_1_Val, StrengthAfter2, StrengthAfter7, StrengthAfter28, StrengthAfter56, StrengthAfter90, HotWater, ActualValueTolerance, SetPointDeviation, MixerFilling, WasteWaterFactor, FillMonitoringCement, FillRating, CementEmptyingDelay, WaterEmptyingDelay, AdditiveEmptyingDelay, FoamAdditionTime, MixingTime, EmptyingPartlyOpen1, EmptyingPartlyOpen2, EmptyingOpen, ConsistencySetPoint, MaxWCValue, GradingCurvSieveRangeId")] md_recipe_Recipe md_recipe_Recipe, List<string> materialId, List<string> sort)
        {
            if (ModelState.IsValid)
            {
                int i = 0;
                foreach (string s in materialId)
                {
                    md_recipe_Recipe.RecipeMaterials.Add(new md_recipe_RecipeMaterial() { RecipeId = md_recipe_Recipe.Id, MaterialId = Convert.ToInt64(s.Split('|')[0]), Weight = Convert.ToDecimal(s.Split('|')[1]), Value = Convert.ToDecimal(s.Split('|')[2]), Unit = Convert.ToInt32(s.Split('|')[3]),Sort = i,Type = Convert.ToInt32(s.Split('|')[4]), FunctionType = Convert.ToInt32(s.Split('|')[5]) } );
                    i++;
                }
                if (sort != null)
                {
                    foreach (string s in sort)
                    {
                        md_recipe_Recipe.RecipeConcrete.Add(new md_material_RecipeMaterial() { RecipeId = md_recipe_Recipe.Id, MaterialId = Convert.ToInt64(s.Split('|')[0]), Valid = Convert.ToInt32(s.Split('|')[1]), Active = Convert.ToBoolean(s.Split('|')[2]) });
                    }
                }
                md_recipe_Recipe.RecipeTypeId = 1;
                md_recipe_Recipe.IsDeleted = false;
                md_recipe_Recipe.IsActive = true;
                db.Md_recipe_Recipe.Add(md_recipe_Recipe);
                //db.lims_gradingCurv_GradingCurv.Add(lims_gradingCurv_GradingCurv);
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }
            //ViewBag.SieveRangeId = new SelectList(db.Md_sieve_RuleGradingCurve, "Id", "Description");
            //ViewBag.GradingCurvId = new SelectList(db3.lims_gradingCurv_GradingCurv, "Id", "Description");
            //ViewBag.RecipeTypeId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };

            ViewBag.GradingCurvId = new SelectList(db3.lims_gradingCurv_GradingCurv.Where(a => a.Id == 0), "Id", "Description");
            ViewBag.MaterialId = new SelectList(db.Md_masterData_Material.Where(a => a.MaterialGroupId == 5), "Id", "Name");
            ViewBag.GradingCurvSieveRangeId = new SelectList(db3.Md_sieve_RuleGradingCurve, "Id", "Description", md_recipe_Recipe.GradingCurvSieveRangeId);
            ViewBag.RecipeTypeId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };

            return View(md_recipe_Recipe);
        }
        public ActionResult Edit(int id)
        {
            var model = db.Md_recipe_Recipe.Include(m => m.RecipeMaterials).Include(m => m.RecipeConcrete).FirstOrDefault(m => m.Id == id);

            
            
            List<RecipeM.Models.RecipeModelMMaterialDetails> de = new List<RecipeM.Models.RecipeModelMMaterialDetails>();
            foreach(var item in model.RecipeMaterials.Where(a => a.Type == 3).ToList()) { 
            
                var m = db.Md_recipe_Recipe.Find(item.MaterialId);
                de.Add(new RecipeM.Models.RecipeModelMMaterialDetails { Number = m.Number, Description = m.Description, Deliverer = "--", density = "--", IsDeleted = m.IsDeleted ?? false });
            }
            ViewBag.Details = de;
            //model.RecipeMaterials = model.RecipeMaterials.Where(a => a.Type != 3).ToList();
            model.GradingCurvSieveRangeId = 11;
            model.RecipeMaterials = model.RecipeMaterials.OrderBy(m => m.Sort).ToList();
            ViewBag.GradingCurvId = new SelectList(db3.lims_gradingCurv_GradingCurv.Where(a=>a.Id == model.GradingCurvId) , "Id", "SieveLineNumber", model.GradingCurvId);
            //ViewBag.MaterialId = new SelectList(db.Md_masterData_Material.Where(a => a.MaterialGroupId == 5), "Id", "Name");
            ViewBag.GradingCurvSieveRangeId = new SelectList(db3.Md_sieve_RuleGradingCurve, "Id", "Description",model.GradingCurvSieveRangeId);
            ViewBag.RecipeTypeId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description, Number, GradingCurvId, MasterUnit, AirVoidContent, WaterContent, ContentOfCement, Additive, SolidMaterialrest, Aggregate, AggregateFixed, SurfaceDamp, AdditionWater, WetConcreteDensity, MealGrain, MealFineGrain, MortarContent, ClementPaste, Price, MixingTime, InitialProduction_0_Count, InitialProduction_0_Val, InitialProduction_1_Count, InitialProduction_1_Val, SteadyProduction_0_Count, SteadyProduction_0_Val, SteadyProduction_1_Count, SteadyProduction_1_Val, StrengthAfter2, StrengthAfter7, StrengthAfter28, StrengthAfter56, StrengthAfter90, HotWater, ActualValueTolerance, SetPointDeviation, MixerFilling, WasteWaterFactor, FillMonitoringCement, FillRating, CementEmptyingDelay, WaterEmptyingDelay, AdditiveEmptyingDelay, FoamAdditionTime, MixingTime, EmptyingPartlyOpen1, EmptyingPartlyOpen2, EmptyingOpen, ConsistencySetPoint, MaxWCValue, GradingCurvSieveRangeId")] md_recipe_Recipe md_recipe_Recipe, List<string> materialId, List<string> sort)
        {
            foreach(var e in ModelState.Where(a => a.Value.Errors.Count() > 0).ToList()){

                var s = e.Key + " " + e.Value.Value.RawValue.ToString();
            }

            if (ModelState.IsValid)
            {
                db.Entry(md_recipe_Recipe).State = EntityState.Modified;
                db.Entry(md_recipe_Recipe).Collection(st => st.RecipeMaterials).Load();
                db.Entry(md_recipe_Recipe).Collection(st => st.RecipeConcrete).Load();
                md_recipe_Recipe.RecipeMaterials.Clear();
                md_recipe_Recipe.RecipeConcrete.Clear();
                int i = 0;
                foreach (string s in materialId)
                {
                    md_recipe_Recipe.RecipeMaterials.Add(new md_recipe_RecipeMaterial() { RecipeId = md_recipe_Recipe.Id, MaterialId = Convert.ToInt64(s.Split('|')[0]), Weight = Convert.ToDecimal(s.Split('|')[1]), Value = Convert.ToDecimal(s.Split('|')[2]), Unit = Convert.ToInt32(s.Split('|')[3]),Sort = i, Type = Convert.ToInt32(s.Split('|')[4]), FunctionType = Convert.ToInt32(s.Split('|')[5]) });
                    i++;
                }
                if (sort != null)
                {
                    foreach (string s in sort)
                    {
                        md_recipe_Recipe.RecipeConcrete.Add(new md_material_RecipeMaterial() { RecipeId = md_recipe_Recipe.Id, MaterialId = Convert.ToInt64(s.Split('|')[0]), Valid = Convert.ToInt32(s.Split('|')[1]), Active = Convert.ToBoolean(s.Split('|')[2]) });
                    }
                }
                md_recipe_Recipe.IsDeleted = false;
                md_recipe_Recipe.IsActive = true;
                md_recipe_Recipe.RecipeTypeId = 1;
                //db.Md_recipe_Recipe.Add(md_recipe_Recipe);
                //db.lims_gradingCurv_GradingCurv.Add(lims_gradingCurv_GradingCurv);
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }
            //ViewBag.SieveRangeId = new SelectList(db.Md_sieve_RuleGradingCurve, "Id", "Description");
            ViewBag.GradingCurvId = new SelectList(db3.lims_gradingCurv_GradingCurv.Where(a => a.Id == md_recipe_Recipe.GradingCurvId), "Id", "SieveLineNumber", md_recipe_Recipe.GradingCurvId).ToList();
            //ViewBag.GradingCurvId = new SelectList(db3.lims_gradingCurv_GradingCurv, "Id", "Description");
            ViewBag.GradingCurvSieveRangeId = new SelectList(db3.Md_sieve_RuleGradingCurve, "Id", "Description", md_recipe_Recipe.GradingCurvSieveRangeId);
            ViewBag.RecipeTypeId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };
            return View(md_recipe_Recipe);
        }
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = db.Md_recipe_Recipe.Include(m => m.RecipeMaterials).Include(m => m.RecipeConcrete).FirstOrDefault(m => m.Id == id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            var model = db.Md_recipe_Recipe.Find(id);
            model.IsActive = true;
            model.RecipeTypeId = 1;
            model.IsDeleted = true;
            db.Entry(model).State = EntityState.Modified;
            //db.Md_recipe_Recipe.Remove(model);
            db.SaveChanges(User.Identity.Name);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Copy(long IdCopy, string DescriptionCopy, string NumberCopy)
        {
            var model = db.Md_recipe_Recipe.Include(a => a.RecipeMaterials).Include(a => a.RecipeConcrete).AsNoTracking().FirstOrDefault(a => a.Id == IdCopy);
            model.Id = 0;
            model.Number = NumberCopy;
            model.Description = DescriptionCopy;
            db.Md_recipe_Recipe.Add(model);
            db.SaveChanges(User.Identity.Name);
            return RedirectToAction("Edit", new { id = model.Id });

        }
        public ActionResult getGradingCurv(int id)
        {
            //db3.Configuration.ProxyCreationEnabled = false;
            List<lims_aggregate_Test> tests = new List<lims_aggregate_Test>();
            var model = db.Lims_gradingCurv_GradingCurv.FirstOrDefault(a => a.Id == id);
            model.Lims_gradingCurvMaterial = model.Lims_gradingCurvMaterial.OrderBy(s => s.Sort).ToList();
            {
                foreach (var r in model.Lims_gradingCurvMaterial)
                {
                    tests.Add(db3.Lims_aggregate_Test.Where(t => t.MaterialId == r.MaterialId).FirstOrDefault());
                }
            }



            return Json(new { data1 = model, data2 = tests });
        }
        public ActionResult choosMaterial(PCMS.Models.PaginationModel pg)
        {

            //var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId != 1 && m.MaterialGroupId != 5).OrderBy(a => a.Id).ToList();
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId != 5).OrderBy(a => a.Id).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Name = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => (m.ShortName ?? "").ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ShortName = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ArticleNumber = pgFF.colVal;
                                break;
                            case "MaterialGroupId":
                                model = model.Where(m => m.MaterialGroupId == Convert.ToInt64(pgFF.colVal)).ToList();
                                ViewBag.MaterialGroupId = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }
            ViewBag.MaterialGroup = new SelectList(db.Md_material_MaterialGroup.Where(a => a.Id != 1 && a.Id != 5), "Id", "Description", ViewBag.MaterialGroupId);
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
        public ActionResult choosConcrete(PCMS.Models.PaginationModel pg)
        {

            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 5).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Name = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => (m.ShortName ?? "").ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ShortName = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ArticleNumber = pgFF.colVal;
                                break;
                            case "MaterialGroupId":
                                model = model.Where(m => m.MaterialGroupId == Convert.ToInt64(pgFF.colVal)).ToList();
                                ViewBag.MaterialGroupId = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }
            ViewBag.MaterialGroup = new SelectList(db.Md_material_MaterialGroup.Where(a => a.Id != 1 && a.Id != 5), "Id", "Description", ViewBag.MaterialGroupId);
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
        public ActionResult getMaterialById(int id)
        {
            return Json(db.Md_masterData_Material.Find(id));
        }
        public ActionResult gradingCurvsJson(string q)
        {
            // Änderungen gemacht von Hakan Matzir am 25.02.2016 Start
            //var model = db.Lims_gradingCurv_GradingCurv.Select(a => new { a.Description, a.SieveLineNumber, a.Id }).OrderBy(a=> a.SieveLineNumber).ToList();
            var model = db.Lims_gradingCurv_GradingCurv.Where(a => a.IsDeleted == false).Select(a => new { a.Description, a.SieveLineNumber, a.Id }).OrderBy(a => a.SieveLineNumber).ToList();
            // Änderungen gemacht von Hakan Matzir am 25.02.2016 Ende

            if (!String.IsNullOrEmpty(q))
                model = model.Where(a => a.SieveLineNumber.Contains(q)).ToList();
            return Json(new { res = model });
        }
        public ActionResult ReportDetail(long? Id)
        {
            try
            {
                //var model = db.Md_recipe_Recipe.Find(Id);
                var model = db.Md_recipe_Recipe.Include(m => m.RecipeMaterials).Include(m => m.RecipeConcrete).FirstOrDefault(m => m.Id == Id);
                Areas.RecipeM.Models.RecipeMContext dbM = new RecipeM.Models.RecipeMContext();
                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();

                int i = 1;
                decimal allVal = 0;
                foreach (var dataset in model.RecipeMaterials)
                {

                    if (dataset.Type == 1)
                    {
                        var m = db.Md_masterData_Material.Find(dataset.MaterialId);
                        if (dataset.Type == 1)
                        {
                            allVal += dataset.Value;
                        }
                        var vol = Math.Round(((dataset.Weight / m.Density) ?? 0), 2).ToString();
                        var fix = dataset.Weight;
                        if (m.MaterialGroupId == 4 && dataset.Unit == 1)
                        {
                            var waterSum = model.RecipeMaterials.Where(a => a.Type == 1 && a.Md_material_Material.MaterialGroupId == 4 && a.Unit == 0).Sum(a => a.Value);
                            vol = Math.Round(((dataset.Weight /( m.Density - 2))  * -1 ?? 0), 2).ToString();
                            fix = Math.Floor(dataset.Weight -(waterSum * (dataset.Value / 100) ));
                        }
                        if (m.MaterialGroupId == 4 && dataset.Unit == 0)
                        {
                            
                            fix = 0;
                        }

                        ds.List.AddListRow(i.ToString(), m.MaterialNumber, m.Name, dataset.Unit == 0 ? "kg" : "%"  , Math.Round( dataset.Value,2 ).ToString(), Math.Round(m.Density ?? 0,2).ToString() ?? "", vol ,(dataset.Weight).ToString(), fix.ToString(), "", "", "", "", "", "", "", "", "", "", "");
                    }
                    else if (dataset.Type == 2)
                    {
                        var m = dbM.Md_equipment.Find(dataset.MaterialId);


                        string functType = "";

                        ds.List.AddListRow(i.ToString(), m.Number.ToString(), m.Description, functType, dataset.Value.ToString(), "--", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                    else if (dataset.Type == 3)
                    {
                        var m = db.Md_recipe_Recipe.Find(dataset.MaterialId);


                        string functType = "";

                        ds.List.AddListRow(i.ToString(), m.Number, m.Description, functType, "--", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }


                    i++;
                }
                ReportDataSource DSReport = new ReportDataSource("Materialien", ds.List.ToList());

                //ReportDataSource DSReport = new ReportDataSource("ds", ds.RuleGradingCurve.ToList());

                var db2 = new MasterData.Models.MasterDataContext();
                long sortId = model.RecipeConcrete.FirstOrDefault().Md_material_Material.SortId ?? 0;
                var wz = db2.Md_material_SortDetails.Where(a => a.Id == sortId ).Select(a => a.WZValue).FirstOrDefault();

                List<ReportParameter> repParams = new List<ReportParameter>();

                repParams.Add(new ReportParameter("RezeptName", model.Description));
                repParams.Add(new ReportParameter("RezeptNummer", model.Number));
                repParams.Add(new ReportParameter("MinCharge", model.MinCharge != null ? model.MinCharge.ToString() : "-"));
                repParams.Add(new ReportParameter("MaxCharge", model.MaxCharge != null ? model.MaxCharge.ToString() : "-"));
                repParams.Add(new ReportParameter("MixingTime", model.MixingTime != null ? model.MixingTime.ToString() : "-"));
                repParams.Add(new ReportParameter("SieveLine", model.Lims_gradingCurv_GradingCurv != null ? model.Lims_gradingCurv_GradingCurv.SieveLineNumber : "-"));
                repParams.Add(new ReportParameter("wz", wz != null ? wz.ToString() : "-"));
                repParams.Add(new ReportParameter("wastedWater", (model.WasteWaterFactor ?? 0).ToString() ));
                repParams.Add(new ReportParameter("AirContent", (model.AirVoidContent).ToString()));
                repParams.Add(new ReportParameter("MealGrain", (model.MealGrain).ToString()));
                repParams.Add(new ReportParameter("ClementPaste", (model.ClementPaste).ToString()));


                repParams.Add(new ReportParameter("FabrikatNummer", model.RecipeConcrete.FirstOrDefault() != null ? model.RecipeConcrete.FirstOrDefault().Md_material_Material.MaterialNumber : "-"));
                repParams.Add(new ReportParameter("FabrikatName", model.RecipeConcrete.FirstOrDefault() != null ? model.RecipeConcrete.FirstOrDefault().Md_material_Material.Name : "-"));
                repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
                
                
                repParams.Add(new ReportParameter("Sum", allVal.ToString()));
                //repParams.Add(new ReportParameter("materialDescription", model.Md_material_Material.Name));
                //repParams.Add(new ReportParameter("materialCategory", model.Md_material_Material.MaterialTypeId.ToString()));
                //repParams.Add(new ReportParameter("producerName", "fehlt"));
                //repParams.Add(new ReportParameter("producerStreet", "fehlt"));
                //repParams.Add(new ReportParameter("producerZIP", "fehlt"));
                //repParams.Add(new ReportParameter("producerCity", "fehlt"));
                //repParams.Add(new ReportParameter("deliveryNoteNo", model.DeliveryNoteNo));
                //repParams.Add(new ReportParameter("delivererName", "fehlt"));
                //repParams.Add(new ReportParameter("delivererStreet", "fehlt"));
                //repParams.Add(new ReportParameter("delivererZIP", "fehlt"));
                //repParams.Add(new ReportParameter("delivererCity", "fehlt"));
                //repParams.Add(new ReportParameter("probeTakingDate", model.ProbTakingDatetime.Value.ToShortDateString()));
                //repParams.Add(new ReportParameter("probePlace", model.ProbPlaceId.ToString()));
                //repParams.Add(new ReportParameter("probePlace", model.ProbPlaceDetail.ToString()));
                //repParams.Add(new ReportParameter("probeTaker", model.UserId.ToString()));
                //repParams.Add(new ReportParameter("probeType", model.ProbTypeId.ToString()));
                //repParams.Add(new ReportParameter("probeQuantity", model.Quantity.ToString()));
                //repParams.Add(new ReportParameter("probeEnteringDate", model.ProbEnteringDatetime.Value.ToShortDateString()));
                //repParams.Add(new ReportParameter("sieveSet", model.SievSetId.ToString()));
                //repParams.Add(new ReportParameter("kValue", model.KValue.ToString()));
                //repParams.Add(new ReportParameter("dValue", model.DValue.ToString()));


                string SievesList = "";
                //for (int i = 0; i < Sieves.Count; i++)
                //{
                //    SievesList += Sieves[i].ToString() + ";";
                //}
                //for (int i = 0; i < (17 - Sieves.Count); i++)
                //{
                //    SievesList += "-;";
                //}

                //repParams.Add(new ReportParameter("SievesList", SievesList));

                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/Recipe/RecipeMDetail.rdlc";
                //reportViewer.ShowPrintButton = false;
                //reportViewer.LocalReport.DataSources.Add(DSReportSieves);
                //reportViewer.LocalReport.DataSources.Add(DSReportA);
                //reportViewer.LocalReport.DataSources.Add(DSReportB);
                //reportViewer.LocalReport.DataSources.Add(DSReportC);
                //reportViewer.LocalReport.DataSources.Add(DSReportU);

                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");
            }
            catch (Exception e)
            {
                PCMS.Helper.ExceptionHelper.LogException(e, User.Identity.Name);
                return View("~/Views/Shared/Exception", e);
            }
        }
        public ActionResult ReportDetail2(long? Id)
        {
            try
            {
                //var model = db.Md_recipe_Recipe.Find(Id);
                var model = db.Md_recipe_Recipe.Include(m => m.RecipeMaterials).Include(m => m.RecipeConcrete).FirstOrDefault(m => m.Id == Id);
                Areas.RecipeM.Models.RecipeMContext dbM = new RecipeM.Models.RecipeMContext();
                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();

                int l = 1;
                decimal allVal = 0;
                foreach (var dataset in model.RecipeMaterials)
                {

                    if (dataset.Type == 1)
                    {
                        var m = db.Md_masterData_Material.Find(dataset.MaterialId);
                        if (dataset.Type == 1)
                        {
                            allVal += dataset.Value;
                        }
                        var vol = Math.Round(((dataset.Weight / m.Density) ?? 0), 2).ToString();
                        var fix = dataset.Weight;
                        if (m.MaterialGroupId == 4 && dataset.Unit == 1)
                        {
                            var waterSum = model.RecipeMaterials.Where(a => a.Type == 1 && a.Md_material_Material.MaterialGroupId == 4 && a.Unit == 0).Sum(a => a.Value);
                            vol = Math.Round(((dataset.Weight / (m.Density - 2)) * -1 ?? 0), 2).ToString();
                            fix = Math.Floor(dataset.Weight - (waterSum * (dataset.Value / 100)));
                        }
                        if (m.MaterialGroupId == 4 && dataset.Unit == 0)
                        {

                            fix = 0;
                        }

                        ds.List.AddListRow(l.ToString(), m.MaterialNumber, m.Name, dataset.Unit == 0 ? "kg" : "%", Math.Round(dataset.Value, 2).ToString(), Math.Round(m.Density ?? 0, 2).ToString() ?? "", vol, (dataset.Weight).ToString(), fix.ToString(), "", "", "", "", "", "", "", "", "", "", "");
                    }
                    else if (dataset.Type == 2)
                    {
                        var m = dbM.Md_equipment.Find(dataset.MaterialId);


                        string functType = "";

                        ds.List.AddListRow(l.ToString(), m.Number.ToString(), m.Description, functType, dataset.Value.ToString(), "--", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                    else if (dataset.Type == 3)
                    {
                        var m = db.Md_recipe_Recipe.Find(dataset.MaterialId);


                        string functType = "";

                        ds.List.AddListRow(l.ToString(), m.Number, m.Description, functType, "--", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }


                    l++;
                }
                ReportDataSource DSReport = new ReportDataSource("Materialien", ds.List.ToList());

                //ReportDataSource DSReport = new ReportDataSource("ds", ds.RuleGradingCurve.ToList());

                var db2 = new MasterData.Models.MasterDataContext();
                long sortId = model.RecipeConcrete.FirstOrDefault().Md_material_Material.SortId ?? 0;
                var wz = db2.Md_material_SortDetails.Where(a => a.Id == sortId).Select(a => a.WZValue).FirstOrDefault();

                List<ReportParameter> repParams = new List<ReportParameter>();

                repParams.Add(new ReportParameter("RezeptName", model.Description));
                repParams.Add(new ReportParameter("RezeptNummer", model.Number));
                repParams.Add(new ReportParameter("MinCharge", model.MinCharge != null ? model.MinCharge.ToString() : "-"));
                repParams.Add(new ReportParameter("MaxCharge", model.MaxCharge != null ? model.MaxCharge.ToString() : "-"));
                repParams.Add(new ReportParameter("MixingTime", model.MixingTime != null ? model.MixingTime.ToString() : "-"));
                repParams.Add(new ReportParameter("SieveLine", model.Lims_gradingCurv_GradingCurv != null ? model.Lims_gradingCurv_GradingCurv.SieveLineNumber : "-"));
                repParams.Add(new ReportParameter("SieveDescription", model.Lims_gradingCurv_GradingCurv != null ? model.Lims_gradingCurv_GradingCurv.Description : "-"));
                repParams.Add(new ReportParameter("wz", wz != null ? wz.ToString() : "-"));
                repParams.Add(new ReportParameter("wastedWater", (model.WasteWaterFactor ?? 0).ToString()));
                repParams.Add(new ReportParameter("AirContent", (model.AirVoidContent).ToString()));
                repParams.Add(new ReportParameter("MealGrain", (model.MealGrain).ToString()));
                repParams.Add(new ReportParameter("ClementPaste", (model.ClementPaste).ToString()));
                repParams.Add(new ReportParameter("WetConcreteDensity", (model.WetConcreteDensity).ToString()));
                repParams.Add(new ReportParameter("WaterContent", (model.WaterContent).ToString()));
                repParams.Add(new ReportParameter("Additive", (model.Additive).ToString()));
                repParams.Add(new ReportParameter("Wzeq", (model.WZValueEq).ToString()));
                repParams.Add(new ReportParameter("MealFineGrain", (model.MealFineGrain).ToString()));
                repParams.Add(new ReportParameter("MortarContent", (model.MortarContent).ToString()));
                repParams.Add(new ReportParameter("SolidMaterialrest", (model.SolidMaterialrest).ToString()));
                repParams.Add(new ReportParameter("ContentOfCement", (model.ContentOfCement).ToString()));
                

                if (model.RecipeConcrete.FirstOrDefault() != null)
                {
                    repParams.Add(new ReportParameter("FabrikatNummer", model.RecipeConcrete.FirstOrDefault() != null ? model.RecipeConcrete.FirstOrDefault().Md_material_Material.MaterialNumber : "-"));
                    repParams.Add(new ReportParameter("FabrikatName", model.RecipeConcrete.FirstOrDefault() != null ? model.RecipeConcrete.FirstOrDefault().Md_material_Material.Name : "-"));
                    repParams.Add(new ReportParameter("FabrikatComment1", model.RecipeConcrete.FirstOrDefault() != null ? model.RecipeConcrete.FirstOrDefault().Md_material_Material.Comment1 : "-"));

                    var sort = model.RecipeConcrete.FirstOrDefault().Md_material_Material;
                    var sortD = db2.Md_material_SortDetails.Where(a => a.Id ==( sort.SortId ?? 0)).FirstOrDefault();
                    if(sortD != null)
                    {
                        repParams.Add(new ReportParameter("Exposure", db2.Md_material_ExposureKombi.Where(a => a.Id == sortD.ExposureKombiId).Select(b => b.ShortText).FirstOrDefault()));
                        repParams.Add(new ReportParameter("CompressionStrenght", db2.Md_material_CompressionStrenghtGroup.Where(a => a.Id == sortD.CompressionId).Select(b => b.Class).FirstOrDefault()));
                        repParams.Add(new ReportParameter("ConsistencyGroup", db2.Md_material_ConsistencyGroup.Where(a => a.Id == sortD.ConsistencyId).Select(b => b.Class).FirstOrDefault()));



                    }
                    
                }
                //Sieblinie
                PCMSDataSet ds2 = new PCMSDataSet();
                List<string> Sieves = Definitions.allSieves.ToList();
                LimsContext LimsContext = new LimsContext();
                var rule = LimsContext.Md_sieve_RuleGradingCurve.Where(a => a.Id == model.GradingCurvSieveRangeId).FirstOrDefault();
               
                
                
                //Dictionary<string, decimal> durchgang = model.getKumulativList();
                //var fields = durchgang.Values.ToList();
                //Keine Sieblinie
                if(model.Lims_gradingCurv_GradingCurv != null)
                {
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[0], model.Lims_gradingCurv_GradingCurv.S0_063, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[1], model.Lims_gradingCurv_GradingCurv.S0_125, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[2], model.Lims_gradingCurv_GradingCurv.S0_25, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[3], model.Lims_gradingCurv_GradingCurv.S0_5, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[4], model.Lims_gradingCurv_GradingCurv.S1, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[5], model.Lims_gradingCurv_GradingCurv.S1_4, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[6], model.Lims_gradingCurv_GradingCurv.S2, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[7], model.Lims_gradingCurv_GradingCurv.S2_8, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[8], model.Lims_gradingCurv_GradingCurv.S4, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[9], model.Lims_gradingCurv_GradingCurv.S5_6, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[10], model.Lims_gradingCurv_GradingCurv.S8, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[11], model.Lims_gradingCurv_GradingCurv.S11_2, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[12], model.Lims_gradingCurv_GradingCurv.S16, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[13], model.Lims_gradingCurv_GradingCurv.S22_4, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[14], model.Lims_gradingCurv_GradingCurv.S31_5, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[15], model.Lims_gradingCurv_GradingCurv.S45, -1, -1, -1);
                    ds2.RuleGradingCurve.AddRuleGradingCurveRow(Sieves[16], model.Lims_gradingCurv_GradingCurv.S63, -1, -1, -1);
                }
                //Keine Grenzsieblinien
                if(rule != null)
                {
                    for (int sl = 0; sl < rule.md_sieve_SieveSet.Sieves.Count; sl++)
                    {
                        for (int ua = 0; ua < ds2.RuleGradingCurve.Count; ua++)
                        {
                            rule.md_sieve_SieveSet.Sieves = rule.md_sieve_SieveSet.Sieves.OrderBy(a => a.Size).ToList();
                            string sieveSi = Normalize(rule.md_sieve_SieveSet.Sieves.ElementAt(sl).Size).ToString();

                            if (ds2.RuleGradingCurve[ua].Sieve == sieveSi)
                            {
                                switch (sl)
                                {
                                    case 0:
                                        ds2.RuleGradingCurve[ua].B = (rule.A0 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B0 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C0 ?? -1);
                                        break;
                                    case 1:
                                        ds2.RuleGradingCurve[ua].B = (rule.A1 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B1 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C1 ?? -1);
                                        break;
                                    case 2:
                                        ds2.RuleGradingCurve[ua].B = (rule.A2 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B2 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C2 ?? -1);
                                        break;
                                    case 3:
                                        ds2.RuleGradingCurve[ua].B = (rule.A3 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B3 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C3 ?? -1);
                                        break;
                                    case 4:
                                        ds2.RuleGradingCurve[ua].B = (rule.A4 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B4 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C4 ?? -1);
                                        break;
                                    case 5:
                                        ds2.RuleGradingCurve[ua].B = (rule.A5 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B5 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C5 ?? -1);
                                        break;
                                    case 6:
                                        ds2.RuleGradingCurve[ua].B = (rule.A6 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B6 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C6 ?? -1);
                                        break;
                                    case 7:
                                        ds2.RuleGradingCurve[ua].B = (rule.A7 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B7 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C7 ?? -1);
                                        break;
                                    case 8:
                                        ds2.RuleGradingCurve[ua].B = (rule.A8 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B8 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C8 ?? -1);
                                        break;
                                    case 9:
                                        ds2.RuleGradingCurve[ua].B = (rule.A9 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B9 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C9 ?? -1);
                                        break;
                                    case 10:
                                        ds2.RuleGradingCurve[ua].B = (rule.A10 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B10 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C10 ?? -1);
                                        break;
                                    case 11:
                                        ds2.RuleGradingCurve[ua].B = (rule.A11 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B11 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C11 ?? -1);
                                        break;
                                    case 12:
                                        ds2.RuleGradingCurve[ua].B = (rule.A12 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B12 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C12 ?? -1);
                                        break;
                                    case 13:
                                        ds2.RuleGradingCurve[ua].B = (rule.A13 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B13 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C13 ?? -1);
                                        break;
                                    case 14:
                                        ds2.RuleGradingCurve[ua].B = (rule.A14 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B14 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C14 ?? -1);
                                        break;
                                    case 15:
                                        ds2.RuleGradingCurve[ua].B = (rule.A15 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B15 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C15 ?? -1);
                                        break;
                                    case 16:
                                        ds2.RuleGradingCurve[ua].B = (rule.A16 ?? -1);
                                        ds2.RuleGradingCurve[ua].C = (rule.B16 ?? -1);
                                        ds2.RuleGradingCurve[ua].U = (rule.C16 ?? -1);
                                        break;
                                    default:
                                        break;
                                }
                                break;

                            }
                        }
                    }
                }
                

                //int counter = 0;
                //foreach (string s in Sieves)
                //{
                //    ds.RuleGradingCurve.AddRuleGradingCurveRow(s, A[counter], B[counter], C[counter], U[counter]);
                //    counter++;
                //}
                int count = 0;
                decimal lastValue = 0;
                //B
                for (int i = 0; i < ds2.RuleGradingCurve.Count; i++)
                {
                    if (ds2.RuleGradingCurve[i].B == -1)
                    {

                        if (lastValue == 100)
                        {
                            ds2.RuleGradingCurve[i].B = 100;
                        }
                        else
                        {
                            decimal top = -2;
                            count = 0;
                            for (int r = i + 1; r < ds2.RuleGradingCurve.Count; r++)
                            {
                                count++;
                                if (ds2.RuleGradingCurve[r].B != -1)
                                {
                                    top = ds2.RuleGradingCurve[r].B;
                                    break;
                                }

                            }
                            ds2.RuleGradingCurve[i].B = lastValue + ((top - lastValue) / (count + 1));
                           // ds2.RuleGradingCurve[i].U = 1;
                            lastValue = ds2.RuleGradingCurve[i].B;
                        }
                    }
                    else
                    {
                        lastValue = ds2.RuleGradingCurve[i].B;
                    }
                }
                //C
                count = 0;
                lastValue = 0;
                for (int i = 0; i < ds2.RuleGradingCurve.Count; i++)
                {
                    if (ds2.RuleGradingCurve[i].C == -1)
                    {
                        if (lastValue == 100)
                        {
                            ds2.RuleGradingCurve[i].C = 100;
                        }
                        else {
                            decimal top = -2;
                            count = 0;
                            for (int r = i + 1; r < ds2.RuleGradingCurve.Count; r++)
                            {
                                count++;
                                if (ds2.RuleGradingCurve[r].C != -1)
                                {
                                    top = ds2.RuleGradingCurve[r].C;
                                    break;
                                }

                            }
                            ds2.RuleGradingCurve[i].C = lastValue + ((top - lastValue) / (count + 1));
                            //ds2.RuleGradingCurve[i].U = 2;
                            lastValue = ds2.RuleGradingCurve[i].C;
                        }
                    }
                    else
                    {
                        lastValue = ds2.RuleGradingCurve[i].C;
                    }
                }
                //U
                count = 0;
                lastValue = 0;
                for (int i = 0; i < ds2.RuleGradingCurve.Count; i++)
                {
                    if (ds2.RuleGradingCurve[i].U == -1)
                    {
                        if (lastValue == 100)
                        {
                            ds2.RuleGradingCurve[i].U = 100;
                        }
                        else {
                            decimal top = -2;
                            count = 0;
                            for (int r = i + 1; r < ds2.RuleGradingCurve.Count; r++)
                            {
                                count++;
                                if (ds2.RuleGradingCurve[r].U != -1)
                                {
                                    top = ds2.RuleGradingCurve[r].U;
                                    break;
                                }

                            }
                            ds2.RuleGradingCurve[i].U = lastValue + ((top - lastValue) / (count + 1));
                            //ds2.RuleGradingCurve[i].U = 2;
                            lastValue = ds2.RuleGradingCurve[i].U;
                        }
                    }
                    else
                    {
                        lastValue = ds2.RuleGradingCurve[i].U;
                    }
                }

                ReportDataSource DSReport2 = new ReportDataSource("ds", ds2.RuleGradingCurve.ToList());




                repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));


                repParams.Add(new ReportParameter("Sum", allVal.ToString()));
                //repParams.Add(new ReportParameter("materialDescription", model.Md_material_Material.Name));
                //repParams.Add(new ReportParameter("materialCategory", model.Md_material_Material.MaterialTypeId.ToString()));
                //repParams.Add(new ReportParameter("producerName", "fehlt"));
                //repParams.Add(new ReportParameter("producerStreet", "fehlt"));
                //repParams.Add(new ReportParameter("producerZIP", "fehlt"));
                //repParams.Add(new ReportParameter("producerCity", "fehlt"));
                //repParams.Add(new ReportParameter("deliveryNoteNo", model.DeliveryNoteNo));
                //repParams.Add(new ReportParameter("delivererName", "fehlt"));
                //repParams.Add(new ReportParameter("delivererStreet", "fehlt"));
                //repParams.Add(new ReportParameter("delivererZIP", "fehlt"));
                //repParams.Add(new ReportParameter("delivererCity", "fehlt"));
                //repParams.Add(new ReportParameter("probeTakingDate", model.ProbTakingDatetime.Value.ToShortDateString()));
                //repParams.Add(new ReportParameter("probePlace", model.ProbPlaceId.ToString()));
                //repParams.Add(new ReportParameter("probePlace", model.ProbPlaceDetail.ToString()));
                //repParams.Add(new ReportParameter("probeTaker", model.UserId.ToString()));
                //repParams.Add(new ReportParameter("probeType", model.ProbTypeId.ToString()));
                //repParams.Add(new ReportParameter("probeQuantity", model.Quantity.ToString()));
                //repParams.Add(new ReportParameter("probeEnteringDate", model.ProbEnteringDatetime.Value.ToShortDateString()));
                //repParams.Add(new ReportParameter("sieveSet", model.SievSetId.ToString()));
                //repParams.Add(new ReportParameter("kValue", model.KValue.ToString()));
                //repParams.Add(new ReportParameter("dValue", model.DValue.ToString()));


                string SievesList = "";
                //for (int i = 0; i < Sieves.Count; i++)
                //{
                //    SievesList += Sieves[i].ToString() + ";";
                //}
                //for (int i = 0; i < (17 - Sieves.Count); i++)
                //{
                //    SievesList += "-;";
                //}

                //repParams.Add(new ReportParameter("SievesList", SievesList));

                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/Recipe/ConcreteDetails.rdlc";
                //reportViewer.ShowPrintButton = false;
                //reportViewer.LocalReport.DataSources.Add(DSReportSieves);
                //reportViewer.LocalReport.DataSources.Add(DSReportA);
                //reportViewer.LocalReport.DataSources.Add(DSReportB);
                //reportViewer.LocalReport.DataSources.Add(DSReportC);
                //reportViewer.LocalReport.DataSources.Add(DSReportU);

                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.DataSources.Add(DSReport2);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");
            }
            catch (Exception e)
            {
                PCMS.Helper.ExceptionHelper.LogException(e, User.Identity.Name);
                return View("~/Views/Shared/Exception", e);
            }
        }
        public static decimal Normalize(decimal value)
        {
            return value / 1.000000000000000000000000000000000m;
        }
        //edit Bei Manuel Schall 2016_10_19
        public static void calcRecipeMaterialByMaterialId(long id,string UserName)
        {
            Areas.Recipe.Models.RecipeContext dbR = new Recipe.Models.RecipeContext();
            var recipe = dbR.Md_recipe_Recipe.Include(a => a.RecipeMaterials).Where(a => a.RecipeMaterials.Any(b => b.MaterialId == id) && a.IsDeleted == false).ToList();
            foreach (var re in recipe)
            {
                calcMaterialValues(re,dbR,UserName);
            }
        }
        //edit Bei Manuel Schall 2016_10_19
        public static void calcMaterialValues(Recipe.Models.md_recipe_Recipe recipe, RecipeContext dbR,string UserName)
        {
            try
            {
                decimal calcVolum = 0;
                decimal calcWater = 0;
                decimal calcCement = 0;
                decimal calcAdditive = 0;
                decimal calcAdditeiveKValue = 0;

                decimal newWeight = 0;

                foreach (var mat in recipe.RecipeMaterials.OrderBy(a => a.Sort))
                {

                    var material = dbR.Md_masterData_Material.Find(mat.MaterialId);
                    if (material.MaterialGroupId != 1)//Kein Zuschlag
                    {
                        if (mat.Unit == 0)//0=kg
                        {
                            calcVolum += mat.Value / material.Density ?? 1;
                            if (material.MaterialGroupId == 4)//Wasser
                            {
                                calcWater += mat.Value;
                                //newWeight = mat.Value;
                            }
                            else if (material.MaterialGroupId == 2) //Zement
                            {
                                calcCement += mat.Value;
                            }
                            else if (material.MaterialGroupId == 3) //Zusatsmittel
                            {
                                calcAdditive += mat.Value / material.Density ?? 1;
                                //newWeight = mat.Value;
                            }
                            else if (material.MaterialGroupId == 6) //Additive
                            {
                                //calcAdditive += mat.Value / material.Density ?? 1;
                                MasterData.Models.MasterDataContext db = new MasterData.Models.MasterDataContext();
                                var additiveDetails = db.Md_material_AdditivesDetail.Find(material.AdditivesId);
                                if (additiveDetails != null)
                                {
                                    if (additiveDetails.KValue != null)
                                    {
                                        calcAdditeiveKValue += mat.Value * (additiveDetails.KValue ?? 0);
                                    }
                                }
                            }
                        }
                        else if (mat.Unit == 1)
                        {
                            if (material.MaterialGroupId == 4) //Wasser
                            {
                                decimal waterWeight = Math.Round(calcWater * (mat.Value / 100));
                                decimal fix = Math.Floor((waterWeight + 8) / 17);
                                decimal vol = (waterWeight + fix) * (((material.Density ?? 1) - 2) * -1);
                                calcVolum += vol - waterWeight;
                                newWeight = waterWeight + fix;
                                if (Math.Round(mat.Weight) != Math.Round(newWeight))
                                {
                                    mat.Weight = Math.Round(newWeight, 2);
                                    dbR.Entry(mat).State = EntityState.Modified;
                                    //dbR.Entry(recipe).Collection(st => st.RecipeMaterials).Load();
                                    dbR.SaveChanges();
                                }
                            }
                            else if (material.MaterialGroupId == 3) //Zusatsmittel
                            {
                                decimal cemWeight = calcCement * (mat.Value / 100);
                                decimal vol = cemWeight / material.Density ?? 1;
                                calcVolum += vol;
                                //newWeight = cemWeight;
                            }
                        }
                    }
                }
                decimal gradingVolum = 1000 - calcVolum - recipe.AirVoidContent ;
                foreach (var mat in recipe.RecipeMaterials.OrderBy(a => a.Sort))
                {
                    var material = dbR.Md_masterData_Material.Find(mat.MaterialId);
                    if (mat.Md_material_Material.MaterialGroupId == 1)// Zuschlag
                    {
                        if (mat.Unit == 1)//0=kg
                        {
                            decimal airPercent = (gradingVolum - (1000 * recipe.AirVoidContent / 100)) / gradingVolum;
                            decimal matVol = gradingVolum / 100 * mat.Value * airPercent;

                            newWeight = matVol * material.Density ?? 1;
                            if (Math.Round(mat.Weight) != Math.Round(newWeight))
                            {
                                mat.Weight = Math.Round(newWeight, 2);
                                dbR.Entry(mat).State = EntityState.Modified;
                                //dbR.Entry(recipe).Collection(st => st.RecipeMaterials).Load();
                                dbR.SaveChanges();
                            }
                        }
                        else if (mat.Unit == 0)
                        {
                            //newWeight = cemWeight;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Helper.ExceptionHelper.LogException(e, UserName);
            }
        }
    }
}