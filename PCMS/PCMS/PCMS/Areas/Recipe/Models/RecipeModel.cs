﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace PCMS.Areas.Recipe.Models
{

    public class RecipeContext : DbContext
    {
        public RecipeContext()
            : base("DefaultConnection")
        {
            //this.Configuration.LazyLoadingEnabled = false;
        }

       // public virtual DbSet<md_recipe_Recipe> Md_sieve_Sieves { get; set; }
        public virtual DbSet<lims_gradingCurv_GradingCurv> Lims_gradingCurv_GradingCurv { get; set; }
        public virtual DbSet<md_masterData_Material> Md_masterData_Material { get; set; }
        public virtual DbSet<md_recipe_Recipe> Md_recipe_Recipe { get; set; }
        public virtual DbSet<md_material_MaterialGroup> Md_material_MaterialGroup { get; set; }

        public System.Data.Entity.DbSet<PCMS.Areas.RecipeM.Models.md_equipment> md_equipment { get; set; }
        public virtual DbSet<LIMS.Models.AuditLog> Auditlog { get; set; }

        public int SaveChanges(string userId)
        {
            // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
            //foreach (var ent in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
            //{
            //    // For each changed record, get the audit record entries and add them
            //    foreach (LIMS.Models.AuditLog x in Helper.AuditLogH.GetAuditRecordsForChange(ent, userId))
            //    {
            //        this.Auditlog.Add(x);
            //    }
            //}
            //
            // Call the original SaveChanges(), which will save both the changes made and the audit records
            return base.SaveChanges();
        }

        //public virtual DbSet<md_material_GradingGroup> md_material_GradingGroup { get; set; }

        // public virtual DbSet<md_order_Company_Contact> Md_order_Company_Contact { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<md_sieve_SieveSet>()
        //        .HasMany(up => up.Sieves)
        //        .WithMany(course => course.SieveSet)
        //        .Map(mc =>
        //        {
        //            mc.ToTable("md_sieve_SieveSet_Sieves");
        //            mc.MapLeftKey("SieveSetId");
        //            mc.MapRightKey("SievesId");
        //        }

        //    );

        //    base.OnModelCreating(modelBuilder);
        //}


    }

    public class md_recipe_SieveLine
    {
        public long RuleGradingCurveId { get; set; }
    }
    [Table("md_recipe_Recipe")]
    public partial class md_recipe_Recipe
    {
        public md_recipe_Recipe()
        {
            this.RecipeMaterials = new List<md_recipe_RecipeMaterial>();
            RecipeConcrete = new List<md_material_RecipeMaterial>();
        }
        [Key]
        public long Id { get; set; }
        [DisplayName("Bezeichnung")]
        public string Description { get; set; }
        [DisplayName("Rezeptnummer")]
        public string Number { get; set; }
        [DisplayName("Max. Charge [Kg]")]
        public decimal? MaxCharge { get; set; }
        [DisplayName("Min. Charge [Kg]")]
        public decimal? MinCharge { get; set; }
        public string NumberEDV { get; set; }
        [DisplayName("Sieblinie")]
        public long? GradingCurvId { get; set; }
        [DisplayName("Regelsieblinie")]
        public long? GradingCurvSieveRangeId { get; set; }
        [DisplayName("Material")]
        public long MaterialId { get; set; }
        [DisplayName("Rezeptgruppe")]
        public long RecipeTypeId { get; set; }
        public decimal ? AirContent { get; set; }
        [DisplayName("Konsistenz-Sollwert")]
        public decimal ? ConsistencySetPoint { get; set; }
        [DisplayName("W/Z-Faktor max.")]
        public decimal ? MaxWCValue { get; set; }
        [DisplayName("Warmwasser")]
        public bool ? HotWater { get; set; }
        [DisplayName("Istwerttoleranz")]
        public int ? ActualValueTolerance { get; set; }
        [DisplayName("Sollwertabweichung")]
        public int ? SetPointDeviation { get; set; }
        [DisplayName("Mischerbefüllung")]
        public int ? MixerFilling { get; set; }
        [DisplayName("Schmutzwasserfaktor")]
        public int ? WasteWaterFactor { get; set; }
        public int ? BatchProgram { get; set; }
        [DisplayName("Füllerbew.% v.Zement")]
        public int ? FillMonitoringCement { get; set; }
        [DisplayName("Füllerbewertung")]
        public int ? FillRating { get; set; }
        [DisplayName("Zemententleerverz.")]
        public int ? CementEmptyingDelay { get; set; }
        [DisplayName("Wasserleerverz.")]
        public int ? WaterEmptyingDelay { get; set; }
        [DisplayName("Zusatzleerverz.")]
        public int ? AdditiveEmptyingDelay { get; set; }
        [DisplayName("Schaumzugabezeit")]
        public int ? FoamAdditionTime { get; set; }
        [DisplayName("Mischzeit")]
        public int ? MixingTime { get; set; }
        [DisplayName("Entleerung teiloffen 1")]
        public int ? EmptyingPartlyOpen1 { get; set; }
        [DisplayName("Entleerung teiloffen 2")]
        public int ? EmptyingPartlyOpen2 { get; set; }
        [DisplayName("Entleerung offen")]
        public int ? EmptyingOpen { get; set; }
        public long ?SuitabilityGroupId { get; set; }
        public long ?ConcreteFamilyId { get; set; }
        public long ?ExposureGroupId { get; set; }
        public long ?MaterialGroupId { get; set; }
        [DisplayName("Berechnung für")]
        [DefaultValue(1)]
        public int MasterUnit { get; set; }
        [DisplayName("Luftgehalt[%]")]
        [DefaultValue(0)]
        public decimal AirVoidContent { get; set; }
        [DisplayName("Wassergehalt")]
        [DefaultValue(0)]
        public int WaterContent { get; set; }
        [DisplayName("Zementgehalt")]
        public int ContentOfCement { get; set; }
        [DisplayName("Zusatzmittel")]
        public decimal Additive { get; set; }
        [DisplayName("Festst. im Restw.")]
        public int SolidMaterialrest { get; set; }
        [DisplayName("Gesteinskörnung")]
        public int Aggregate { get; set; }
        [DisplayName("Gesteink. fest")]
        public int AggregateFixed { get; set; }
        [DisplayName("Oberflächenfeuchte")]
        public int SurfaceDamp { get; set; }
        [DisplayName("Zugabewasser")]
        public int AdditionWater { get; set; }
        [DisplayName("Frischbetondichte")]
        public int WetConcreteDensity { get; set; }
        [DisplayName("Mehlkorn")]
        public int MealGrain { get; set; }
        [DisplayName("Mehl-/Feinstkorn")]
        public int MealFineGrain { get; set; }
        [DisplayName("Mörtegehalt")]
        public int MortarContent { get; set; }
        [DisplayName("Zementleim")]
        public int ClementPaste { get; set; }
        [DisplayName("w/z-Wert")]
        public decimal WZValue { get; set; }
        [DisplayName("w/z(eq)")]
        public decimal WZValueEq { get; set; }
        [DisplayName("Preis")]
        public decimal Price { get; set; }
        public int? InitialProduction_0_Count { get; set; }
        public int? InitialProduction_0_Val { get; set; }
        public int? InitialProduction_1_Count { get; set; }
        public int? InitialProduction_1_Val { get; set; }
        public int? SteadyProduction_0_Count { get; set; }
        public int? SteadyProduction_0_Val { get; set; }
        public int? SteadyProduction_1_Count { get; set; }
        public int? SteadyProduction_1_Val { get; set; }
        [DisplayName("2 Tagen")]
        public decimal? StrengthAfter2 { get; set; }
        [DisplayName("7 Tagen")]
        public decimal? StrengthAfter7 { get; set; }
        [DisplayName("28 Tagen")]
        public decimal? StrengthAfter28 { get; set; }
        [DisplayName("56 Tagen")]
        public decimal? StrengthAfter56 { get; set; }
        [DisplayName("90 Tagen")]
        public decimal? StrengthAfter90 { get; set; }


        public long? HashCode { get; set; }
        public string Comment { get; set; }
        public long? FacilityId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        [ForeignKey("GradingCurvId")]       
        public virtual lims_gradingCurv_GradingCurv Lims_gradingCurv_GradingCurv { get; set; }
        [ScriptIgnore]
        public ICollection<md_recipe_RecipeMaterial> RecipeMaterials { get; set; }
        [ScriptIgnore]
        public ICollection<md_material_RecipeMaterial> RecipeConcrete { get; set; }

    }

    [Table("lims_gradingCurv_GradingCurv")]
    public partial class lims_gradingCurv_GradingCurv
    {
        public lims_gradingCurv_GradingCurv()
        {
            Lims_gradingCurvMaterial = new List<lims_gradingCurvMaterial>();
        }
        [Key]
        public long Id { get; set; }
        [DisplayName("Rezeptgruppe")]
        public long ResciveGroupId { get; set; }
        [DisplayName("Sieblinie")]
        public string SieveLineNumber { get; set; }
        [DisplayName("Bezeichnung")]
        public string Description { get; set; }
        [DisplayName("Regelsieblinie")]
        public long SieveRangeId { get; set; }
        [DisplayName("Preis")]
        public decimal Price { get; set; }
        public decimal S0_063 { get; set; }
        public decimal S0_125 { get; set; }
        public decimal S0_25 { get; set; }
        public decimal S0_5 { get; set; }
        public decimal S1 { get; set; }
        public decimal S1_4 { get; set; }
        public decimal S2 { get; set; }
        public decimal S2_8 { get; set; }
        public decimal S4 { get; set; }
        public decimal S5_6 { get; set; }
        public decimal S8 { get; set; }
        public decimal S11_2 { get; set; }
        public decimal S16 { get; set; }
        public decimal S22_4 { get; set; }
        public decimal S31_5 { get; set; }
        public decimal S45 { get; set; }
        public decimal S63 { get; set; }
        public decimal KValue { get; set; }

        // Änderungen Hakan Matzir 25.02.2016 Start

        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }

        // Änderungen Hakan Matzir 25.02.2016 Ende

        //[ForeignKey("SieveRangeId")]
        //[DisplayName("Nach Norm")]
        //public virtual Sieve.Models.md_sieve_RuleGradingCurve Md_sieve_RuleGradingCurve { get; set; }
        //[InverseProperty("lims_gradingCurv_GradingCurv")]
        //public virtual ICollection<lims_gradingCurvMaterial> CradingMaterial { get; set; }
        [ScriptIgnore]
        public virtual ICollection<lims_gradingCurvMaterial> Lims_gradingCurvMaterial { get; set; }
    }
    [Table("md_material_Material")]
    public partial class md_masterData_Material
    {
        public md_masterData_Material()
        {
            Lims_gradingCurvMaterial = new List<lims_gradingCurvMaterial>();
        }
        [Key]
        public long Id { get; set; }
        public long? MaterialGroupId { get; set; }
        public long? MaterialTypeId { get; set; }
        public decimal? Price { get; set; }
        public long? DelivererId { get; set; }
        public long? FacilitiesId { get; set; }

        [DisplayName("Artikelnummer")]
        public string ArticleNumber { get; set; }
        [DisplayName("Materialnummer")]
        public string MaterialNumber { get; set; }
        [DisplayName("Bezeichnung")]
        public string Name { get; set; }
        [DisplayName("Kurz-Bezeichnung")]
        public string ShortName { get; set; }
        [DisplayName("Dichte")]
        public decimal? Density { get; set; }
        [DisplayName("Einheit")]
        public string Unit { get; set; }
        [DisplayName("Bemerkung 1")]
        public string Comment1 { get; set; }
        [DisplayName("Bemerkung 2")]
        public string Comment2 { get; set; }
        [DisplayName("Bemerkung 3")]
        public string Comment3 { get; set; }
        [DisplayName("Bemerkung 4")]
        public string Comment4 { get; set; }
        [DisplayName("Bemerkung 5")]
        public string Comment5 { get; set; }
        [DisplayName("Chipnummer")]
        public string RFID { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        // Detail FK_Tabellen
        public long? AdditivesId { get; set; }
        public long? AdmixturesId { get; set; }
        public long? AggregateId { get; set; }
        public long? BinderId { get; set; }
        public long? SortId { get; set; }
        [ForeignKey("DelivererId")]
        public virtual md_masterData_Deliverer Md_masterData_Deliverer { get; set; }
        [ForeignKey("AdditivesId")]
        public virtual Areas.MasterData.Models.md_material_AdditivesDetail Md_material_AdditivesDetail { get; set; }
        //[InverseProperty("md_masterData_Material")]
        //public virtual ICollection<lims_gradingCurvMaterial> CradingMaterial { get; set; }
        [ScriptIgnore]
        public ICollection<lims_gradingCurvMaterial> Lims_gradingCurvMaterial { get; set; }


    }
    [Table("lims_gradingCurvMaterial")]
    public partial class lims_gradingCurvMaterial
    {
        [Key, Column(Order = 0)]
        public long GradingCurvId { get; set; }
        [Key, Column(Order = 1)]
        public long MaterialId { get; set; }
        public decimal Percent { get; set; }

        public int Sort { get; set; }
        [ForeignKey("GradingCurvId")]
        // [InverseProperty("CradingMaterial")]
        [ScriptIgnore]
        public lims_gradingCurv_GradingCurv Lims_gradingCurv_GradingCurv { get; set; }
        [ForeignKey("MaterialId")]
        // [InverseProperty("CradingMaterial")]
        [ScriptIgnore]
        public virtual md_masterData_Material Md_masterData_Material { get; set; }
    }
    [Table("md_Deliverer")]
    public partial class md_masterData_Deliverer
    {
        [Key]
        public long? Id { get; set; }
        public long? AddressId { get; set; }
        public long? ContactId { get; set; }
        [DisplayName("Lieferant")]
        public string Name { get; set; }
        [DisplayName("Lieferantennummer")]
        public string DelivererId { get; set; }
        [DisplayName("Lieferantennummer EDV")]
        public string DelivererIdEDV { get; set; }
        [DisplayName("Aktiv")]
        public bool IsActive { get; set; }
        [DisplayName("Gelöscht")]
        public bool IsDeleted { get; set; }
    }
    [Table("md_recipe_RecipeMaterial")]
    public partial class md_recipe_RecipeMaterial
    {
        [Key, Column(Order = 0)]
        public long RecipeId { get; set; }
        public long MaterialId { get; set; }
        public decimal Weight { get; set; }
        public decimal Value { get; set; }
        public int Unit { get; set; }
        [Key, Column(Order = 1)]
        public long Sort { get; set; }
        public int Type { get; set; }
        public int? FunctionType { get; set; }
        [ForeignKey("RecipeId")]
        public virtual md_recipe_Recipe Md_recipe_Recipe { get; set; }
        [ForeignKey("MaterialId")]
        public virtual md_masterData_Material Md_material_Material { get; set; }
    }
    [Table("md_material_RecipeMaterial")]
    public partial class md_material_RecipeMaterial
    {
        [Key, Column(Order = 0)]
        public long RecipeId { get; set; }
        [Key, Column(Order = 1)]
        public long MaterialId { get; set; }
        public int Valid { get; set; }
        public bool Active { get; set; }
        [ForeignKey("RecipeId")]
        public virtual md_recipe_Recipe Md_recipe_Recipe { get; set; }
        [ForeignKey("MaterialId")]
        public virtual md_masterData_Material Md_material_Material { get; set; }
    }

    [Table("md_material_MaterialGroup")]
    public partial class md_material_MaterialGroup
    {
        [Key]
        public long Id { get; set; }
        public string Description { get; set; }
    }


    public class RecipeModel
    {
    }
}