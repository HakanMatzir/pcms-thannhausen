﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_MaterialController : Controller
    {
        MasterDataContext db = new MasterDataContext();
        
        // GET:
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a=>a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }

            ViewBag.Index = "all";
            return View("Index", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult IndexAdditives(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 6).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }

            ViewBag.Index = "additives";
            return View("Index", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));
        }

        public ActionResult IndexAggregates(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 1).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }

            ViewBag.Index = "aggregates";
            return View("Index", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));
        }

        public ActionResult IndexBinder(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 2).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }

            ViewBag.Index = "binder";
            return View("Index",model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));
        }

        public ActionResult IndexAdmixtures(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 3).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }

            ViewBag.Index = "admixtures";
            return View("Index", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));
        }

        public ActionResult IndexWater(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 4).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }

            ViewBag.Index = "water";
            return View("Index", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));
        }

        public ActionResult IndexSort(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 5).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }

            ViewBag.Index = "sort";
            return View("Index", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));
        }

        // Get
        public ActionResult CreateAggregates()
        {
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Create = "aggregates";
            return View("Create");
        }

        // Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAggregates(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.MaterialGroupId = 1;
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Md_material_AggregateDetails.Add(model.aggregateDetails);
                db.SaveChanges();
                model.material.AggregateId = model.aggregateDetails.Id;
                db.Md_masterData_Material.Add(model.material);
                db.SaveChanges();
                
                return RedirectToAction("IndexAggregates");
            }
            ViewBag.Create = "aggregates";
            return View("Create", model);
        }

        public ActionResult CreateAdditives()
        {
            //var configValue = db.Md_material_Config.Find(1).CompanyTypeId;
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Create = "additives";
            return View("Create");
        }

        // Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAdditives(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.MaterialGroupId = 6;
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Md_material_AdditivesDetail.Add(model.additivesDetails);
                db.SaveChanges();
                model.material.AdditivesId = model.additivesDetails.Id;
                db.Md_masterData_Material.Add(model.material);
                db.SaveChanges();

                return RedirectToAction("IndexAdditives");
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Create = "additives";
            return View("Create",model);
        }

        // Get
        public ActionResult CreateBinders()
        {
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Create = "binder";
            return View("Create");
        }

        // Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateBinders(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.MaterialGroupId = 2;
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Md_material_BinderDetails.Add(model.binderDetails);
                db.SaveChanges();
                model.material.BinderId = model.binderDetails.Id;
                db.Md_masterData_Material.Add(model.material);
                db.SaveChanges();
                return RedirectToAction("IndexBinder");
            }

            ViewBag.Create = "binder";
            return View("Create", model);
        }

        // Get
        public ActionResult CreateAdmixtures()
        {
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Create = "admixtures";
            return View("Create");
        }

        // Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAdmixtures(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.MaterialGroupId = 3;
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Md_material_AdmixturesDetails.Add(model.admixturesDetails);
                db.SaveChanges();
                model.material.AdmixturesId = model.admixturesDetails.Id;
                db.Md_masterData_Material.Add(model.material);
                db.SaveChanges();
                
                return RedirectToAction("IndexAdmixtures");
            }

            ViewBag.Create = "admixtures";
            return View("Create", model);
        }

        // Get
        public ActionResult CreateWater()
        {
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Create = "water";
            return View("Create");
        }

        // Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateWater(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.MaterialGroupId = 4;
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Md_masterData_Material.Add(model.material);
                db.SaveChanges();
                return RedirectToAction("IndexWater");
            }
            ViewBag.Create = "water";
            return View("Create",model);
        }

        // Get
        public ActionResult CreateSort()
        {
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Create = "sort";
            return View("Create");
        }

        // Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSort(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.MaterialGroupId = 5; 
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Md_material_SortDetails.Add(model.sortDetails);
                db.SaveChanges();
                model.material.SortId = model.sortDetails.Id;
                db.Md_masterData_Material.Add(model.material);
                db.SaveChanges();

                return RedirectToAction("IndexSort");
            }

            ViewBag.Create = "sort";
            return View("Create", model);
        }

        // GET
        public ActionResult Delete(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model.material = db.Md_masterData_Material.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(MasterDataContactView model, long id)
        {
            model.material.IsActive = true;
            model.material.IsDeleted = true;
            db.Entry(model.material).State = EntityState.Modified;
            db.SaveChanges();
            if(model.material.MaterialGroupId == 1)
            {
                return RedirectToAction("IndexAggregates");
            }
            if (model.material.MaterialGroupId == 2)
            {
                return RedirectToAction("IndexBinder");
            }
            if (model.material.MaterialGroupId == 3)
            {
                return RedirectToAction("IndexAdmixtures");
            }
            if (model.material.MaterialGroupId == 4)
            {
                return RedirectToAction("IndexWater");
            }
            if (model.material.MaterialGroupId == 5)
            {
                return RedirectToAction("IndexSort");
            }
            if (model.material.MaterialGroupId == 6)
            {
                return RedirectToAction("IndexAdditives");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        
        public ActionResult EditAdditives(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Material.Find(id) == null)
            {
                return HttpNotFound();
            }
            else
            {
                model.material = db.Md_masterData_Material.Find(id);
                model.deliverer = db.Md_masterData_Deliverer.Find(model.material.DelivererId);
                model.facili = db.Facilities.Find(model.material.FacilitiesId);
                model.materialType = db.Md_material_MaterialType.Find(model.material.MaterialTypeId);

                model.additivesDetails = db.Md_material_AdditivesDetail.Find(model.material.AdditivesId);
                model.standard = db.Md_masterData_Material_Standard.Find(model.additivesDetails.NormId);
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Edit = "additives";
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAdditives(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Entry(model.material).State = EntityState.Modified;
                db.Entry(model.additivesDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexAdditives");
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Edit = "additives";
            return View("Edit", model);
        }

        public ActionResult EditBinders(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Material.Find(id) == null)
            {
                return HttpNotFound();
            }
            else
            {
                model.material = db.Md_masterData_Material.Find(id);
                model.deliverer = db.Md_masterData_Deliverer.Find(model.material.DelivererId);
                model.facili = db.Facilities.Find(model.material.FacilitiesId);
                model.materialType = db.Md_material_MaterialType.Find(model.material.MaterialTypeId);

                model.binderDetails = db.Md_material_BinderDetails.Find(model.material.BinderId);
                model.compCement = db.Md_material_CompressionGroupCement.Find(model.binderDetails.CompressionCemId);
                model.standard = db.Md_masterData_Material_Standard.Find(model.binderDetails.NormId);
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Edit = "binder";
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditBinders(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Entry(model.material).State = EntityState.Modified;
                db.Entry(model.binderDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexBinder");
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Edit = "binder";
            return View("Edit", model);
        }

        public ActionResult EditAggregates(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Material.Find(id) == null)
            {
                return HttpNotFound();
            }
            else
            {
                model.material = db.Md_masterData_Material.Find(id);
                model.deliverer = db.Md_masterData_Deliverer.Find(model.material.DelivererId);
                model.facili = db.Facilities.Find(model.material.FacilitiesId);
                model.materialType = db.Md_material_MaterialType.Find(model.material.MaterialTypeId);

                model.aggregateDetails = db.Md_material_AggregateDetails.Find(model.material.AggregateId);
                model.aggregate = db.Md_material_Aggregate.Find(model.aggregateDetails.AggregateId);
                model.gravelType = db.Md_material_GravelType.Find(model.aggregateDetails.GravelTypeId);
                model.gravelGroup = db.Md_material_GravelGroup.Find(model.aggregateDetails.GravelGroupId);
                model.rock = db.Md_material_RockType.Find(model.aggregateDetails.RockTypeId);
                model.alkali = db.Md_material_AlkaliGroup.Find(model.aggregateDetails.AlkaliGroupId);
                model.petrograph = db.Md_material_PetrographicType.Find(model.aggregateDetails.PetrographicTypeId);
                model.geo = db.Md_material_GeologicalSource.Find(model.aggregateDetails.GeologicalSourceId);
                model.standard = db.Md_masterData_Material_Standard.Find(model.aggregateDetails.NormId);
                model.suitability = db.Md_masterData_Suitability.Find(model.aggregateDetails.SuitabilityId);
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Edit = "aggregates";
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAggregates(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Entry(model.material).State = EntityState.Modified;
                db.Entry(model.aggregateDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexAggregates");
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Edit = "aggregates";
            return View("Edit", model);
        }

        public ActionResult EditAdmixtures(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Material.Find(id) == null)
            {
                return HttpNotFound();
            }
            else
            {
                model.material = db.Md_masterData_Material.Find(id);
                model.deliverer = db.Md_masterData_Deliverer.Find(model.material.DelivererId);
                model.facili = db.Facilities.Find(model.material.FacilitiesId);
                model.materialType = db.Md_material_MaterialType.Find(model.material.MaterialTypeId);

                model.admixturesDetails = db.Md_material_AdmixturesDetails.Find(model.material.AdmixturesId);
                model.standard = db.Md_masterData_Material_Standard.Find(model.admixturesDetails.NormId);
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Edit = "admixtures";
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAdmixtures(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Entry(model.material).State = EntityState.Modified;
                db.Entry(model.admixturesDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexAdmixtures");
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Edit = "admixtures";
            return View("Edit", model);
        }

        public ActionResult EditWater(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Material.Find(id) == null)
            {
                return HttpNotFound();
            }
            else
            {
                model.material = db.Md_masterData_Material.Find(id);
                model.deliverer = db.Md_masterData_Deliverer.Find(model.material.DelivererId);
                model.facili = db.Facilities.Find(model.material.FacilitiesId);
                model.materialType = db.Md_material_MaterialType.Find(model.material.MaterialTypeId);
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Edit = "water";
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditWater(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Entry(model.material).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexWater");
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Edit = "water";
            return View("Edit", model);
        }

        public ActionResult EditSort(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            if (db.Md_masterData_Material.Find(id) == null)
            {
                return HttpNotFound();
            }
            else
            {
                model.material = db.Md_masterData_Material.Find(id);
                model.deliverer = db.Md_masterData_Deliverer.Find(model.material.DelivererId);
                model.facili = db.Facilities.Find(model.material.FacilitiesId);
                model.materialType = db.Md_material_MaterialType.Find(model.material.MaterialTypeId);

                model.sortDetails = db.Md_material_SortDetails.Find(model.material.SortId);
                model.standard = db.Md_masterData_Material_Standard.Find(model.sortDetails.NormId);
                model.progress = db.Md_material_Progress.Find(model.sortDetails.ProgressId);
                model.consistency = db.Md_material_ConsistencyGroup.Find(model.sortDetails.ConsistencyId);
                model.compression = db.Md_material_CompressionStrenghtGroup.Find(model.sortDetails.CompressionId);
                model.chlorid = db.Md_material_ChlorideGroup.Find(model.sortDetails.ChloridId);
                model.exposure = db.Md_material_ExposureKombi.Find(model.sortDetails.ExposureKombiId);
                model.concrete = db.Md_masterData_ConcreteFamily.Find(model.sortDetails.ConcreteId);
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Edit = "sort";
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSort(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Entry(model.material).State = EntityState.Modified;
                db.Entry(model.sortDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexSort");
            }
            var companytype = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
            ViewBag.CompanyType = companytype;
            ViewBag.Edit = "sort";
            return View("Edit", model);
        }

        public ActionResult ChooseGroup()
        {
            var model = db.Md_material_MaterialGroup.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a=>a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseStandard()
        {
            var model = db.Md_masterData_Material_Standard.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a=>a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseConcrete()
        {
            var model = db.Md_masterData_ConcreteFamily.OrderBy(a=>a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseCompression()
        {
            var model = db.Md_material_CompressionStrenghtGroup.OrderBy(a=>a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseChlorid()
        {
            var model = db.Md_material_ChlorideGroup.OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseSuitability()
        {
            var model = db.Md_masterData_Suitability.OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseConsistency()
        {
            var model = db.Md_material_ConsistencyGroup.OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseExpsure()
        {
            var model = db.Md_material_ExposureKombi.OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseClient()
        {
            var model = db.Md_material_Client.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseDeliverer()
        {
            var model = db.Md_masterData_Deliverer.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseAlkali()
        {
            var model = db.Md_material_AlkaliGroup.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseRockType()
        {
            var model = db.Md_material_RockType.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseAggregate()
        {
            var model = db.Md_material_Aggregate.OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseGravelType()
        {
            var model = db.Md_material_GravelType.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseGravelGroup()
        {
            var model = db.Md_material_GravelGroup.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Sort).ToList();
            return View(model);
        }

        public ActionResult ChooseGeoSource()
        {
            var model = db.Md_material_GeologicalSource.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ChoosePetroType()
        {
            var model = db.Md_material_PetrographicType.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ChooseFacilities()
        {
            var model = db.Facilities.ToList();
            return View(model);
        }

        public ActionResult ChooseMaterialType(int id)
        {
            var model = db.Md_material_MaterialType.Where(a => a.MaterialGroupId == id).ToList();
            return View(model);
        }

        public ActionResult ChooseColor()
        {
            var model = db.Md_material_Color.ToList();
            return View(model);
        }

        public ActionResult ChooseCompCem()
        {
            var model = db.Md_material_CompressionGroupCement.ToList();
            return View(model);
        }

        public ActionResult ChooseProgress()
        {
            var model = db.Md_material_Progress.ToList();
            return View(model);
        }
    }
}