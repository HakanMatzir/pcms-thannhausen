﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_BrandController : Controller
    {
        MasterDataContext db = new MasterDataContext();
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 8).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Name = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ShortName = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.MaterialNumber = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ArticleNumber = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }
            
            return View("Index", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        // Get
        public ActionResult Create()
        {
            ViewBag.Compression = new SelectList(db.Md_material_CompressionStrenghtGroup.Where(a => a.IsActive == true && a.IsDeleted == false && a.MaterialGroupId == 8).ToList(), "Id", "Class");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.MaterialGroupId = 8;
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Md_material_Brand.Add(model.brand);
                db.SaveChanges();
                model.material.BrandId = model.brand.Id;
                db.Md_masterData_Material.Add(model.material);
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }
            ViewBag.Compression = new SelectList(db.Md_material_CompressionStrenghtGroup.Where(a => a.IsActive == true && a.IsDeleted == false && a.MaterialGroupId == 8).ToList(), "Id", "Class");
            return View("Create", model);
        }

        // GET
        public ActionResult Edit(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Material.Find(id) == null)
            {
                return HttpNotFound();
            }
            else
            {
                model.material = db.Md_masterData_Material.Find(id);
                model.deliverer = db.Md_masterData_Deliverer.Find(model.material.DelivererId);
                model.facili = db.Facilities.Find(model.material.FacilitiesId);
                model.materialType = db.Md_material_MaterialType.Find(model.material.MaterialTypeId);

                model.brand = db.Md_material_Brand.Find(model.material.BrandId);
            }
            ViewBag.Compression = new SelectList(db.Md_material_CompressionStrenghtGroup.Where(a => a.IsActive == true && a.IsDeleted == false && a.MaterialGroupId == 8).ToList(), "Id", "Class");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Entry(model.material).State = EntityState.Modified;
                db.Entry(model.brand).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Compression = new SelectList(db.Md_material_CompressionStrenghtGroup.Where(a => a.IsActive == true && a.IsDeleted == false && a.MaterialGroupId == 8).ToList(), "Id", "Class");
            return View(model);
        }

        // GET
        public ActionResult Delete(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model.material = db.Md_masterData_Material.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
             
            ViewBag.RecipeCheck = db.Md_material_RecipeMaterial.Where(a => a.MaterialId == id && a.Md_Recipe.IsActive == true && a.Md_Recipe.IsDeleted == false).Select(a => a.Md_Recipe.Number).ToList().Count();
            ViewBag.RecipeNumber = db.Md_material_RecipeMaterial.Where(a => a.MaterialId == id && a.Md_Recipe.IsActive == true && a.Md_Recipe.IsDeleted == false).Select(a => a.Md_Recipe.Number).ToList();
            return View(model);
        }

        // POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(MasterDataContactView model, long id)
        {
            model.material.IsActive = true;
            model.material.IsDeleted = true;
            db.Entry(model.material).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");

        }

        //#################################################################################################################################
        //######################################## Festigkeitsklasse hinzufügen und löschen Start #########################################
        //#################################################################################################################################

        public ActionResult CreateCompression()
        {
            return View();
        }

        public ActionResult SaveCompression(string description, string compClass, decimal min, decimal max)
        {
            md_material_CompressionStrenghtGroup model = new md_material_CompressionStrenghtGroup();
            if (ModelState.IsValid)
            {

                model.IsActive = true;
                model.IsDeleted = false;
                model.Description = description;
                model.Class = compClass;
                model.Min = min;
                model.Max = max;
                model.MaterialGroupId = 8;
                db.Md_material_CompressionStrenghtGroup.Add(model);
                db.SaveChanges();

                return Json(new { id = model.Id });
            }
            return Json(false);
        }

        public ActionResult DeleteCompression(md_material_CompressionStrenghtGroup model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_CompressionStrenghtGroup.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveCompression(md_material_CompressionStrenghtGroup model, long? id)
        {
            model = db.Md_material_CompressionStrenghtGroup.Find(id);
            model.IsDeleted = true;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        // ++++++++++++++++++++++++++++++++++ Report
        public ActionResult Report(string pgS)
        {
            PCMS.Models.PaginationModel pg = JsonConvert.DeserializeObject<PCMS.Models.PaginationModel>(pgS);
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 8).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }



            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            foreach (var dataset in model)
            {
                ds.List.AddListRow(dataset.Id.ToString(), dataset.MaterialNumber, dataset.ArticleNumber.ToString(), dataset.Name.ToString(), dataset.ShortName != null ? dataset.ShortName.ToString() : "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Header_00", "ID"));
            repParams.Add(new ReportParameter("Hide_00", "true"));
            repParams.Add(new ReportParameter("Header_01", "Materialnummer"));
            repParams.Add(new ReportParameter("Hide_01", "false"));
            repParams.Add(new ReportParameter("Header_02", "Artikelnummer"));
            repParams.Add(new ReportParameter("Hide_02", "false"));
            repParams.Add(new ReportParameter("Header_03", "Bezeichnung"));
            repParams.Add(new ReportParameter("Hide_03", "false"));
            repParams.Add(new ReportParameter("Header_04", "Kurz-Bezeichnung"));
            repParams.Add(new ReportParameter("Hide_04", "false"));
            repParams.Add(new ReportParameter("Header_05", "Kurz-Bezeichnung"));
            repParams.Add(new ReportParameter("Hide_05", "true"));
            repParams.Add(new ReportParameter("Header_06", "Aktiv"));
            repParams.Add(new ReportParameter("Hide_06", "true"));
            repParams.Add(new ReportParameter("Header_07", ""));
            repParams.Add(new ReportParameter("Hide_07", "true"));
            repParams.Add(new ReportParameter("Header_08", ""));
            repParams.Add(new ReportParameter("Hide_08", "true"));
            repParams.Add(new ReportParameter("Header_09", ""));
            repParams.Add(new ReportParameter("Hide_09", "true"));

            repParams.Add(new ReportParameter("Title", "Material"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Fabrikate Übersicht"));
            repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/MasterData/ListReport.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;

            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);
            //ViewBag.ReportViewer = reportViewer;
            //return View();


        }
    }
}