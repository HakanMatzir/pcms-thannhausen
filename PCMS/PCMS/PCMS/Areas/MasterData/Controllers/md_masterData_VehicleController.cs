﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PCMS.Models;
using PagedList;
using PCMS.Helper;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_VehicleController : Controller
    {
        private MasterDataContext db = new MasterDataContext();

        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_masterData_Vehicle.Where(m => m.IsActive == true && m.IsDeleted == false).Include(m=>m.Md_Customer).Include(m=>m.md_Facilities).Include(m => m.md_masterData_Driver).Include(m => m.Md_masterData_VehicleType).OrderBy(a => a.Id).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "VehicleNumber":
                                model = model.Where(m => m.VehicleNumber != null && m.VehicleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.VehicleNumber = pgFF.colVal;
                                break;
                            case "PlateNumber":
                                model = model.Where(m => m.PlateNumber != null && m.PlateNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.NumberPlate = pgFF.colVal;
                                break;
                            case "Capacity":
                                model = model.Where(m => m.Capacity != null && m.Capacity.ToString().ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Capacity = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Md_Customer.Name != null && m.Md_Customer.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Md_masterData_Customer = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "VehicleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.VehicleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.VehicleNumber).ToList();
                    break;
                case "PlateNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.PlateNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.PlateNumber).ToList();
                    break;
                case "Capacity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Capacity).ToList();
                    else
                        model = model.OrderByDescending(m => m.Capacity).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_Customer.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_Customer.Name).ToList();
                    break;
                    //case "Name":
                    //    if (pg.orderDir.Equals("desc"))
                    //        model = model.OrderBy(m => m.Md_masterData_Customer.Name).ToList();
                    //    else
                    //        model = model.OrderByDescending(m => m.Md_masterData_Customer.Name).ToList();
                    //    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        // GET
        public ActionResult Create()
        {
            ViewBag.VehicleTypeId = new SelectList(db.Md_masterData_VehicleType.ToList(), "Id", "Description");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.vehicle.IsDeleted = false;
                model.vehicle.IsActive = true;
                db.Md_masterData_Vehicle.Add(model.vehicle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.VehicleTypeId = new SelectList(db.Md_masterData_VehicleType.ToList(), "Id", "Description");
            return View(model);
        }

        // GET
        public ActionResult Edit(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model.vehicle = db.Md_masterData_Vehicle.Find(id);
            model.facili = db.Facilities.Find(model.vehicle.FacilitiesId);
            model.driver = db.Md_masterData_Driver.Find(model.vehicle.DriverId);
            model.customer = db.Md_masterData_Customer.Find(model.vehicle.CustomerId);
            model.vehicleType = db.Md_masterData_VehicleType.Find(model.vehicle.VehicleTypeId);
            if (model == null)
            {
                return HttpNotFound();
            }
            ViewBag.VehicleTypeId = new SelectList(db.Md_masterData_VehicleType.ToList(), "Id", "Description");
            return View(model);
        }

        // POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.vehicle.IsActive = true;
                model.vehicle.IsDeleted = false;
                db.Entry(model.vehicle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.VehicleTypeId = new SelectList(db.Md_masterData_VehicleType, "Id", "Description", model.vehicle.VehicleTypeId);
            ViewBag.DriverId = new SelectList(db.Md_masterData_Driver, "Id", "DriverId", model.vehicle.DriverId);
            ViewBag.MainLocation = new SelectList(db.Facilities, "Id", "Description", model.vehicle.FacilitiesId);
            return View(model);
        }

        // GET
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_masterData_Vehicle md_masterData_Vehicle = db.Md_masterData_Vehicle.Find(id);
            if (md_masterData_Vehicle == null)
            {
                return HttpNotFound();
            }
            return View(md_masterData_Vehicle);
        }

        // POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(md_masterData_Vehicle model, long id)
        {
            model.IsActive = true;
            model.IsDeleted = true;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ChooseDriver(PaginationModel pg)
        {
            var model = db.Md_masterData_Driver.Where(m => m.IsActive == true && m.IsDeleted == false).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Number":
                                model = model.Where(m => m.DriverNumber != null && m.DriverNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.VehicleNumber = pgFF.colVal;
                                break;
                            case "FirstName":
                                model = model.Where(m => m.Md_masterData_Contact.FirstName != null && m.Md_masterData_Contact.FirstName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.NumberPlate = pgFF.colVal;
                                break;
                            case "LastName":
                                model = model.Where(m => m.Md_masterData_Contact.LastName != null && m.Md_masterData_Contact.LastName.ToString().ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Capacity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Number":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.DriverNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.DriverNumber).ToList();
                    break;
                case "FirstName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Contact.FirstName).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Contact.FirstName).ToList();
                    break;
                case "LastName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Contact.LastName).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Contact.LastName).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult ChooseFacilities(PaginationModel pg)
        {
            var model = db.Facilities.ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Description":
                                model = model.Where(m => m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));
        }

        public ActionResult ChooseVehicleType()
        {
            var model = db.Md_masterData_VehicleType.Where(m=>m.IsActive == true && m.IsDeleted == false).ToList();
            return View(model);
        }

        public ActionResult ChooseCustomer(PaginationModel pg)
        {
            var model = db.Md_masterData_Customer.Where(m => m.IsActive == true && m.IsDeleted == false).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "CustomerId":
                                model = model.Where(m => m.CustomerId != null && m.CustomerId.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "CustomerId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CustomerId).ToList();
                    else
                        model = model.OrderByDescending(m => m.CustomerId).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).ToList();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).ToList();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}




