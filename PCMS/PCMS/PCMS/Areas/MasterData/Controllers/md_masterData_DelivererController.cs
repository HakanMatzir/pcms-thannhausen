﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_DelivererController : Controller
    {
        MasterDataContext db = new MasterDataContext();

        // GET: MasterData/md_masterData_Deliverer
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_masterData_Deliverer.Where(m => m.IsActive == true && m.IsDeleted == false).Include(m => m.Md_masterData_Address).Include(m => m.Md_masterData_Contact).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "DelivererId":
                                model = model.Where(m => m.DelivererId.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address != null && m.Md_masterData_Address.Street != null && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address != null && m.Md_masterData_Address.ZipCode != null &&  m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address != null && m.Md_masterData_Address.City != null && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "DelivererId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.DelivererId).ToList();
                    else
                        model = model.OrderByDescending(m => m.DelivererId).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).ToList();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).ToList();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        // Get
        public ActionResult Create()
        {
            return View();
        }

        // Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.deliverer.IsActive = true;
                model.deliverer.IsDeleted = false;

                db.Md_masterData_Address.Add(model.address);
                db.Md_masterData_Contact.Add(model.contact);
                db.SaveChanges();

                model.deliverer.AddressId = model.address.Id;
                model.deliverer.ContactId = model.contact.Id;
                db.Md_masterData_Deliverer.Add(model.deliverer);
                
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Edit(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Deliverer.Find(id) == null)
            {
                return HttpNotFound();
            }

            model.deliverer = db.Md_masterData_Deliverer.Find(id);
            model.address = db.Md_masterData_Address.Find(model.deliverer.AddressId);
            model.contact = db.Md_masterData_Contact.Find(model.deliverer.ContactId);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.deliverer.IsActive = true;
                model.deliverer.IsDeleted = false;
                db.Entry(model.deliverer).State = EntityState.Modified;
                db.Entry(model.address).State = EntityState.Modified;
                db.Entry(model.contact).State = EntityState.Modified;
                
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET
        public ActionResult Delete(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Deliverer.Find(id) == null)
            {
                return HttpNotFound();
            }

            model.deliverer = db.Md_masterData_Deliverer.Find(id);

            return View(model);
        }

        // POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(MasterDataContactView model, long id)
        {
            if (ModelState.IsValid)
            {
                model.deliverer.IsDeleted = true;
                db.Entry(model.deliverer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }
}