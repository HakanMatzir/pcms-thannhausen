﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PCMS.Helper;

namespace PCMS.Areas.Order.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_VehicleGroupController : Controller
    {
        private MasterDataContext db = new MasterDataContext();

        // GET: Order/md_order_VehicleGroup
        public ActionResult Index()
        {
            return View(db.Md_masterData_VehicleGroup.ToList());
        }

        //GET: Order/md_order_VehicleGroup/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_masterData_VehicleGroup md_masterData_VehicleGroup = db.Md_masterData_VehicleGroup.Find(id);
            if (md_masterData_VehicleGroup == null)
            {
                return HttpNotFound();
            }
            return View(md_masterData_VehicleGroup);
        }

        // GET: Order/md_order_VehicleGroup/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Order/md_order_VehicleGroup/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Description")] md_masterData_VehicleGroup md_masterData_VehicleGroup)
        {
            if (ModelState.IsValid)
            {
                db.Md_masterData_VehicleGroup.Add(md_masterData_VehicleGroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(md_masterData_VehicleGroup);
        }

        // GET: Order/md_order_VehicleGroup/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_masterData_VehicleGroup md_order_VehicleGroup = db.Md_masterData_VehicleGroup.Find(id);
            if (md_order_VehicleGroup == null)
            {
                return HttpNotFound();
            }
            return View(md_order_VehicleGroup);
        }

        // POST: Order/md_order_VehicleGroup/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description")] md_masterData_VehicleGroup md_masterData_VehicleGroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(md_masterData_VehicleGroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(md_masterData_VehicleGroup);
        }

        // GET: Order/md_order_VehicleGroup/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_masterData_VehicleGroup md_order_VehicleGroup = db.Md_masterData_VehicleGroup.Find(id);
            if (md_order_VehicleGroup == null)
            {
                return HttpNotFound();
            }
            return View(md_order_VehicleGroup);
        }

        // POST: Order/md_order_VehicleGroup/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            md_masterData_VehicleGroup md_masterData_VehicleGroup = db.Md_masterData_VehicleGroup.Find(id);
            db.Md_masterData_VehicleGroup.Remove(md_masterData_VehicleGroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
