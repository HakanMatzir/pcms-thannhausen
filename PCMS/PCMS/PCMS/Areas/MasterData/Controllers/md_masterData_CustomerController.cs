﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_CustomerController : Controller
    {
        public bool checkTrader = true;
        public bool checkRate = true;
        private MasterDataContext db = new MasterDataContext();

        // GET: MasterData/md_masterData_Customer
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_masterData_Customer.Include(m=>m.Md_masterData_Contact).Include(m=>m.Md_masterData_Address).Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "CustomerId":
                                model = model.Where(m => m.CustomerId.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address!=null && m.Md_masterData_Address.Street != null && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode != null && m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address!= null && m.Md_masterData_Address.City != null && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).ToList();
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "CustomerId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CustomerId).ToList();
                    else
                        model = model.OrderByDescending(m => m.CustomerId).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).ToList();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).ToList();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        // Get
        public ActionResult Create()
        {
            ViewBag.Number = db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CustomerNumber).FirstOrDefault();
            return View();
        }

        // Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MasterDataContactView model)
        {
            if(!String.IsNullOrEmpty(model.customer.TraderId))
            {
                if (db.Md_masterData_Trader.Any(m => m.TraderId == model.customer.TraderId) == false)
                {
                    checkTrader = false;
                }
                else
                {
                    checkTrader = true;
                }
            }

            if(!String.IsNullOrEmpty(model.custRating.Description))
            {
                if(db.Md_masterData_CustomerRating.Any(m => m.Description == model.custRating.Description) == false)
                {
                    checkRate = false;
                }
                else
                {
                    checkRate = true;
                }
            }

            if (ModelState.IsValid && checkTrader == true && checkRate == true)
            {
                model.customer.IsActive = true;
                model.customer.IsDeleted = false;

                db.Md_masterData_Address.Add(model.address);
                if (db.Md_masterData_Address.Add(model.address) != null)
                {
                    db.SaveChanges();
                }
                db.Md_masterData_Contact.Add(model.contact);
                if (db.Md_masterData_Contact.Add(model.contact) != null)
                {
                    db.SaveChanges();
                }

                model.customer.AddressId = model.address.Id;
                model.customer.ContactId = model.contact.Id;
                db.Md_masterData_Customer.Add(model.customer);
                
                db.SaveChanges();
        
                try
                {
                    var CustomerNumber = Convert.ToInt64(db.Md_material_Config.Where(a => a.Id == 1).Select(a => a.CustomerNumber).FirstOrDefault());
                    CustomerNumber = CustomerNumber+1;
                    string query = "UPDATE md_Config SET CustomerNumber = " + CustomerNumber.ToString() + " WHERE Id = 1";
                    db.Database.ExecuteSqlCommand(query);
                }
                catch
                {

                }
                
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("Fehler", "Fehlermeldung");
                return View();
            }
        }

        public ActionResult Edit(MasterDataContactView model, long? id)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (db.Md_masterData_Customer.Find(id) == null)
                {
                    return HttpNotFound();
                }

                model.customer = db.Md_masterData_Customer.Find(id);
                model.address = db.Md_masterData_Address.Find(model.customer.AddressId);
                model.contact = db.Md_masterData_Contact.Find(model.customer.ContactId);
                model.custRating = db.Md_masterData_CustomerRating.Find(model.customer.CustomerRatingId);

                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterDataContactView model)
        {

            if (!String.IsNullOrEmpty(model.customer.TraderId))
            {
                if (db.Md_masterData_Trader.Any(m => m.TraderId == model.customer.TraderId) == false)
                {
                    checkTrader = false;
                }
                else
                {
                    checkTrader = true;
                }
            }

            if (!String.IsNullOrEmpty(model.custRating.Description))
            {
                if (db.Md_masterData_CustomerRating.Any(m => m.Description == model.custRating.Description) == false)
                {
                    checkRate = false;
                }
                else
                {
                    checkRate = true;
                }
            }

            if (checkTrader == true && checkRate == true)
            {
                model.customer.IsActive = true;
                model.customer.IsDeleted = false;
                //Änderung Manu 2016_03_09
                if (model.contact.Id == null)
                {
                    db.Entry(model.contact).State = EntityState.Added;
                    db.SaveChanges();
                    model.customer.ContactId = model.contact.Id;
                }
                else
                    db.Entry(model.contact).State = EntityState.Modified;

                db.Entry(model.customer).State = EntityState.Modified;
                db.Entry(model.address).State = EntityState.Modified;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET
        public ActionResult Delete(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Customer.Find(id) == null)
            {
                return HttpNotFound();
            }

            model.customer = db.Md_masterData_Customer.Find(id);

            return View(model);
        }

        // POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(MasterDataContactView model, long id)
        {
            if (ModelState.IsValid)
            {
                model.customer.IsActive = true;
                model.customer.IsDeleted = true;
                db.Entry(model.customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult MarkConstructionSite(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_ConstructionSite.Find(id) == null)
            {
                return HttpNotFound();
            }

            model.constructionSite = db.Md_masterData_ConstructionSite.Find(id);

            return View(model);
        }

        public bool MarkConstructionSiteConfirmed(long id)
        {
            try
            {
                MasterDataContactView model = new MasterDataContactView();
                model.constructionSite = db.Md_masterData_ConstructionSite.Find(id);
                model.constructionSite.IsActive = false;
                db.Entry(model.constructionSite).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public ActionResult DeleteConstructionSite(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_ConstructionSite.Find(id) == null)
            {
                return HttpNotFound();
            }

            model.constructionSite = db.Md_masterData_ConstructionSite.Find(id);

            return View(model);
        }

        [HttpPost, ActionName("DeleteConstructionSite")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConstructionSiteConfirmed(MasterDataContactView model, long id)
        {
            if (ModelState.IsValid)
            {
                var CustomerId = db.Md_masterData_ConstructionSite.Where(a => a.Id == id).Select(a => a.CustomerId).FirstOrDefault();
                model.constructionSite = db.Md_masterData_ConstructionSite.Find(id);
                model.constructionSite.IsActive = true;
                model.constructionSite.IsDeleted = true;
                db.Entry(model.constructionSite).State = EntityState.Modified;
                db.SaveChanges();
                if(CustomerId != null)
                {
                    return RedirectToAction("Edit/" + CustomerId);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        // GET
        public ActionResult ChooseTrader(PaginationModel pg)
        {
            var model = db.Md_masterData_Trader.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "TraderId":
                                model = model.Where(m => m.TraderId.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).ToList();
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "TraderId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.TraderId).ToList();
                    else
                        model = model.OrderByDescending(m => m.TraderId).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).ToList();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).ToList();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));
        }

        public ActionResult ChooseRating()
        {
            var model = db.Md_masterData_CustomerRating.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();
            return View(model);
        }

        public ActionResult ConstructionSiteIndex(long id)
        {
            var model = db.Md_masterData_ConstructionSite.Where(m=>m.CustomerId == id && m.IsDeleted == false).ToList();
            return View(model);
        }

        public ActionResult VehicleIndex(long id)
        {
            var model = db.Md_masterData_Vehicle.Where(m => m.CustomerId == id).ToList();
            return View(model);
        }

        public ActionResult SearchCity(long zip)
        {
            var City = db.Md_ZipCodes.Where(a => a.ZipCode == zip).Select(a => a.City).FirstOrDefault();
            return Json(new { City = City });
        }

        public ActionResult SearchZip(string city)
        {
            var Zip = db.Md_ZipCodes.Where(a => a.City.ToLower() == city.ToLower()).Select(a => a.ZipCode).FirstOrDefault();
            return Json(new { Zip = Zip });
        }
    }
}