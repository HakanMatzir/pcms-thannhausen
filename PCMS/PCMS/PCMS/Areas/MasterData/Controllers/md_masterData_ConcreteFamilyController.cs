﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;


namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_ConcreteFamilyController : Controller
    {
        MasterDataContext db = new MasterDataContext();
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_masterData_ConcreteFamily.Where(a => a.IsActive == true && a.IsDeleted == false).OrderBy(a=>a.Id).ToList();
            
            if(pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Description":
                                model = model.Where(m => m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch(pg.orderCol)
            {
                case "Description":
                    if (pg.orderCol.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(md_material_ConcreteFamilyGroup model)
        {
            model.md_material_Material = db.Md_masterData_Material.Find(model.ReferenceSort);
            ModelState["md_material_Material.Name"].Errors.Clear();
            if (ModelState.IsValid)
            {
                model.IsActive = true;
                model.IsDeleted = false;
                model.TimeTestRequired = false;
                model.AmountTestRequired = false;
                db.Md_masterData_ConcreteFamily.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Edit(md_material_ConcreteFamilyGroup model, long? Id)
        {
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_ConcreteFamily.Find(Id) == null)
            {
                return HttpNotFound();
            }
            model = db.Md_masterData_ConcreteFamily.Find(Id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(md_material_ConcreteFamilyGroup model)
        {
            model.md_material_Material = db.Md_masterData_Material.Find(model.ReferenceSort);
            ModelState["md_material_Material.Name"].Errors.Clear();
            if (ModelState.IsValid)
            {
                model.IsActive = true;
                model.IsDeleted = false;
                model.AmountTestRequired = false;
                model.TimeTestRequired = false;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(md_material_ConcreteFamilyGroup model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_ConcreteFamily.Find(id) == null)
            {
                return HttpNotFound();
            }

            model = db.Md_masterData_ConcreteFamily.Find(id);
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(md_material_ConcreteFamilyGroup model, long id)
        {
            if (ModelState.IsValid)
            {
                model = db.Md_masterData_ConcreteFamily.Find(id);
                model.IsDeleted = true;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult SelectReferenceSort(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(a => a.IsActive == true && a.IsDeleted == false && a.MaterialGroupId == 5).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Number = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Number = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Name":
                    if (pg.orderCol.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderCol.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
                case "MaterialNumber":
                    if (pg.orderCol.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
    }
}