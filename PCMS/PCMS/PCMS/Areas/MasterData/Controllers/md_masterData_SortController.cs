﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_SortController : Controller
    {
        MasterDataContext db = new MasterDataContext();
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 5).OrderBy(a => a.Id).ToList();
            long familyId = -1;
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Name = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ShortName = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.MaterialNumber = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ArticleNumber = pgFF.colVal;
                                break;
                            case "Family":
                                long id = Convert.ToInt64(pgFF.colVal);
                                familyId = id;
                                List<long?> ids = db.Md_material_SortDetails.Where(a => a.Id != null && a.ConcreteFamilyId == id).Select(a => a.Id).ToList();
                                List<long> idsWON = new List<long>();
                                foreach(var l in ids)
                                {
                                    if (idsWON != null)
                                        idsWON.Add(l ?? 0);
                                }
                                model = model.Where(a => a.SortId != null && idsWON.Contains(a.SortId ?? 0)).ToList();
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }

            ViewBag.Index = "sort";
            ViewBag.Family = db.Md_masterData_ConcreteFamily.Where(a => a.IsActive == true && a.IsDeleted == false).ToList();
            ViewBag.sortDetails = db.Md_material_SortDetails.ToList();
            ViewBag.FamilySelect = new SelectList(db.Md_masterData_ConcreteFamily.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description",familyId);
            return View("Index", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult Create()
        {
            
            ViewBag.Family = new SelectList(db.Md_masterData_ConcreteFamily.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.DNType = new SelectList(db.Md_order_DeliveryNoteType.OrderBy(a => a.Id).ToList(), "Id", "Description");
            ViewBag.Expo = new SelectList(db.Md_material_ExposureKombi.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "ShortText");
            ViewBag.Progress = new SelectList(db.Md_material_Progress.Where(a => a.MaterialTypeId == 5 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.Chlorid = new SelectList(db.Md_material_ChlorideGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.Consistency = new SelectList(db.Md_material_ConsistencyGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.CompressionStrenght = new SelectList(db.Md_material_CompressionStrenghtGroup.Where(a => a.MaterialGroupId == 5 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.MaterialType = new SelectList(db.Md_material_MaterialType.Where(a => a.MaterialGroupId == 5 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.Norm = new SelectList(db.Md_masterData_Material_Standard.Where(a => a.MaterialTypeId == 5 && a.IsDeleted == false && a.IsActive == true).ToList(), "Id", "ShortName");
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            return View("Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.MaterialGroupId = 5;
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Md_material_SortDetails.Add(model.sortDetails);
                db.SaveChanges();
                model.material.SortId = model.sortDetails.Id;
                db.Md_masterData_Material.Add(model.material);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            
            ViewBag.Family = new SelectList(db.Md_masterData_ConcreteFamily.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.DNType = new SelectList(db.Md_order_DeliveryNoteType.OrderBy(a => a.Id).ToList(), "Id", "Description");
            ViewBag.Expo = new SelectList(db.Md_material_ExposureKombi.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "ShortText");
            ViewBag.Progress = new SelectList(db.Md_material_Progress.Where(a => a.MaterialTypeId == 5 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.Chlorid = new SelectList(db.Md_material_ChlorideGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.Consistency = new SelectList(db.Md_material_ConsistencyGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.CompressionStrenght = new SelectList(db.Md_material_CompressionStrenghtGroup.Where(a => a.MaterialGroupId == 5 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.MaterialType = new SelectList(db.Md_material_MaterialType.Where(a => a.MaterialGroupId == 5 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.Norm = new SelectList(db.Md_masterData_Material_Standard.Where(a => a.MaterialTypeId == 5 && a.IsDeleted == false && a.IsActive == true).ToList(), "Id", "ShortName");
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            return View("Create", model);
        }

        public ActionResult Edit(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Material.Find(id) == null)
            {
                return HttpNotFound();
            }
            else
            {
                model.material = db.Md_masterData_Material.Find(id);
                model.deliverer = db.Md_masterData_Deliverer.Find(model.material.DelivererId);
                model.facili = db.Facilities.Find(model.material.FacilitiesId);
                model.materialType = db.Md_material_MaterialType.Find(model.material.MaterialTypeId);

                model.sortDetails = db.Md_material_SortDetails.Find(model.material.SortId);
                model.standard = db.Md_masterData_Material_Standard.Find(model.sortDetails.NormId);
                model.progress = db.Md_material_Progress.Find(model.sortDetails.ProgressId);
                model.consistency = db.Md_material_ConsistencyGroup.Find(model.sortDetails.ConsistencyId);
                model.compression = db.Md_material_CompressionStrenghtGroup.Find(model.sortDetails.CompressionId);
                model.chlorid = db.Md_material_ChlorideGroup.Find(model.sortDetails.ChloridId);
                model.exposure = db.Md_material_ExposureKombi.Find(model.sortDetails.ExposureKombiId);
                model.concrete = db.Md_masterData_ConcreteFamily.Find(model.sortDetails.ConcreteId);
                model.dnType = db.Md_order_DeliveryNoteType.Find(model.sortDetails.DeliveryNoteTypeId);
                model.concrete = db.Md_masterData_ConcreteFamily.Find(model.sortDetails.ConcreteFamilyId);
                
            }
            
            ViewBag.Family = new SelectList(db.Md_masterData_ConcreteFamily.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.DNType = new SelectList(db.Md_order_DeliveryNoteType.OrderBy(a => a.Id).ToList(), "Id", "Description");
            ViewBag.Expo = new SelectList(db.Md_material_ExposureKombi.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "ShortText");
            ViewBag.Progress = new SelectList(db.Md_material_Progress.Where(a => a.MaterialTypeId == 5 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.Chlorid = new SelectList(db.Md_material_ChlorideGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.Consistency = new SelectList(db.Md_material_ConsistencyGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.CompressionStrenght = new SelectList(db.Md_material_CompressionStrenghtGroup.Where(a => a.MaterialGroupId == 5 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.MaterialType = new SelectList(db.Md_material_MaterialType.Where(a => a.MaterialGroupId == 5 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.Norm = new SelectList(db.Md_masterData_Material_Standard.Where(a => a.MaterialTypeId == 5 && a.IsDeleted == false && a.IsActive == true).ToList(), "Id", "ShortName");
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Entry(model.material).State = EntityState.Modified;
                db.Entry(model.sortDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            ViewBag.Family = new SelectList(db.Md_masterData_ConcreteFamily.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.DNType = new SelectList(db.Md_order_DeliveryNoteType.OrderBy(a => a.Id).ToList(), "Id", "Description");
            ViewBag.Expo = new SelectList(db.Md_material_ExposureKombi.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "ShortText");
            ViewBag.Progress = new SelectList(db.Md_material_Progress.Where(a => a.MaterialTypeId == 5 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.Chlorid = new SelectList(db.Md_material_ChlorideGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.Consistency = new SelectList(db.Md_material_ConsistencyGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.CompressionStrenght = new SelectList(db.Md_material_CompressionStrenghtGroup.Where(a => a.MaterialGroupId == 5 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.MaterialType = new SelectList(db.Md_material_MaterialType.Where(a => a.MaterialGroupId == 5 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.material.MaterialTypeId);
            ViewBag.Norm = new SelectList(db.Md_masterData_Material_Standard.Where(a => a.MaterialTypeId == 5 && a.IsDeleted == false && a.IsActive == true).ToList(), "Id", "ShortName", model.binderDetails.NormId);
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();

            return View(model);
        }

        [HttpPost]
        public ActionResult Copy(long IdCopy, string DescriptionCopy, string NumberCopy)
        {
            var model = db.Md_masterData_Material.AsNoTracking().FirstOrDefault(a => a.Id == IdCopy);
            var sortModel = db.Md_material_SortDetails.Find(model.SortId);
            md_material_SortDetails SortModelNew = new md_material_SortDetails();
            SortModelNew.NormId = sortModel.NormId;
            SortModelNew.ProgressId = sortModel.ProgressId;
            SortModelNew.Verification = sortModel.Verification;
            SortModelNew.WZValue = sortModel.WZValue;
            SortModelNew.Aggregate = sortModel.Aggregate;
            SortModelNew.ChloridId = sortModel.ChloridId;
            SortModelNew.CompressionId = sortModel.CompressionId;
            SortModelNew.ConcreteFamilyId = sortModel.ConcreteFamilyId;
            SortModelNew.ConcreteId = sortModel.ConcreteId;
            SortModelNew.ConsistencyId = sortModel.ConsistencyId;
            SortModelNew.CuringTime1 = sortModel.CuringTime1;
            SortModelNew.CuringTime2 = sortModel.CuringTime2;
            SortModelNew.CuringTime3 = sortModel.CuringTime3;
            SortModelNew.CuringTime4 = sortModel.CuringTime4;
            SortModelNew.DeliveryNoteTypeId = sortModel.DeliveryNoteTypeId;
            SortModelNew.ExposureKombiId = sortModel.ExposureKombiId;

            db.Md_material_SortDetails.Add(SortModelNew);
            db.SaveChanges();

            model.SortId = SortModelNew.Id;
            model.Id = 0;
            model.MaterialNumber = NumberCopy;
            model.Name = DescriptionCopy;
            db.Md_masterData_Material.Add(model);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = model.Id });
        }

        public ActionResult Delete(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model.material = db.Md_masterData_Material.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }

            ViewBag.RecipeCheck = db.Md_material_RecipeMaterial.Where(a => a.MaterialId == id && a.Md_Recipe.IsActive == true && a.Md_Recipe.IsDeleted == false).Select(a => a.Md_Recipe.Number).ToList().Count();
            ViewBag.RecipeNumber = db.Md_material_RecipeMaterial.Where(a => a.MaterialId == id && a.Md_Recipe.IsActive == true && a.Md_Recipe.IsDeleted == false).Select(a => a.Md_Recipe.Number).ToList();
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(MasterDataContactView model, long id)
        {
            model.material = db.Md_masterData_Material.Find(id);
            model.material.IsActive = true;
            model.material.IsDeleted = true;
            db.Entry(model.material).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        //#################################################################################################################################
        //########################################## Entwicklung hinzufügen und löschen Start #############################################
        //#################################################################################################################################

        public ActionResult CreateExpo()
        {
            return View();
        }

        public ActionResult SaveExpo(string shortText, string longText)
        {
            md_material_ExposureKombi model = new md_material_ExposureKombi();
            if (ModelState.IsValid)
            {
                model.ShortText = shortText;
                model.LongText = longText;
                model.IsActive = true;
                model.IsDeleted = false;
                db.Md_material_ExposureKombi.Add(model);
                db.SaveChanges();

                return Json(new { id = model.Id });
            }

            return Json(false);
        }

        public ActionResult DeleteExpo(md_material_ExposureKombi model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_ExposureKombi.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveExpo(md_material_ExposureKombi model, long? id)
        {
            model = db.Md_material_ExposureKombi.Find(id);

            model.IsDeleted = true;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //#################################################################################################################################
        //########################################## Entwicklung hinzufügen und löschen Start #############################################
        //#################################################################################################################################

        public ActionResult CreateProgress()
        {
            return View();
        }

        public ActionResult SaveProgress(string description)
        {
            md_material_Progress model = new md_material_Progress();
            if (ModelState.IsValid)
            {
                model.MaterialTypeId = 5;
                model.IsActive = true;
                model.IsDeleted = false;
                model.Description = description;
                db.Md_material_Progress.Add(model);
                db.SaveChanges();

                return Json(new { id = model.Id });
            }

            return Json(false);
        }

        public ActionResult DeleteProgress(md_material_Progress model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_Progress.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveProgress(md_material_Progress model, long? id)
        {
            model = db.Md_material_Progress.Find(id);

            model.IsDeleted = true;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //#################################################################################################################################
        //############################################## Chlorid hinzufügen und löschen Start #############################################
        //#################################################################################################################################

        public ActionResult CreateChlorid()
        {
            return View();
        }

        public ActionResult SaveChlorid(string Class, decimal value, string description)
        {
            md_material_ChlorideGroup model = new md_material_ChlorideGroup();
            if (ModelState.IsValid)
            {
                model.MaterialTypeId = 5;
                model.IsActive = true;
                model.IsDeleted = false;
                model.Value = value;
                model.Description = description;
                model.Class = Class;
                db.Md_material_ChlorideGroup.Add(model);
                db.SaveChanges();

                return Json(new { id = model.Id });
            }

            return Json(false);
        }

        public ActionResult DeleteChlorid(md_material_ChlorideGroup model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_ChlorideGroup.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveChlorid(md_material_ChlorideGroup model, long? id)
        {
            model = db.Md_material_ChlorideGroup.Find(id);

            model.IsDeleted = true;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //#################################################################################################################################
        //############################################### Norm hinzufügen und löschen Start ###############################################
        //#################################################################################################################################

        public ActionResult CreateStandard()
        {
            return View();
        }

        public ActionResult SaveStandard(string name, string shortname, string description, string comment)
        {
            md_masterData_Standard model = new md_masterData_Standard();
            if (ModelState.IsValid)
            {
                model.MaterialTypeId = 5;
                model.IsActive = true;
                model.IsDeleted = false;
                model.Name = name;
                model.ShortName = shortname;
                model.Description = description;
                model.Comment = comment;
                db.Md_masterData_Material_Standard.Add(model);
                db.SaveChanges();

                return Json(new { id = model.Id });
            }

            return Json(false);
        }

        public ActionResult DeleteStandard(md_masterData_Standard model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_masterData_Material_Standard.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveStandard(md_masterData_Standard model, long? id)
        {
            model = db.Md_masterData_Material_Standard.Find(id);

            model.IsDeleted = true;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //####################################################################################################################################
        //########################################### Material Typ hinzufügen und löschen Start ##############################################
        //####################################################################################################################################

        public ActionResult CreateMaterialType()
        {
            return View();
        }

        public ActionResult SaveMaterialType(string description)
        {
            md_material_MaterialType model = new md_material_MaterialType();

            model.IsActive = true;
            model.IsDeleted = false;
            model.Description = description;
            model.MaterialGroupId = 5;
            db.Md_material_MaterialType.Add(model);
            db.SaveChanges();

            return Json(new { id = model.Id, type = model.Description });
        }

        public ActionResult DeleteMaterialType(md_material_MaterialType model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_MaterialType.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveMaterialType(md_material_MaterialType model, long? id, string description)
        {
            model = db.Md_material_MaterialType.Find(id);
            model.IsDeleted = true;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //####################################################################################################################################
        //########################################## Festigkeitskl. hinzufügen und löschen Start #############################################
        //####################################################################################################################################

        public ActionResult CreateCompressionStrenght()
        {
            return View();
        }

        public ActionResult SaveCompressionStrenght(string description, string Class, decimal min, decimal max)
        {
            md_material_CompressionStrenghtGroup model = new md_material_CompressionStrenghtGroup();
            model.MaterialGroupId = 5;
            model.Class = Class;
            model.IsDeleted = false;
            model.IsActive = true;
            model.Description = description;
            model.Min = min;
            model.Max = max;

            db.Md_material_CompressionStrenghtGroup.Add(model);
            db.SaveChanges();

            return Json(new { id = model.Id, type = model.Class });
        }

        public ActionResult DeleteCompressionStrenght(md_material_CompressionStrenghtGroup model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_CompressionStrenghtGroup.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveCompressionStrenght(md_material_CompressionStrenghtGroup model, int? id)
        {
            model = db.Md_material_CompressionStrenghtGroup.Find(id);
            model.IsDeleted = true;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //####################################################################################################################################
        //########################################## Konsistenzkl. hinzufügen und löschen Start ##############################################
        //####################################################################################################################################

        public ActionResult CreateConsistency()
        {
            return View();
        }

        public ActionResult SaveConsistency(string description, string Class, decimal? ValueMin, decimal? ValueMax, string CompactClass, decimal? CompactValueMin, decimal? CompactValueMax )
        {
            md_material_ConsistencyGroup model = new md_material_ConsistencyGroup();
            model.Class = Class;
            model.IsDeleted = false;
            model.IsActive = true;
            model.Description = description;
            model.ValueMin = ValueMin;
            model.ValueMax = ValueMax;
            model.CompactingClass = CompactClass;
            model.CompactingValueMax = CompactValueMax;
            model.CompactingValueMin = CompactValueMin;

            db.Md_material_ConsistencyGroup.Add(model);
            db.SaveChanges();

            return Json(new { id = model.Id, type = model.Class });
        }

        public ActionResult DeleteConsistency(md_material_ConsistencyGroup model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_ConsistencyGroup.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveConsistency(md_material_ConsistencyGroup model, int? id)
        {
            model = db.Md_material_ConsistencyGroup.Find(id);
            model.IsDeleted = true;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }
    }
}