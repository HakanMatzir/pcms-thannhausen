﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_WaterController : Controller
    {
        MasterDataContext db = new MasterDataContext();
        Areas.Recipe.Models.RecipeContext dbR = new Recipe.Models.RecipeContext();
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 4).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }
            return View("Index", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult Create()
        {
            ViewBag.MaterialType = new SelectList(db.Md_material_MaterialType.Where(a => a.MaterialGroupId == 4 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.MaterialGroupId = 4;
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Md_masterData_Material.Add(model.material);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.MaterialType = new SelectList(db.Md_material_MaterialType.Where(a => a.MaterialGroupId == 4 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            return View(model);
        }

        public ActionResult Edit(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Material.Find(id) == null)
            {
                return HttpNotFound();
            }
            else
            {
                model.material = db.Md_masterData_Material.Find(id);
                model.deliverer = db.Md_masterData_Deliverer.Find(model.material.DelivererId);
                model.facili = db.Facilities.Find(model.material.FacilitiesId);
            }

            ViewBag.MaterialType = new SelectList(db.Md_material_MaterialType.Where(a => a.MaterialGroupId == 4 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.material.MaterialTypeId);
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                decimal? dens = db.Md_masterData_Material.AsNoTracking().Where(a=>a.Id == model.material.Id ).FirstOrDefault().Density;
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Entry(model.material).State = EntityState.Modified;
                db.SaveChanges();
                //neuberechnen der Rezepte
                if (dens != 0 && dens != model.material.Density)
                {
                    Recipe.Controllers.md_recipe_RecipeController.calcRecipeMaterialByMaterialId(model.material.Id ?? 0, User.Identity.Name);
                }
                return RedirectToAction("Index");
            }

            ViewBag.MaterialType = new SelectList(db.Md_material_MaterialType.Where(a => a.MaterialGroupId == 4 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.material.MaterialTypeId);
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();

            return View(model);
        }

        public ActionResult Delete(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model.material = db.Md_masterData_Material.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            ViewBag.RecipeCheck = db.Md_recipeMaterialM.Where(a => a.MaterialId == id && a.Md_recipe_Recipe.IsActive == true && a.Md_recipe_Recipe.IsDeleted == false).Select(a => a.Md_recipe_Recipe.Number).ToList().Count();
            ViewBag.RecipeNumber = db.Md_recipeMaterialM.Where(a => a.MaterialId == id && a.Md_recipe_Recipe.IsActive == true && a.Md_recipe_Recipe.IsDeleted == false).Select(a => a.Md_recipe_Recipe.Number).ToList();
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(MasterDataContactView model, long id)
        {
            model.material.IsActive = true;
            model.material.IsDeleted = true;
            db.Entry(model.material).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");

        }

        //####################################################################################################################################
        //########################################### Material Typ hinzufügen und löschen Start ##############################################
        //####################################################################################################################################

        public ActionResult CreateMaterialType()
        {
            return View();
        }

        public ActionResult SaveMaterialType(string description)
        {
            md_material_MaterialType model = new md_material_MaterialType();

            model.IsActive = true;
            model.IsDeleted = false;
            model.Description = description;
            model.MaterialGroupId = 4;
            db.Md_material_MaterialType.Add(model);
            db.SaveChanges();

            return Json(new { id = model.Id, type = model.Description });
        }

        public ActionResult DeleteMaterialType(md_material_MaterialType model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_MaterialType.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveMaterialType(md_material_MaterialType model, long? id, string description)
        {
            model.IsActive = true;
            model.IsDeleted = true;
            model.MaterialGroupId = 4;
            model.Description = description;
            model.Id = id;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }
        //edit Bei Manuel Schall 2016_10_19
        /// <summary>
        /// Bei einer Änderung der Dichte müssen alle Rezepte neu berechnet werden
        /// </summary>
        /// <param name="id">MaterialId des gänderten Materials</param>

        
    }
}