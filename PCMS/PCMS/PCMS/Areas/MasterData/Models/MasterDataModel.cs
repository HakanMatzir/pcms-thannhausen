﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace PCMS.Areas.MasterData.Models
{
    public class MasterDataContext : DbContext
    {
        public MasterDataContext()
            : base("DefaultConnection")
        {
        }

        public virtual DbSet<md_masterData_VehicleGroup> Md_masterData_VehicleGroup { get; set; }
        public virtual DbSet<md_masterData_Address> Md_masterData_Address { get; set; }
        public virtual DbSet<md_masterData_AddressTamplate> Md_masterData_AddressTamplate { get; set; }
        public virtual DbSet<md_masterData_Company> Md_masterData_Company { get; set; }
        public virtual DbSet<md_masterData_CompanyGroup> Md_masterData_CompanyGroup { get; set; }
        public virtual DbSet<md_masterData_CompanyRating> Md_masterData_CompanyRating { get; set; }
        public virtual DbSet<md_masterData_Contact> Md_masterData_Contact { get; set; }
        public virtual DbSet<md_masterData_ConstructionSite> Md_masterData_ConstructionSite { get; set; }
        public virtual DbSet<md_masterData_CustomerRating> Md_masterData_CustomerRating { get; set; }
        public virtual DbSet<md_masterData_Customer> Md_masterData_Customer { get; set; }
        public virtual DbSet<md_masterData_Trader> Md_masterData_Trader { get; set; }
        public virtual DbSet<md_masterData_Deliverer> Md_masterData_Deliverer { get; set; }
        public virtual DbSet<md_masterData_Material> Md_masterData_Material { get; set; }
        public virtual DbSet<Customer_ConstractionSite> Md_masterData_CustomerConstructionSite { get; set; }
        public virtual DbSet<md_material_MaterialGroup> Md_material_MaterialGroup { get; set; }
        public virtual DbSet<md_masterData_Standard> Md_masterData_Material_Standard { get; set; }
        public virtual DbSet<md_masterData_SieveSet> Md_masterData_SievSet { get; set; }
        public virtual DbSet<md_material_ConcreteFamilyGroup> Md_masterData_ConcreteFamily { get; set; }
        public virtual DbSet<md_material_ChlorideGroup> Md_material_ChlorideGroup { get; set; }
        public virtual DbSet<md_material_CompressionStrenghtGroup> Md_material_CompressionStrenghtGroup { get; set; }
        public virtual DbSet<md_material_ConsistencyGroup> Md_material_ConsistencyGroup { get; set; }
        public virtual DbSet<md_material_MaxAggregateSizeGroup> Md_material_MaxAggregateSizeGroup { get; set; }
        public virtual DbSet<md_material_ExposureKombi> Md_material_ExposureKombi { get; set; }
        public virtual DbSet<md_masterData_Suitability> Md_masterData_Suitability { get; set; }
        public virtual DbSet<md_recipe_Recipe> Md_recipe_Recipe { get; set; }
        public virtual DbSet<md_material_Client> Md_material_Client { get; set; }
        public virtual DbSet<md_material_RockType> Md_material_RockType { get; set; }
        public virtual DbSet<md_material_GravelGroup> Md_material_GravelGroup { get; set; }
        public virtual DbSet<md_material_GravelType> Md_material_GravelType { get; set; }
        public virtual DbSet<md_material_PetrographicType> Md_material_PetrographicType { get; set; }
        public virtual DbSet<md_material_GeologicalSource> Md_material_GeologicalSource { get; set; }
        public virtual DbSet<md_material_AlkaliGroup> Md_material_AlkaliGroup { get; set; }
        public virtual DbSet<md_material_Aggregate> Md_material_Aggregate { get; set; }
        public virtual DbSet<md_material_MaterialType> Md_material_MaterialType { get; set; }
        public virtual DbSet<md_material_Color> Md_material_Color { get; set; }
        public virtual DbSet<md_material_CompressionGroupCement> Md_material_CompressionGroupCement { get; set; }
        public virtual DbSet<md_material_SortDetails> Md_material_SortDetails { get; set; }
        public virtual DbSet<md_material_Progress> Md_material_Progress { get; set; }
        public virtual DbSet<md_PriceMarkUp> Md_PriceMarkUp { get; set; }
        public virtual DbSet<md_material_RetardationTime> Md_RetardationTime { get; set; }
        

        //public System.Data.Entity.DbSet<PCMS.Areas.MasterData.Models.md_masterData_VehicleType> md_masterData_VehicleType { get; set; }
        public virtual DbSet<md_masterData_VehicleType> Md_masterData_VehicleType { get; set; }
        public System.Data.Entity.DbSet<PCMS.Areas.MasterData.Models.Facilities> Facilities { get; set; }
        public System.Data.Entity.DbSet<PCMS.Areas.MasterData.Models.md_masterData_Driver> Md_masterData_Driver { get; set; }
        public System.Data.Entity.DbSet<PCMS.Areas.MasterData.Models.md_masterData_Vehicle> Md_masterData_Vehicle { get; set; }
        public virtual DbSet<md_order_DeliveryNoteType> Md_order_DeliveryNoteType { get; set; }

        public virtual DbSet<md_material_AggregateDetails> Md_material_AggregateDetails { get; set; }
        public virtual DbSet<md_material_AdditivesDetail> Md_material_AdditivesDetail { get; set; }
        public virtual DbSet<md_material_AdmixturesDetails> Md_material_AdmixturesDetails { get; set; }
        public virtual DbSet<md_material_BinderDetails> Md_material_BinderDetails { get; set; }
        public virtual DbSet<md_masterData_ConcentratesDetails> Md_material_Concentrates { get; set; }
        public virtual DbSet<md_material_BrandDetails> Md_material_Brand { get; set; }

        public virtual DbSet<md_masterData_Config> Md_material_Config { get; set; }
        public virtual DbSet<md_recipe_RecipeMaterialM> Md_recipeMaterialM { get; set; }
        public virtual DbSet<md_material_RecipeMaterial> Md_material_RecipeMaterial { get; set; }

        public virtual DbSet<md_ZipCodes> Md_ZipCodes { get; set; }

        public virtual DbSet<md_sieve_RuleGradingCurve> Md_RuleGrading { get; set; }
        //public virtual DbSet<md_material_ConcreteFamilyGroup> Md_material_ConcreteFamily { get; set; }
    }

    [Table("md_sieve_RuleGradingCurve")]
    public partial class md_sieve_RuleGradingCurve
    {
        public long Id { get; set; }
        [DisplayName("Nummer")]
        public string Number { get; set; }
        [DisplayName("Name")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

        [Table("md_ZipCodes")]
    public partial class md_ZipCodes
    {
        public long Id { get; set; }
        public long ZipCode { get; set; }
        public string City { get; set; }
    }

    [Table("md_material_ConcreteFamilyGroup")]
    public partial class md_material_ConcreteFamilyGroup
    {
        public long Id { get; set; }
        [DisplayName("Betonfamilie")]
        public string Description { get; set; }
        public int InitialProduction_0_Count { get; set; }
        public int InitialProduction_0_Val { get; set; }
        public int InitialProduction_1_Count { get; set; }
        public int InitialProduction_1_Val { get; set; }
        public int SteadyProduction_0_Count { get; set; }
        public int SteadyProduction_0_val { get; set; }
        public int SteadyProduction_1_Count { get; set; }
        public int SteadyProduction_1_Val { get; set; }
        [DisplayName("2 Tagen")]
        public decimal StrengthAfter2 { get; set; }
        [DisplayName("7 Tagen")]
        public decimal StrengthAfter7 { get; set; }
        [DisplayName("28 Tagen")]
        public decimal StrengthAfter28 { get; set; }
        [DisplayName("56 Tagen")]
        public decimal StrengthAfter56 { get; set; }
        [DisplayName("90 Tagen")]
        public decimal StrengthAfter90 { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool TimeTestRequired { get; set; }
        public bool AmountTestRequired { get; set; }
        [DisplayName("Erstherstellung Aktiv")]
        public bool InitialProduction { get; set; }
        [DisplayName("Referenz Sorte")]
        public long ReferenceSort { get; set; }
        [DisplayName("Verfahren")]
        public int? proceed { get; set; }

        [ForeignKey("ReferenceSort")]
        public virtual md_masterData_Material md_material_Material { get; set; }
    }


    [Table("md_order_DeliveryNoteType")]
    public partial class md_order_DeliveryNoteType
    {
        public long Id { get; set; }
        [DisplayName("Lieferscheintyp")]
        public string Description { get; set; }
    }

    [Table("md_material_RetardationTime")]
    public partial class md_material_RetardationTime
    {
        public long Id { get; set; }
        [DisplayName("Zeit")]
        public decimal RetardTime { get; set; }
        [DisplayName("Prozentwert")]
        public decimal Value { get; set; }
        public long MaterialId { get; set; }
        [DisplayName("Saison")]
        public int Season { get; set; }
        [ForeignKey("MaterialId")]
        public virtual md_masterData_Material md_material_Material { get; set; }
    }


    [Table("md_material_ExposureGroup")]
    public partial class md_expoGroup
    {
        public long Id { get; set; }
        [DisplayName("Beschreibung")]
        public string Description { get; set; }
        [DisplayName("Klasse")]
        public string Class { get; set; }
        [DisplayName("Beschreibung (lang)")]
        public string ApplicationExample { get; set; }
    }

    [Table("md_material_BrandDetails")]
    public partial class md_material_BrandDetails
    {
        public long Id { get; set; }
        [DisplayName("Norm")]
        public string Norm { get; set; }
        [DisplayName("Körnung")]
        public string Aggregate { get; set; }
        [DisplayName("Eignung")]
        public string Ability { get; set; }
        [DisplayName("Fabrikat Typ")]
        public string BrandType { get; set; }
        [DisplayName("Festigkeitsklasse")]
        public long? CompressionClassId { get; set; }
    }


    [Table("md_Config")]
    public partial class md_masterData_Config
    {
        public long Id { get; set; }
        public int CompanyTypeId { get; set; }
        public string CustomerNumber { get; set; }
        public int Season { get; set; }
    }

    [Table("md_material_ConcentratesDetails")]
    public partial class md_masterData_ConcentratesDetails
    {
        public long Id { get; set; }
        [DisplayName("Norm")]
        public string Norm { get; set; }
        [DisplayName("Farbe")]
        public string Color { get; set; }
        [DisplayName("Bezeichnung laut Norm")]
        public string DescriptionStandard { get; set; }
        [DisplayName("Eignungsnachweis durch")]
        public string Verification { get; set; }
        [DisplayName("Prüfzeichen")]
        public string Certification { get; set; }
        [DisplayName("Ausstellungsdatum")]
        public DateTime? DateOfIssue { get; set; }
        [DisplayName("Gültigkeitsdatum")]
        public DateTime? TerminationDate { get; set; }
    }

    [Table("md_PriceMarkUp")]
    public partial class md_PriceMarkUp
    {
        public long Id { get; set; }
        [DisplayName("Bezeichnung")]
        public string Description { get; set; }
        [DisplayName("Kurzbezeichnung")]
        public string ShortDescription { get; set; }
        [DisplayName("Preis Typ")]
        public long? PriceType { get; set; }
        [DisplayName("EDV-Code")]
        public string EDVCode { get; set; }
        [DisplayName("Menge")]
        public decimal? Amount { get; set; }
        [DisplayName("Mengen Einheit")]
        public string AmountUnit { get; set; }
        [DisplayName("Verkaufspreis")]
        public decimal? Price { get; set; }
        [DisplayName("Währung")]
        public string PriceUnit { get; set; }
        [DisplayName("Datum von")]
        public DateTime? DateFrom { get; set; }
        [DisplayName("Datum bis")]
        public DateTime? DateUntil { get; set; }
        [DisplayName("Uhrzeit von")]
        public TimeSpan? TimeFrom { get; set; }
        [DisplayName("Uhrzeit bis")]
        public TimeSpan? TimeUntil { get; set; }
        [DisplayName("So")]
        public bool Sunday { get; set; }
        [DisplayName("Mo")]
        public bool Monday { get; set; }
        [DisplayName("Di")]
        public bool Tuesday { get; set; }
        [DisplayName("Mi")]
        public bool Wednesday { get; set; }
        [DisplayName("Do")]
        public bool Thursday { get; set; }
        [DisplayName("Fr")]
        public bool Friday { get; set; }
        [DisplayName("Sa")]
        public bool Saturday { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_recipe_RecipeMaterialM")]
    public partial class md_recipe_RecipeMaterialM
    {
        [Key, Column(Order = 0)]
        public long RecipeId { get; set; }
        public long MaterialId { get; set; }
        public decimal Weight { get; set; }
        public decimal Value { get; set; }
        public int Unit { get; set; }
        [Key, Column(Order = 1)]
        public long Sort { get; set; }
        public int Type { get; set; }
        public int FunctionType { get; set; }
        public decimal? FValue { get; set; }
        [ForeignKey("RecipeId")]
        public virtual md_recipe_Recipe Md_recipe_Recipe { get; set; }
        //[ForeignKey("MaterialId")]
        //public virtual md_masterData_Material Md_material_Material { get; set; }
    }


    [Table("md_recipe_RecipeMaterial")]
    public partial class md_recipe_RecipeMaterial
    {
        [Key, Column(Order = 0)]
        public long? RecipeId { get; set; }
        [Key, Column(Order = 1)]
        public long? MaterialId { get; set; }
        public decimal? Weight { get; set; }
        public long Sort { get; set; }
        public int Type { get; set; }
        public int? FunctionType { get; set; }
        [ForeignKey("RecipeId")]
        public virtual md_recipe_Recipe Md_Recipe { get; set; }

        [ForeignKey("MaterialId")]
        public virtual md_masterData_Material Md_Material { get; set; }
    }

    [Table("md_material_RecipeMaterial")]
    public partial class md_material_RecipeMaterial
    {
        [Key, Column(Order = 0)]
        public long? RecipeId { get; set; }
        [Key, Column(Order = 1)]
        public long? MaterialId { get; set; }
        public int? Valid { get; set; }
        public bool? Active { get; set; }

        [ForeignKey("RecipeId")]
        public virtual md_recipe_Recipe Md_Recipe { get; set; }

        [ForeignKey("MaterialId")]
        public virtual md_masterData_Material Md_Material { get; set; }
    }

    [Table("md_material_SortDetails")]
    public partial class md_material_SortDetails
    {
        public long? Id { get; set; }
        public long? NormId { get; set; }
        public long? ProgressId { get; set; }
        public long? ConsistencyId { get; set; }
        public long? CompressionId { get; set; }
        public long? ChloridId { get; set; }
        public long? ExposureKombiId { get; set; }
        public long? ConcreteId { get; set; }
        public long? ConcreteFamilyId { get; set; }
        

        [DisplayName("Körnung")]
        public string Aggregate { get; set; }
        [DisplayName("WZ-Gehalt")]
        public decimal? WZValue { get; set; }
        [DisplayName("Eignung")]
        public string Verification { get; set; }
        [DisplayName("Nachbehandlung 1")]
        public decimal? CuringTime1 { get; set; }
        [DisplayName("Nachbehandlung 2")]
        public decimal? CuringTime2 { get; set; }
        [DisplayName("Nachbehandlung 3")]
        public decimal? CuringTime3 { get; set; }
        [DisplayName("Nachbehandlung 4")]
        public decimal? CuringTime4 { get; set; }
        public long? DeliveryNoteTypeId { get; set; }

        
    }

    [Table("md_material_Progress")]
    public partial class md_material_Progress
    {
        public long? Id { get; set; }
        [DisplayName("Entwicklung")]
        public string Description { get; set; }
        public long MaterialTypeId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }


    [Table("md_material_CompressionGroupCement")]
    public partial class md_material_CompressionGroupCement
    {
        public long? Id { get; set; }
        [DisplayName("Festigkeitsklasse")]
        public string Description { get; set; }
        [DisplayName("Nach 2 Tagen")]
        public string Value2Days { get; set; }
        [DisplayName("Nach 7 Tagen")]
        public string Value7Days { get; set; }
        [DisplayName("Nach 28 Tagen")]
        public string Value28Days { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }


    [Table("md_material_Color")]
    public partial class md_material_Color
    {
        public long? Id { get; set; }
        public long? MaterialGroupId { get; set; }
        [DisplayName("Farbe")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_material_AggregateDetails")]
    public partial class md_material_AggregateDetails
    {
        public long? Id { get; set; }
        public long? AggregateGroupId { get; set; }
        public long? AggregateId { get; set; }
        public long? GravelTypeId { get; set; }
        public long? GravelGroupId { get; set; }
        public long? RockTypeId { get; set; }
        public long? AlkaliGroupId { get; set; }
        public long? GeologicalSourceId { get; set; }
        public long? PetrographicTypeId { get; set; }
        public long? NormId { get; set; }
        public long? SuitabilityId { get; set; }
        [DisplayName("Kategorie")]
        public string Category { get; set; }
        [DisplayName("Prüfzeichen")]
        public string CECertification { get; set; }
        [DisplayName("Ausstellungsdatum")]
        public DateTime? DateOfIssue { get; set; }
        [DisplayName("Gültigkeitsdatum")]
        public DateTime? TerminationDate { get; set; }
        public long? RuleGradingCurveId { get; set; }

        [ForeignKey("RuleGradingCurveId")]
        public virtual md_sieve_RuleGradingCurve Md_RuleGrading { get; set; }
    }

    [Table("md_material_AdditivesDetails")]
    public partial class md_material_AdditivesDetail
    {
        public long? Id { get; set; }
        public long? NormId { get; set; }

        [DisplayName("Form")]
        public string Form { get; set; }
        [DisplayName("Farbe")]
        public string Color { get; set; }
        [DisplayName("Zusatzstoff-Typ")]
        public string Type { get; set; }
        [DisplayName("k-Wert")]
        public decimal? KValue { get; set; }
        [DisplayName("Berücksichtigung beim Wasser")]
        public string ConsiderationWater { get; set; }
        [DisplayName("Berücksichtigung beim Mehlkorngehalt")]
        public string ConsiderationGrainFlourValue { get; set; }
        [DisplayName("Faserart")]
        public string FiberGroup { get; set; }
        [DisplayName("Faserform")]
        public string FiberForm { get; set; }
        [DisplayName("Fasergeometrie")]
        public string FiberGeometric { get; set; }

        [DisplayName("Norm Kurz-Bezeichnung")]
        public string ShortDescriptionNorm { get; set; }
        [DisplayName("Norm Bezeichnung")]
        public string DescriptionNorm { get; set; }

        [DisplayName("Eignungs-Nachweis durch")]
        public string Verification { get; set; }
        [DisplayName("Prüfzeichen")]
        public string CECertification { get; set; }
        [DisplayName("Ausstellungsdatum")]
        public DateTime? DateOfIssue { get; set; }
        [DisplayName("Gültigkeitsdatum")]
        public DateTime? TerminationDate { get; set; }
    }

    [Table("md_material_AdmixturesDetails")]
    public partial class md_material_AdmixturesDetails
    {
        public long? Id { get; set; }
        public long? NormId { get; set; }
        public long? Color { get; set; }
        [DisplayName("Form")]
        public string Form { get; set; } 
        [DisplayName("Dosierempf. Herstellung M-% v.Z.")]
        public decimal? DoseSensitiveProcent { get; set; }
        [DisplayName("Dosierempf. Herstellung ml/kg v.Z.")]
        public decimal? DoseSensitiveMl { get; set; }
        [DisplayName("max. Dosiermenge M-% v.Z.")]
        public decimal? MaxDoseProcent { get; set; }
        [DisplayName("min. Dosiermenge M-% v.Z.")]
        public decimal? MinDoseProcent { get; set; }
        [DisplayName("max. Dosiermenge ml/kg v.Z.")]
        public decimal? MaxDoseMl { get; set; }
        [DisplayName("min. Dosiermenge ml/kg v.Z.")]
        public decimal? MinDoseMl { get; set; }
        [DisplayName("Wasserreduktion")]
        public decimal? WaterReduction { get; set; }
        [DisplayName("Berücksichtigung beim Wasser")]
        public string ConsiderationWater { get; set; }
        [DisplayName("Hauptwirkstoffe")]
        public string MainIngredient { get; set; }
        [DisplayName("Norm Kurz-Bezeichnung")]
        public string ShortDescriptionNorm { get; set; }
        [DisplayName("Norm Bezeichnung")]
        public string DescriptionNorm { get; set; }

        [DisplayName("Eignungs-Nachweis durch")]
        public string Verification { get; set; }
        [DisplayName("Prüfzeichen")]
        public string CECertification { get; set; }
        [DisplayName("Ausstellungsdatum")]
        public DateTime? DateOfIssue { get; set; }
        [DisplayName("Gültigkeitsdatum")]
        public DateTime? TerminationDate { get; set; }
    }

    [Table("md_material_BinderDetails")]
    public partial class md_material_BinderDetails
    {
        public long? Id { get; set; }
        public long? NormId { get; set; }
        public long? CompressionCemId { get; set; }
        public long? Color { get; set; }
        [DisplayName("Benennung")]
        public string Designation { get; set; }
        [DisplayName("Eigenschaften")]
        public string Features { get; set; }
        [DisplayName("Eignungs-Nachweis durch")]
        public string Verification { get; set; }
        [DisplayName("Prüfzeichen")]
        public string CECertification { get; set; }
        [DisplayName("Ausstellungsdatum")]
        public DateTime? DateOfIssue { get; set; }
        [DisplayName("Gültigkeitsdatum")]
        public DateTime? TerminationDate { get; set; }
    }

    [Table("md_material_MaterialType")]
    public partial class md_material_MaterialType
    {
        public long? Id { get; set; }
        [DisplayName("Art")]
        public string Description { get; set; }
        public long? MaterialGroupId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_material_Aggregate")]
    public partial class md_material_Aggregate
    {
        public long? Id { get; set; }
        [DisplayName("Körnung")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_material_Client")]
    public partial class md_material_Client
    {
        public long? Id { get; set; }
        [DisplayName("Mandant-Name")]
        public string ClientName { get; set; }
        [DisplayName("Mandant-Nummer")]
        public string ClientId { get; set; }
        [DisplayName("Mandant Beschreibung")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_material_RockType")]
    public partial class md_material_RockType
    {
        public long? Id { get; set; }
        [DisplayName("Gesteinsart")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_material_GradinGroup")]
    public partial class md_material_GravelGroup
    {
        public long? Id { get; set; }
        [DisplayName("Korngruppe")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int? Sort { get; set; }
    }

    [Table("md_material_GravelType")]
    public partial class md_material_GravelType
    {
        public long? Id { get; set; }
        [DisplayName("Kornart")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_material_PetrographicType")]
    public partial class md_material_PetrographicType
    {
        public long? Id { get; set; }
        [DisplayName("Petrograph. Typ")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_material_GeologicalSource")]
    public partial class md_material_GeologicalSource
    {
        public long? Id { get; set; }
        [DisplayName("Geologischer Herkunft")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_material_AlkaliGroup")]
    public partial class md_material_AlkaliGroup
    {
        public long? Id { get; set; }
        [DisplayName("Alkali-Klasse")]
        public string Class { get; set; }
        public string WO { get; set; }
        public string WF { get; set; }
        public string WA { get; set; }
        [DisplayName("Gehalt")]
        public string ContentValue { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_recipe_Recipe")]
    public partial class md_recipe_Recipe
    {
        public long Id { get; set; }
        [DisplayName("Rezept Bezeichnung")]
        public string Description { get; set; }
        [DisplayName("Rezeptnummer")]
        public string Number { get; set; }
        [DisplayName("Rezeptnummer EDV")]
        public string NumberEDV { get; set; }
        public long? MaterialId { get; set; }
        public long? RecipeTypeId { get; set; }
        public decimal? AirContent { get; set; }
        public decimal? ConsistencySetPoint { get; set; }
        public decimal? MaxWCValue { get; set; }
        public bool? HotWater { get; set; }
        public int? ActualValueTolerance { get; set; }
        public int? SetPointDeviation { get; set; }
        public int? MixerFilling { get; set; }
        public int? WasteWaterFactor { get; set; }
        public int? BatchProgram { get; set; }
        public int? FillMonitoringCement { get; set; }
        public int? FillRating { get; set; }
        public int? CementEmptyingDelay { get; set; }
        public int? WaterEmptyingDelay { get; set; }
        public int? AdditiveEmptyingDelay { get; set; }
        public int? FoamAdditionTime { get; set; }
        public int? MixingTime { get; set; }
        public int? EmptyingPartlyOpen1 { get; set; }
        public int? EmptyingPartlyOpen2 { get; set; }
        public int? EmptyingOpen { get; set; }
        public long? SuitabilityGroupId { get; set; }
        public long? ConcreteFamilyId { get; set; }
        public long? ExposureGroupId { get; set; }
        public long? MaterialGroupId { get; set; }
        [DisplayName("Max Charge")]
        public decimal? MaxCharge { get; set; }
        [DisplayName("Min Charge")]
        public decimal? MinCharge { get; set; }
        public string Comment { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }

        //[ForeignKey("MaterialId")]
        //public virtual md_masterData_Material Md_Material { get; set; }
    }

    [Table("md_material_ChlorideGroup")]
    public partial class md_material_ChlorideGroup
    {
        public long? Id { get; set; }
        [DisplayName("Chloridklasse Beschreibung")]
        public string Description { get; set; }
        [DisplayName("Chloridklasse")]
        public string Class { get; set; }
        [DisplayName("Wert")]
        public decimal? Value { get; set; }
        public long MaterialTypeId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_material_CompressionStrenghtGroup")]
    public partial class md_material_CompressionStrenghtGroup
    {
        public long? Id { get; set; }
        [DisplayName("Beschreibung")]
        public string Description { get; set; }
        [DisplayName("Festigkeitsklasse")]
        public string Class { get; set; }
        [DisplayName("Min")]
        public decimal? Min { get; set; }
        [DisplayName("Max")]
        public decimal? Max { get; set; }
        public long? MaterialGroupId { get; set; }
        [DisplayName("Aktiv")]
        public bool IsActive { get; set; }
        [DisplayName("Gelöscht")]
        public bool IsDeleted { get; set; }
    }

    [Table("md_material_ConsistencyGroup")]
    public partial class md_material_ConsistencyGroup
    {
        public long? Id { get; set; }
        [DisplayName("Konsistenzklasse Beschreibung")]
        public string Description { get; set; }
        [DisplayName("Konsistenzklasse")]
        public string Class { get; set; }
        [DisplayName("Ausbreitmaß Min")]
        public decimal? ValueMin { get; set; }
        [DisplayName("Ausbreitmaß Max")]
        public decimal? ValueMax { get; set; }
        [DisplayName("Verdichtung")]
        public string CompactingClass { get; set; }
        [DisplayName("Verdichtungsmaß Min")]
        public decimal? CompactingValueMin { get; set; }
        [DisplayName("Verdichtungsmaß Max")]
        public decimal? CompactingValueMax { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_material_MaxAggregateSizeGroup")]
    public partial class md_material_MaxAggregateSizeGroup
    {
        public long? Id { get; set; }
        [DisplayName("Größtkorn D-Max")]
        public string Description { get; set; }
    }


    [Table("md_material_ExposureKombi")]
    public partial class md_material_ExposureKombi
    {
        public long? Id { get; set; }
        public int? ExposureClassId { get; set; }
        [DisplayName("Expositionsklasse(n)")]
        public string ShortText { get; set; }
        [DisplayName("Langer Text")]
        public string LongText { get; set; }
        [DisplayName("Kurzbeschreibung")]
        public string ShortDescription { get; set; }
        [DisplayName("Beispiel")]
        public string Example { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }


    [Table("md_material_SuitabilityGroup")]
    public partial class md_masterData_Suitability
    {
        public long? Id { get; set; }
        [DisplayName("Eignung")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_sieve_SieveSet")]
    public partial class md_masterData_SieveSet
    {
        public long? Id { get; set; }
        [DisplayName("Sieblinie")]
        public string Description { get; set; }
        [DisplayName("Sieblinie Nummer")]
        public string Number { get; set; }
    }

    [Table("md_material_Standard")]
    public partial class md_masterData_Standard
    {
        public long? Id { get; set; }
        public long? MaterialTypeId { get; set; }
        [DisplayName("Norm")]
        public string Name { get; set; }
        [DisplayName("Kurzzeichen laut Norm")]
        public string ShortName { get; set; }
        [DisplayName("Beschreibung")]
        public string Description { get; set; }
        [DisplayName("Bemerkung zur Norm")]
        public string Comment { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_material_Material")]
    public partial class md_masterData_Material
    {
        public md_masterData_Material()
        {
            this.RecipeMaterials = new List<md_recipe_RecipeMaterial>();
            RecipeMaterialsM = new List<md_recipe_RecipeMaterialM>();
            RecipeConcrete = new List<md_material_RecipeMaterial>();
        }

        public long? Id { get; set; }
        public long? MaterialGroupId { get; set; }
        public long? MaterialTypeId { get; set; }
        [DisplayName("Preis")]
        public decimal? Price { get; set; }
        public long? DelivererId { get; set; }
        public long? FacilitiesId { get; set; }

        [Required]
        [DisplayName("Artikelnummer")]
        public string ArticleNumber { get; set; }
        [DisplayName("Materialnummer")]
        public string MaterialNumber { get; set; }
        [Required]
        [DisplayName("Bezeichnung")]
        public string Name { get; set; }
        [DisplayName("Kurz-Bezeichnung")]
        public string ShortName { get; set; }
        [DisplayName("Dichte")]
        public decimal? Density { get; set; }
        [DisplayName("Einheit")]
        public string Unit { get; set; }
        [DisplayName("Bemerkung 1")]
        public string Comment1 { get; set; }
        [DisplayName("Bemerkung 2")]
        public string Comment2 { get; set; }
        [DisplayName("Bemerkung 3")]
        public string Comment3 { get; set; }
        [DisplayName("Bemerkung 4")]
        public string Comment4 { get; set; }
        [DisplayName("Bemerkung 5")]
        public string Comment5 { get; set; }
        [DisplayName("Chipnummer")]
        public string RFID { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        // Detail FK_Tabellen
        public long? AdditivesId { get; set; }
        public long? AdmixturesId { get; set; }
        public long? AggregateId { get; set; }
        public long? BinderId { get; set; }
        public long? SortId { get; set; }
        public long? ConcentratesId { get; set; }
        public long? BrandId { get; set; }

        [ScriptIgnore]
        public ICollection<md_recipe_RecipeMaterial> RecipeMaterials { get; set; }
        [ScriptIgnore]
        public ICollection<md_recipe_RecipeMaterialM> RecipeMaterialsM { get; set; }
        [ScriptIgnore]
        public ICollection<md_material_RecipeMaterial> RecipeConcrete { get; set; }
    }

    [Table("md_material_MaterialGroup")]
    public partial class md_material_MaterialGroup
    {
        public long? Id { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_Customer_ConstructionSite")]
    public partial class Customer_ConstractionSite
    {
        public long? Id { get; set; }
        public long? ConstructionSiteId { get; set; }
        public long? CustomerId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("ConstructionSiteId")]
        public virtual md_masterData_ConstructionSite ConstSite { get; set; }

        [ForeignKey("CustomerId")]
        public virtual md_masterData_ConstructionSite Customer { get; set; }
    }

    [Table("Facilities")]
    public partial class Facilities
    {
        public long? Id { get; set; }
        [DisplayName("Hauptwerk")]
        public string Description { get; set; }
    }

    [Table("md_Vehicle")]
    public partial class md_masterData_Vehicle
    {
        public long? Id { get; set; }
        public long? VehicleTypeId { get; set; }
        public long? CustomerId { get; set; }
        [DisplayName("Fahrernummer")]
        public long? DriverId { get; set; }
        [DisplayName("Hauptwerk")]
        public long? FacilitiesId { get; set; }
        [DisplayName("Fahrzeugnummer")]
        public string VehicleNumber { get; set; }
        [DisplayName("Nummernschild")]
        public string PlateNumber { get; set; }
        [DisplayName("Fahrzeugnummer")]
        public string VehicleId { get; set; }
        [DisplayName("Kapazität")]
        public int? Capacity { get; set; }
        [DisplayName("Mietwaagen")]
        public bool Rental { get; set; }
        [DisplayName("Abholer")]
        public bool Collector { get; set; }
        [DisplayName("Entleerprogram")]
        public int? EmptyingProgram { get; set; }
        [DisplayName("RFID-Karte")]
        public string RFID { get; set; }
        [DisplayName("Leergewicht")]
        public decimal? TareWeight { get; set; }
        [DisplayName("Maximale Gesamtgewicht")]
        public decimal? MaxTotal { get; set; }
        [DisplayName("letzte Tarawiegung")]
        [Column(TypeName = "datetime2")]
        public DateTime? LastDateTareWeighing { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("VehicleTypeId")]
        public virtual md_masterData_VehicleType Md_masterData_VehicleType { get; set; }

        [ForeignKey("DriverId")]
        public virtual md_masterData_Driver md_masterData_Driver { get; set; }

        [ForeignKey("FacilitiesId")]
        public virtual Facilities md_Facilities { get; set; }

        [ForeignKey("CustomerId")]
        public virtual md_masterData_Customer Md_Customer { get; set; }
    }

    [Table("md_VehicleType")]
    public partial class md_masterData_VehicleType
    {
        public long? Id { get; set; }
        [DisplayName("Fahrzeug Typ")]
        public string Description { get; set; }
        [DisplayName("Beschreibung")]
        public string Comment { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    [Table("md_Deliverer")]
    public partial class md_masterData_Deliverer
    {
        public long? Id { get; set; }
        public long? AddressId { get; set; }
        public long? ContactId { get; set; }
        [DisplayName("Lieferant")]
        public string Name { get; set; }
        [DisplayName("Lieferantennummer")]
        public string DelivererId { get; set; }
        [DisplayName("Lieferantennummer EDV")]
        public string DelivererIdEDV { get; set; }
        [DisplayName("Aktiv")]
        public bool IsActive { get; set; }
        [DisplayName("Gelöscht")]
        public bool IsDeleted { get; set; }

        [ForeignKey("AddressId")]
        public virtual md_masterData_Address Md_masterData_Address { get; set; }

        [ForeignKey("ContactId")]
        public virtual md_masterData_Contact Md_masterData_Contact { get; set; }
    }

    [Table("md_Trader")]
    public partial class md_masterData_Trader
    {
        public long? Id { get; set; }
        public long? AddressId { get; set; }
        public long? ContactId { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("Händlernummer")]
        public string TraderId { get; set; }
        [DisplayName("Händlernummer EDV")]
        public string TraderIdEDV { get; set; }
        [DisplayName("Aktiv")]
        public bool IsActive { get; set; }
        [DisplayName("Gelöscht")]
        public bool IsDeleted { get; set; }

        [ForeignKey("AddressId")]
        public virtual md_masterData_Address Md_masterData_Address { get; set; }

        [ForeignKey("ContactId")]
        public virtual md_masterData_Contact Md_masterData_Contact { get; set; }
    }

    [Table("md_CustomerRating")]
    public partial class md_masterData_CustomerRating
    {
        public long? Id { get; set; }
        [DisplayName("Bezeichnung")]
        public string Description { get; set; }
        [DisplayName("Kommentar")]
        public string Comment { get; set; }
        [DisplayName("Aktiv")]
        public bool IsActive { get; set; }
        [DisplayName("Gelöscht")]
        public bool IsDeleted { get; set; }
    }

    [Table("md_Customer")]
    public partial class md_masterData_Customer
    {
        public long? Id { get; set; }
        public long? AddressId { get; set; }
        public long? ContactId { get; set; }
        public long? CustomerRatingId { get; set; }
        [DisplayName("Händlernummer")]
        public string TraderId { get; set; }
        [DisplayName("Kundenname")]
        public string Name { get; set; }
        [DisplayName("Kundennummer")]
        public string CustomerId { get; set; }
        [DisplayName("Kundennummer EDV")]
        public string CustomerIdEDV { get; set; }
        [DisplayName("Rabatt €/m³")]
        public decimal? Discount { get; set; }
        [DisplayName("Aktiv")]
        public bool IsActive { get; set; }
        [DisplayName("Gelöscht")]
        public bool IsDeleted { get; set; }

        [ForeignKey("CustomerRatingId")]
        public virtual md_masterData_CustomerRating Md_masterData_CustomerRating { get; set; }

        [ForeignKey("AddressId")]
        public virtual md_masterData_Address Md_masterData_Address { get; set; }

        [ForeignKey("ContactId")]
        public virtual md_masterData_Contact Md_masterData_Contact { get; set; }

        public virtual IEnumerable<md_masterData_CustomerRating> CustumerRat { get; set; }
    }

    [Table("md_Driver")]
    public partial class md_masterData_Driver
    {
        public long? Id { get; set; }
        public long? AddressId { get; set; }
        public long? ContactId { get; set; }
        [DisplayName("Fahrernummer")]
        public string DriverNumber { get; set; }
        [DisplayName("RFID")]
        public string RFID { get; set; }
        [DisplayName("Aktiv")]
        public bool? IsActive { get; set; }
        [DisplayName("Gelöscht")]
        public bool? IsDeleted { get; set; }

        [ForeignKey("AddressId")]
        public virtual md_masterData_Address Md_masterData_Address { get; set; }

        [ForeignKey("ContactId")]
        public virtual md_masterData_Contact Md_masterData_Contact { get; set; }
    }

    [Table("md_order_ConstructionSite")]
    public partial class md_masterData_ConstructionSite
    {
        public long? Id { get; set; }
        public long? AddressId { get; set; }
        public long? CustomerId { get; set; }
        [DisplayName("Baustellennummer")]
        public string ConstructionSiteId { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("Lieferzyklus")]
        public int? DeliveryCycle { get; set; }


        public long DeliveryTimeTick { get; set; }
        [NotMapped]
        [DisplayName("Lieferzeit")]
        public TimeSpan? DeliveryTime
        {
            get { return TimeSpan.FromTicks(DeliveryTimeTick); }
            set
            {
                if (value.HasValue)
                    DeliveryTimeTick = value.Value.Ticks;
                else
                    DeliveryTimeTick = 0;
            }
        }

        public long DistanceTimeTick { get; set; }
        [NotMapped]
        [DisplayName("Baustelle Entfernung")]
        public TimeSpan? DistanceTime
        {
            get
            {
                return TimeSpan.FromTicks(DistanceTimeTick);
            }

            set
            {
                if (value.HasValue)
                    DistanceTimeTick = value.Value.Ticks;
                else
                    DistanceTimeTick = 0;
            }
        }

        public long UnloadingTimeTick { get; set; }
        [NotMapped]
        [DisplayName("Entladezeit")]
        public TimeSpan? UnloadingTime
        {
            get { return TimeSpan.FromTicks(UnloadingTimeTick); }

            set
            {
                if (value.HasValue)
                    UnloadingTimeTick = value.Value.Ticks;
                else
                    UnloadingTimeTick = 0;
            }
        }

        [DisplayName("Aktiv")]
        public bool IsActive { get; set; }
        [DisplayName("Gelöscht")]
        public bool IsDeleted { get; set; }

        [ForeignKey("AddressId")]
        public virtual md_masterData_Address Md_masterData_Address { get; set; }

        [ForeignKey("CustomerId")]
        public virtual md_masterData_Customer Md_masterData_Customer { get; set; }
    }

    [Table("md_order_Address")]
    public partial class md_masterData_Address
    {
        public long? Id { get; set; }
        [DisplayName("Land")]
        public string Country { get; set; }
        [DisplayName("Stadt")]
        public string City { get; set; }
        [DisplayName("Plz")]
        public string ZipCode { get; set; }
        [DisplayName("Bundesland")]
        public string State { get; set; }
        [DisplayName("Strasse")]
        public string Street { get; set; }
        [DisplayName("Adresse")]
        public string Address
        {
            get
            {
                return Street + " " + City + " " + Country;
            }
        }
    }
    [Table("md_order_AddressTamplate")]
    public partial class md_masterData_AddressTamplate
    {
        public long? Id { get; set; }
        [DisplayName("Stadt")]
        public string City { get; set; }
        public string Addition { get; set; }
        [DisplayName("Plz")]
        public string Zip { get; set; }
        [DisplayName("Vorwahl")]
        public string AreaCode { get; set; }
        [DisplayName("Bundesland")]
        public string State { get; set; }
        [DisplayName("Land")]
        public string Country { get; set; }
    }

    [Table("md_order_Company")]
    public partial class md_masterData_Company
    {
        public md_masterData_Company()
        {
            Contacts = new List<md_masterData_Contact>();
        }
        [Key]
        public long? Id { get; set; }
        [DisplayName("FirmenArt")]
        public long? CompanyGroupId { get; set; }
        [DisplayName("Bewertung")]
        public long? CompanyRatingId { get; set; }
        [DisplayName("Baustelle")]
        public long? ConstructionSiteId { get; set; }
        [DisplayName("Adresse")]
        public long? AddressId { get; set; }
        [DisplayName("Name")]
        public string CompanyName { get; set; }
        public string Mobile { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        [DisplayName("Ansprechpartner")]
        public string CotactPersonName { get; set; }
        [ForeignKey("CompanyGroupId")]
        public virtual md_masterData_CompanyGroup Md_masterData_CompanyGroup { get; set; }
        [ForeignKey("CompanyRatingId")]
        public virtual md_masterData_CompanyRating Md_masterData_CompanyRating { get; set; }
        [ForeignKey("AddressId")]
        public virtual md_masterData_Address Md_masterData_Address { get; set; }

        public virtual ICollection<md_masterData_Contact> Contacts { get; set; }
    }
    [Table("md_order_CompanyGroup")]
    public partial class md_masterData_CompanyGroup
    {
        public long? Id { get; set; }
        [DisplayName("Beschreibung")]
        public string Description { get; set; }
    }
    [Table("md_order_CompanyRating")]
    public partial class md_masterData_CompanyRating
    {
        public long? Id { get; set; }
        [DisplayName("Beschreibung")]
        public string Description { get; set; }
    }
    [Table("md_order_Contact")]
    public partial class md_masterData_Contact
    {
        public md_masterData_Contact()
        {
            Companys = new List<md_masterData_Company>();
        }
        [Key]
        public long? Id { get; set; }
        [DisplayName("Vorname")]
        public string FirstName { get; set; }
        [DisplayName("Nachname")]
        public string LastName { get; set; }
        [DisplayName("Mobilnummer")]
        public string Mobile { get; set; }
        [DisplayName("Telefon")]
        public string Tel { get; set; }
        [DisplayName("Telefax")]
        public string Fax { get; set; }
        public string Email { get; set; }
        public virtual ICollection<md_masterData_Company> Companys { get; set; }
        //public virtual ICollection<md_masterData_Vehicle> Vehicles { get; set; }
        public string FirmenListe
        {
            get
            {
                string firmList = String.Empty;
                foreach (md_masterData_Company compa in this.Companys)
                {
                    firmList += compa.CompanyName + ", ";
                }
                return firmList;
            }
        }
    }

    [Table("md_order_VehicleGroup")]
    public partial class md_masterData_VehicleGroup
    {
        public long? Id { get; set; }
        public string Description { get; set; }
    }
}
